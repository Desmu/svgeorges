import fs from 'fs';
import path from 'path';
import webfont from 'webfont';

/**
 * Copie d'un fichier. Merci StackOverflow https://stackoverflow.com/a/26038979/12634214 .
 *
 * @param {string} source Chemin du fichier à copier.
 * @param {string} target Chemin du fichier copié.
 */
function copyFileSync(source, target) {
  let targetFile = target;
  if (fs.existsSync(target)) {
    if (fs.lstatSync(target).isDirectory()) {
      targetFile = path.join(target, path.basename(source));
    }
  }
  fs.writeFileSync(targetFile, fs.readFileSync(source));
}

/**
 * Copie d'un dossier. Merci StackOverflow https://stackoverflow.com/a/26038979/12634214 .
 *
 * @param {string} source Chemin du dossier à copier.
 * @param {string} target Chemin du dossier copié.
 */
function copyFolderRecursiveSync(source, target) {
  let files = [];
  const targetFolder = path.join(target, path.basename(source));
  if (targetFolder.indexOf('node_modules') === -1) {
    if (!fs.existsSync(targetFolder)) {
      fs.mkdirSync(targetFolder);
    }
    if (fs.lstatSync(source).isDirectory()) {
      files = fs.readdirSync(source);
      files.forEach((file) => {
        const curSource = path.join(source, file);
        if (fs.lstatSync(curSource).isDirectory()) {
          copyFolderRecursiveSync(curSource, targetFolder);
        } else {
          copyFileSync(curSource, targetFolder);
        }
      });
    }
  }
}

/**
 * Minification des fichiers d'un dossier.
 *
 * @param {string} folder Chemin du dossier à minifier.
 * @param {Array} ignoredPaths Tableau des noms de fichiers à ne pas minifier.
 * @param {Array} filesToRemove Tableau des noms de fichiers à supprimer.
 */
function minify(folder, ignoredPaths, filesToRemove) {
  if (fs.lstatSync(folder).isDirectory()) {
    const files = fs.readdirSync(folder);
    files.forEach((file) => {
      const curSource = path.join(folder, file);
      if (fs.lstatSync(curSource).isDirectory()) {
        minify(curSource, ignoredPaths, filesToRemove);
      } else {
        const extensions = ['.css', '.html', '.js', '.json', '.ncx', '.opf', '.smil', '.svg', '.xhtml', '.xml'];
        if (filesToRemove.indexOf(file) !== -1) {
          fs.unlinkSync(curSource);
        } else if (extensions.indexOf(path.extname(curSource)) !== -1) {
          fs.readFile(curSource, 'utf-8', (er, data) => {
            if (er) throw er;
            let dataMinified = data;
            const isIgnored = ignoredPaths.some((ignoredPath) => {
              if (curSource.indexOf(ignoredPath) !== -1) {
                return true;
              }
              return false;
            });
            if (!isIgnored) {
              dataMinified = dataMinified.replace(/\/\*[\s\S]*?\*\/|([^\\:]|^)\/\/.*|<!--[\s\S]*?-->$/gm, '');
              dataMinified = dataMinified.replace(/\r?\n|\r/g, ' ');
              dataMinified = dataMinified.replace(/ {2}/g, '');
            }
            fs.writeFile(curSource, dataMinified, 'utf-8', (err) => {
              if (err) throw err;
            });
          });
        }
      }
    });
  }
}

if (fs.existsSync('./build')) {
  fs.rmSync('./build', { recursive: true });
}
fs.mkdirSync('./build', { recursive: true });
copyFolderRecursiveSync('./api', './build');
copyFolderRecursiveSync('./bots', './build');
copyFolderRecursiveSync('./website', './build');

minify('./build', ['base64fonts.css', 'render.svg'], ['render.gif', 'README.md', 'configuration.example.json', 'discord_install.js', 'matrix_install.js', 'twitch_install.js']);

webfont.default({
  files: 'website/assets/fonts/georgeslasaucisse/glyphs/*.svg',
  fontName: 'Georges la Saucisse',
  formats: ['ttf', 'woff'],
}).then((result) => {
  fs.writeFileSync('build/website/assets/fonts/georgeslasaucisse/GeorgesLaSaucisse.ttf', result.ttf);
  fs.writeFileSync('build/website/assets/fonts/georgeslasaucisse/GeorgesLaSaucisse.woff', result.woff);
}).catch((error) => {
  throw error;
});
