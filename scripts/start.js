import { spawn } from 'child_process';

/** Script d'installation de l'environnement en mode de développement. */
const npmInstall = spawn('npm', ['install'], { stdio: 'inherit' });
npmInstall.on('exit', () => {
  spawn('npm', ['audit', 'fix'], { stdio: 'inherit' });
  const npmInstallWebsite = spawn('npm', ['install', '--prefix', './website'], { stdio: 'inherit' });
  npmInstallWebsite.on('exit', () => {
    spawn('npm', ['audit', 'fix'], { stdio: 'inherit' });
    const npmInstallBots = spawn('npm', ['install', '--prefix', './bots'], { stdio: 'inherit' });
    npmInstallBots.on('exit', () => {
      spawn('npm', ['audit', 'fix'], { stdio: 'inherit' });
      const npmInstallApi = spawn('npm', ['install', '--prefix', './api'], { stdio: 'inherit' });
      npmInstallApi.on('exit', () => {
        spawn('npm', ['audit', 'fix'], { stdio: 'inherit' });
      });
    });
  });
});
