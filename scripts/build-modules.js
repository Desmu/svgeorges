import { exec, spawn } from 'child_process';
import fs from 'fs';

/**
 * Installation des librairies pour l'API.
 */
function apiInstall() {
  fs.mkdirSync('./build/api/node_modules', { recursive: true });
  const npmInstall = spawn('npm', ['install', '--omit=dev', '--prefix', './build/api'], { stdio: 'inherit' });
  npmInstall.on('exit', () => {
    exec('cd build/api');
    spawn('npm', ['audit', 'fix'], { stdio: 'inherit' });
  });
}

/**
 * Installation des librairies pour les bots.
 */
function botsInstall() {
  fs.mkdirSync('./build/bots/node_modules', { recursive: true });
  const npmInstall = spawn('npm', ['install', '--omit=dev', '--prefix', './build/bots'], { stdio: 'inherit' });
  npmInstall.on('exit', () => {
    exec('cd build/bots');
    spawn('npm', ['audit', 'fix'], { stdio: 'inherit' });
  });
}

/**
 * Installation des librairies pour le site.
 */
function websiteInstall() {
  fs.mkdirSync('./build/website/node_modules', { recursive: true });
  const npmInstall = spawn('npm', ['install', '--omit=dev', '--prefix', './build/website'], { stdio: 'inherit' });
  npmInstall.on('exit', () => {
    exec('cd build/website');
    spawn('npm', ['audit', 'fix'], { stdio: 'inherit' });
  });
}

apiInstall();
botsInstall();
websiteInstall();
