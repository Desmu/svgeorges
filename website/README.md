# SVGeorges - Site Web

Site officiel de Georges la Saucisse, conçu sous forme de jeu vidéo en SVG avec un moteur customisé, inclut des pages jouables et un lecteur multimédia de bandes dessinées et de vidéos conçues en SVG. https://www.georgeslasaucisse.fr/

## Architecture

- `assets` : fichiers statiques des visuels (SVG et polices d'écriture).
  * `components` : fichiers SVG associés à d'autres fichiers (CSS d'animations).
  * `fonts` : polices d'écriture utilisées.
  * `vectors` : fichiers SVG seuls, certains sont rangés par format d'aventure (`portrait` et `landscape` pour les pages web, `comic` et `video` pour les aventures du lecteur multimédia).
- `projects` : fichiers relatifs aux bandes dessinées `comics`, vidéos `videos`, pages web `webpages` créées avec le moteur.
- `src` : logique de code.

## Librairies utilisées

- [Gif.js](http://jnordberg.github.io/gif.js/) : Librairie de création de GIF pour exporter les BD.
- [Tone.js](https://tonejs.github.io/) : Librairie de manipulation de la Web Audio API pour les vidéos.
- [Free Font](http://savannah.gnu.org/projects/freefont/) : Police d'écriture des textes.
