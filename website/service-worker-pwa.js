/** Nom du cache, à actualiser à chaque mise à jour (georgeslasaucisse-vYYYYMMDDHHMMSS) */
const cacheName = 'georgeslasaucisse-v20231214174500';

/**
 * Navigation dans un dossier décrit par le fichier cache.json .
 *
 * @param {Array} cacheFiles Tableau des chemins relatifs complets des fichiers.
 * @param {string} path Chemin actuel.
 * @param {string} folder Nom du dossier.
 * @param {Array} files Tableau de noms des fichiers du dossier.
 */
function folderCrawl(cacheFiles, path, folder, files) {
  if (folder === '.') {
    files.forEach((file) => {
      cacheFiles.push(`${path}${file}`);
    });
  } else if (Array.isArray(files)) {
    files.forEach((file) => {
      cacheFiles.push(`${path}${folder}/${file}`);
    });
  } else {
    path += `${folder}/`;
    Object.entries(files).forEach(([fo, fi]) => {
      folderCrawl(cacheFiles, path, fo, fi);
    });
  }
}

// Installation du cache.
self.addEventListener('install', async (e) => {
  const cacheFiles = [];
  const filesList = await (await fetch('./src/sw/cache.json')).json();
  Object.entries(filesList).forEach(([folder, files]) => {
    folderCrawl(cacheFiles, './', folder, files);
  });
  e.waitUntil(caches.open(cacheName).then((cache) => cache.addAll(cacheFiles)));
});

// Mise à jour du cache.
self.addEventListener('fetch', (e) => {
  e.respondWith(caches.match(e.request).then((r) => r || fetch(e.request).then((response) => caches.open(cacheName).then((cache) => {
    cache.put(e.request, response.clone());
    return response;
  }))));
});

// Suppression du cache inutilisé.
self.addEventListener('activate', (e) => {
  e.waitUntil(caches.keys().then((keyList) => Promise.all(keyList.map((key) => ((key !== cacheName) ? caches.delete(key) : true)))));
});
