/**
 * Barre de lecture du comic.
 */
export default class Reader {
  /**
   * Listeners à placer sur la barre de lecture.
   *
   * @param {object} projectIndex Liste des projets.
   */
  static navigationEvents(projectIndex) {
    document.querySelectorAll('#svgreader .home, .btn-close').forEach((element) => {
      element.addEventListener('click', () => {
        document.querySelector('header').classList.remove('d-none');
        document.querySelector('nav').classList.remove('d-none');
        document.querySelector('nav').classList.add('d-flex');
        document.querySelector('main').classList.add('d-none');
        document.querySelector('#svgreader').classList.add('d-none');
      });
    });

    document.querySelector('#svgreader .prev').addEventListener('click', () => {
      projectIndex.comics.some((comic, index) => {
        if (comic.id === document.querySelector('#comic').getAttribute('data-id')) {
          if (index === 0) {
            document.querySelector(`[href="#${projectIndex.comics[projectIndex.comics.length - 1].id}"]`).click();
            return true;
          }
          document.querySelector(`[href="#${projectIndex.comics[index - 1].id}"]`).click();
          return true;
        }
        return false;
      });
    });

    document.querySelector('#svgreader .next').addEventListener('click', () => {
      projectIndex.comics.some((comic, index) => {
        if (comic.id === document.querySelector('#comic').getAttribute('data-id')) {
          if (index === projectIndex.comics.length - 1) {
            document.querySelector(`[href="#${projectIndex.comics[0].id}"]`).click();
            return true;
          }
          document.querySelector(`[href="#${projectIndex.comics[index + 1].id}"]`).click();
          return true;
        }
        return false;
      });
    });

    document.querySelector('#svgreader .save').addEventListener('click', async () => {
      if (document.querySelector('#svgreader .save').innerText === '🖪') {
        document.querySelector('#svgreader .save').innerText = '⧗';
        document.querySelector('#svgreader .save').setAttribute('aria-label', 'Ça enregistre ...');
        document.querySelector('#svgreader .save').classList.remove('cursor-pointer');
        const svg = document.querySelector('#comic').cloneNode(true);
        const height = 900;
        const width = 750;
        ['aria-labelledby', 'data-id', 'role', 'style'].forEach((attributeName) => {
          svg.removeAttribute(attributeName);
        });
        svg.setAttribute('xmlns:xlink', 'http://www.w3.org/1999/xlink');
        svg.setAttribute('height', height);
        svg.setAttribute('width', width);
        const style = document.createElement('style');
        style.setAttribute('type', 'text/css');
        let cssRules = await fetch('src/css/base64fonts.css').then((text) => text.text());
        let maxDuration = 0;
        Array.from(document.styleSheets).forEach((stylesheet) => {
          if (stylesheet.href.indexOf(`projects/comics/${document.querySelector('#comic').getAttribute('data-id')}/styles.css`) !== -1) {
            Array.from(stylesheet.cssRules).forEach((cssRule) => {
              if (cssRule.style && cssRule.style.animationDuration !== '') {
                let animationDuration = parseFloat(cssRule.style.animationDuration.replace('s', '')) * 1000;
                if (cssRule.style.animationDirection === 'alternate') {
                  animationDuration *= 2;
                }
                if (animationDuration > maxDuration) {
                  maxDuration = animationDuration;
                }
              }
              cssRules += cssRule.cssText.replace(/\r?\n|\r/g, ' ');
            });
          }
        });
        style.innerHTML = cssRules;
        svg.prepend(style);
        const downloadAnchorNodeSvg = document.createElement('a');
        downloadAnchorNodeSvg.setAttribute('href', URL.createObjectURL(new Blob([new XMLSerializer().serializeToString(svg)], { type: 'image/svg+xml' })));
        downloadAnchorNodeSvg.setAttribute('download', `${document.querySelector('#comic').getAttribute('data-id')}.gif`);
        document.body.appendChild(downloadAnchorNodeSvg);
        downloadAnchorNodeSvg.click();
        downloadAnchorNodeSvg.remove();
        const duration = maxDuration;
        const fps = 60;
        const delay = 1000 / fps;
        const gif = new GIF({
          height,
          width,
          workerScript: '../../node_modules/gif.js/dist/gif.worker.js',
          debug: true,
        });
        gif.on('finished', (blob) => {
          const downloadAnchorNodeGif = document.createElement('a');
          downloadAnchorNodeGif.setAttribute('href', URL.createObjectURL(blob));
          downloadAnchorNodeGif.setAttribute('download', `${document.querySelector('#comic').getAttribute('data-id')}.gif`);
          document.body.appendChild(downloadAnchorNodeGif);
          downloadAnchorNodeGif.click();
          downloadAnchorNodeGif.remove();
          document.querySelector('#svgreader .save').innerText = '🖪';
          document.querySelector('#svgreader .save').setAttribute('aria-label', 'Enregistrer');
          document.querySelector('#svgreader .save').classList.add('cursor-pointer');
        });
        Reader.processGif(svg, gif, duration, delay, 0);
      }
    });
  }

  /**
   * Boucle récursive ajoutant les frames du SVG au fichier GIF avant de faire le rendu.
   *
   * @param {object} svg Élément SVG source.
   * @param {object} gif Objet GIF cible.
   * @param {number} duration Durée totale du GIF en millisecondes.
   * @param {number} delay Temps s'écoulant entre chaque frame du GIF en millisecondes.
   * @param {number} time Temps s'étant écoulé dans le GIF depuis le début du processus en millisecondes.
   */
  static async processGif(svg, gif, duration, delay, time) {
    svg.querySelectorAll('[class*="animation"]').forEach((child) => {
      child.style.setProperty('animation-delay', `-${time}ms`);
    });
    const img = new Image();
    img.onload = () => {
      gif.addFrame(img, {
        delay,
      });
      time += delay;
      if (time <= duration) {
        Reader.processGif(svg, gif, duration, delay, time);
      } else {
        gif.render();
      }
    };
    img.src = URL.createObjectURL(new Blob([new XMLSerializer().serializeToString(svg)], { type: 'image/svg+xml' }));
  }

  /**
   *
   */
  constructor() {
    /** Timeout durant lequel le lecteur est affiché. */
    this.readertimeout = null;

    document.querySelector('#comic').addEventListener('mousemove', () => {
      this.initReader();
    });

    document.addEventListener('keydown', (e) => {
      if (e.key === 'Tab') {
        this.initReader();
      }
    });
  }

  /** Barre masquée après trois secondes sans bouger la souris. */
  hideReader() {
    this.readertimeout = setTimeout(() => {
      document.querySelector('#svgreader').style.transform = 'translateX(-50%) translateY(100%)';
    }, 3000);
  }

  /** Initialisation des événements de la barre. */
  initReader() {
    this.showReader();
    this.hideReader();
  }

  /** Affichage de la barre. */
  showReader() {
    document.querySelector('#svgreader').style.transform = 'translateX(-50%) translateY(0px)';
    clearTimeout(this.readertimeout);
  }
}
