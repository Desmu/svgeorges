/**
 * Manipulation des planches de BD.
 */
export default class Comic {
  /**
   * Fonction générique d'ajout d'un SVG à la bande dessinée avec placement.
   *
   * @param {number} caseIndex Index de la case concernée.
   * @param {string} svgElem Sélecteur de classe de l'élément.
   * @param {string} transformProperty Transformation à appliquer.
   * @param {boolean} isCloned true pour un SVG déjà ajouté à la planche précédemment.
   */
  static addPerso(caseIndex, svgElem, transformProperty, isCloned = false) {
    const svgToParse = (isCloned) ? document.querySelectorAll(svgElem)[0].cloneNode(true) : document.querySelectorAll(svgElem)[0];
    svgToParse.setAttribute('transform', transformProperty);
    if (isCloned) {
      svgToParse.querySelectorAll('[id]').forEach((elemWithId) => {
        elemWithId.remove();
      });
    }
    document.querySelectorAll('.page > g')[caseIndex].appendChild(svgToParse);
  }

  /**
   * Création d'un phylactère.
   *
   * @param {number} caseIndex Index de la case concernée.
   * @param {string} text Texte à ajouter.
   * @param {string} direction Direction du point d'origine de la queue du phylactère.
   * @param {number} x Coordonnée x de départ du texte.
   * @param {number} y Coordonnée y de départ du texte.
   * @param {boolean} preventAppend true si l'ajout au DOM n'est pas automatisé.
   * @returns {string} Le phylactère si preventAppend est à true, une chaîne vide sinon.
   */
  static addPhylactere(caseIndex, text, direction = '', x = 0, y = 0, preventAppend = false) {
    const fontSize = 12;
    const multilineText = text.split('\\n ');
    const height = 20 * multilineText.length + 10;
    let width = 0;
    multilineText.forEach((textLine) => {
      const lineWidth = (textLine.length + 1) * (fontSize / 1.5);
      if (lineWidth > width) {
        width = lineWidth;
      }
    });
    let phylactere = `<text font-family="Free Mono, monospace" font-size="${fontSize}px">`;
    multilineText.forEach((lineText, index) => {
      phylactere += `<tspan x="${x}" y="${y + (index * 20)}">${lineText}</tspan>`;
    });
    phylactere += '</text>';
    if (direction) {
      let polyline = '';
      switch (direction) {
        case 'n':
          polyline = `${(x - 10) + (width / 2) - 10},${y - 19} ${(x - 10) + (width / 2)},${y - 38} ${(x - 10) + (width / 2) + 10},${y - 19}`;
          break;
        case 'ne':
          polyline = `${x + width - 40},${y - 19} ${x + width - 20},${y - 38} ${x + width - 20},${y - 19}`;
          break;
        case 'se':
          polyline = `${x + width - 40},${y - 21 + height} ${x + width - 20},${y + height} ${x + width - 20},${y - 21 + height}`;
          break;
        case 's':
          polyline = `${(x - 10) + (width / 2) - 10},${y - 21 + height} ${(x - 10) + (width / 2)},${y + height} ${(x - 10) + (width / 2) + 10},${y - 21 + height}`;
          break;
        case 'sw':
          polyline = `${x},${y - 21 + height} ${x},${y + height} ${x + 20},${y - 21 + height}`;
          break;
        case 'nw':
          polyline = `${x},${y - 19} ${x},${y - 38} ${x + 20},${y - 19}`;
          break;
        default:
          break;
      }
      phylactere = `<rect fill="#fff" stroke="#000" x="${x - 10}" y="${y - 20}" height="${height}" width="${width}" rx="10" ry="10"/>${phylactere}<polyline fill="#fff" stroke="#000" points="${polyline}"/>`;
    }
    if (preventAppend) {
      return phylactere;
    }
    document.querySelectorAll('.page > g')[caseIndex].innerHTML += phylactere;
    return '';
  }

  /**
   * Ajout d'un clip de case permettant de placer voire d'animer des éléments sans qu'ils ne débordent de la case.
   *
   * @param {number} step Case du comic à clipper.
   * @param {object} elements Tableau d'éléments DOM définissant les éléments à placer ou à animer.
   * @param {string} animationName Nom de l'animation à appliquer.
   * @param {string} phylactere Définition d'un phylactère.
   */
  static clipCase(step, elements, animationName = null, phylactere = null) {
    const index = document.querySelectorAll('[id^="case_overflow"]').length;
    document.querySelectorAll('.page > g')[step].querySelector(':first-child').setAttribute('id', `case_overflow_${index}`);
    document.querySelectorAll('.page > g')[step].innerHTML += `
      <clipPath id="clip_case_overflow_${index}">
        <use xlink:href="#case_overflow_${index}"/>
      </clipPath>
    `;
    if (animationName && phylactere) {
      document.querySelectorAll('.page > g')[step].innerHTML += `
      <g clip-path="url(#clip_case_overflow_${index})">
        <g class="${animationName}">${phylactere}</g>
        <g id="perso_zone_${index}"></g>
      </g>
    `;
    } else {
      document.querySelectorAll('.page > g')[step].innerHTML += `
      <g clip-path="url(#clip_case_overflow_${index})">
        <g id="perso_zone_${index}"></g>
      </g>
    `;
    }
    elements.forEach((element) => {
      document.querySelector(`#perso_zone_${index}`).appendChild(element);
    });
  }

  /**
   * @param {string} date Date de création de la BD au format DD/MM/YYYY.
   * @param {string} title Nom de la BD.
   * @param {Array} svgPaths Tableau de chaînes de caractères désignant les chemins de fichiers à charger.
   * @param {Function} callback Fonction de retour appelée une fois les fichiers chargés.
   */
  constructor(date, title, svgPaths, callback) {
    /** File temporelle des cases de la BD. */
    this.timeline = [];
    /** Identifiant du projet. */
    this.id = document.querySelector('#comic').getAttribute('data-id');
    /** Script de la BD. */
    this.alt = document.querySelector('#comic desc').innerHTML.split('\n\n');
    document.querySelector('#comic desc').innerHTML = document.querySelector('#comic desc').innerHTML.replace(/\\n /g, '');
    document.querySelector('.page .date').innerHTML = date;
    document.querySelector('.page .title').innerHTML = title;
    this.parseSVG(svgPaths, callback);
  }

  /**
   * Ajout des fichiers SVG à la planche.
   *
   * @param {Array} svgPaths Tableau de chaînes de caractères désignant les chemins de fichiers à charger.
   * @param {Function} callback Fonction de retour appelée une fois les fichiers chargés.
   */
  parseSVG(svgPaths, callback) {
    let svgHTML = '';
    Promise.all(svgPaths.map((svgPath) => fetch(svgPath).then((resp) => resp.text()))).then((svgTexts) => {
      svgTexts.forEach((svgText) => {
        const svgElem = document.createElementNS('http://www.w3.org/2000/svg', 'g');
        svgElem.innerHTML = svgText;
        svgHTML += `<g class="g_${svgElem.firstElementChild.firstElementChild.classList[0]}">${svgElem.firstElementChild.innerHTML}</g>`;
      });
      document.querySelector('#planche').innerHTML += svgHTML;
      callback();
      this.timeline.forEach((item, index) => {
        if (this.timeline[index] !== null) {
          this.timeline[index]();
        }
      });
      window.comicCache[this.id] = document.querySelector('#comic').innerHTML;
    });
  }
}
