/**
 * Manipulations visuelles d'une vidéo.
 */
export default class Video {
  /**
   * Changement de la position d'un élément.
   *
   * @param {string} selector Sélecteur CSS à pointer.
   * @param {string} value Valeur de la position.
   * @param {number} delay Temps d'attente avant la modification.
   */
  static setTransform(selector, value, delay = 0) {
    setTimeout(() => {
      document.querySelectorAll(selector).forEach((element) => {
        element.style.transform = value;
      });
    }, delay);
  }

  /**
   * Changement de la visibilité d'un élément.
   *
   * @param {object} element Élément à modifier.
   * @param {string} value Valeur de la visibilité (block ou none).
   * @param {number} delay Temps d'attente avant la modification.
   */
  static toggleVisibility(element, value, delay = 0) {
    setTimeout(() => {
      switch (value) {
        case 'block':
          element.classList.remove('d-none');
          element.classList.add('d-block');
          break;
        case 'none':
          element.classList.remove('d-block');
          element.classList.add('d-none');
          break;
        default:
          break;
      }
    }, delay);
  }

  /**
   * @param {Array} svgPaths Tableau de chaînes de caractères désignant les chemins de fichiers à charger.
   * @param {object} subtitles Objet de sous-titres.
   * @param {Function} callfront Fonction de retour appelée une fois les fichiers chargés.
   * @param {Function} callback Fonction de retour appelée une fois les divers effets chargés.
   */
  constructor(svgPaths, subtitles, callfront, callback) {
    /** File temporelle des animations de la vidéo. */
    this.timeline = [];
    /** Identifiant du projet. */
    this.id = document.querySelector('#video').getAttribute('data-id');
    /** Liste des animations CSS à exécuter pendant la vidéo. */
    this.animations = [];
    /** Objet de sous-titres. */
    this.subtitles = subtitles;
    this.parseSVG(svgPaths, callfront, callback);
  }

  /**
   * Démarrage d'une animation CSS.
   *
   * @param {string} selector Sélecteur CSS à pointer.
   * @param {string} properties Valeur de propriété CSS à assigner.
   * @param {number} time Seconde de départ de l'animation.
   * @param {number} delay Décalage de la seconde de départ.
   */
  executeAnimation(selector, properties, time, delay = 0) {
    document.querySelectorAll(selector).forEach((element) => {
      element.classList.add('animated');
    });
    if (delay !== 0) {
      document.querySelectorAll(selector).forEach((element) => {
        element.style.animation = properties;
        element.style.animationDelay = `${delay}s`;
      });
    } else {
      document.querySelectorAll(selector).forEach((element) => {
        element.style.animation = properties;
      });
    }
    this.animations.push([selector, (time + delay)]);
  }

  /**
   * Toutes les animations de la vidéo sont exécutées jusqu'à arriver au temps voulu. L'état des animations en cours est récupéré.
   *
   * @param {number} newtime Seconde de la vidéo vers laquelle se rendre.
   */
  moveVideo(newtime) {
    document.querySelectorAll('.animated').forEach((element) => {
      element.style.animation = 'none';
      element.classList.remove('animated');
    });
    for (let i = 0; i <= newtime; i += 1) {
      if ((this.timeline[i]) && (this.timeline[i] !== null)) {
        if (i !== newtime) {
          this.timeline[i](true);
        } else {
          this.timeline[i]();
        }
      }
    }
    this.animations.forEach((value) => {
      document.querySelectorAll(value[0]).forEach((element) => {
        element.style.animationDelay = `-${newtime - value[1]}s`;
      });
      if (Tone.Transport.state === 'paused') {
        document.querySelectorAll('.animated').forEach((element) => {
          element.style.animationPlayState = 'paused';
        });
      } else {
        document.querySelectorAll('.animated').forEach((element) => {
          element.style.animationPlayState = 'running';
        });
      }
    });
  }

  /**
   * Ajout des fichiers SVG à la scène.
   *
   * @param {Array} svgPaths Tableau de chaînes de caractères désignant les chemins de fichiers à charger.
   * @param {Function} callfront Fonction de retour appelée une fois les fichiers chargés.
   * @param {Function} callback Fonction de retour appelée une fois les divers effets chargés.
   */
  parseSVG(svgPaths, callfront, callback) {
    let svgHTML = '';
    Promise.all(svgPaths.map((svgPath) => fetch(svgPath).then((resp) => resp.text()))).then((svgTexts) => {
      svgTexts.forEach((svgText) => {
        const svgElem = document.createElementNS('http://www.w3.org/2000/svg', 'g');
        svgElem.innerHTML = svgText;
        svgHTML += `<g class="g_${svgElem.firstElementChild.firstElementChild.classList[0]}">${svgElem.firstElementChild.innerHTML}</g>`;
      });
      document.querySelector('#scene').innerHTML += svgHTML;
      if (callfront) {
        callfront();
        if (this.style) {
          window.videoCache[this.id].style = this.style;
          window.videoCache[this.id].style.forEach((cssRule) => {
            document.querySelector('#videoStyle').sheet.insertRule(cssRule, 0);
          });
        }
      }
      window.videoCache[this.id].structure = document.querySelector('#video').innerHTML;
      callback();
      for (let index = 0; index < this.timeline.length; index += 1) {
        if (!this.timeline[index]) {
          this.timeline[index] = null;
        }
        const item = this.timeline[index];
        const ev = new Tone.ToneEvent(() => {
          if (item !== null) {
            item();
          }
          this.subtitles.printSubtitle(index);
        });
        ev.start(index);
        window.videoCache[this.id].events[index] = ev;
      }
    });
  }

  /**
   * Arrêt d'une animation CSS.
   *
   * @param {string} selector Sélecteur CSS à pointer.
   * @param {string} properties Valeur de propriété CSS à assigner.
   * @param {number} delay Temps d'attente avant la modification.
   */
  unexecuteAnimation(selector, properties, delay = 0) {
    setTimeout(() => {
      document.querySelectorAll(selector).forEach((element) => {
        if (element.classList.contains('animated')) {
          element.getAnimations().forEach((animation) => {
            if (element.style.animationIterationCount !== 'infinite') {
              animation.finish();
            }
          });
          element.style.animation = 'none';
          element.classList.remove('animated');
        }
        element.style.transform = properties;
      });
      for (let i = 0; i < this.animations.length; i += 1) {
        if (this.animations[i][0] === selector) {
          this.animations.splice(i, 1);
        }
      }
    }, delay);
  }
}
