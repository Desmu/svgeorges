/**
 * Barre de lecture de la vidéo.
 */
export default class Player {
  /** Actualisation de l'affichage du temps. */
  static getVideoTime() {
    const currenttime = parseInt(Tone.Transport.seconds, 10);
    document.querySelector('#progressrange').value = currenttime;
    document.querySelector('#currenttime').innerHTML = Player.secondsToTime(currenttime);
  }

  /** Arrêt d'une vidéo après une action de navigation. */
  static killVideo() {
    if (Tone.Transport.state !== 'paused') {
      Player.playPause();
    }
    Tone.Transport.seconds = 0;
    const cachedVideo = window.videoCache[document.querySelector('#video').getAttribute('data-id')];
    if (cachedVideo) {
      cachedVideo.events.forEach((ev) => {
        ev.stop();
      });
      cachedVideo.playerLoop.stop();
      cachedVideo.audio.stopAudio();
      cachedVideo.tonePlayer.unsync();
      for (let i = 0; i < cachedVideo.specialAnimationsIntervals.length; i += 1) {
        clearInterval(cachedVideo.specialAnimationsIntervals[i]);
      }
      for (let i = 0; i < cachedVideo.specialAnimationsTimeouts.length; i += 1) {
        clearTimeout(cachedVideo.specialAnimationsTimeouts[i]);
      }
      document.querySelector('#subtitles_box').classList.remove(...document.querySelector('#subtitles_box').classList);
      document.querySelector('#subtitles_box').innerHTML = '';
    }
  }

  /**
   * Clic sur la barre de progression.
   *
   * @param {object} video Objet Video.
   */
  static movePlayer(video) {
    const newtime = document.querySelector('#progressrange').value;
    video.moveVideo(newtime);
    Tone.Transport.seconds = newtime;
    video.subtitles.printSubtitle(newtime, true);
    if (Tone.Transport.state !== 'paused') {
      Player.getVideoTime();
    } else {
      document.querySelector('#currenttime').innerHTML = Player.secondsToTime(document.querySelector('#progressrange').value);
    }
  }

  /**
   * Listeners à placer sur la barre de lecture.
   *
   * @param {object} projectIndex Liste des projets.
   */
  static navigationEvents(projectIndex) {
    document.querySelectorAll('#svgplayer .home, .btn-close').forEach((element) => {
      element.addEventListener('click', () => {
        document.querySelector('header').classList.remove('d-none');
        document.querySelector('nav').classList.remove('d-none');
        document.querySelector('nav').classList.add('d-flex');
        document.querySelector('main').classList.add('d-none');
        document.querySelector('#svgplayer').classList.add('d-none');
        Player.killVideo();
      });
    });

    document.querySelector('#svgplayer .prev').addEventListener('click', () => {
      projectIndex.videos.some((video, index) => {
        if (video.id === document.querySelector('#video').getAttribute('data-id')) {
          Player.killVideo();
          if (index === 0) {
            document.querySelector(`[href="#${projectIndex.videos[projectIndex.videos.length - 1].id}"]`).click();
            return true;
          }
          document.querySelector(`[href="#${projectIndex.videos[index - 1].id}"]`).click();
          return true;
        }
        return false;
      });
    });

    document.querySelector('#svgplayer .next').addEventListener('click', () => {
      projectIndex.videos.some((video, index) => {
        if (video.id === document.querySelector('#video').getAttribute('data-id')) {
          Player.killVideo();
          if (index === projectIndex.videos.length - 1) {
            document.querySelector(`[href="#${projectIndex.videos[0].id}"]`).click();
            return true;
          }
          document.querySelector(`[href="#${projectIndex.videos[index + 1].id}"]`).click();
          return true;
        }
        return false;
      });
    });
  }

  /** Clic sur le bouton Play / Pause. */
  static playPause() {
    if (Tone.Transport.state === 'paused') {
      Tone.Transport.start();
      document.querySelector('#playpause').innerHTML = '⏸';
      document.querySelectorAll('.animated').forEach((element) => {
        element.style.animationPlayState = 'running';
      });
    } else {
      Tone.Transport.pause();
      document.querySelector('#playpause').innerHTML = '▶';
      document.querySelectorAll('.animated').forEach((element) => {
        element.style.animationPlayState = 'paused';
      });
    }
  }

  /**
   * Conversion du nombre de secondes en un compteur temporel conventionnel.
   *
   * @param {number} time Nombre de secondes à convertir.
   * @returns {string} Compteur de temps au format MM:SS .
   */
  static secondsToTime(time) {
    return `${parseInt(time / 60, 10)}:${(parseInt(time % 60, 10) < 10) ? (`0${parseInt(time % 60, 10)}`) : (parseInt(time % 60, 10))}`;
  }

  /**
   * @param {object} video Objet Video.
   * @param {number} totaltime Durée de la vidéo.
   */
  constructor(video, totaltime) {
    /** Timeout durant lequel le lecteur est affiché. */
    this.playertimeout = null;
    /** Durée de la vidéo. */
    this.totaltime = totaltime;
    /** Loop de la durée du player. */
    this.loop = null;

    document.querySelector('#video').addEventListener('mousemove', () => {
      this.initPlayer();
    });

    document.addEventListener('keydown', (e) => {
      if (e.key === 'Tab') {
        this.initPlayer();
      }
    });

    document.querySelector('#playpause').outerHTML = document.querySelector('#playpause').outerHTML;
    document.querySelector('#playpause').addEventListener('click', () => {
      Player.playPause();
    });

    document.querySelector('#progressrange').outerHTML = document.querySelector('#progressrange').outerHTML;
    document.querySelector('#progressrange').addEventListener('input', () => {
      Player.movePlayer(video);
      this.initPlayer();
    });

    this.parsePlayer();
  }

  /** Barre masquée après trois secondes sans bouger la souris. */
  hidePlayer() {
    this.playertimeout = setTimeout(() => {
      document.querySelector('#svgplayer').style.transform = 'translateX(-50%) translateY(100%)';
    }, 3000);
  }

  /** Initialisation des événements de la barre. */
  initPlayer() {
    this.showPlayer();
    this.hidePlayer();
  }

  /**
   * Démarrage du lecteur.
   */
  parsePlayer() {
    document.querySelector('#progressrange').setAttribute('max', this.totaltime);
    document.querySelector('#totaltime').innerHTML = Player.secondsToTime(this.totaltime);
    this.loop = new Tone.Loop(Player.getVideoTime, 1);
    this.loop.start().stop(this.totaltime + 1);
  }

  /** Affichage de la barre. */
  showPlayer() {
    document.querySelector('#svgplayer').style.transform = 'translateX(-50%) translateY(0px)';
    clearTimeout(this.playertimeout);
  }
}
