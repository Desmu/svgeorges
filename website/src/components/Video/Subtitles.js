/**
 * Gestion des sous-titres.
 */
export default class Subtitles {
  /** Clic sur le bouton d'activation des sous-titres. */
  static showSubtitles() {
    if ((document.querySelector('#subtitles_box').style.display === '') || (document.querySelector('#subtitles_box').style.display === 'none')) {
      document.querySelector('#subtitles_box').style.display = 'block';
    } else {
      document.querySelector('#subtitles_box').style.display = 'none';
    }
  }

  /**
   * @param {string} vttPath Chemin du fichier WebVTT.
   */
  constructor(vttPath) {
    /** Timeout d'affichage d'un sous-titre. */
    this.timeoutShow = null;
    /** Timeout de retrait d'un sous-titre. */
    this.timeoutHide = null;
    this.parseVTT(vttPath);
  }

  /**
   * Conversion d'un fichier WebVTT en tableau de répliques.
   *
   * @param {string} vttPath Chemin du fichier WebVTT à convertir.
   */
  async parseVTT(vttPath) {
    const vttFile = await fetch(vttPath).then((text) => text.text());
    const vttLines = vttFile.split('\n');
    const subtitles = [];
    vttLines.forEach((vttLine, index) => {
      if ((vttLine !== '') && (vttLine.indexOf('WEBVTT - ') === -1) && (vttLine.indexOf('</v>') === -1) && (vttLine.indexOf(' --> ') !== 1)) {
        const beginMinute = parseInt(vttLine.split(' --> ')[0].split(':')[0], 10);
        const beginSecond = parseInt(vttLine.split(' --> ')[0].split(':')[1].split('.')[0], 10);
        const beginMilliSecond = parseInt(vttLine.split(' --> ')[0].split(':')[1].split('.')[1], 10);
        const endMinute = parseInt(vttLine.split(' --> ')[1].split(':')[0], 10);
        const endSecond = parseInt(vttLine.split(' --> ')[1].split(':')[1].split('.')[0], 10);
        const endMilliSecond = parseInt(vttLine.split(' --> ')[1].split(':')[1].split('.')[1], 10);
        subtitles.push({
          begin: beginMinute * 60000 + beginSecond * 1000 + beginMilliSecond,
          end: endMinute * 60000 + endSecond * 1000 + endMilliSecond,
          text: vttLines[index + 1].replace(/(<([^>]+)>)/gi, ''),
          voice: vttLines[index + 1].substring(vttLines[index + 1].indexOf(' ') + 1, vttLines[index + 1].indexOf('>')).replace(/ & /g, '_').toLowerCase(),
        });
      }
    });
    /** Objet de sous-titres. */
    this.subtitles = subtitles;
  }

  /**
   * Actualisation des sous-titres affichés à chaque seconde.
   *
   * @param {number} t Seconde du sous-titre à afficher.
   * @param {boolean} progressbar true si l'actualisation est faite après un clic sur la barre de progression.
   */
  printSubtitle(t, progressbar = false) {
    clearTimeout(this.timeoutShow);
    clearTimeout(this.timeoutHide);
    const time = parseInt(t, 10) * 1000;
    if (time === 0) {
      this.timeoutShow = setTimeout(() => {
        document.querySelector('#subtitles_box').classList.add(`texte_${this.subtitles[0].voice}`);
        document.querySelector('#subtitles_box').innerHTML = `<strong>${this.subtitles[0].voice.replace(/_/g, ' & ').toUpperCase()}</strong> : ${this.subtitles[0].text}`;
      }, this.subtitles[0].begin);
    } else {
      this.subtitles.every((subtitle, index) => {
        if (parseInt(subtitle.end / 1000, 10) === (time / 1000)) {
          this.timeoutHide = setTimeout(() => {
            document.querySelector('#subtitles_box').classList.remove(...document.querySelector('#subtitles_box').classList);
            document.querySelector('#subtitles_box').innerHTML = '';
          }, subtitle.end % time);
        }
        if (parseInt(subtitle.begin / 1000, 10) === (time / 1000)) {
          this.timeoutShow = setTimeout(() => {
            document.querySelector('#subtitles_box').classList.add(`texte_${subtitle.voice}`);
            document.querySelector('#subtitles_box').innerHTML = `<strong>${subtitle.voice.replace(/_/g, ' & ').toUpperCase()}</strong> : ${subtitle.text}`;
          }, subtitle.begin % time);
        }
        if (parseInt(subtitle.begin / 1000, 10) > (time / 1000)) {
          if (progressbar === true) {
            if (this.subtitles[index - 1] && parseInt(this.subtitles[index - 1].end / 1000, 10) > (time / 1000)) {
              document.querySelector('#subtitles_box').classList.remove(...document.querySelector('#subtitles_box').classList);
              document.querySelector('#subtitles_box').classList.add(`texte_${this.subtitles[index - 1].voice}`);
              document.querySelector('#subtitles_box').innerHTML = `<strong>${this.subtitles[index - 1].voice.replace(/_/g, ' & ').toUpperCase()}</strong> : ${this.subtitles[index - 1].text}`;
            } else {
              document.querySelector('#subtitles_box').classList.remove(...document.querySelector('#subtitles_box').classList);
              document.querySelector('#subtitles_box').innerHTML = '';
            }
          }
          return false;
        }
        return true;
      });
    }
  }
}
