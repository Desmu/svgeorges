/**
 * Manipulation de Tone.
 */
export default class Audio {
  /**
   * Mélangeur de tableau.
   *
   * @param {Array} a Tableau.
   * @returns {Array} Le tableau mélangé.
   */
  static arrayShuffle(a) {
    for (let i = a.length - 1; i > 0; i -= 1) {
      const j = Math.floor(Math.random() * (i + 1));
      const x = a[i];
      a[i] = a[j];
      a[j] = x;
    }
    return a;
  }

  /**
   * Mélangeur de notes.
   *
   * @param {Array} notes Tableau de notes.
   * @returns {Array} Le tableau de notes mélangées.
   */
  static shuffleNotes(notes) {
    if (notes.length !== 0) {
      let namesNotes = [];
      notes.forEach((note) => {
        namesNotes.push(note.name);
      });
      namesNotes = Audio.arrayShuffle(namesNotes);
      Object.entries(notes).forEach(([index, note]) => {
        note.name = namesNotes[index];
      });
      return notes;
    }
    return [];
  }

  /**
   * @param {number} beat Rythme de la musique accompagnant la vidéo en battements par minute.
   * @param {number} strophesStart Seconde de démarrage de la musique.
   * @param {number} strophesAmount Nombre de strophes de la musique.
   * @param {number} strophesDuration Durée d'une strophe de la musique.
   * @param {object} notesPath Fichiers JSON de notes.
   * @param {Function} callback Fonction de retour appelée une fois tout l'audio chargé.
   */
  constructor(beat = 0, strophesStart = 0, strophesAmount = 0, strophesDuration = 0, notesPath = {}, callback = null) {
    /** Tableau des objets d'instruments utilisés. */
    this.instruments = [];
    /** Rythme de la musique accompagnant la vidéo en battements par minute. */
    this.beat = beat;
    /** Seconde de démarrage de la musique. */
    this.strophesStart = strophesStart;
    /** Nombre de strophes de la musique. */
    this.strophesAmount = strophesAmount;
    /** Durée d'une strophe de la musique. */
    this.strophesDuration = strophesDuration;
    /** Tableau des secondes auxquelles démarrent les strophes. */
    this.strophesIndexes = [];
    for (let i = 0; i < (strophesAmount + 1); i += 1) {
      this.strophesIndexes[i] = strophesStart + strophesDuration * i;
    }
    /** Objet de notes issues de fichiers JSON. */
    this.notes = {};
    if (Object.keys(notesPath).length !== 0) {
      this.parseNotes(notesPath, callback);
    }
  }

  /**
   * Récupération de notes provenant de fichiers JSON
   *
   * @param {object} notesPath Fichiers JSON de notes.
   * @param {Function} callback Fonction de retour appelée une fois les divers effets chargés.
   */
  async parseNotes(notesPath, callback) {
    const notes = {};
    await Promise.all(Object.entries(notesPath).map(async ([instrument, notePath]) => {
      await fetch(notePath).then((json) => json.json()).then((json) => { notes[instrument] = json.notes; });
    }));
    /** Objet final de notes. */
    this.notes = notes;
    callback();
  }

  /** Démarrage de l'audio. */
  startAudio() {
    this.instruments.forEach((instrument) => {
      if (instrument.strophes) {
        instrument.strophes.forEach((strophe) => {
          instrument.tool.start(strophe.start).stop(strophe.stop);
        });
      } else if (instrument.start && instrument.stop) {
        instrument.tool.start(instrument.start).stop(instrument.stop);
      }
    });
  }

  /** Arrêt de l'audio. */
  stopAudio() {
    this.instruments.forEach((instrument) => {
      instrument.tool.stop(0);
    });
  }
}
