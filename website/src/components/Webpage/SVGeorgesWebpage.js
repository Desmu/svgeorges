import Utils from '../../js/Utils.js';

/**
 * Élément désignant la structure de base d'une page du site.
 */
export default class SVGeorgesWebpage extends HTMLElement {
  /**
   * Création d'un phylactère.
   *
   * @param {object} svgElem Élément auquel est attaché le texte.
   * @param {string} text Texte à ajouter.
   * @param {string} direction Direction du point d'origine de la queue du phylactère.
   * @returns {object} L'élément de phylactère.
   */
  static addPhylactere(svgElem, text, direction) {
    const fontSize = 16;
    const multilineText = text.split('<br>');
    let x = 0;
    let y = 0;
    const height = 20 * multilineText.length + 10;
    let width = 0;
    multilineText.forEach((textLine) => {
      const lineWidth = textLine.length * (fontSize / 1.5);
      if (lineWidth > width) {
        width = lineWidth;
      }
    });
    let polyline = '';
    for (let i = 0; i < svgElem.transform.baseVal.numberOfItems; i += 1) {
      if (svgElem.transform.baseVal.getItem(i).type === SVGTransform.SVG_TRANSFORM_TRANSLATE) {
        x = svgElem.transform.baseVal.getItem(i).matrix.e;
        y = svgElem.transform.baseVal.getItem(i).matrix.f;
      }
    }
    switch (direction) {
      case 'n':
        x = x - width / 2 + svgElem.getBBox().width / 2;
        y += svgElem.getBBox().height + 40;
        polyline = `${(x - 10) + (width / 2) - 10},${y - 19} ${(x - 10) + (width / 2)},${y - 38} ${(x - 10) + (width / 2) + 10},${y - 19}`;
        break;
      case 'ne':
        x -= width;
        y += svgElem.getBBox().height + 40;
        polyline = `${x + width - 40},${y - 19} ${x + width - 20},${y - 38} ${x + width - 20},${y - 19}`;
        break;
      case 'se':
        x -= width;
        y -= height;
        polyline = `${x + width - 40},${y - 21 + height} ${x + width - 20},${y + height} ${x + width - 20},${y - 21 + height}`;
        break;
      case 's':
        x = x - width / 2 + svgElem.getBBox().width / 2;
        y -= height;
        polyline = `${(x - 10) + (width / 2) - 10},${y - 21 + height} ${(x - 10) + (width / 2)},${y + height} ${(x - 10) + (width / 2) + 10},${y - 21 + height}`;
        break;
      case 'sw':
        x += svgElem.getBBox().width;
        y -= height;
        polyline = `${x},${y - 21 + height} ${x},${y + height} ${x + 20},${y - 21 + height}`;
        break;
      case 'nw':
        x += svgElem.getBBox().width;
        y += svgElem.getBBox().height + 40;
        polyline = `${x},${y - 19} ${x},${y - 38} ${x + 20},${y - 19}`;
        break;
      default:
        break;
    }
    let elemName = '';
    svgElem.classList.forEach((className) => {
      if (/g_.*/.test(className)) {
        [, elemName] = className.split('_');
      }
    });
    let phylactere = `<rect fill="#fff" stroke="#000" x="${x - 10}" y="${y - 20}" height="${height}" width="${width}" rx="10" ry="10"/><text>`;
    multilineText.forEach((lineText, index) => {
      phylactere += `<tspan x="${x}" y="${y + (index * 20)}">${lineText}</tspan>`;
    });
    phylactere += `</text><polyline fill="#fff" stroke="#000" points="${polyline}"/>`;
    const phylactereElem = document.createElementNS('http://www.w3.org/2000/svg', 'g');
    phylactereElem.classList.add(`phylactere_${elemName}`);
    phylactereElem.innerHTML = phylactere;
    return phylactereElem;
  }

  /**
   * Vérifie si deux éléments entrent en collision.
   *
   * @param {object} source Élément entrant en collision.
   * @param {object} dest Élément entré en collision.
   * @returns {boolean} true si la collision est effective.
   */
  static checkCollision(source, dest) {
    const sourceBounds = source.getBoundingClientRect();
    const destBounds = dest.getBoundingClientRect();
    if ((
      (sourceBounds.x >= destBounds.x)
      && (sourceBounds.x <= (destBounds.x + destBounds.width))
      && (sourceBounds.y >= destBounds.y)
      && (sourceBounds.y <= (destBounds.y + destBounds.height))
    ) || (
      ((sourceBounds.x + sourceBounds.width) >= destBounds.x)
      && ((sourceBounds.x + sourceBounds.width) <= (destBounds.x + destBounds.width))
      && (sourceBounds.y >= destBounds.y)
      && (sourceBounds.y <= (destBounds.y + destBounds.height))
    ) || (
      (sourceBounds.x >= destBounds.x)
      && (sourceBounds.x <= (destBounds.x + destBounds.width))
      && ((sourceBounds.y + sourceBounds.height) >= destBounds.y)
      && ((sourceBounds.y + sourceBounds.height) <= (destBounds.y + destBounds.height))
    ) || (
      ((sourceBounds.x + sourceBounds.width) >= destBounds.x)
      && ((sourceBounds.x + sourceBounds.width) <= (destBounds.x + destBounds.width))
      && ((sourceBounds.y + sourceBounds.height) >= destBounds.y)
      && ((sourceBounds.y + sourceBounds.height) <= (destBounds.y + destBounds.height))
    )) {
      return true;
    }
    return false;
  }

  /**
   * Événement de click sur une ancre SVG.
   *
   * @param {object} e Événement Javascript envoyé.
   * @param {string} elementName Sélecteur de l'élément ayant déclenché l'événement.
   */
  static clickAnchor(e, elementName) {
    if ((e.type === 'keydown') && (e.key === 'ArrowUp')) {
      document.querySelectorAll('.anchor-svg').forEach((anchorSVG) => {
        if (SVGeorgesWebpage.checkCollision(document.querySelector(elementName), anchorSVG.firstElementChild)) {
          anchorSVG.dispatchEvent((new Event('click')));
        }
      });
    }
  }

  /**
   * Événements de préparation d'un déplacement horizontal.
   *
   * @param {object} e Événement Javascript envoyé.
   */
  static moveLeftRight(e) {
    if ((document.querySelectorAll('g[class^="popup_"]:not(.d-none)').length === 0) && !(((e.type === 'mousedown') || (e.type === 'touchstart')) && e.target.closest('.freeze_move'))) {
      document.querySelectorAll('.moveLeftRight').forEach((movableElement) => {
        switch (e.type) {
          case 'keydown': case 'keyup':
            if ((e.key === 'ArrowLeft') || (e.key === 'ArrowRight')) {
              switch (e.key) {
                case 'ArrowLeft':
                  SVGeorgesWebpage.startMove(movableElement, 'left');
                  break;
                case 'ArrowRight':
                  SVGeorgesWebpage.startMove(movableElement, 'right');
                  break;
                default:
                  break;
              }
            }
            break;
          case 'mousedown':
            if (movableElement.getBoundingClientRect().x > e.clientX) {
              SVGeorgesWebpage.startMove(movableElement, 'left');
            } else if (movableElement.getBoundingClientRect().x < e.clientX) {
              SVGeorgesWebpage.startMove(movableElement, 'right');
            }
            break;
          case 'touchstart':
            if (movableElement.getBoundingClientRect().x > e.touches[0].clientX) {
              SVGeorgesWebpage.startMove(movableElement, 'left');
            } else if (movableElement.getBoundingClientRect().x < e.touches[0].clientX) {
              SVGeorgesWebpage.startMove(movableElement, 'right');
            }
            break;
          default:
            break;
        }
      });
    }
  }

  /**
   * Événements de préparation d'un déplacement vertical.
   *
   * @param {object} e Événement Javascript envoyé.
   */
  static moveUpDown(e) {
    document.querySelectorAll('.moveUpDown').forEach((movableElement) => {
      if ((e.key === 'ArrowUp') || (e.key === 'ArrowDown')) {
        switch (e.key) {
          case 'ArrowUp':
            SVGeorgesWebpage.startMove(movableElement, 'up');
            break;
          case 'ArrowDown':
            SVGeorgesWebpage.startMove(movableElement, 'down');
            break;
          default:
            break;
        }
      }
    });
  }

  /**
   * Changement des données des balises hors SVG.
   *
   * @param {object} iframe Iframe à changer.
   * @param {object} data Données à analyser.
   * @param {boolean} hasDataSrc true si plusieurs iframes sont situées dans le même élément SVG.
   */
  static parseData(iframe, data, hasDataSrc = false) {
    let alt = document.querySelector('[slot="web-page-details"]').getAttribute('data-alt');
    Object.entries(data).forEach(([key, value]) => {
      if (key.indexOf('embed_') === 0) {
        iframe.setAttribute('src', value);
        if (hasDataSrc) {
          iframe.setAttribute('data-src', value);
        }
      } else if ((key.indexOf('nom_') !== -1) && (alt.indexOf('$nom') !== -1)) {
        alt = alt.replace('$nom', value);
        iframe.setAttribute('title', value);
      } else if ((key.indexOf('description_') !== -1) && (alt.indexOf('$description') !== -1)) {
        alt = alt.replace('$description', value);
      } else if ((key.indexOf('script_') !== -1) && (alt.indexOf('$script') !== -1)) {
        alt = alt.replace('$script', value);
        if (iframe.parentNode.querySelector('.embed_script')) {
          iframe.parentNode.querySelector('.embed_script').innerHTML = (new DOMParser()).parseFromString(value, 'text/html').querySelector('body').innerHTML;
        }
      } else if ((key.indexOf('url_') !== -1) && (alt.indexOf('$url') !== -1)) {
        alt = alt.replace('$url', value);
      }
    });
    document.querySelector('[slot="web-page-details"]').innerHTML = alt;
    const prevButton = document.createElement('button');
    prevButton.type = 'button';
    prevButton.innerHTML = 'Précédent';
    document.querySelector('[slot="web-page-details"]').appendChild(prevButton);
    prevButton.addEventListener('click', () => {
      SVGeorgesWebpage.parseSVGGallery(iframe.closest('g'), 'prev');
    });
    const nextButton = document.createElement('button');
    nextButton.type = 'button';
    nextButton.innerHTML = 'Suivant';
    document.querySelector('[slot="web-page-details"]').appendChild(nextButton);
    nextButton.addEventListener('click', () => {
      SVGeorgesWebpage.parseSVGGallery(iframe.closest('g'), 'next');
    });
  }

  /**
   * Activation des boutons de galeries intégrées aux SVG, avec gestion des ordres personnalisés.
   *
   * @param {object} svgElemGrouped Élément SVG contenant la galerie.
   * @param {string} direction 'next' ou 'prev', désignant le sens de déplacement de la galerie.
   */
  static parseSVGGallery(svgElemGrouped, direction) {
    const dataTag = document.querySelector('[slot="web-page-details"]');
    if (dataTag.getAttribute('data-embed')) {
      const apiData = dataTag.getAttribute('data-embed').split('/');
      const minIndex = (dataTag.getAttribute('data-min')) ? parseInt(dataTag.getAttribute('data-min'), 10) : 1;
      const maxIndex = (dataTag.getAttribute('data-max')) ? parseInt(dataTag.getAttribute('data-max'), 10) : 1000;
      if (parseInt(apiData[apiData.length - 1], 10) !== ((direction === 'next') ? maxIndex : minIndex)) {
        let lastIndex = parseInt(apiData.pop(), 10);
        let lastIndexCustom = false;
        const customOrder = document.querySelector('[slot="web-page-details"]').getAttribute('data-order');
        if (customOrder && customOrder !== '') {
          const customOrderIndexes = customOrder.split(',');
          customOrderIndexes.forEach((co, i) => {
            customOrderIndexes[i] = parseFloat(co, 10);
          });
          if (customOrderIndexes.indexOf(lastIndex) !== -1) {
            if ((direction === 'next') && customOrderIndexes[customOrderIndexes.indexOf(lastIndex) + 1]) {
              lastIndex = customOrderIndexes[customOrderIndexes.indexOf(lastIndex) + 1];
              lastIndexCustom = true;
            } else if ((direction === 'prev') && customOrderIndexes[customOrderIndexes.indexOf(lastIndex) - 1]) {
              lastIndex = customOrderIndexes[customOrderIndexes.indexOf(lastIndex) - 1];
              lastIndexCustom = true;
            }
          }
        }
        if (!lastIndexCustom) {
          if (direction === 'next') {
            lastIndex += 1;
          } else if (direction === 'prev') {
            lastIndex -= 1;
          }
        }
        apiData.push(lastIndex);
        dataTag.setAttribute('data-embed', apiData.join('/'));
        svgElemGrouped.querySelectorAll('.embed_selector option').forEach((option) => {
          if (option.value === apiData.join('/')) {
            option.selected = true;
          }
        });
        fetch(apiData.join('/')).then((response) => response.json()).then((data) => {
          SVGeorgesWebpage.parseData(svgElemGrouped.querySelector('iframe'), data);
        });
      }
    }
  }

  /**
   * Gestion de l'ouverture et de la fermeture d'une popup.
   *
   * @param {object} embedDataTag Balise de la popup.
   */
  static popupCloser(embedDataTag) {
    if (embedDataTag.className.indexOf('popup_') !== -1) {
      document.querySelector(`g.${embedDataTag.className}`).classList.add('d-none');
      document.querySelector(`g.${embedDataTag.className} .btn-close`).addEventListener('click', () => {
        document.querySelector(`g.${embedDataTag.className}`).classList.add('d-none');
      });
      document.addEventListener('keydown', (e) => {
        if (e.key === 'Escape') {
          document.querySelector(`g.${embedDataTag.className}`).classList.add('d-none');
        }
      });
    }
  }

  /**
   * Événement de passage d'un personnage sur une ancre SVG.
   *
   * @param {object} e Événement Javascript envoyé.
   * @param {string} elementName Sélecteur de l'élément ayant déclenché l'événement.
   */
  static showAnchor(e, elementName) {
    document.querySelectorAll('.anchor-svg').forEach((anchorSVG) => {
      if (SVGeorgesWebpage.checkCollision(document.querySelector(elementName), anchorSVG.firstElementChild)) {
        anchorSVG.dispatchEvent((new Event('mouseover')));
      } else if (anchorSVG.querySelector('.anchorname')) {
        anchorSVG.dispatchEvent((new Event('mouseout')));
      }
    });
    setTimeout(() => {
      if (document.querySelector('.moving')) {
        SVGeorgesWebpage.showAnchor(null, elementName);
      }
    }, 100);
  }

  /**
   * Événément d'affichage d'un phylactère.
   *
   * @param {object} e Événement Javascript envoyé.
   */
  static showPhylactere(e) {
    if ((e.currentTarget instanceof HTMLDocument) && (e.key === ' ')) {
      document.querySelectorAll('[class^="phylactere"]').forEach((phylactere) => {
        phylactere.classList.toggle('d-none');
      });
    } else if (e.type === 'click') {
      let elemName = '';
      if (e.currentTarget && (e.currentTarget !== null)) {
        e.currentTarget.classList.forEach((className) => {
          if (/g_.*/.test(className)) {
            [, elemName] = className.split('_');
          }
        });
      }
      if (document.querySelector(`.phylactere_${elemName}`)) {
        document.querySelectorAll(`.phylactere_${elemName}`).forEach((phylactere) => {
          phylactere.classList.toggle('d-none');
        });
      }
    }
  }

  /**
   * Événément d'exécution d'un déplacement.
   *
   * @param {object} movableElement Élément SVG à déplacer.
   * @param {string} direction Sens de déplacement.
   */
  static startMove(movableElement, direction) {
    if (!movableElement.moveInterval) {
      movableElement.moveInterval = setInterval(() => {
        if (!movableElement.classList.contains('moving')) {
          movableElement.classList.add('moving');
        }
        let elemName = '';
        movableElement.classList.forEach((className) => {
          if (/g_.*/.test(className)) {
            [, elemName] = className.split('_');
          }
        });
        if (document.querySelector(`.phylactere_${elemName}`)) {
          document.querySelectorAll(`.phylactere_${elemName}`).forEach((phylactere) => {
            SVGeorgesWebpage.startPhylactereMove(phylactere, direction);
          });
        }
        for (let i = 0; i < movableElement.transform.baseVal.numberOfItems; i += 1) {
          if (movableElement.transform.baseVal.getItem(i).type === SVGTransform.SVG_TRANSFORM_TRANSLATE) {
            let x = movableElement.transform.baseVal.getItem(i).matrix.e;
            let y = movableElement.transform.baseVal.getItem(i).matrix.f;
            if (!movableElement.firstElementChild.getAttribute('transform')) {
              movableElement.firstElementChild.setAttribute('transform', 'scale(1, 1)');
            }
            switch (direction) {
              case 'up':
                y -= 1;
                break;
              case 'right':
                x += 1;
                if (movableElement.firstElementChild.getAttribute('transform') === 'scale(-1, 1)') {
                  movableElement.firstElementChild.setAttribute('transform', 'scale(1, 1)');
                  x -= movableElement.firstElementChild.getBBox().width;
                }
                break;
              case 'down':
                y += 1;
                break;
              case 'left':
                x -= 1;
                if (movableElement.firstElementChild.getAttribute('transform') === 'scale(1, 1)') {
                  movableElement.firstElementChild.setAttribute('transform', 'scale(-1, 1)');
                  x += movableElement.firstElementChild.getBBox().width;
                }
                break;
              default:
                break;
            }
            movableElement.transform.baseVal.getItem(i).setTranslate(x, y);
          }
        }
      }, 10);
    }
  }

  /**
   * Déplacement d'un phylactère.
   *
   * @param {object} phylactere L'élément DOM du phylactère.
   * @param {string} direction Sens de déplacement.
   */
  static startPhylactereMove(phylactere, direction) {
    let x = parseInt(phylactere.querySelector('rect').getAttribute('x'), 10);
    let y = parseInt(phylactere.querySelector('rect').getAttribute('y'), 10);
    if (phylactere.transform.baseVal.numberOfItems === 0) {
      phylactere.setAttribute('transform', 'translate(0, 0)');
    }
    for (let i = 0; i < phylactere.transform.baseVal.numberOfItems; i += 1) {
      if (phylactere.transform.baseVal.getItem(i).type === SVGTransform.SVG_TRANSFORM_TRANSLATE) {
        x = phylactere.transform.baseVal.getItem(i).matrix.e;
        y = phylactere.transform.baseVal.getItem(i).matrix.f;
        switch (direction) {
          case 'up':
            y -= 1;
            break;
          case 'right':
            x += 1;
            break;
          case 'down':
            y += 1;
            break;
          case 'left':
            x -= 1;
            break;
          default:
            break;
        }
        phylactere.transform.baseVal.getItem(i).setTranslate(x, y);
      }
    }
  }

  /**
   * Événément d'arrêt d'un déplacement.
   */
  static stopMove() {
    document.querySelectorAll('.stopMove').forEach((movableElement) => {
      if (movableElement.classList.contains('moving')) {
        movableElement.classList.remove('moving');
      }
      clearInterval(movableElement.moveInterval);
      movableElement.moveInterval = null;
    });
  }

  /**
   *
   */
  constructor() {
    super();
    this.parseHTML();
  }

  /**
   * Rendu de l'élément HTML.
   */
  async parseHTML() {
    const pageData = await fetch(`./projects/webpages/${this.getAttribute('data-page')}/webpage.json`).then((response) => response.json());
    const pageText = await fetch('./src/components/Webpage/SVGeorgesWebpage.html').then((response) => response.text());
    const pageHTML = (new DOMParser()).parseFromString(pageText, 'text/html').querySelector('template').content;
    this.attachShadow({ mode: 'open' }).appendChild(pageHTML);

    let baseSVG = '';
    if (window.matchMedia('(orientation: portrait)').matches) {
      baseSVG = await (await fetch('./src/components/Webpage/WebPagePortrait.svg')).text();
      this.querySelector('main').innerHTML = baseSVG;
      if (pageData.title.portait) {
        document.querySelector('#webpageTitle').innerHTML = pageData.title.portait;
      }
      if (pageData.desc.portait) {
        document.querySelector('#webpageDesc').innerHTML = pageData.desc.portait;
      }
    } else if (window.matchMedia('(orientation: landscape)').matches) {
      baseSVG = await (await fetch('./src/components/Webpage/WebPageLandscape.svg')).text();
      this.querySelector('main').innerHTML = baseSVG;
      if (pageData.title.landscape) {
        document.querySelector('#webpageTitle').innerHTML = pageData.title.landscape;
      }
      if (pageData.desc.landscape) {
        document.querySelector('#webpageDesc').innerHTML = pageData.desc.landscape;
      }
    }

    if (pageData.svgPaths) {
      await this.parseSVG(pageData.svgPaths);
    }

    this.parseHTMLBulle();
    this.parseHTMLEmbed();
    this.parseHTMLList();

    document.addEventListener('keyup', (e) => {
      if ((e.key === 'Enter') && (document.activeElement.classList.contains('btn-link-svg'))) {
        document.activeElement.dispatchEvent(new Event('click'));
      }
    });

    this.shadowRoot.querySelector('header').style.display = 'none';
    this.querySelector('main').style.display = 'block';
  }

  /**
   * Ajout des fichiers SVG à la page.
   *
   * @param {Array} svgPaths Tableau de chaînes de caractères désignant les chemins de fichiers à charger.
   */
  async parseSVG(svgPaths) {
    await Promise.all(svgPaths.map((svgPath) => {
      if (((window.matchMedia('(orientation: portrait)').matches) && (svgPath.coordinates.portrait)) || ((window.matchMedia('(orientation: landscape)').matches) && (svgPath.coordinates.landscape))) {
        if (svgPath.asset) {
          return fetch(`./assets/${svgPath.asset}`).then((resp) => resp.text());
        }
        return `<svg>${Utils[svgPath.function]().outerHTML}</svg>`;
      }
      return false;
    })).then((svgTexts) => {
      svgTexts.forEach((svgText) => {
        if (svgText) {
          const svgElem = document.createElementNS('http://www.w3.org/2000/svg', 'g');
          svgElem.innerHTML = svgText;
          const svgElemGrouped = document.createElementNS('http://www.w3.org/2000/svg', 'g');
          svgElemGrouped.innerHTML = svgElem.firstElementChild.innerHTML;
          const elementName = svgElem.firstElementChild.firstElementChild.classList[0];
          svgElemGrouped.classList.add(`g_${elementName}`);
          const element = svgPaths.find((path) => {
            if (window.matchMedia('(orientation: portrait)').matches) {
              return (!path.used && path.coordinates.portrait && (path.name === elementName));
            }
            if (window.matchMedia('(orientation: landscape)').matches) {
              return (!path.used && path.coordinates.landscape && (path.name === elementName));
            }
            return false;
          });
          element.used = true;
          if (window.matchMedia('(orientation: portrait)').matches) {
            svgElemGrouped.setAttribute('transform', `translate(${element.coordinates.portrait.x}, ${element.coordinates.portrait.y})`);
          } else if (window.matchMedia('(orientation: landscape)').matches) {
            svgElemGrouped.setAttribute('transform', `translate(${element.coordinates.landscape.x}, ${element.coordinates.landscape.y})`);
          }
          if ((element.animations) && (!document.querySelector(`link[href="assets/${element.animations}"]`))) {
            const link = document.createElement('link');
            link.setAttribute('rel', 'stylesheet');
            link.setAttribute('href', `assets/${element.animations}`);
            document.head.appendChild(link);
          }
          if (element.anchor) {
            svgElemGrouped.classList.add('anchor-svg');
          }
          if (element.alt) {
            document.querySelector('[slot="web-page-details"]').setAttribute('data-alt', element.alt);
          }
          const eventsPostAppend = [];
          if (element.events) {
            if (element.events.document) {
              Object.entries(element.events.document).forEach(([eventName, eventFunctions]) => {
                eventFunctions.forEach((eventFunction) => {
                  svgElemGrouped.classList.add(eventFunction);
                  switch (eventName) {
                    case 'touchstart': case 'touchend':
                      document.querySelector('.webpage').addEventListener(eventName, SVGeorgesWebpage[eventFunction], { passive: true });
                      break;
                    default:
                      document.querySelector('.webpage').addEventListener(eventName, SVGeorgesWebpage[eventFunction]);
                      break;
                  }
                });
              });
            }
            if (element.events.this) {
              Object.entries(element.events.this).forEach(([eventName, eventFunctions]) => {
                eventFunctions.forEach((eventFunction) => {
                  svgElemGrouped.classList.add('cursor-pointer');
                  eventsPostAppend.push([eventName, eventFunction]);
                });
              });
            }
          }
          if (element.anchorevents) {
            Object.entries(element.anchorevents).forEach(([eventName, eventFunctions]) => {
              eventFunctions.forEach((eventFunction) => {
                svgElemGrouped.classList.add(eventFunction);
                switch (eventName) {
                  case 'touchstart': case 'touchend':
                    document.querySelector('.webpage').addEventListener(eventName, (e) => { SVGeorgesWebpage[eventFunction](e, `.${element.name}`); }, { passive: true });
                    break;
                  default:
                    document.querySelector('.webpage').addEventListener(eventName, (e) => { SVGeorgesWebpage[eventFunction](e, `.${element.name}`); });
                    break;
                }
              });
            });
          }
          this.querySelector('main #place').appendChild(svgElemGrouped);
          if (element.pattern) {
            Utils[element.pattern](svgElemGrouped.querySelector('pattern'));
          }
          eventsPostAppend.forEach(([eventName, eventFunction]) => {
            svgElemGrouped.addEventListener(eventName, SVGeorgesWebpage[eventFunction]);
          });
          if (svgElemGrouped.querySelector('.prev') && svgElemGrouped.querySelector('.next')) {
            svgElemGrouped.querySelector('.prev').addEventListener('click', () => {
              SVGeorgesWebpage.parseSVGGallery(svgElemGrouped, 'prev');
            });
            svgElemGrouped.querySelector('.next').addEventListener('click', () => {
              SVGeorgesWebpage.parseSVGGallery(svgElemGrouped, 'next');
            });
          }
          if (svgElemGrouped.querySelector('.embed_switcher')) {
            svgElemGrouped.querySelectorAll('.embed_switcher').forEach((embedSwitcher) => {
              embedSwitcher.addEventListener('click', () => {
                document.querySelector('[slot="web-page-details"]').setAttribute('data-embed', embedSwitcher.getAttribute('data-api'));
                const dataMin = (parseInt(embedSwitcher.getAttribute('data-min'), 10) === 0) ? 0 : parseInt(embedSwitcher.getAttribute('data-min'), 10) || 1;
                let dataMax = parseInt(embedSwitcher.getAttribute('data-max'), 10) || 1000;
                const dataOrder = embedSwitcher.getAttribute('data-order') || '';
                document.querySelector('[slot="web-page-details"]').setAttribute('data-min', dataMin);
                document.querySelector('[slot="web-page-details"]').setAttribute('data-max', dataMax);
                document.querySelector('[slot="web-page-details"]').setAttribute('data-order', dataOrder);
                let options = '';
                let customOrderIndexes = [];
                let previousIndex = null;
                let apiLink = embedSwitcher.getAttribute('data-api').split('/');
                apiLink.pop();
                apiLink = apiLink.join('/');
                if (dataOrder !== '') {
                  customOrderIndexes = dataOrder.split(',');
                  customOrderIndexes.forEach((co, ind) => {
                    customOrderIndexes[ind] = parseFloat(co, 10);
                  });
                  if (customOrderIndexes.length > dataMax) {
                    dataMax = customOrderIndexes.length;
                  }
                }
                for (let i = dataMin; i <= dataMax; i += 1) {
                  let optionIndex = i;
                  if ((dataOrder !== '') && (customOrderIndexes.indexOf(previousIndex) !== -1) && customOrderIndexes[customOrderIndexes.indexOf(previousIndex) + 1]) {
                    optionIndex = customOrderIndexes[customOrderIndexes.indexOf(previousIndex) + 1];
                  }
                  previousIndex = optionIndex;
                  options += `<option value="${apiLink}/${optionIndex}">${optionIndex}</option>`;
                }
                svgElemGrouped.querySelector('.embed_selector').innerHTML = options;
                svgElemGrouped.querySelector('.embed_selector').addEventListener('change', () => {
                  document.querySelector('[slot="web-page-details"]').setAttribute('data-embed', svgElemGrouped.querySelector('.embed_selector option:checked').value);
                  fetch(svgElemGrouped.querySelector('.embed_selector option:checked').value).then((response) => response.json()).then((data) => {
                    SVGeorgesWebpage.parseData(svgElemGrouped.querySelector('iframe'), data);
                  });
                });
                fetch(embedSwitcher.getAttribute('data-api')).then((response) => response.json()).then((data) => {
                  SVGeorgesWebpage.parseData(svgElemGrouped.querySelector('iframe'), data);
                });
                if ((svgElemGrouped.querySelector('.embed_epub_generator')) && (svgElemGrouped.querySelector('.embed_epub_generator').classList.contains('d-none'))) {
                  svgElemGrouped.querySelector('.embed_epub_generator').classList.remove('d-none');
                }
              });
            });
          }
          if (svgElemGrouped.querySelector('.embed_epub_generator')) {
            svgElemGrouped.querySelector('.embed_epub_generator').addEventListener('change', () => {
              if ((document.querySelector('.funkwhale_conte').getAttribute('data-embed')) && (svgElemGrouped.querySelector('.embed_epub_generator').value !== '')) {
                const epubUrl = document.querySelector('.funkwhale_conte').getAttribute('data-embed').split('/');
                epubUrl.pop();
                epubUrl.push('epub', svgElemGrouped.querySelector('.embed_epub_generator').value);
                const savedText = svgElemGrouped.querySelector('.embed_epub_generator :first-child').innerText;
                svgElemGrouped.querySelector('.embed_epub_generator :first-child').innerText = 'Génération en cours';
                svgElemGrouped.querySelector('.embed_epub_generator').value = '';
                svgElemGrouped.querySelector('.embed_epub_generator').setAttribute('disabled', true);
                let epubName = '';
                fetch(epubUrl.join('/')).then((response) => {
                  [, epubName] = response.headers.get('Content-Disposition').split('=');
                  return response.blob();
                }).then((epubBlob) => {
                  svgElemGrouped.querySelector('.embed_epub_generator :first-child').innerText = savedText;
                  svgElemGrouped.querySelector('.embed_epub_generator').removeAttribute('disabled');
                  const downloadAnchorNodeEpub = document.createElement('a');
                  downloadAnchorNodeEpub.setAttribute('href', URL.createObjectURL(epubBlob));
                  downloadAnchorNodeEpub.setAttribute('download', epubName);
                  document.body.appendChild(downloadAnchorNodeEpub);
                  downloadAnchorNodeEpub.click();
                  downloadAnchorNodeEpub.remove();
                });
              }
            });
          }
          if (element.anchor) {
            svgElemGrouped.classList.add('cursor-pointer');
            svgElemGrouped.addEventListener('click', () => {
              if (element.anchor.href) {
                if (element.anchor.href.indexOf('https://') !== -1) {
                  window.location.href = element.anchor.href;
                } else if (element.anchor.href !== '#') {
                  document.querySelector(`a[href="${element.anchor.href}"]`).click();
                }
              } else if (element.anchor.popup) {
                document.querySelector(`g.${element.anchor.popup}`).classList.remove('d-none');
                if (document.querySelectorAll(`g.${element.anchor.popup} iframe`)) {
                  document.querySelectorAll(`g.${element.anchor.popup} iframe`).forEach((iframe) => {
                    iframe.classList.add('d-none');
                    iframe.setAttribute('src', '');
                  });
                  document.querySelectorAll(`g.${element.anchor.popup} iframe`)[element.anchor.id - 1].setAttribute('src', document.querySelectorAll(`g.${element.anchor.popup} iframe`)[element.anchor.id - 1].getAttribute('data-src'));
                  document.querySelectorAll(`g.${element.anchor.popup} iframe`)[element.anchor.id - 1].classList.remove('d-none');
                }
              }
            });
            svgElemGrouped.addEventListener('mouseover', () => {
              if (!svgElemGrouped.querySelector('.anchorname')) {
                let position = '';
                switch (element.anchor.position) {
                  case 'nw':
                    position = 'x="0" y="-10"';
                    break;
                  case 'ne':
                    position = `x="${svgElemGrouped.getBBox().width + 10}" y="10"`;
                    break;
                  case 'se':
                    position = `x="${svgElemGrouped.getBBox().width + 10}" y="${svgElemGrouped.getBBox().height + 15}"`;
                    break;
                  case 'sw':
                    position = `x="0" y="${svgElemGrouped.getBBox().height + 15}"`;
                    break;
                  default:
                    break;
                }
                if (element.anchor.href) {
                  svgElemGrouped.innerHTML += `<text class="anchorname" font-weight="900" ${position}>${((element.anchor.href.indexOf('https://') !== -1) || (element.anchor.href === '#')) ? element.anchor.name : document.querySelector(`a[href="${element.anchor.href}"]`).innerText}</text>`;
                } else if (element.anchor.popup) {
                  svgElemGrouped.innerHTML += `<text class="anchorname" font-weight="900" ${position}>${element.anchor.name}</text>`;
                }
              }
            });
            svgElemGrouped.addEventListener('mouseout', () => {
              if (svgElemGrouped.querySelector('.anchorname')) {
                svgElemGrouped.querySelector('.anchorname').remove();
              }
            });
          }
        }
      });
    });
  }

  /**
   * Ajout des phylactères sur les SVG désignés.
   */
  parseHTMLBulle() {
    if (this.querySelector('.bulle')) {
      this.querySelectorAll('.bulle').forEach((bulleElem) => {
        bulleElem.classList.forEach((className) => {
          if (/bulle-.*/.test(className)) {
            this.querySelector('#place').appendChild(SVGeorgesWebpage.addPhylactere(this.querySelector(`.g_${className.split('-')[1]}`), bulleElem.innerHTML, bulleElem.getAttribute('data-direction')));
          }
        });
      });
    }
  }

  /**
   * Chargement des iframes intégrés aux SVG.
   */
  parseHTMLEmbed() {
    if (this.querySelector('[data-embed]')) {
      Array.from(this.querySelectorAll('[data-embed]')).map(async (embedDataTag) => {
        if (document.querySelector(`g.${embedDataTag.className} iframe`)) {
          const embedData = await fetch(embedDataTag.getAttribute('data-embed')).then((response) => response.json());
          if (Array.isArray(embedData)) {
            embedData.forEach((embed, index) => {
              if (document.querySelectorAll(`g.${embedDataTag.className} iframe`)[index]) {
                SVGeorgesWebpage.parseData(document.querySelectorAll(`g.${embedDataTag.className} iframe`)[index], embed, true);
              }
            });
          } else {
            SVGeorgesWebpage.parseData(document.querySelector(`g.${embedDataTag.className} iframe`), embedData);
          }
          SVGeorgesWebpage.popupCloser(embedDataTag);
        }
      });
    }
  }

  /**
   * Chargement des listes intégrées aux SVG.
   */
  parseHTMLList() {
    if (this.querySelector('[data-list]')) {
      Array.from(this.querySelectorAll('[data-list]')).map(async (embedDataTag) => {
        if (document.querySelector(`g.${embedDataTag.className} foreignObject div`)) {
          const embedData = await fetch(embedDataTag.getAttribute('data-list')).then((response) => response.json());
          let listHTML = '<ul>';
          Object.values(embedData).forEach((data) => {
            listHTML += '<li>';
            Object.entries(data).forEach(([name, value]) => {
              if (name.indexOf('nom_') !== -1) {
                listHTML += `<strong>${value}</strong> `;
              } else if (name.indexOf('description_') !== -1) {
                listHTML += `<em>${value}</em> `;
              } else if (name.indexOf('url_') !== -1) {
                listHTML += `<a href="${value}">${value}</a>`;
              }
            });
            listHTML += '</li>';
          });
          listHTML += '</ul>';
          embedDataTag.innerHTML = listHTML;
          document.querySelector(`g.${embedDataTag.className} foreignObject div`).innerHTML = listHTML + document.querySelector(`g.${embedDataTag.className} foreignObject div`).innerHTML;
          SVGeorgesWebpage.popupCloser(embedDataTag);
        }
      });
    }
  }
}
