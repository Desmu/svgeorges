import SVGNavigation from './SVGNavigation.js';

/**
 * Navigation générale du site.
 */
export default class HTMLNavigation {
  /**
   * Activation des liens de navigation.
   *
   * @param {boolean} mainLoaded true si la navigation de la barre principale est déjà fonctionnelle.
   */
  static navigation(mainLoaded = false) {
    const pages = ['accueil', 'bd', 'videos', 'sons', 'jeux', 'contes', 'reseaux', 'svgeorges'];
    let currentPage = '';
    pages.forEach((page) => {
      document.querySelectorAll(`${mainLoaded ? '.webpage ' : ''}a[href="/${(page !== 'accueil') ? page : ''}"]`).forEach((element) => {
        element.addEventListener('click', async (e) => {
          e.preventDefault();
          document.querySelector('div.webpage').innerHTML = await fetch(`projects/webpages/${page}/webpage.html`).then((response) => response.text());
          if (page === 'svgeorges') {
            SVGNavigation.load();
          }
          let subpage = '';
          if (window.location.href.indexOf('#') !== -1) {
            subpage = `#${window.location.href.split('#')[1]}`;
          }
          window.history.pushState({}, document.querySelector('title').innerText, (page !== 'accueil') ? `/${page}${subpage}` : '/');
          HTMLNavigation.navigation(true);
        });
      });
      if (window.location.href.indexOf(page) !== -1) {
        currentPage = page;
      }
      document.querySelectorAll('a[href^="/"]').forEach((pageLink) => {
        pageLink.setAttribute('aria-hidden', false);
      });
      document.querySelectorAll(`a[href="/${currentPage}"]`).forEach((pageLink) => {
        pageLink.setAttribute('aria-hidden', true);
      });
    });
    if (!mainLoaded) {
      document.querySelector(`nav a[href="/${currentPage}"]`).click();
    }
  }

  /**
   *
   */
  constructor() {
    /** Timeout durant lequel la navigation est affichée. */
    this.navigationtimeout = null;

    document.querySelector('body').addEventListener('mousemove', () => {
      this.initNavigation();
    });
    document.addEventListener('keydown', (e) => {
      if (e.key === 'Tab') {
        this.initNavigation();
      }
    });
  }

  /** Barre masquée après trois secondes sans bouger la souris. */
  hideNavigation() {
    this.navigationtimeout = setTimeout(() => {
      document.querySelector('nav').style.transform = 'translateX(-50%) translateY(100%)';
    }, 3000);
  }

  /** Initialisation des événements de la barre. */
  initNavigation() {
    this.showNavigation();
    this.hideNavigation();
  }

  /** Affichage de la barre. */
  showNavigation() {
    document.querySelector('nav').style.transform = 'translateX(-50%) translateY(0px)';
    clearTimeout(this.navigationtimeout);
  }
}
