/**
 * Fonctions utilitaires communes aux différents formats.
 */
export default class Utils {
  /**
   * Changement d'une coordonnée d'un chemin dessiné.
   *
   * @param {number} x Coordonnée X.
   * @param {number} y Coordonnée Y.
   * @returns {Array} Tableau de coordonnées modifié.
   */
  static changeXY(x, y) {
    const headsOrTails = Utils.getRandomInt(4);
    switch (headsOrTails) {
      case 0:
        x -= Utils.getRandomInt(15);
        y -= Utils.getRandomInt(15);
        break;
      case 1:
        x -= Utils.getRandomInt(15);
        y += Utils.getRandomInt(15);
        break;
      case 2:
        x += Utils.getRandomInt(15);
        y -= Utils.getRandomInt(15);
        break;
      case 3:
        x += Utils.getRandomInt(15);
        y += Utils.getRandomInt(15);
        break;
      default:
        break;
    }
    return [x, y];
  }

  /**
   * Génération d'un arbre SVG.
   *
   * @param {number} height Hauteur approximative de l'arbre en pixels.
   * @param {number} width Largeur approximative de l'arbre en pixels.
   * @returns {object} Un objet SVG représentant l'arbre.
   */
  static generateArbre(height = 600, width = 60) {
    const arbreElem = document.createElementNS('http://www.w3.org/2000/svg', 'g');
    arbreElem.classList.add('arbre');
    const arbreElemTronc = document.createElementNS('http://www.w3.org/2000/svg', 'rect');
    const arbreHeight = Utils.getRandomInt(height) + 10;
    const arbreWidth = Utils.getRandomInt(width) + 10;
    arbreElemTronc.setAttribute('fill', '#808080');
    arbreElemTronc.setAttribute('stroke', '#000');
    arbreElemTronc.setAttribute('height', arbreHeight);
    arbreElemTronc.setAttribute('width', arbreWidth);
    arbreElem.appendChild(arbreElemTronc);
    for (let j = 0; j < (arbreWidth / 5); j += 1) {
      const arbreElemBranche = document.createElementNS('http://www.w3.org/2000/svg', 'path');
      arbreElemBranche.setAttribute('fill', 'transparent');
      arbreElemBranche.setAttribute('stroke', '#000');
      let x = Utils.getRandomInt(arbreWidth);
      let y = 0;
      let path = `M ${x} ${y} `;
      const numberSteps = 5 + Utils.getRandomInt(5);
      for (let k = 0; k < numberSteps; k += 1) {
        [x, y] = Utils.changeXY(x, y);
        path += `C ${x} ${y}, ${x} ${y}, `;
        if (k + 1 < numberSteps) {
          [x, y] = Utils.changeXY(x, y);
          path += `${x} ${y} `;
        } else {
          path += `${x} ${y}`;
        }
        if (k % 2 === 0) {
          const arbreElemFeuille = document.createElementNS('http://www.w3.org/2000/svg', 'rect');
          const feuilleDimension = Utils.getRandomInt(5) + 1;
          arbreElemFeuille.setAttribute('fill', '#fff');
          arbreElemFeuille.setAttribute('stroke', '#000');
          arbreElemFeuille.setAttribute('stroke-width', 0.5);
          arbreElemFeuille.setAttribute('x', x);
          arbreElemFeuille.setAttribute('y', y);
          arbreElemFeuille.setAttribute('height', feuilleDimension);
          arbreElemFeuille.setAttribute('width', feuilleDimension);
          arbreElemFeuille.setAttribute('transform', `translate(${x + feuilleDimension / 2} ${y + feuilleDimension / 2}) skewX(${Utils.getRandomInt(90) - 45}) skewY(${Utils.getRandomInt(90) - 45}) translate(${(x + feuilleDimension / 2) * (-1)} ${(y + feuilleDimension / 2) * (-1)}) rotate(${Utils.getRandomInt(360)} ${x + feuilleDimension / 2} ${y + feuilleDimension / 2})`);
          arbreElem.appendChild(arbreElemFeuille);
        }
      }
      arbreElemBranche.setAttribute('d', path);
      arbreElem.appendChild(arbreElemBranche);
    }
    return arbreElem;
  }

  /**
   * Génération d'une bulle d'eau en SVG.
   *
   * @returns {object} Un objet SVG représentant la bulle.
   */
  static generateBulle() {
    const size = Utils.getRandomInt(5) + 5;
    const bulle = document.createElementNS('http://www.w3.org/2000/svg', 'circle');
    bulle.setAttribute('fill', '#fff');
    bulle.setAttribute('stroke', '#000');
    bulle.setAttribute('stroke-width', size / 10);
    bulle.setAttribute('r', size);
    return bulle;
  }

  /**
   * Génération d'un pattern d'animation pour une bulle d'eau.
   *
   * @param {object} bulle Élément DOM représentant la bulle.
   * @param {number} index Index de la bulle concernée.
   * @param {number} height Hauteur de la zone de génération.
   * @param {number} width Largeur de la zone de génération.
   */
  static generateBulleAnimation(bulle, index, height = 230, width = 230) {
    const cxStart = Utils.getRandomInt(width);
    const cyStart = height;
    let cxEnd = cxStart;
    let cyEnd = cyStart;
    const vx = Utils.getRandomInt(5) - Utils.getRandomInt(5);
    const vy = (Utils.getRandomInt(5) * -1) - 1;
    const frameTime = 1000 / 60;
    let framesAmount = 0;
    while ((cxEnd > 0) && (cxEnd < width) && (cyEnd > 0)) {
      cxEnd += vx;
      cyEnd += vy;
      framesAmount += 1;
    }
    const rules = [
      `
      @keyframes animation_bulle_${index} {
        from { transform: translate(${cxStart}px, ${cyStart}px); }
        to { transform: translate(${cxEnd}px, ${cyEnd}px); }
      }
      `,
      `
      .animation_bulle_${index} {
        animation-duration: ${(frameTime * framesAmount) / 1000}s;
        animation-iteration-count: infinite;
        animation-name: animation_bulle_${index};
        animation-timing-function: linear;
        transform-box: fill-box;
        transform-origin: 50% 50%;
      }
      `,
    ];
    Array.from(document.styleSheets).forEach((stylesheet) => {
      if (stylesheet.href.indexOf(`projects/comics/${document.querySelector('#comic').getAttribute('data-id')}/styles.css`) !== -1) {
        rules.forEach((rule) => {
          stylesheet.insertRule(rule, 0);
        });
      }
    });
    bulle.classList.add(`animation_bulle_${index}`);
  }

  /**
   * Génération d'un nuage SVG.
   *
   * @returns {object} Un objet SVG représentant le nuage.
   */
  static generateNuage() {
    const nuageElem = document.createElementNS('http://www.w3.org/2000/svg', 'path');
    nuageElem.classList.add('nuage');
    nuageElem.setAttribute('fill', '#fff');
    nuageElem.setAttribute('stroke', '#000');
    const x0 = Utils.getRandomInt(100);
    const y0 = Utils.getRandomInt(100);
    let x = x0;
    let y = y0;
    let path = `M ${x} ${y} `;
    const numberSteps = 5 + Utils.getRandomInt(5);
    for (let j = 0; j < numberSteps; j += 1) {
      [x, y] = Utils.changeXY(x, y);
      path += `C ${x} ${y}, ${x} ${y}, `;
      if (j + 1 < numberSteps) {
        [x, y] = Utils.changeXY(x, y);
        path += `${x} ${y} `;
      } else {
        path += `${x0} ${y0}`;
      }
    }
    nuageElem.setAttribute('d', path);
    return nuageElem;
  }

  /**
   * Génération d'un entier aléatoire entre 1 et un chiffre maximum.
   *
   * @param {number} max Chiffre maximum.
   * @returns {number} L'entier aléatoire.
   */
  static getRandomInt(max) {
    return Math.floor(Math.random() * Math.floor(max));
  }

  /**
   * Ajout d'un arbre dans une case.
   *
   * @param {Array} arbres Tableau d'arbres.
   * @param {number} step Numéro de la case.
   * @param {number} index Index du tableau d'arbres.
   */
  static placeArbreComic(arbres, step, index) {
    document.querySelectorAll('.page > g')[step].appendChild(arbres[index]);
    document.querySelectorAll('.arbre')[index].setAttribute('transform', `translate(175, ${200 - parseInt(document.querySelectorAll('.arbre')[index].querySelector('rect').getAttribute('height'), 10)})`);
  }

  /**
   * Ajout d'un nuage dans une case.
   *
   * @param {Array} nuages Tableau de nuages.
   * @param {number} step Numéro de la case.
   * @param {number} index Index du tableau de nuages.
   */
  static placeNuageComic(nuages, step, index) {
    document.querySelectorAll('.page > g')[step].appendChild(nuages[index]);
    document.querySelectorAll('.nuage')[index].setAttribute('transform', `translate(${50 + Utils.getRandomInt(75)}, ${50 + Utils.getRandomInt(75)})`);
  }

  /**
   * Génération d'un <path> aléatoire à base de la plupart des modes de dessin possibles.
   *
   * @param {number} maxValue Coordonnée maximale d'un point du <path>
   * @param {number} amountVariations Nombre d'étapes de <path> à dessiner.
   * @returns {string} Chemin du <path> à dessiner.
   */
  static randomPath(maxValue = 50, amountVariations = 10) {
    const svgPathLetters = [null, 'HV', 'LMT', null, 'QS', null, 'C', 'A'];
    let result = '';
    for (let i = 0; i < amountVariations; i += 1) {
      let amountNumbers = Utils.getRandomInt(svgPathLetters.length);
      while (svgPathLetters[amountNumbers] === null) {
        amountNumbers = Utils.getRandomInt(svgPathLetters.length);
      }
      const letter = svgPathLetters[amountNumbers][Utils.getRandomInt(svgPathLetters[amountNumbers].length)];
      result += `${letter} `;
      for (let j = 0; j < amountNumbers; j += 1) {
        let signe = '';
        if (Utils.getRandomInt(2) === 1) {
          signe = '-';
        }
        let separation = ' ';
        if (((amountNumbers === 4) || (amountNumbers === 6)) && (j % 2 === 1)) {
          separation = ', ';
        } else if (((j === 1) || (j === 4)) && (amountNumbers === 7)) {
          separation = ', ';
        }
        if (((j === 3) || (j === 4)) && (amountNumbers === 7)) {
          result += Utils.getRandomInt(2) + separation;
        } else {
          result += signe + Utils.getRandomInt(maxValue) + separation;
        }
      }
    }
    return result;
  }

  /**
   * Génération d'un pattern de cailloux aléatoire.
   *
   * @param {object} element Élément auquel appliquer le pattern.
   */
  static randomCailloux(element) {
    for (let i = 0; i < 20; i += 1) {
      const caillouElem = document.createElementNS('http://www.w3.org/2000/svg', 'circle');
      caillouElem.setAttribute('fill', '#000');
      caillouElem.setAttribute('stroke', '#000');
      caillouElem.setAttribute('r', Utils.getRandomInt(4));
      caillouElem.setAttribute('cx', Utils.getRandomInt(100));
      caillouElem.setAttribute('cy', Utils.getRandomInt(100));
      element.appendChild(caillouElem);
    }
  }
}
