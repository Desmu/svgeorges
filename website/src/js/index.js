import HTMLNavigation from './HTMLNavigation.js';
import SVGeorgesWebpage from '../components/Webpage/SVGeorgesWebpage.js';

/**
 * Instances des medias SVG déjà chargés.
 */
window.comicCache = [];
window.videoCache = [];

window.addEventListener('load', () => {
  window.customElements.define('svgeorges-webpage', SVGeorgesWebpage);
  const navigation = new HTMLNavigation();
  HTMLNavigation.navigation();
  if ('serviceWorker' in navigator) {
    if (window.matchMedia('(display-mode: browser)').matches) {
      navigator.serviceWorker.register('./service-worker-browser.js');
    } else {
      navigator.serviceWorker.register('./service-worker-pwa.js');
    }
  }
});
