import '../../node_modules/gif.js/dist/gif.js';
import '../../node_modules/tone/build/Tone.js';
import Reader from '../components/Comic/Reader.js';
import Player from '../components/Video/Player.js';
import Subtitles from '../components/Video/Subtitles.js';

/**
 * Navigation générale du lecteur SVG.
 */
export default class SVGNavigation {
  /**
   * Point d'entrée du chargement d'une vidéo.
   *
   * @param {string} project Nom du projet à charger.
   */
  static async loadVideoProject(project) {
    if (document.querySelector('#videoStyle')) {
      document.querySelector('#videoStyle').href = `./projects/videos/${project}/styles.css`;
    } else {
      const link = document.createElement('link');
      link.setAttribute('id', 'videoStyle');
      link.setAttribute('rel', 'stylesheet');
      link.setAttribute('href', `./projects/videos/${project}/styles.css`);
      document.head.appendChild(link);
    }

    if (Object.prototype.hasOwnProperty.call(window.videoCache, project)) {
      document.querySelector('#video').innerHTML = window.videoCache[project].structure;
      if (window.videoCache[project].style) {
        window.videoCache[project].style.forEach((cssRule) => {
          document.querySelector('#videoStyle').sheet.insertRule(cssRule, 0);
        });
      }
      window.videoCache[project].events.forEach((ev, index) => {
        ev.start(index);
      });
      window.videoCache[project].start();
      window.videoCache[project].tonePlayer.toDestination().sync();
      Player.playPause();
    } else {
      window.videoCache[project] = {
        audio: null,
        events: [],
        playerLoop: null,
        specialAnimationsIntervals: [],
        specialAnimationsTimeouts: [],
        structure: '',
        style: [],
        tonePlayer: null,
        totaltime: 0,
      };
      const audio = (await import(`../../projects/videos/${project}/audio.js`)).default;
      const video = (await import(`../../projects/videos/${project}/video.js`)).default;

      const tonePlayer = new Tone.Player(`./projects/videos/${project}/voices.mp3`, (() => {
        window.videoCache[project].totaltime = parseInt(tonePlayer.buffer.duration, 10);
        window.videoCache[project].audio = audio;
        window.videoCache[project].start = () => {
          const player = new Player(video, window.videoCache[project].totaltime);
          window.videoCache[project].playerLoop = player.loop;
          audio.startAudio();
          player.initPlayer();
          setTimeout(() => {
            Tone.Transport.start('+0.1');
            tonePlayer.start();
            video.timeline[0]();
          }, 1);
          document.querySelector('#svgplayer').classList.remove('d-none');
          document.querySelector('#svgplayer').classList.add('d-flex');
        };
        window.videoCache[project].start();
        tonePlayer.toDestination().sync();
        window.videoCache[project].tonePlayer = tonePlayer;
      }));
    }
  }

  /**
   * Point d'entrée du chargement d'une bande dessinée.
   *
   * @param {string} project Nom du projet à charger.
   */
  static async loadComicProject(project) {
    document.querySelector('#subtitles_box').classList.add('d-none');

    if (document.querySelector('#comicStyle')) {
      document.querySelector('#comicStyle').href = `./projects/comics/${project}/styles.css`;
    } else {
      const link = document.createElement('link');
      link.setAttribute('id', 'comicStyle');
      link.setAttribute('rel', 'stylesheet');
      link.setAttribute('href', `./projects/comics/${project}/styles.css`);
      document.head.appendChild(link);
    }

    if (Object.prototype.hasOwnProperty.call(window.comicCache, project)) {
      document.querySelector('#comic').innerHTML = window.comicCache[project];
    } else {
      const alt = (await (await fetch(`./projects/comics/${project}/text.txt`)).text());
      document.querySelector('#comic desc').innerHTML = alt;
      const comic = (await import(`../../projects/comics/${project}/comic.js`)).default;
    }
    const reader = new Reader();
    reader.initReader();
    document.querySelector('#svgreader').classList.remove('d-none');
    document.querySelector('#svgreader').classList.add('d-flex');
  }

  /**
   * Instructions au chargement du lecteur.
   */
  static async load() {
    const comicSVG = await (await fetch('./src/components/Comic/Comic.svg')).text();
    const videoSVG = await (await fetch('./src/components/Video/Video.svg')).text();
    document.querySelector('header').classList.remove('d-none');
    document.querySelector('main').classList.add('d-none');
    document.querySelector('main').innerHTML += comicSVG + videoSVG;

    const projectIndex = await (await fetch('./projects/webpages/svgeorges/webpage.json')).json();
    projectIndex.videos.forEach((video) => {
      document.querySelector('#videoList').innerHTML += `
        <li>
          <a href="#${video.id}" class="videoLink ${video.hidden ? 'd-none' : 'd-block'}">
            <img src="./projects/videos/${video.id}/thumb.png" alt="${video.desc}">
            <figcaption>${video.title}</figcaption>
          </a>
        </li>
      `;
    });
    projectIndex.comics.forEach((comic) => {
      document.querySelector('#comicList').innerHTML += `
        <li>
          <a href="#${comic.id}" class="comicLink d-block">
            <img src="./projects/comics/${comic.id}/thumb.png" alt="${comic.desc}">
            <figcaption>${comic.title}</figcaption>
          </a>
        </li>
      `;
    });

    document.querySelectorAll('header a').forEach((projectLink) => {
      projectLink.addEventListener('click', () => {
        document.querySelector('#video').outerHTML = videoSVG;
        document.querySelector('#comic').outerHTML = comicSVG;
        document.querySelector('main').classList.remove('d-none');
        document.querySelector('nav').classList.add('d-none');
        document.querySelector('header').classList.add('d-none');
        if (projectLink.classList.contains('videoLink')) {
          document.querySelector('#video').setAttribute('data-id', projectLink.hash.replace('#', ''));
          document.querySelector('#comic').classList.add('d-none');
          document.querySelector('#video').classList.remove('d-none');
          document.querySelector('#videotitle').innerHTML = projectLink.querySelector('figcaption').innerHTML;
          document.querySelector('#video title').innerHTML = projectLink.querySelector('figcaption').innerHTML;
          SVGNavigation.loadVideoProject(document.querySelector('#video').getAttribute('data-id'));
        } else if (projectLink.classList.contains('comicLink')) {
          document.querySelector('#comic').setAttribute('data-id', projectLink.hash.replace('#', ''));
          document.querySelector('#video').classList.add('d-none');
          document.querySelector('#comic').classList.remove('d-none');
          document.querySelector('#comictitle').innerHTML = projectLink.querySelector('figcaption').innerHTML;
          document.querySelector('#comic title').innerHTML = projectLink.querySelector('figcaption').innerHTML;
          SVGNavigation.loadComicProject(document.querySelector('#comic').getAttribute('data-id'));
        }
      });
      projectLink.addEventListener('mouseover', () => {
        projectLink.style.transform = 'scale(1.05, 1.05)';
      });
      projectLink.addEventListener('mouseout', () => {
        projectLink.style.transform = 'scale(1, 1)';
      });
      document.addEventListener('keyup', (e) => {
        if ((e.key === 'Tab') && (document.activeElement === projectLink)) {
          projectLink.style.transform = 'scale(1.05, 1.05)';
        } else {
          projectLink.style.transform = 'scale(1, 1)';
        }
      });
    });

    Player.navigationEvents(projectIndex);
    Reader.navigationEvents(projectIndex);
    document.querySelector('#subtitles').addEventListener('click', () => {
      Subtitles.showSubtitles();
    });

    if (window.location.href.split('#').length > 1) {
      const a = document.querySelector(`a[href="#${window.location.href.split('#')[1]}"]`);
      if (a) {
        a.scrollIntoView();
        a.classList.add('animation_thumbnail');
        setTimeout(() => {
          a.classList.remove('animation_thumbnail');
        }, 3000);
      }
    }
  }
}
