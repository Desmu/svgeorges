import Comic from '../../../src/components/Comic/Comic.js';
import Utils from '../../../src/js/Utils.js';

/** Fichiers SVG à charger. */
const svgPaths = [
  './assets/components/Georges/Georges.svg',
  './assets/vectors/backgrounds/comic/sol.svg',
  './assets/vectors/misc/plongeoir.svg',
  './assets/vectors/backgrounds/comic/eau.svg',
];

/** Contenu de la BD. */
const comic = new Comic('15/05/2021', 'N11', svgPaths, () => {
  for (let i = 0; i < 25; i += 1) {
    const bulle = Utils.generateBulle();
    document.querySelectorAll('.page > g')[2].appendChild(bulle);
    Utils.generateBulleAnimation(bulle, i);
  }

  comic.timeline = [
    (step = 0) => {
      Comic.addPerso(step, '.g_sol', 'translate(0, 200)');
      Comic.addPerso(step, '.g_georges', 'translate(10, 108)');
      Comic.addPhylactere(step, comic.alt[step].split('\n')[1].replace('GEORGES - ', ''), 'sw', 30, 50);
    },
    (step = 1) => {
      Comic.addPerso(step, '.g_plongeoir', 'translate(0, 165)');
      Comic.clipCase(step, [document.querySelectorAll('.g_georges')[0].cloneNode(true)], 'animation_plongeon', Comic.addPhylactere(step, comic.alt[step].split('\n')[1].replace('GEORGES - ', ''), 'se', -170, 30, true));
      document.querySelectorAll('.g_georges')[1].setAttribute('transform', 'translate(-40, 82)');
      document.querySelectorAll('.g_georges')[1].querySelector('.jambe_gauche').classList.add('animation_georges_marche_jambe_gauche');
      document.querySelectorAll('.g_georges')[1].querySelector('.jambe_droite').classList.add('animation_georges_marche_jambe_droite');
      document.querySelectorAll('.g_georges')[1].querySelector('.georges').classList.add('animation_plongeon');
      Comic.addPerso(step, '.g_eau', 'translate(0, 200)');
    },
    (step = 2) => {
      Comic.clipCase(step, [document.querySelectorAll('.g_georges')[0].cloneNode(true)], 'animation_tour_1s', Comic.addPhylactere(step, comic.alt[step].split('\n')[1].replace('GEORGES - ', ''), 's', 40, 60, true));
      document.querySelectorAll('.g_georges')[2].setAttribute('transform', 'translate(70, 120)');
      document.querySelectorAll('.g_georges')[2].querySelector('.jambe_gauche').setAttribute('transform', 'rotate(45 15 65)');
      document.querySelectorAll('.g_georges')[2].querySelector('.jambe_droite').setAttribute('transform', 'rotate(-45 30 65)');
      document.querySelectorAll('.g_georges')[2].querySelector('.georges').classList.add('animation_tour_1s');
    },
    (step = 3) => {
      document.querySelectorAll('.page > g')[step].innerHTML += `
        <text font-size="8px" font-family="Free Mono, monospace" x="5" y="10">La Gazette d'Hertzenfoulchenheim</text>
        <text font-size="14px" font-family="Free Mono, monospace" font-weight="900" x="5" y="30">L'EAU DE LA PISCINE ÉTAIT</text>
        <text font-size="36px" font-family="Free Mono, monospace" font-weight="900" x="5" y="65">PÉTILLANTE</text>
        <text font-size="9px" font-family="Free Mono, monospace">
          <tspan x="5" y="90">Stupeur en cet endroit</tspan>
          <tspan x="5" y="105">fraîchement rénové. Les</tspan>
          <tspan x="5" y="120">clients de ce lieu de</tspan>
          <tspan x="5" y="135">loisirs ont été secoués</tspan>
          <tspan x="5" y="150">par les remous d'une aire</tspan>
          <tspan x="5" y="165">d'eau bien trop agitée.</tspan>
          <tspan x="5" y="180">Le gérant des lieux a été</tspan>
          <tspan x="5" y="195">placé en garde à vue. La</tspan>
          <tspan x="5" y="210">personne âgée de 47 ans</tspan>
          <tspan x="5" y="225">a déclaré "Bonjour, je</tspan>
        </text>
        <rect fill="#fff" stroke="#000" x="165" y="85" width="55" height="55"/>
        <text font-size="7px" font-family="Free Mono, monospace" font-weight="900">
          <tspan x="150" y="150">"jemenoiejemenoie</tspan>
          <tspan x="150" y="165">jemenoiejemenoie"</tspan>
          <tspan x="155" y="180">témoigne un des</tspan>
          <tspan x="160" y="195">clients encore</tspan>
          <tspan x="165" y="210">sous le choc.</tspan>
        </text>
      `;
      Comic.addPerso(step, '.g_georges', 'scale(0.5) translate(360, 180)', true);
      document.querySelectorAll('.g_georges')[3].querySelector('.jambe_gauche').setAttribute('transform', 'rotate(45 15 65)');
      document.querySelectorAll('.g_georges')[3].querySelector('.jambe_droite').setAttribute('transform', 'rotate(-45 30 65)');
    },
  ];
});

export default comic;
