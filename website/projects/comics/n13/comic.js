import Comic from '../../../src/components/Comic/Comic.js';

/** Fichiers SVG à charger. */
const svgPaths = [
  './assets/components/Georges/Georges.svg',
  './assets/vectors/misc/valise.svg',
  './assets/vectors/backgrounds/comic/sol.svg',
];

/** Contenu de la BD. */
const comic = new Comic('25/06/2022', 'N13', svgPaths, () => {
  comic.timeline = [
    (step = 0) => {
      Comic.addPerso(step, '.g_sol', 'translate(0, 200)');
      Comic.addPerso(step, '.g_georges', 'translate(25, 108)');
      Comic.addPerso(step, '.g_valise', 'translate(130, 150)');
      Comic.addPhylactere(step, comic.alt[step].split('\n')[1].replace('GEORGES - ', ''), 'sw', 30, 30);
    },
    (step = 1) => {
      Comic.addPerso(step, '.g_sol', 'translate(0, 200)', true);
      Comic.addPerso(step, '.g_georges', 'translate(25, 108)', true);
      Comic.addPerso(step, '.g_valise', 'translate(130, 150)', true);
      document.querySelectorAll('.page > g')[step].innerHTML += `<g class="animation_texte_6">${Comic.addPhylactere(step, comic.alt[step].split('\n')[1].replace('GEORGES - ', ''), 'sw', 30, 80, true)}</g>`;
      document.querySelectorAll('.g_georges')[1].querySelector('.fermeture_haut').classList.add('animation_repli_fermetures');
      document.querySelectorAll('.g_georges')[1].querySelector('.fermeture_bas').classList.add('animation_repli_fermetures');
      document.querySelectorAll('.g_georges')[1].querySelector('.jambe_gauche').classList.add('animation_repli_jambe_gauche');
      document.querySelectorAll('.g_georges')[1].querySelector('.jambe_droite').classList.add('animation_repli_jambe_droite');
      document.querySelectorAll('.g_georges')[1].querySelector('.corps').classList.add('animation_repli_corps');
      document.querySelectorAll('.page > g')[step].innerHTML += `
        <g class="animation_texte_1">
          <text font-family="Free Mono, monospace" font-weight="bold" letter-spacing="-2px" x="40" y="130">Vuuuuuuuu</text>
        </g>
        <g class="animation_texte_2">
          <text font-family="Free Mono, monospace" font-weight="bold" letter-spacing="-2px" x="40" y="130">Crouiiiic</text>
        </g>
        <g class="animation_texte_3">
          <text font-family="Free Mono, monospace" font-weight="bold" letter-spacing="-2px" x="40" y="130">B-B-B-B-B</text>
        </g>
        <g class="animation_texte_4">
          <text font-family="Free Mono, monospace" font-weight="bold" letter-spacing="-2px" x="40" y="130">Vzuiuiuuu</text>
        </g>
        <g class="animation_texte_5">
          <text font-family="Free Mono, monospace" font-weight="bold" letter-spacing="-2px" x="40" y="130">HSCHLAQUE</text>
        </g>
      `;
    },
    (step = 2) => {
      Comic.addPhylactere(step, comic.alt[step].split('\n')[1], '', 50, 100);
    },
    (step = 3) => {
      Comic.addPerso(step, '.g_sol', 'translate(0, 200)', true);
      Comic.addPerso(step, '.g_georges', 'translate(25, 108)', true);
      Comic.addPerso(step, '.g_valise', 'translate(130, 150)', true);
      document.querySelectorAll('.g_georges')[2].querySelector('.fermeture_haut').classList.add('fermetures_repli');
      document.querySelectorAll('.g_georges')[2].querySelector('.fermeture_bas').classList.add('fermetures_repli');
      document.querySelectorAll('.g_georges')[2].querySelector('.jambe_gauche').classList.add('jambe_gauche_repli');
      document.querySelectorAll('.g_georges')[2].querySelector('.jambe_droite').classList.add('jambe_droite_repli');
      document.querySelectorAll('.g_georges')[2].querySelector('.corps').classList.add('corps_repli');
      Comic.addPhylactere(step, comic.alt[step].split('\n')[1].replace('GEORGES - ', ''), 'sw', 30, 80);
    },
  ];
});

export default comic;
