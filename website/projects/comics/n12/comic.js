import Comic from '../../../src/components/Comic/Comic.js';

/** Fichiers SVG à charger. */
const svgPaths = [
  './assets/components/Georges/Georges.svg',
  './assets/vectors/characters/gugusse.svg',
  './assets/vectors/misc/support_metallique_roue.svg',
  './assets/vectors/backgrounds/comic/parquet.svg',
  './assets/vectors/backgrounds/comic/terre.svg',
];

/** Contenu de la BD. */
const comic = new Comic('22/05/2022', 'N12', svgPaths, () => {
  comic.timeline = [
    (step = 0) => {
      Comic.addPerso(step, '.g_parquet', 'translate(0, 200)');
      Comic.addPerso(step, '.g_georges', 'translate(20, 108)');
      Comic.addPerso(step, '.g_gugusse', 'translate(190, 115)');
      Comic.addPhylactere(step, comic.alt[step].split('\n')[1].replace('GUGUSSE - ', ''), 'se', 20, 40);
    },
    (step = 1) => {
      Comic.addPerso(step, '.g_parquet', 'translate(0, 200)', true);
      Comic.addPerso(step, '.g_georges', 'translate(20, 108)', true);
      Comic.addPerso(step, '.g_gugusse', 'translate(190, 115)', true);
      Comic.addPhylactere(step, comic.alt[step].split('\n')[1].replace('GUGUSSE - ', ''), 'se', 20, 40);
      document.querySelectorAll('.g_georges')[1].querySelector('.jambe_gauche').classList.add('animation_georges_trottinage_alternatif_jambe_gauche');
      document.querySelectorAll('.g_georges')[1].querySelector('.jambe_droite').classList.add('animation_georges_trottinage_alternatif_jambe_droite');
    },
    (step = 2) => {
      Comic.addPerso(step, '.g_parquet', 'translate(0, 200)', true);
      Comic.addPerso(step, '.g_georges', 'translate(20, 108)', true);
      Comic.addPerso(step, '.g_gugusse', 'translate(190, 115)', true);
      Comic.addPhylactere(step, comic.alt[step].split('\n')[1].replace('GUGUSSE - ', ''), 'se', 90, 50);
      document.querySelectorAll('.g_georges')[2].querySelector('.jambe_gauche').classList.add('animation_georges_trottinage_jambe_gauche');
      document.querySelectorAll('.g_georges')[2].querySelector('.jambe_droite').classList.add('animation_georges_trottinage_jambe_droite');
    },
    (step = 3) => {
      Comic.addPerso(step, '.g_terre', 'translate(0, 200)');
      Comic.addPerso(step, '.g_georges', 'translate(60, 108)', true);
      Comic.addPerso(step, '.g_support_metallique_roue', 'translate(75, 165)');
      Comic.addPhylactere(step, comic.alt[step].split('\n')[1], '', 15, 20);
      Comic.addPhylactere(step, comic.alt[step].split('\n')[2].replace('GEORGES - ', ''), 'sw', 120, 90);
      document.querySelectorAll('.g_georges')[3].querySelector('.jambe_gauche').classList.add('animation_georges_roue_jambe_gauche');
      document.querySelectorAll('.g_georges')[3].querySelector('.jambe_droite').classList.add('animation_georges_roue_jambe_droite');
    },
  ];
});

export default comic;
