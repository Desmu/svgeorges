import Comic from '../../../src/components/Comic/Comic.js';
import Utils from '../../../src/js/Utils.js';

/** Fichiers SVG à charger. */
const svgPaths = [
  './assets/components/Georges/Georges.svg',
  './assets/vectors/characters/raphael.svg',
];

/** Contenu de la BD. */
const comic = new Comic('30/12/2020', 'N10', svgPaths, () => {
  comic.nuages = [];
  for (let i = 0; i < 12; i += 1) {
    comic.nuages.push(Utils.generateNuage());
  }

  comic.timeline = [
    (step = 0) => {
      Utils.placeNuageComic(comic.nuages, step, 0);
      Utils.placeNuageComic(comic.nuages, step, 1);
      Utils.placeNuageComic(comic.nuages, step, 2);
      Comic.addPerso(step, '.g_georges', 'translate(10, 108)');
      document.querySelectorAll('.g_georges')[0].querySelector('.georges').classList.add('animation_flottement_3s');
      Comic.addPhylactere(step, comic.alt[step].split('\n')[1].replace('GEORGES - ', ''), 'sw', 30, 30);
    },
    (step = 1) => {
      Utils.placeNuageComic(comic.nuages, step, 3);
      Utils.placeNuageComic(comic.nuages, step, 4);
      Utils.placeNuageComic(comic.nuages, step, 5);
      Comic.addPerso(step, '.g_georges', 'translate(10, 108)', true);
      document.querySelectorAll('.g_georges')[1].querySelector('.georges').classList.add('animation_flottement_3s');
      Comic.addPhylactere(step, comic.alt[step].split('\n')[1].replace('GEORGES - ', ''), 'sw', 20, 30);
    },
    (step = 2) => {
      Utils.placeNuageComic(comic.nuages, step, 6);
      Utils.placeNuageComic(comic.nuages, step, 7);
      Utils.placeNuageComic(comic.nuages, step, 8);
      Comic.clipCase(step, [document.querySelectorAll('.g_raphael')[0]], 'animation_papillon', Comic.addPhylactere(step, comic.alt[step].split('\n')[1].replace('RAPHAËL - ', ''), 'se', 35, 70, true));
      Comic.addPerso(step, '.g_georges', 'translate(10, 108)', true);
      document.querySelectorAll('.g_georges')[2].querySelector('.georges').classList.add('animation_flottement_3s');
      document.querySelectorAll('.g_raphael')[0].setAttribute('transform', 'translate(100, 100)');
      document.querySelectorAll('.g_raphael')[0].querySelector('.raphael').classList.add('animation_papillon');
      document.querySelectorAll('.g_raphael')[0].querySelector('.oreille_gauche').classList.add('animation_raphael_oreille_gauche');
      document.querySelectorAll('.g_raphael')[0].querySelector('.oreille_droite').classList.add('animation_raphael_oreille_droite');
    },
    (step = 3) => {
      Utils.placeNuageComic(comic.nuages, step, 9);
      Utils.placeNuageComic(comic.nuages, step, 10);
      Utils.placeNuageComic(comic.nuages, step, 11);
      Comic.addPerso(step, '.g_georges', 'translate(10, 108)', true);
      document.querySelectorAll('.g_georges')[3].querySelector('.georges').classList.add('animation_flottement_3s');
      Comic.addPhylactere(step, comic.alt[step].split('\n')[1].replace('GEORGES - ', ''), 'sw', 20, 60);
    },
  ];
});

export default comic;
