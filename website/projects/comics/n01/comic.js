import Comic from '../../../src/components/Comic/Comic.js';

/** Fichiers SVG à charger. */
const svgPaths = [
  './assets/components/Georges/Georges.svg',
  './assets/vectors/backgrounds/comic/sol.svg',
];

/** Contenu de la BD. */
const comic = new Comic('19/05/2018', 'N01', svgPaths, () => {
  comic.timeline = [
    (step = 0) => {
      Comic.addPerso(step, '.g_sol', 'translate(0, 200)');
      Comic.addPerso(step, '.g_georges', 'translate(10, 108)');
      Comic.addPhylactere(step, comic.alt[step].split('\n')[1].replace('GEORGES - ', ''), 'sw', 30, 50);
    },
    (step = 1) => {
      Comic.addPerso(step, '.g_georges', 'translate(75, 100)', true);
      Comic.addPhylactere(step, comic.alt[step].split('\n')[1].replace('GEORGES - ', ''), 's', 40, 40);
      document.querySelectorAll('.g_georges')[1].querySelector('.jambe_gauche').setAttribute('transform', 'rotate(45 15 65)');
      document.querySelectorAll('.g_georges')[1].querySelector('.jambe_droite').setAttribute('transform', 'rotate(-45 30 65)');
      document.querySelectorAll('.g_georges')[1].querySelector('.georges').classList.add('animation_tour_2s');
    },
    (step = 2) => {
      Comic.addPerso(step, '.g_sol', 'translate(0, 200)', true);
      Comic.addPerso(step, '.g_georges', 'translate(10, 108)', true);
      Comic.addPhylactere(step, comic.alt[step].split('\n')[1].replace('GEORGES - ', ''), 'sw', 30, 30);
    },
    (step = 3) => {
      Comic.addPhylactere(step, comic.alt[step].split('\n')[1], '', 25, 100);
    },
  ];
});

export default comic;
