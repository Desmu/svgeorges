import Comic from '../../../src/components/Comic/Comic.js';

/** Fichiers SVG à charger. */
const svgPaths = [
  './assets/components/Georges/Georges.svg',
  './assets/vectors/backgrounds/comic/sol.svg',
];

/** Contenu de la BD. */
const comic = new Comic('20/05/2018', 'N02', svgPaths, () => {
  comic.timeline = [
    (step = 0) => {
      Comic.addPerso(step, '.g_sol', 'translate(0, 200)');
      Comic.addPerso(step, '.g_georges', 'translate(10, 108)');
      document.querySelectorAll('.g_georges')[0].querySelector('.fermeture_haut').remove();
      document.querySelectorAll('.g_georges')[0].querySelector('.georges').innerHTML += '<path fill="#fff" stroke="#000" d="M 0 12 L 20 12"/>';
      Comic.addPhylactere(step, comic.alt[step].split('\n')[1].replace('GEORGES - ', ''), 'sw', 40, 30);
    },
    (step = 1) => {
      Comic.addPerso(step, '.g_sol', 'translate(0, 200)', true);
      Comic.addPerso(step, '.g_georges', 'translate(75, 105)', true);
      document.querySelectorAll('.g_georges')[1].querySelector('.jambe_gauche').setAttribute('transform', 'rotate(45 15 65)');
      document.querySelectorAll('.g_georges')[1].querySelector('.jambe_droite').setAttribute('transform', 'rotate(-45 30 65)');
      document.querySelectorAll('.g_georges')[1].querySelector('.georges').classList.add('animation_danse_05s');
      Comic.addPhylactere(step, comic.alt[step].split('\n')[1].replace('GEORGES - ', ''), 's', 20, 30);
      document.querySelectorAll('.page > g')[step].innerHTML += `
        <g class="animation_danse_05s">
          <text font-family="Free Mono, monospace" font-weight="bold" letter-spacing="-2px" x="10" y="120">♪ CHDONG ♪</text>
        </g>
        <g class="animation_danse_05s">
          <text font-family="Free Mono, monospace" font-weight="bold" letter-spacing="-2px" x="120" y="130">♪ CHDONG ♪</text>
        </g>
      `;
    },
    (step = 2) => {
      Comic.addPhylactere(step, comic.alt[step].split('\n')[1], '', 30, 100);
    },
    (step = 3) => {
      Comic.addPerso(step, '.g_sol', 'translate(0, 200)', true);
      Comic.addPerso(step, '.g_georges', 'translate(10, 108)', true);
      Comic.addPhylactere(step, comic.alt[step].split('\n')[1].replace('GEORGES - ', ''), 'sw', 30, 50);
    },
  ];
});

export default comic;
