import Comic from '../../../src/components/Comic/Comic.js';

/** Fichiers SVG à charger. */
const svgPaths = [
  './assets/components/Georges/Georges.svg',
  './assets/vectors/characters/gugusse.svg',
  './assets/vectors/misc/pancarte_mime_kine.svg',
  './assets/vectors/backgrounds/comic/sol_cirque.svg',
];

/** Contenu de la BD. */
const comic = new Comic('30/06/2022', 'N16', svgPaths, () => {
  comic.timeline = [
    (step = 0) => {
      Comic.addPerso(step, '.g_sol_cirque', 'translate(0, 140)');
      Comic.addPerso(step, '.g_georges', 'translate(10, 48)');
      Comic.addPerso(step, '.g_pancarte_mime_kine', 'translate(140, 90)');
      Comic.clipCase(step, [
        document.querySelectorAll('.g_gugusse')[0],
        document.querySelectorAll('.g_gugusse')[0].cloneNode(true),
        document.querySelectorAll('.g_gugusse')[0].cloneNode(true),
      ]);
      document.querySelectorAll('.g_gugusse')[0].setAttribute('transform', 'translate(30, 190)');
      document.querySelectorAll('.g_gugusse')[1].setAttribute('transform', 'translate(90, 190)');
      document.querySelectorAll('.g_gugusse')[2].setAttribute('transform', 'translate(150, 190)');
      Comic.addPhylactere(step, comic.alt[step].split('\n')[1].replace('GEORGES - ', ''), 'sw', 40, 30);
    },
    (step = 1) => {
      Comic.addPerso(step, '.g_sol_cirque', 'translate(0, 140)', true);
      Comic.addPerso(step, '.g_georges', 'translate(10, 48)', true);
      Comic.addPerso(step, '.g_pancarte_mime_kine', 'translate(140, 90)', true);
      Comic.clipCase(step, [
        document.querySelectorAll('.g_gugusse')[0].cloneNode(true),
        document.querySelectorAll('.g_gugusse')[0].cloneNode(true),
        document.querySelectorAll('.g_gugusse')[0].cloneNode(true),
      ]);
      document.querySelectorAll('.g_gugusse')[3].setAttribute('transform', 'translate(30, 190)');
      document.querySelectorAll('.g_gugusse')[4].setAttribute('transform', 'translate(90, 190)');
      document.querySelectorAll('.g_gugusse')[5].setAttribute('transform', 'translate(150, 190)');
      document.querySelectorAll('.g_georges')[1].querySelector('.fermeture_haut').classList.add('fermetures_repli_velo');
      document.querySelectorAll('.g_georges')[1].querySelector('.fermeture_bas').classList.add('fermetures_repli_velo');
      document.querySelectorAll('.g_georges')[1].querySelector('.jambe_gauche').classList.add('animation_jambe_gauche_repli_velo');
      document.querySelectorAll('.g_georges')[1].querySelector('.jambe_droite').classList.add('animation_jambe_droite_repli_velo');
      document.querySelectorAll('.g_georges')[1].querySelector('.corps').classList.add('corps_repli_velo');
      Comic.addPhylactere(step, comic.alt[step].split('\n')[1].replace('GUGUSSE - ', ''), 's', 20, 160);
      Comic.addPhylactere(step, comic.alt[step].split('\n')[2].replace('AUTRE GUGUSSE - ', ''), 's', 120, 140);
    },
    (step = 2) => {
      Comic.addPerso(step, '.g_sol_cirque', 'translate(0, 140)', true);
      Comic.addPerso(step, '.g_georges', 'translate(10, 48)', true);
      Comic.clipCase(step, [
        document.querySelectorAll('.g_gugusse')[0].cloneNode(true),
        document.querySelectorAll('.g_gugusse')[0].cloneNode(true),
        document.querySelectorAll('.g_gugusse')[0].cloneNode(true),
        document.querySelectorAll('.g_pancarte_mime_kine')[0].cloneNode(true),
      ]);
      document.querySelectorAll('.g_gugusse')[6].setAttribute('transform', 'translate(30, 190)');
      document.querySelectorAll('.g_gugusse')[7].setAttribute('transform', 'translate(90, 190)');
      document.querySelectorAll('.g_gugusse')[8].setAttribute('transform', 'translate(150, 190)');
      document.querySelectorAll('.g_georges')[2].querySelector('.fermeture_haut').classList.add('fermetures_repli_velo');
      document.querySelectorAll('.g_georges')[2].querySelector('.fermeture_bas').classList.add('fermetures_repli_velo');
      document.querySelectorAll('.g_georges')[2].querySelector('.jambe_gauche').classList.add('jambe_gauche_repli_velo');
      document.querySelectorAll('.g_georges')[2].querySelector('.jambe_droite').classList.add('jambe_droite_repli_velo');
      document.querySelectorAll('.g_georges')[2].querySelector('.corps').classList.add('corps_repli_velo');
      document.querySelectorAll('.g_pancarte_mime_kine')[2].querySelector('.feuille_decrochable').classList.add('animation_feuille_decrochee');
      Comic.addPhylactere(step, comic.alt[step].split('\n')[1].replace('GEORGES - ', ''), 'sw', 20, 30);
    },
    (step = 3) => {
      Comic.addPerso(step, '.g_sol_cirque', 'translate(0, 140)', true);
      Comic.addPerso(step, '.g_georges', 'translate(10, 48)', true);
      Comic.addPerso(step, '.g_pancarte_mime_kine', 'translate(140, 90)', true);
      document.querySelectorAll('.g_pancarte_mime_kine')[3].querySelector('.feuille_decrochable').remove();
      Comic.clipCase(step, [
        document.querySelectorAll('.g_gugusse')[0].cloneNode(true),
        document.querySelectorAll('.g_gugusse')[0].cloneNode(true),
        document.querySelectorAll('.g_gugusse')[0].cloneNode(true),
      ]);
      document.querySelectorAll('.g_gugusse')[9].setAttribute('transform', 'translate(30, 190)');
      document.querySelectorAll('.g_gugusse')[10].setAttribute('transform', 'translate(90, 190)');
      document.querySelectorAll('.g_gugusse')[11].setAttribute('transform', 'translate(150, 190)');
      document.querySelectorAll('.g_georges')[3].querySelector('.fermeture_haut').classList.add('fermetures_repli_velo');
      document.querySelectorAll('.g_georges')[3].querySelector('.fermeture_bas').classList.add('fermetures_repli_velo');
      document.querySelectorAll('.g_georges')[3].querySelector('.jambe_gauche').classList.add('jambe_gauche_repli_velo');
      document.querySelectorAll('.g_georges')[3].querySelector('.jambe_droite').classList.add('jambe_droite_repli_velo');
      document.querySelectorAll('.g_georges')[3].querySelector('.corps').classList.add('corps_repli_velo');
      Comic.addPhylactere(step, comic.alt[step].split('\n')[1].replace('GEORGES - ', ''), 'sw', 20, 30);
    },
  ];
});

export default comic;
