import Comic from '../../../src/components/Comic/Comic.js';

/** Fichiers SVG à charger. */
const svgPaths = [
  './assets/components/Georges/Georges.svg',
  './assets/vectors/misc/lave_linge_tv.svg',
  './assets/vectors/backgrounds/comic/sol.svg',
];

/** Contenu de la BD. */
const comic = new Comic('20/05/2018', 'N03', svgPaths, () => {
  comic.timeline = [
    (step = 0) => {
      Comic.addPerso(step, '.g_sol', 'translate(0, 200)');
      Comic.addPerso(step, '.g_georges', 'translate(10, 108)');
      Comic.addPerso(step, '.g_lave_linge_tv', 'translate(160, 135)');
      Comic.addPhylactere(step, comic.alt[step].split('\n')[1].replace('GEORGES - ', ''), 'sw', 30, 30);
    },
    (step = 1) => {
      Comic.addPerso(step, '.g_sol', 'translate(0, 200)', true);
      Comic.addPerso(step, '.g_georges', 'translate(10, 108)', true);
      Comic.addPerso(step, '.g_lave_linge_tv', 'translate(160, 135)', true);
      Comic.addPhylactere(step, comic.alt[step].split('\n')[1].replace('LAVE-LINGE-TV - ', ''), 'se', 20, 50);
    },
    (step = 2) => {
      Comic.addPerso(step, '.g_sol', 'translate(0, 200)', true);
      Comic.addPerso(step, '.g_georges', 'translate(10, 108)', true);
      Comic.addPerso(step, '.g_lave_linge_tv', 'translate(160, 135)', true);
      document.querySelectorAll('.g_lave_linge_tv')[2].setAttribute('filter', 'url(#feErode)');
      document.querySelectorAll('.g_lave_linge_tv')[2].querySelector('.lave_linge_tv').classList.add('animation_flottement_01s');
      document.querySelectorAll('.page > g')[step].innerHTML += `
        <g class="animation_flottement_01s">
          <text font-size="72px" font-family="Free Mono, monospace" font-weight="bold" letter-spacing="-10px" x="10" y="100">BVUIII</text>
        </g>
      `;
    },
    (step = 3) => {
      Comic.addPerso(step, '.g_sol', 'translate(0, 200)', true);
      Comic.addPerso(step, '.g_georges', 'translate(10, 108)', true);
      Comic.addPerso(step, '.g_lave_linge_tv', 'translate(160, 135)', true);
      Comic.addPhylactere(step, comic.alt[step].split('\n')[1].replace('GEORGES - ', ''), 'sw', 30, 30);
    },
  ];
});

export default comic;
