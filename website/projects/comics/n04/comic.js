import Comic from '../../../src/components/Comic/Comic.js';

/** Fichiers SVG à charger. */
const svgPaths = [
  './assets/components/Georges/Georges.svg',
  './assets/vectors/characters/florent.svg',
  './assets/vectors/backgrounds/comic/sol.svg',
];

/** Contenu de la BD. */
const comic = new Comic('26/05/2018', 'N04', svgPaths, () => {
  comic.timeline = [
    (step = 0) => {
      Comic.addPerso(step, '.g_sol', 'translate(0, 200)');
      Comic.addPerso(step, '.g_georges', 'translate(10, 108)');
      Comic.addPerso(step, '.g_florent', 'translate(165, 108)');
      document.querySelectorAll('.g_florent')[0].querySelector('.florent').setAttribute('transform', 'scale(-1, 1) translate(-55, 0)');
      Comic.addPhylactere(step, comic.alt[step].split('\n')[1].replace('GEORGES - ', ''), 'sw', 30, 30);
    },
    (step = 1) => {
      Comic.clipCase(step, [document.querySelectorAll('.g_florent')[0].cloneNode(true)], 'animation_decollage', Comic.addPhylactere(step, comic.alt[step].split('\n')[1].replace('FLORENT - ', ''), 'se', 95, 95, true));
      Comic.addPerso(step, '.g_sol', 'translate(0, 200)', true);
      Comic.addPerso(step, '.g_georges', 'translate(10, 108)', true);
      document.querySelectorAll('.g_florent')[1].setAttribute('transform', 'translate(210, 108)');
      document.querySelectorAll('.g_florent')[1].querySelector('.jambe_gauche').setAttribute('transform', 'rotate(45 15 65)');
      document.querySelectorAll('.g_florent')[1].querySelector('.jambe_droite').setAttribute('transform', 'rotate(-45 30 65)');
      document.querySelectorAll('.g_florent')[1].querySelector('.florent').classList.add('animation_decollage_pivot');
    },
    (step = 2) => {
      Comic.addPerso(step, '.g_sol', 'translate(0, 200)', true);
      Comic.addPerso(step, '.g_georges', 'translate(10, 108)', true);
      Comic.addPhylactere(step, comic.alt[step].split('\n')[1].replace('FLORENT - ', ''), 'ne', 30, 50);
    },
    (step = 3) => {
      Comic.addPerso(step, '.g_sol', 'translate(0, 200)', true);
      Comic.addPerso(step, '.g_georges', 'translate(10, 108)', true);
      Comic.addPhylactere(step, comic.alt[step].split('\n')[1].replace('GEORGES - ', ''), 'sw', 30, 30);
    },
  ];
});

export default comic;
