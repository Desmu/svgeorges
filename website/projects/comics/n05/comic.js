import Comic from '../../../src/components/Comic/Comic.js';
import Utils from '../../../src/js/Utils.js';

/** Fichiers SVG à charger. */
const svgPaths = [
  './assets/components/Georges/Georges.svg',
  './assets/vectors/characters/florent.svg',
];

/** Contenu de la BD. */
const comic = new Comic('26/05/2018', 'N05', svgPaths, () => {
  comic.nuages = [];
  for (let i = 0; i < 12; i += 1) {
    comic.nuages.push(Utils.generateNuage());
  }

  comic.timeline = [
    (step = 0) => {
      Utils.placeNuageComic(comic.nuages, step, 0);
      Utils.placeNuageComic(comic.nuages, step, 1);
      Utils.placeNuageComic(comic.nuages, step, 2);
      Comic.addPerso(step, '.g_florent', 'translate(10, 108)');
      document.querySelectorAll('.g_florent')[0].querySelector('.florent').classList.add('animation_flottement_2s');
      Comic.addPhylactere(step, comic.alt[step].split('\n')[1].replace('FLORENT - ', ''), 'sw', 30, 30);
    },
    (step = 1) => {
      Utils.placeNuageComic(comic.nuages, step, 3);
      Utils.placeNuageComic(comic.nuages, step, 4);
      Utils.placeNuageComic(comic.nuages, step, 5);
      Comic.clipCase(step, [document.querySelectorAll('.g_georges')[0]], 'animation_vol', Comic.addPhylactere(step, comic.alt[step].split('\n')[1].replace('GEORGES - ', ''), 'se', 85, 95, true));
      Comic.addPerso(step, '.g_florent', 'translate(10, 108)', true);
      document.querySelectorAll('.g_florent')[1].querySelector('.florent').classList.add('animation_flottement_2s');
      document.querySelectorAll('.g_georges')[0].setAttribute('transform', 'translate(165, 108)');
      document.querySelectorAll('.g_georges')[0].querySelector('.jambe_gauche').setAttribute('transform', 'rotate(45 15 65)');
      document.querySelectorAll('.g_georges')[0].querySelector('.jambe_droite').setAttribute('transform', 'rotate(-45 30 65)');
      document.querySelectorAll('.g_georges')[0].querySelector('.georges').classList.add('animation_vol');
    },
    (step = 2) => {
      Utils.placeNuageComic(comic.nuages, step, 6);
      Utils.placeNuageComic(comic.nuages, step, 7);
      Utils.placeNuageComic(comic.nuages, step, 8);
      Comic.addPerso(step, '.g_florent', 'translate(10, 108)', true);
      document.querySelectorAll('.g_florent')[2].querySelector('.florent').classList.add('animation_flottement_2s');
      Comic.addPhylactere(step, comic.alt[step].split('\n')[1].replace('FLORENT - ', ''), 'sw', 30, 30);
    },
    (step = 3) => {
      Utils.placeNuageComic(comic.nuages, step, 9);
      Utils.placeNuageComic(comic.nuages, step, 10);
      Utils.placeNuageComic(comic.nuages, step, 11);
      Comic.addPerso(step, '.g_florent', 'translate(10, 108)', true);
      document.querySelectorAll('.g_florent')[3].querySelector('.florent').classList.add('animation_flottement_2s');
      Comic.addPhylactere(step, comic.alt[step].split('\n')[1].replace('GEORGES - ', ''), 'ne', 30, 50);
      Comic.addPhylactere(step, comic.alt[step].split('\n')[2].replace('GEORGES - ', ''), 'ne', 95, 180);
    },
  ];
});

export default comic;
