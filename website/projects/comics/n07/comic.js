import Comic from '../../../src/components/Comic/Comic.js';
import Utils from '../../../src/js/Utils.js';

/** Fichiers SVG à charger. */
const svgPaths = [
  './assets/components/Georges/Georges.svg',
  './assets/vectors/backgrounds/comic/sol.svg',
];

/** Contenu de la BD. */
const comic = new Comic('05/12/2020', 'N07', svgPaths, () => {
  comic.arbres = [];
  for (let i = 0; i < 4; i += 1) {
    comic.arbres.push(Utils.generateArbre(150, 40));
  }

  comic.timeline = [
    (step = 0) => {
      Utils.placeArbreComic(comic.arbres, step, 0);
      Comic.addPerso(step, '.g_sol', 'translate(0, 200)');
      Comic.addPerso(step, '.g_georges', 'translate(10, 108)');
      Comic.addPhylactere(step, comic.alt[step].split('\n')[1].replace('GEORGES - ', ''), 'sw', 30, 50);
      document.querySelectorAll('.page > g')[step].innerHTML += `
        <rect fill="#fff" stroke="#000" x="170" y="190" height="10" width="40"/>
        <text font-size="8px" font-family="Free Mono, monospace" x="175" y="198">CÈDRE</text>
      `;
    },
    (step = 1) => {
      Utils.placeArbreComic(comic.arbres, step, 1);
      Comic.addPerso(step, '.g_sol', 'translate(0, 200)', true);
      Comic.addPerso(step, '.g_georges', 'translate(25, 103)', true);
      document.querySelectorAll('.g_georges')[1].querySelector('.jambe_gauche').setAttribute('transform', 'rotate(45 15 65)');
      document.querySelectorAll('.g_georges')[1].querySelector('.jambe_droite').setAttribute('transform', 'rotate(-45 30 65)');
      document.querySelectorAll('.g_georges')[1].querySelector('.georges').classList.add('animation_danse_025s');
      Comic.addPhylactere(step, comic.alt[step].split('\n')[1].replace('GEORGES - ', ''), 'sw', 30, 50);
      document.querySelectorAll('.page > g')[step].innerHTML += `
        <rect fill="#fff" stroke="#000" x="160" y="190" height="10" width="50"/>
        <text font-size="8px" font-family="Free Mono, monospace" x="165" y="198">BOULEAU</text>
      `;
    },
    (step = 2) => {
      Utils.placeArbreComic(comic.arbres, step, 2);
      Comic.addPerso(step, '.g_sol', 'translate(0, 200)', true);
      Comic.addPerso(step, '.g_georges', 'translate(25, 103)', true);
      document.querySelectorAll('.g_georges')[2].querySelector('.jambe_gauche').setAttribute('transform', 'rotate(45 15 65)');
      document.querySelectorAll('.g_georges')[2].querySelector('.jambe_droite').setAttribute('transform', 'rotate(-45 30 65)');
      document.querySelectorAll('.g_georges')[2].querySelector('.georges').classList.add('animation_danse_025s');
      Comic.addPhylactere(step, comic.alt[step].split('\n')[1].replace('GEORGES - ', ''), 'sw', 30, 50);
      document.querySelectorAll('.page > g')[step].innerHTML += `
        <rect fill="#fff" stroke="#000" x="160" y="190" height="10" width="45"/>
        <text font-size="8px" font-family="Free Mono, monospace" x="165" y="198">MÉLÈZE</text>
      `;
    },
    (step = 3) => {
      Utils.placeArbreComic(comic.arbres, step, 3);
      Comic.addPerso(step, '.g_sol', 'translate(0, 200)', true);
      Comic.addPerso(step, '.g_georges', 'translate(10, 108)', true);
      document.querySelectorAll('.g_georges')[3].querySelector('.jambe_gauche').setAttribute('transform', 'rotate(45 15 65)');
      document.querySelectorAll('.g_georges')[3].querySelector('.jambe_droite').setAttribute('transform', 'rotate(-45 30 65)');
      Comic.addPhylactere(step, comic.alt[3].split('\n')[1].replace('GEORGES - ', ''), 'sw', 20, 30);
      Comic.addPhylactere(step, comic.alt[step].split('\n')[2].replace('CONSERVATEUR - ', ''), 'se', 60, 70);
      document.querySelectorAll('.page > g')[step].innerHTML += `
        <rect fill="#fff" stroke="#000" x="150" y="190" height="10" width="75"/>
        <text font-size="8px" font-family="Free Mono, monospace" x="155" y="198">CONSERVATEUR</text>
      `;
      Comic.addPhylactere(step, '.georges', comic.alt[step].split('\n')[3].replace('GEORGES - ', ''), 75, 215, 30, 110, 'nw');
    },
  ];
});

export default comic;
