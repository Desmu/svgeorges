import Comic from '../../../src/components/Comic/Comic.js';

/** Fichiers SVG à charger. */
const svgPaths = [
  './assets/components/Georges/Georges.svg',
  './assets/vectors/characters/raphael.svg',
  './assets/vectors/backgrounds/comic/sol.svg',
];

/** Contenu de la BD. */
const comic = new Comic('27/12/2020', 'N09', svgPaths, () => {
  comic.timeline = [
    (step = 0) => {
      Comic.addPerso(step, '.g_sol', 'translate(0, 200)');
      Comic.addPerso(step, '.g_georges', 'translate(10, 108)');
      Comic.addPerso(step, '.g_raphael', 'translate(140, 83)');
      Comic.addPhylactere(step, comic.alt[step].split('\n')[1].replace('GEORGES - ', ''), 'sw', 20, 30);
    },
    (step = 1) => {
      Comic.addPerso(step, '.g_sol', 'translate(0, 200)', true);
      Comic.addPerso(step, '.g_georges', 'translate(10, 108)', true);
      Comic.addPerso(step, '.g_raphael', 'translate(140, 83)', true);
      Comic.addPhylactere(step, comic.alt[step].split('\n')[1].replace('RAPHAËL - ', ''), 'se', 20, 30);
    },
    (step = 2) => {
      Comic.addPerso(step, '.g_sol', 'translate(0, 200)', true);
      Comic.addPerso(step, '.g_georges', 'translate(10, 108)', true);
      Comic.addPerso(step, '.g_raphael', 'translate(140, 83)', true);
      document.querySelectorAll('.g_raphael')[2].querySelector('.oreille_gauche').classList.add('animation_raphael_oreille_gauche');
      document.querySelectorAll('.g_raphael')[2].querySelector('.oreille_droite').classList.add('animation_raphael_oreille_droite');
    },
    (step = 3) => {
      Comic.addPerso(step, '.g_sol', 'translate(0, 200)', true);
      Comic.addPerso(step, '.g_georges', 'translate(10, 108)', true);
      Comic.addPerso(step, '.g_raphael', 'translate(140, 83)', true);
      Comic.addPhylactere(step, comic.alt[step].split('\n')[1].replace('RAPHAËL - ', ''), 'se', 40, 30);
    },
  ];
});

export default comic;
