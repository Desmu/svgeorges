import Comic from '../../../src/components/Comic/Comic.js';
import Utils from '../../../src/js/Utils.js';

/** Fichiers SVG à charger. */
const svgPaths = [
  './assets/components/Georges/Georges.svg',
  './assets/vectors/characters/theo.svg',
];

/** Contenu de la BD. */
const comic = new Comic('28/06/2022', 'N15', svgPaths, () => {
  comic.nuages = [];
  for (let i = 0; i < 12; i += 1) {
    comic.nuages.push(Utils.generateNuage());
  }

  comic.timeline = [
    (step = 0) => {
      Utils.placeNuageComic(comic.nuages, step, 0);
      Utils.placeNuageComic(comic.nuages, step, 1);
      Utils.placeNuageComic(comic.nuages, step, 2);
      Comic.addPerso(step, '.g_georges', 'translate(10, 108)');
      Comic.addPerso(step, '.g_theo', 'translate(150, 112)');
      document.querySelectorAll('.g_georges')[0].querySelector('.georges').classList.add('animation_flottement_3s');
      document.querySelectorAll('.g_theo')[0].querySelector('.theo').classList.add('animation_aplatissage_flottement');
    },
    (step = 1) => {
      Utils.placeNuageComic(comic.nuages, step, 3);
      Utils.placeNuageComic(comic.nuages, step, 4);
      Utils.placeNuageComic(comic.nuages, step, 5);
      Comic.addPerso(step, '.g_georges', 'translate(10, 108)', true);
      Comic.addPerso(step, '.g_theo', 'translate(150, 112)', true);
      document.querySelectorAll('.g_georges')[1].querySelector('.georges').classList.add('animation_flottement_3s');
      document.querySelectorAll('.g_theo')[1].querySelector('.theo').classList.remove('animation_aplatissage_flottement');
      document.querySelectorAll('.g_theo')[1].querySelector('.theo').classList.add('animation_aplati_flottement');
      document.querySelectorAll('.page > g')[step].querySelectorAll('.nuage').forEach((nuage) => {
        nuage.classList.add('animation_aplatissage_centre');
      });
    },
    (step = 2) => {
      Utils.placeNuageComic(comic.nuages, step, 6);
      Utils.placeNuageComic(comic.nuages, step, 7);
      Utils.placeNuageComic(comic.nuages, step, 8);
      Comic.addPerso(step, '.g_georges', 'translate(10, 108)', true);
      Comic.addPerso(step, '.g_theo', 'translate(150, 112)', true);
      document.querySelectorAll('.g_georges')[2].querySelector('.georges').classList.add('animation_flottement_3s');
      document.querySelectorAll('.g_theo')[2].querySelector('.theo').classList.remove('animation_aplatissage_flottement');
      document.querySelectorAll('.g_theo')[2].querySelector('.theo').classList.add('animation_aplati_flottement');
      document.querySelectorAll('.page > g')[step].querySelectorAll('.nuage').forEach((nuage) => {
        nuage.setAttribute('transform', `${nuage.getAttribute('transform')} scale(1, 0.5)`);
      });
      document.querySelectorAll('.page > g')[step].classList.add('animation_aplatissage_case');
    },
    (step = 3) => {
      Utils.placeNuageComic(comic.nuages, step, 9);
      Utils.placeNuageComic(comic.nuages, step, 10);
      Utils.placeNuageComic(comic.nuages, step, 11);
      Comic.addPerso(step, '.g_georges', 'translate(10, 108)', true);
      Comic.addPerso(step, '.g_theo', 'translate(150, 112)', true);
      Comic.addPhylactere(step, comic.alt[step].split('\n')[1].replace('GEORGES - ', ''), 'sw', 40, 50);
      document.querySelectorAll('.g_georges')[3].querySelector('.georges').classList.add('animation_flottement_3s');
      document.querySelectorAll('.g_theo')[3].querySelector('.theo').classList.remove('animation_aplatissage_flottement');
      document.querySelectorAll('.g_theo')[3].querySelector('.theo').classList.add('animation_aplati_flottement');
      document.querySelectorAll('.page > g')[step].querySelectorAll('.nuage').forEach((nuage) => {
        nuage.setAttribute('transform', `${nuage.getAttribute('transform')} scale(1, 0.5)`);
      });
      document.querySelectorAll('.page > g')[step].setAttribute('transform', `${document.querySelectorAll('.page > g')[step].getAttribute('transform')} scale(1, 0.33)`);
      document.querySelectorAll('.page > g')[step].setAttribute('transform-origin', '0 115');
    },
  ];
});

export default comic;
