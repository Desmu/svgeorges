import Comic from '../../../src/components/Comic/Comic.js';

/** Fichiers SVG à charger. */
const svgPaths = [
  './assets/components/Georges/Georges.svg',
  './assets/vectors/characters/gugusse.svg',
  './assets/vectors/backgrounds/comic/sol.svg',
];

/** Contenu de la BD. */
const comic = new Comic('16/12/2020', 'N08', svgPaths, () => {
  comic.timeline = [
    (step = 0) => {
      Comic.addPerso(step, '.g_sol', 'translate(0, 200)');
      Comic.addPerso(step, '.g_georges', 'translate(10, 108)');
      Comic.addPhylactere(step, comic.alt[step].split('\n')[1].replace('GEORGES - ', ''), 'sw', 30, 50);
    },
    (step = 1) => {
      Comic.addPhylactere(step, comic.alt[step].split('\n')[1], '', 70, 110);
    },
    (step = 2) => {
      Comic.addPerso(step, '.g_sol', 'translate(0, 200)', true);
      Comic.addPerso(step, '.g_georges', 'translate(10, 108)', true);
      Comic.addPerso(step, '.g_gugusse', 'translate(180, 115)');
      Comic.addPhylactere(step, comic.alt[step].split('\n')[1].replace('GUGUSSE - ', ''), 'se', 30, 30);
      document.querySelectorAll('.g_gugusse')[0].querySelector('.gugusse').classList.add('animation_sautillement');
    },
    (step = 3) => {
      Comic.addPerso(step, '.g_sol', 'translate(0, 200)', true);
      Comic.addPerso(step, '.g_georges', 'translate(10, 108)', true);
      Comic.addPerso(step, '.g_gugusse', 'translate(180, 115)', true);
      Comic.addPhylactere(step, comic.alt[step].split('\n')[1].replace('GEORGES - ', ''), 'sw', 30, 30);
      document.querySelectorAll('.g_gugusse')[1].querySelector('.gugusse').classList.remove('animation_sautillement');
    },
  ];
});

export default comic;
