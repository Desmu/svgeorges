import Comic from '../../../src/components/Comic/Comic.js';

/** Fichiers SVG à charger. */
const svgPaths = [
  './assets/components/Georges/Georges.svg',
  './assets/vectors/characters/theo.svg',
  './assets/vectors/backgrounds/comic/sol.svg',
];

/** Contenu de la BD. */
const comic = new Comic('26/06/2022', 'N14', svgPaths, () => {
  comic.timeline = [
    (step = 0) => {
      Comic.addPerso(step, '.g_sol', 'translate(0, 200)');
      Comic.addPerso(step, '.g_georges', 'translate(10, 108)');
      Comic.addPerso(step, '.g_theo', 'translate(160, 112)');
      Comic.addPhylactere(step, comic.alt[step].split('\n')[1].replace('GEORGES - ', ''), 'sw', 30, 50);
    },
    (step = 1) => {
      Comic.addPerso(step, '.g_sol', 'translate(0, 200)', true);
      Comic.addPerso(step, '.g_georges', 'translate(10, 108)', true);
      Comic.addPerso(step, '.g_theo', 'translate(160, 112)', true);
      document.querySelectorAll('.page > g')[step].innerHTML += `<g class="animation_aplatissage_bulle">${Comic.addPhylactere(step, comic.alt[step].split('\n')[1].replace('THÉO-GANDALF - ', ''), 'se', 50, 30, true)}</g>`;
      document.querySelectorAll('.g_theo')[1].querySelector('.theo').classList.add('animation_aplatissage');
    },
    (step = 2) => {
      Comic.addPerso(step, '.g_sol', 'translate(0, 200)', true);
      Comic.addPerso(step, '.g_georges', 'translate(10, 108)', true);
      Comic.addPerso(step, '.g_theo', 'translate(160, 112)', true);
      document.querySelectorAll('.g_theo')[2].querySelector('.theo').setAttribute('transform', 'scale(1, 0.5) translate(0, 90)');
    },
    (step = 3) => {
      Comic.addPerso(step, '.g_sol', 'translate(0, 200)', true);
      Comic.addPerso(step, '.g_georges', 'translate(10, 108)', true);
      Comic.addPerso(step, '.g_theo', 'translate(160, 112)', true);
      document.querySelectorAll('.g_theo')[3].querySelector('.theo').setAttribute('transform', 'scale(1, 0.5) translate(0, 90)');
      Comic.addPhylactere(step, comic.alt[step].split('\n')[1].replace('GEORGES - ', ''), 'sw', 30, 40);
    },
  ];
});

export default comic;
