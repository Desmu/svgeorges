import Comic from '../../../src/components/Comic/Comic.js';

/** Fichiers SVG à charger. */
const svgPaths = [
  './assets/components/Georges/Georges.svg',
  './assets/vectors/characters/gugusse.svg',
  './assets/vectors/misc/ordinateur.svg',
  './assets/vectors/backgrounds/comic/sol.svg',
];

/** Contenu de la BD. */
const comic = new Comic('29/11/2020', 'N06', svgPaths, () => {
  comic.timeline = [
    (step = 0) => {
      Comic.addPerso(step, '.g_sol', 'translate(0, 200)');
      Comic.addPerso(step, '.g_georges', 'translate(10, 108)');
      Comic.addPerso(step, '.g_ordinateur', 'translate(130, 105)');
      Comic.addPerso(step, '.g_gugusse', 'translate(190, 115)');
      Comic.addPhylactere(step, comic.alt[step].split('\n')[1].replace('GEORGES - ', ''), 'sw', 30, 50);
    },
    (step = 1) => {
      Comic.addPerso(step, '.g_sol', 'translate(0, 200)', true);
      Comic.addPerso(step, '.g_georges', 'translate(10, 108)', true);
      Comic.addPerso(step, '.g_ordinateur', 'translate(130, 105)', true);
      Comic.addPerso(step, '.g_gugusse', 'translate(190, 115)', true);
      document.querySelectorAll('.g_gugusse')[1].querySelector('.bras_gauche').classList.add('animation_gugusse_clavier_bras_gauche');
      document.querySelectorAll('.g_gugusse')[1].querySelector('.bras_droit').classList.add('animation_gugusse_clavier_bras_droit');
      Comic.addPhylactere(step, comic.alt[step].split('\n')[1].replace('GUGUSSE - ', ''), 'se', 20, 50);
      document.querySelectorAll('.page > g')[step].innerHTML += `
        <text class="animation_apparition_alternative_1" font-family="Free Mono, monospace" font-weight="bold" letter-spacing="-2px" x="200" y="110">BIP</text>
        <text class="animation_apparition_alternative_2" font-family="Free Mono, monospace" font-weight="bold" letter-spacing="-2px" x="150" y="130">BOUP</text>
      `;
    },
    (step = 2) => {
      Comic.addPhylactere(step, comic.alt[step].split('\n')[1], '', 40, 120);
    },
    (step = 3) => {
      Comic.addPerso(step, '.g_sol', 'translate(0, 200)', true);
      Comic.addPerso(step, '.g_georges', 'translate(10, 108)', true);
      Comic.addPerso(step, '.g_ordinateur', 'translate(130, 105)', true);
      Comic.addPerso(step, '.g_gugusse', 'translate(190, 115)', true);
      Comic.addPhylactere(step, comic.alt[step].split('\n')[1].replace('GEORGES - ', ''), 'sw', 20, 40);
      Comic.addPhylactere(step, comic.alt[step].split('\n')[2].replace('GUGUSSE - ', ''), 'ne', 80, 190);
    },
  ];
});

export default comic;
