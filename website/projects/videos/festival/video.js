import Utils from '../../../src/js/Utils.js';
import Subtitles from '../../../src/components/Video/Subtitles.js';
import Video from '../../../src/components/Video/Video.js';
import audio from './audio.js';

/** Fichiers SVG à charger. */
const svgPaths = [
  './assets/vectors/text/georgeslasaucisse.svg',
  './assets/vectors/backgrounds/landscape/mur_hall.svg',
  './assets/vectors/backgrounds/video/mur.svg',
  './assets/vectors/backgrounds/video/mur2.svg',
  './assets/vectors/misc/porte_interieur.svg',
  './assets/vectors/misc/couloir_escaliers.svg',
  './assets/vectors/misc/trappe.svg',
  './assets/vectors/misc/couloir_escaliers.svg',
  './assets/vectors/misc/echelle.svg',
  './assets/vectors/misc/couloir_escaliers.svg',
  './assets/vectors/misc/porte_exterieur.svg',
  './assets/vectors/misc/affiches/affiche.svg',
  './assets/vectors/misc/affiches/affiche_festival.svg',
  './assets/vectors/misc/affiches/affiche_mangeurs.svg',
  './assets/vectors/misc/affiches/affiche_motdepasse.svg',
  './assets/vectors/misc/affiches/affiche_citron.svg',
  './assets/vectors/misc/affiches/affiche_langues.svg',
  './assets/vectors/misc/affiches/affiche_administratif.svg',
  './assets/vectors/misc/affiches/affiche_vacances.svg',
  './assets/vectors/misc/affiches/affiche_raphael.svg',
  './assets/vectors/misc/affiches/affiche_fausse.svg',
  './assets/vectors/misc/affiches/affiche_subventions.svg',
  './assets/vectors/misc/affiches/affiche_ecolo.svg',
  './assets/vectors/misc/affiches/affiche_youpiprojet.svg',
  './assets/vectors/text/unecreationdedesmu.svg',
  './assets/vectors/text/florentlabananemagicienne.svg',
  './assets/vectors/text/corentinlatruite.svg',
  './assets/vectors/text/raphaellepanda.svg',
  './assets/vectors/text/maximeleserpentferarepasser.svg',
  './assets/vectors/text/theogandalflecitron.svg',
  './assets/vectors/text/unecasseroledeauchaude.svg',
  './assets/vectors/text/georgeslasaucisseorganiseunfestival.svg',
  './assets/vectors/text/organisationdufestival.svg',
  './assets/vectors/text/georgeslasaucissepointfr.svg',
  './assets/vectors/text/georgeslasaucissearobasemastodonpointdesmupointfr.svg',
  './assets/vectors/characters/gugusse.svg',
  './assets/vectors/characters/gugusse.svg',
  './assets/vectors/characters/gugusse.svg',
  './assets/vectors/characters/gugusse.svg',
  './assets/vectors/characters/soleil.svg',
  './assets/vectors/misc/chaine_hifi.svg',
  './assets/vectors/misc/telephone.svg',
  './assets/vectors/misc/papier.svg',
  './assets/vectors/misc/papier_alu.svg',
  './assets/vectors/misc/papier_pile.svg',
  './assets/vectors/misc/ecrans/ecran.svg',
  './assets/vectors/misc/ecrans/ecran_youpiprojet.svg',
  './assets/vectors/misc/ecrans/ecran_youpiprojet_code.svg',
  './assets/vectors/misc/ecrans/ecran_youpiprojet_demandes.svg',
  './assets/vectors/misc/ecrans/ecran_youpiprojet_demandes_2.svg',
  './assets/vectors/misc/ecrans/ecran_youpiprojet_inscription.svg',
  './assets/vectors/misc/ecrans/ecran_youpiprojet_inscription_choix.svg',
  './assets/vectors/misc/ecrans/ecran_youpiprojet_organisation.svg',
  './assets/vectors/misc/ecrans/ecran_youpiprojet_projet.svg',
  './assets/vectors/misc/ecrans/ecran_youpiprojet_raphael.svg',
  './assets/vectors/misc/ecrans/ecran_youpiprojet_reunion.svg',
  './assets/vectors/misc/ecrans/ecran_youpiprojet_saucisseconnect.svg',
  './assets/vectors/misc/ecrans/ecran_youpiprojet_subventions.svg',
  './assets/vectors/misc/ecrans/ecran_youpiprojet_valide.svg',
  './assets/vectors/misc/ordinateur_bureau.svg',
  './assets/vectors/misc/bureau_subventions.svg',
  './assets/vectors/misc/bibliotheque_tres_mini.svg',
  './assets/components/Georges/Georges.svg',
  './assets/vectors/characters/philippe.svg',
  './assets/vectors/characters/corentin.svg',
  './assets/vectors/characters/florent.svg',
  './assets/vectors/characters/raphael.svg',
  './assets/vectors/characters/maxime.svg',
  './assets/vectors/characters/theo.svg',
  './assets/vectors/characters/casserole.svg',
  './assets/vectors/backgrounds/landscape/sol.svg',
  './assets/vectors/backgrounds/video/plage.svg',
  './assets/vectors/backgrounds/video/sol.svg',
  './assets/vectors/backgrounds/video/sol2.svg',
  './assets/vectors/backgrounds/video/sol3.svg',
  './assets/vectors/text/generique/cetaitgeorgeslasaucisseorganiseunfestival.svg',
  './assets/vectors/text/generique/unevideorealiseepardesmu.svg',
  './assets/vectors/text/generique/montageenjavascriptetensvgsoussvgeorges.svg',
  './assets/vectors/text/generique/retrouvezgeorgessur.svg',
  './assets/vectors/text/generique/mercipourcevisionnagedesolepourvotretemps.svg',
  './assets/vectors/text/generique/voila.svg',
];

/** Contenu de la vidéo. */
const video = new Video(svgPaths, (new Subtitles('./projects/videos/festival/subtitles.vtt')), () => {
  /* Création du chemin du sol de la plage. */
  let cheminPath = '';
  let currY = 0;
  for (let i = 1; i <= 1920; i += 1) {
    let headsOrTails = Utils.getRandomInt(2);
    if (currY === 5) {
      headsOrTails = 0;
    } else if (currY === -5) {
      headsOrTails = 1;
    }
    switch (headsOrTails) {
      case 0:
        currY -= 1;
        break;
      case 1:
        currY += 1;
        break;
      default:
        break;
    }
    cheminPath += `${i},${currY} `;
  }
  document.querySelector('.plage').setAttribute('points', `0,0 ${cheminPath}1920,0 1920,100 0,100`);

  /* Création de neige. */
  video.style = [];
  const limit = 16;
  for (let j = 0; j < 9; j += 1) {
    for (let i = 0; i < limit; i += 1) {
      const floconElem = document.createElementNS('http://www.w3.org/2000/svg', 'g');
      floconElem.classList.add('flocon', `flocon_${i + (j * limit)}`);
      const floconBrancheElem = document.createElementNS('http://www.w3.org/2000/svg', 'path');
      floconBrancheElem.setAttribute('fill', 'transparent');
      floconBrancheElem.setAttribute('stroke', '#fff');
      let x = 0;
      let y = 0;
      let path = `M ${x} ${y} `;
      const numberSteps = Utils.getRandomInt(5);
      for (let k = 0; k < numberSteps; k += 1) {
        [x, y] = Utils.changeXY(x, y);
        path += `L ${x} ${y}`;
        if (k + 1 === numberSteps) {
          [x, y] = Utils.changeXY(x, y);
          path += ' ';
        }
      }
      floconBrancheElem.setAttribute('d', path);
      for (let deg = 60; deg < 360; deg += 60) {
        const floconBrancheElemClone = floconBrancheElem.cloneNode(true);
        floconBrancheElemClone.setAttribute('transform', `rotate(${deg})`);
        floconElem.appendChild(floconBrancheElemClone);
      }
      floconElem.appendChild(floconBrancheElem);
      document.querySelector('.decor').appendChild(floconElem);
      video.style.push(`
        @keyframes chute_de_neige_${i + (j * limit)} {
          from { transform: translate(${i * 120}px, 0px); }
          12.5% { transform: translate(${i * 120 + 50}px, 135px); }
          25% { transform: translate(${i * 120}px, 270px); }
          37.5% { transform: translate(${i * 120 - 50}px, 405px); }
          50% { transform: translate(${i * 120}px, 540px); }
          62.5% { transform: translate(${i * 120 + 50}px, 675px); }
          75% { transform: translate(${i * 120}px, 810px); }
          87.5% { transform: translate(${i * 120 - 50}px, 945px); }
          to { transform: translate(${i * 120}px, 1080px); }
        }
      `);
      video.style.push(`
        .flocon_${i + (j * limit)} {
          animation-delay: ${j}s;
          animation-duration: ${Utils.getRandomInt(15) + 5}s;
          animation-name: chute_de_neige_${i + (j * limit)};
          animation-iteration-count: infinite;
          animation-play-state: paused;
          animation-timing-function: linear;
          transform: translate(${11520 + i * 120}px, -100px);
        }
      `);
    }
  }
}, () => {
  video.timeline = [];
  video.timeline[0] = () => {
    document.querySelectorAll('#scene, .g_georges, .g_florent, .g_corentin, .g_raphael, .g_maxime, .g_theo, .g_gugusse, .papier, .chaine_hifi, .affiche_festival, .unecreationdedesmu, .florentlabananemagicienne, .corentinlatruite, .raphaellepanda, .maximeleserpentferarepasser, .theogandalflecitron, .unecasseroledeauchaude, .georgeslasaucisse, .georgeslasaucisseorganiseunfestival, .georges, .florent, .corentin, .raphael, .maxime, .theo, .corentin .bouche_ouverte, .jambe_gauche, .jambe_droite, .organisationdufestival').forEach((element) => {
      element.style.animation = 'none';
    });
    /* 0 : Initialisation de tout. */
    video.animations = [];
    document.querySelectorAll('#scene').forEach((element) => {
      element.style.transform = 'scale(1)';
      element.style.transformOrigin = '960px 540px';
    });
    document.querySelectorAll('.georges, .florent, .corentin, .raphael, .maxime, .theo, .casserole, .bouche_deux, .gugusse, .philippe, .soleil, .chaine_hifi, .telephone, .papier, .papier_alu, .papier_pile, .affiche, .affiche_festival, .affiche_mangeurs, .affiche_motdepasse, .affiche_citron, .affiche_langues, .affiche_administratif, .affiche_vacances, .affiche_raphael, .affiche_fausse, .affiche_subventions, .affiche_ecolo, .affiche_youpiprojet, .ordinateur_bureau, .bureau_subventions, .bibliotheque_tres_mini, .ecran, .ecran_youpiprojet, .ecran_youpiprojet_code, .ecran_youpiprojet_demandes, .ecran_youpiprojet_demandes_2, .ecran_youpiprojet_inscription, .ecran_youpiprojet_inscription_choix, .ecran_youpiprojet_organisation, .ecran_youpiprojet_projet, .ecran_youpiprojet_raphael, .ecran_youpiprojet_reunion, .ecran_youpiprojet_saucisseconnect, .ecran_youpiprojet_subventions, .ecran_youpiprojet_valide, .flocon, .florentlabananemagicienne, .corentinlatruite, .raphaellepanda, .maximeleserpentferarepasser, .theogandalflecitron, .unecasseroledeauchaude, .georgeslasaucisse, .georgeslasaucisseorganiseunfestival, .georgeslasaucissepointfr, .georgeslasaucissearobasemastodonpointdesmupointfr, .g_paysage_mur_hall, .g_paysage_sol, .g_porte_interieur, .g_couloir_escaliers, .g_trappe, .g_echelle, .g_porte_exterieur, .g_plage, .g_sol, .g_sol2, .g_sol3, .g_mur, .g_mur2, .organisationdufestival, .g_cetaitgeorgeslasaucisseorganiseunfestival, .g_unevideorealiseepardesmu, .g_montageenjavascriptetensvgsoussvgeorges, .g_retrouvezgeorgessur, .g_mercipourcevisionnagedesolepourvotretemps, .g_voila').forEach((element) => {
      Video.toggleVisibility(element, 'none');
    });

    document.querySelector('.g_georges').style.transform = 'translate(100px, 100px)';
    document.querySelector('.g_florent').style.transform = 'translate(-460px, 0px)';
    document.querySelector('.g_corentin').style.transform = 'translate(2050px, 0px)';
    document.querySelector('.g_raphael').style.transform = 'translate(220px, -600px)';
    document.querySelector('.g_maxime').style.transform = 'translate(-420px, 420px)';
    document.querySelector('.g_theo').style.transform = 'translate(720px, -1080px)';
    document.querySelector('.g_casserole').style.transform = 'translate(300px, 300px)';

    document.querySelectorAll('.g_florentlabananemagicienne').forEach((element) => {
      element.style.transform = 'translate(350px, 1000px)';
    });
    document.querySelectorAll('.g_corentinlatruite').forEach((element) => {
      element.style.transform = 'translate(565px, 1000px)';
    });
    document.querySelectorAll('.g_raphaellepanda').forEach((element) => {
      element.style.transform = 'translate(610px, 1000px)';
    });
    document.querySelectorAll('.g_maximeleserpentferarepasser').forEach((element) => {
      element.style.transform = 'translate(265px, 1000px)';
    });
    document.querySelectorAll('.g_theogandalflecitron').forEach((element) => {
      element.style.transform = 'translate(480px, 1000px)';
    });
    document.querySelectorAll('.g_unecasseroledeauchaude').forEach((element) => {
      element.style.transform = 'translate(395px, 1000px)';
    });
    document.querySelectorAll('.g_georgeslasaucisse').forEach((element) => {
      element.style.transform = 'scale(2.5) translate(250px, 100px)';
    });
    document.querySelectorAll('.florentlabananemagicienne, .corentinlatruite, .raphaellepanda, .maximeleserpentferarepasser, .theogandalflecitron, .unecasseroledeauchaude, .georgeslasaucisseorganiseunfestival, .organisationdufestival').forEach((element) => {
      element.style.transform = 'rotateX(90deg)';
    });
    document.querySelectorAll('.florentlabananemagicienne').forEach((element) => {
      element.style.transformOrigin = '610px 37px';
    });
    document.querySelectorAll('.corentinlatruite').forEach((element) => {
      element.style.transformOrigin = '395px 37px';
    });
    document.querySelectorAll('.raphaellepanda').forEach((element) => {
      element.style.transformOrigin = '350px 37px';
    });
    document.querySelectorAll('.maximeleserpentferarepasser').forEach((element) => {
      element.style.transformOrigin = '695px 37px';
    });
    document.querySelectorAll('.theogandalflecitron').forEach((element) => {
      element.style.transformOrigin = '480px 37px';
    });
    document.querySelectorAll('.unecasseroledeauchaude').forEach((element) => {
      element.style.transformOrigin = '565px 37px';
    });
    document.querySelectorAll('.georgeslasaucisse').forEach((element) => {
      element.style.transformOrigin = '235px 115px';
    });
    document.querySelectorAll('.georges .jambe_gauche, .florent .jambe_gauche').forEach((element) => {
      element.style.transformOrigin = '15px 60px';
    });
    document.querySelectorAll('.georges .jambe_droite, .florent .jambe_droite').forEach((element) => {
      element.style.transformOrigin = '30px 60px';
    });

    document.querySelectorAll('.g_organisationdufestival').forEach((element) => {
      element.style.transform = 'translate(150px, 350px)';
    });
    document.querySelectorAll('.organisationdufestival').forEach((element) => {
      element.style.transformOrigin = '800px 125px';
    });
    document.querySelectorAll('.organisationdufestival text').forEach((element) => {
      element.style.transform = 'rotateX(90deg)';
    });
    document.querySelectorAll('.g_georgeslasaucissepointfr').forEach((element) => {
      element.style.transform = 'scale(0.5) translate(490px, 1465px)';
    });
    document.querySelectorAll('.g_georgeslasaucissearobasemastodonpointdesmupointfr').forEach((element) => {
      element.style.transform = 'scale(0.25) translate(985px, 2970px)';
    });
    document.querySelectorAll('.georgeslasaucissepointfr text, .georgeslasaucissearobasemastodonpointdesmupointfr text').forEach((element) => {
      element.setAttribute('font-weight', 900)
    });
    document.querySelectorAll('.g_cetaitgeorgeslasaucisseorganiseunfestival, .g_unevideorealiseepardesmu, .g_montageenjavascriptetensvgsoussvgeorges, .g_retrouvezgeorgessur, .g_mercipourcevisionnagedesolepourvotretemps, .g_voila').forEach((element) => {
      element.style.transform = 'translate(0px, 100px)';
    });

    document.querySelectorAll('.georges, .florent, .maxime, .theo, .casserole').forEach((element) => {
      element.style.transform = 'scale(9) translateY(0)';
    });
    document.querySelectorAll('.soleil, .chaine_hifi').forEach((element) => {
      element.style.transform = 'scale(3) translateY(0)';
    });
    document.querySelectorAll('.gugusse').forEach((element) => {
      element.style.transform = 'rotateY(0) scale(3) translateY(0)';
    });
    document.querySelectorAll('.corentin').forEach((element) => {
      element.style.transform = 'rotate(-90deg) scale(9) translateX(-15px)';
      element.style.transformOrigin = '18px 16px';
    });
    document.querySelectorAll('.raphael').forEach((element) => {
      element.style.transform = 'rotate(0deg) scale(7) translateY(0)';
      element.style.transformOrigin = '42px 60px';
    });
    document.querySelectorAll('.maxime').forEach((element) => {
      element.style.transformOrigin = '40px 50px';
    });
    document.querySelectorAll('.theo').forEach((element) => {
      element.style.transformOrigin = '0px 0px';
    });
    document.querySelectorAll('.corentin .bouche_ouverte').forEach((element) => {
      element.style.transform = 'rotateX(90deg)';
      element.style.transformOrigin = '15px 30px';
    });
    document.querySelectorAll('.chaine_hifi').forEach((element) => {
      element.style.transformOrigin = '105px 50px';
    });

    document.querySelector('.g_paysage_mur_hall').style.transform = 'scale(1.5)';
    document.querySelector('.g_paysage_sol').style.transform = 'scale(1.5) translate(0px, 620px)';
    document.querySelector('.g_porte_interieur').style.transform = 'scale(1.5) translate(0px, 520px)';
    document.querySelectorAll('.g_couloir_escaliers')[0].style.transform = 'scale(1.5) translate(300px, 520px)';
    document.querySelector('.g_trappe').style.transform = 'scale(1.5) translate(450px, 571px)';
    document.querySelectorAll('.g_couloir_escaliers')[1].style.transform = 'scale(1.5) translate(600px, 520px)';
    document.querySelector('.g_echelle').style.transform = 'scale(1.5) translate(750px, 0px)';
    document.querySelectorAll('.g_couloir_escaliers')[2].style.transform = 'scale(1.5) translate(900px, 520px)';
    document.querySelector('.g_porte_exterieur').style.transform = 'scale(1.5) translate(1205px, 520px)';
    
    document.querySelector('.g_plage').style.transform = 'translate(0px, 985px)';
    document.querySelectorAll('.g_gugusse').forEach((element, index) => {
      element.setAttribute('transform', `translate(${200 * index + 1200}, 750)`);
      element.querySelectorAll('.bras_gauche, .bras_droit').forEach((element) => {
        element.style.transformOrigin = '15px 30px';
      });
      element.querySelector('.bras_gauche').style.transform = 'rotate(-135deg)';
      element.querySelector('.bras_droit').style.transform = 'rotate(135deg)';
    });
    document.querySelector('.g_soleil').style.transform = 'translate(1450px, 50px)';
    document.querySelector('.g_chaine_hifi').style.transform = 'translate(300px, 800px)';
    document.querySelectorAll('.gugusse .jambe_gauche, .gugusse .jambe_droite').forEach((element) => {
      element.style.transform = 'rotate(0)';
      element.style.transformOrigin = '15px 60px';
    });
    document.querySelectorAll('.flocon').forEach((element, i) => {
      element.style.animationPlayState = 'paused';
      element.style.transform = `translate(${(i % 16) * 120}px, -100px)`;
    });
    document.querySelector('.g_affiche_festival').style.transform = 'translate(1250px, 0px)';
    document.querySelector('.affiche_festival').style.transform = 'rotateX(90deg) scale(1.5)';

    document.querySelectorAll('.g_sol, .g_sol2, .g_sol3').forEach((element) => {
      element.style.transform = 'translate(0px, 980px)';
    });
    document.querySelector('.g_ordinateur_bureau').style.transform = 'translate(1000px, 695px)';
    document.querySelector('.g_bureau_subventions').style.transform = 'translate(1000px, 785px)';
    document.querySelector('.g_telephone').style.transform = 'translate(1720px, 745px)';
    document.querySelector('.g_bibliotheque_tres_mini').style.transform = 'translate(1700px, 820px)';
    document.querySelector('.g_philippe').style.transform = 'translate(1350px, 725px)';
    document.querySelectorAll('.ordinateur_bureau, .bureau_subventions, .philippe').forEach((element) => {
      element.style.transform = 'scale(3)';
    });
    document.querySelectorAll('.bibliotheque_tres_mini').forEach((element) => {
      element.style.transform = 'scale(2)';
    });
    document.querySelectorAll('.g_affiche, .g_affiche_mangeurs, .g_affiche_motdepasse, .g_affiche_citron, .g_affiche_langues, .g_affiche_administratif, .g_affiche_vacances, .g_affiche_raphael, .g_affiche_fausse, .g_affiche_subventions, .g_affiche_ecolo, .g_affiche_youpiprojet').forEach((element) => {
      element.style.transform = 'translate(1500px, 200px)';
    });
    document.querySelectorAll('.affiche, .affiche_mangeurs, .affiche_motdepasse, .affiche_citron, .affiche_langues, .affiche_administratif, .affiche_vacances, .affiche_raphael, .affiche_fausse, .affiche_subventions, .affiche_ecolo, .affiche_youpiprojet').forEach((element) => {
      element.style.transform = 'scale(0.75)';
    });
    document.querySelectorAll('.g_papier, .g_papier_alu').forEach((element) => {
      element.style.transform = 'translate(295px, 810px)';
    });
    document.querySelectorAll('.papier, .papier_alu').forEach((element) => {
      element.style.transform = 'rotateZ(0) scale(2.5)';
      element.style.transformOrigin = '11px 16px';
    });
    document.querySelectorAll('.g_papier_pile').forEach((element) => {
      element.style.transform = 'translate(295px, 652px)';
    });
    document.querySelectorAll('.papier_pile').forEach((element) => {
      element.style.transform = 'rotateZ(0) scale(2.5)';
      element.style.transformOrigin = '11px 61px';
    });
    document.querySelector('.ordinateur_bureau .videoprojecteur circle').setAttribute('fill', '#fff');
    document.querySelectorAll('.g_ecran, .g_ecran_youpiprojet, .g_ecran_youpiprojet_code, .g_ecran_youpiprojet_demandes, .g_ecran_youpiprojet_demandes_2, .g_ecran_youpiprojet_inscription, .g_ecran_youpiprojet_inscription_choix, .g_ecran_youpiprojet_organisation, .g_ecran_youpiprojet_raphael, .g_ecran_youpiprojet_saucisseconnect, .g_ecran_youpiprojet_subventions, .g_ecran_youpiprojet_valide').forEach((element) => {
      element.style.transform = 'translate(500px, 300px)';
    });
    document.querySelectorAll('.g_ecran_youpiprojet_projet, .g_ecran_youpiprojet_reunion').forEach((element) => {
      element.style.transform = 'translate(1225px, 300px)';
    });
    document.querySelectorAll('.ecran_youpiprojet, .ecran_youpiprojet_code, .ecran_youpiprojet_demandes, .ecran_youpiprojet_demandes_2, .ecran_youpiprojet_inscription, .ecran_youpiprojet_inscription_choix, .ecran_youpiprojet_organisation, .ecran_youpiprojet_projet, .ecran_youpiprojet_reunion, .ecran_youpiprojet_saucisseconnect, .ecran_youpiprojet_subventions, .ecran_youpiprojet_valide').forEach((element) => {
      element.style.transform = 'rotateZ(10deg)';
    });
    document.querySelectorAll('.ecran_youpiprojet_raphael').forEach((element) => {
      element.style.transform = 'rotateZ(-10deg)';
    });
    document.querySelector('.ecran_youpiprojet_inscription .justificatif').style.transform = 'skew(-45deg, -5deg) translate(-10px, -10px)';
    document.querySelector('.ecran_youpiprojet_inscription .nom').value = '';
    document.querySelector('.ecran_youpiprojet_inscription .prenom').value = '';
    document.querySelector('.ecran_youpiprojet_inscription .email').value = '';
    document.querySelector('.ecran_youpiprojet_code .validation').value = '';

    document.querySelector('.ecran_youpiprojet .inscription').replaceWith(document.querySelector('.ecran_youpiprojet .inscription').cloneNode(true));
    document.querySelector('.ecran_youpiprojet_inscription_choix .inscription_saucisseconnect').replaceWith(document.querySelector('.ecran_youpiprojet_inscription_choix .inscription_saucisseconnect').cloneNode(true));
    document.querySelector('.ecran_youpiprojet_inscription_choix .inscription_locale').replaceWith(document.querySelector('.ecran_youpiprojet_inscription_choix .inscription_locale').cloneNode(true));
    document.querySelector('.ecran_youpiprojet_inscription_choix .inscription_rien').replaceWith(document.querySelector('.ecran_youpiprojet_inscription_choix .inscription_rien').cloneNode(true));
    document.querySelector('.ecran_youpiprojet .inscription').addEventListener('click', () => {
      document.querySelector('.ecran_youpiprojet .inscription').innerText = 'Ça sert à rien de cliquer sur les boutons dans une vidéo vous savez.';
    });
    document.querySelector('.ecran_youpiprojet_inscription_choix .inscription_saucisseconnect').addEventListener('click', () => {
      window.open('https://franceconnect.gouv.fr/', '_blank');
    });
    document.querySelector('.ecran_youpiprojet_inscription_choix .inscription_locale').addEventListener('click', () => {
      window.open('https://joinmastodon.org/servers', '_blank');
    });
    document.querySelector('.ecran_youpiprojet_inscription_choix .inscription_rien').addEventListener('click', () => {
      let color = '#';
      for (let i = 0; i < 6; i += 1) {
        color += '0123456789ABCDEF'[Math.floor(Math.random() * 16)];
      }
      document.querySelector('.ecran_youpiprojet_inscription_choix .inscription_rien').style.backgroundColor = color;
      const words = ['BAM', 'BIM', 'BOUERK', 'BOUM', 'CHOUCHOU', 'CLÉ À MOLETTE', 'CRAC', 'CROCROQUE', 'CROUIC', 'FSCHOUIT', 'GROPIF', 'LINOU', 'PAF', 'PIF', 'POUF', 'POUSSIN', 'PWOINPWOINPWOINPWOINPWOIN', 'SBAM', 'SBIM', 'SBOUM', 'THWOMP', 'ZIZOUINZOUIN'];
      document.querySelector('.ecran_youpiprojet_inscription_choix .inscription_rien').innerText = words[Math.floor(Math.random() * words.length)];
    });
    document.querySelector('.ecran_youpiprojet_saucisseconnect .validation').addEventListener('click', () => {
      document.querySelector('.ecran_youpiprojet_saucisseconnect .validation').innerText = 'Pas touche, y\'a que Philippe le Contrôleur qui peut valider les comptes.';
    });
    document.querySelector('.ecran_youpiprojet_subventions #check_8').addEventListener('change', () => {
      document.querySelector('.ecran_youpiprojet_subventions [for="check_1"]').innerText = 'Tout est chaos';
      document.querySelector('.ecran_youpiprojet_subventions [for="check_2"]').innerText = 'À côté';
      document.querySelector('.ecran_youpiprojet_subventions [for="check_3"]').innerText = 'Tous mes idéaux des mots';
      document.querySelector('.ecran_youpiprojet_subventions [for="check_4"]').innerText = 'Âbimés';
      document.querySelector('.ecran_youpiprojet_subventions [for="check_5"]').innerText = 'Je cherche une âme qui';
      document.querySelector('.ecran_youpiprojet_subventions [for="check_6"]').innerText = 'Pourra m\'aider';
      document.querySelector('.ecran_youpiprojet_subventions [for="check_7"]').innerText = 'Je suis d\'une génération';
      document.querySelector('.ecran_youpiprojet_subventions [for="check_8"]').innerText = 'Désenchantée';
    });
    document.querySelectorAll('.ecran_youpiprojet_subventions #check_1, .ecran_youpiprojet_subventions #check_2, .ecran_youpiprojet_subventions #check_3, .ecran_youpiprojet_subventions #check_4').forEach((element) => {
      element.removeAttribute('checked');
    });
    document.querySelectorAll('.ecran_youpiprojet_projet input, .ecran_youpiprojet_projet label').forEach((element) => {
      element.style.pointerEvents = 'none';
    });

    document.querySelectorAll('.decor').forEach((element) => {
      element.querySelector('rect:first-child').setAttribute('fill', '#ffffff');
    });
    document.querySelectorAll('.unecreationdedesmu').forEach((element) => {
      element.style.transform = 'translate(668px, 380px)';
      Video.toggleVisibility(element, 'block');
    });
    video.executeAnimation('.unecreationdedesmu', 'generique_texte_opacity 3.5s linear forwards', 0);

    Video.toggleVisibility(document.querySelector('#loading'), 'none');
  };
  video.timeline[3] = (skip = false) => {
    /* 3 : Lancement du générique, Florent arrive. */
    const delay = 625;
    document.querySelectorAll('.unecreationdedesmu').forEach((element) => {
      Video.toggleVisibility(element, 'none', (skip ? 0 : delay));
    });
    document.querySelectorAll('.florent, .florentlabananemagicienne').forEach((element) => {
      Video.toggleVisibility(element, 'block', (skip ? 0 : delay));
    });
    video.executeAnimation('.g_florent', 'glisse_florent 1.75s linear forwards', 3, delay / 1000);
    video.executeAnimation('.florentlabananemagicienne', 'generique_texte 1.75s linear forwards', 3 + delay / 1000);
  };
  video.timeline[5] = (skip = false) => {
    /* 5 : Florent part, Corentin arrive. */
    const delay = 375;
    document.querySelectorAll('.florent, .florentlabananemagicienne').forEach((element) => {
      Video.toggleVisibility(element, 'none', (skip ? 0 : delay));
    });
    video.unexecuteAnimation('.g_florent', 'translate(1920px, 0px)', (skip ? 0 : delay));
    video.unexecuteAnimation('.florentlabananemagicienne', 'rotateX(90deg)', (skip ? 0 : delay));

    document.querySelectorAll('.corentin, .corentinlatruite').forEach((element) => {
      Video.toggleVisibility(element, 'block', (skip ? 0 : delay));
    });
    video.executeAnimation('.g_corentin', 'glisse_corentin 1.75s linear forwards', 5, delay / 1000);
    video.executeAnimation('.corentinlatruite', 'generique_texte 1.75s linear forwards', 5 + delay / 1000);
  };
  video.timeline[7] = (skip = false) => {
    /* 7 : Corentin part, Raphaël arrive. */
    const delay = 125;
    document.querySelectorAll('.corentin, .corentinlatruite').forEach((element) => {
      Video.toggleVisibility(element, 'none', (skip ? 0 : delay));
    });
    video.unexecuteAnimation('.g_corentin', 'translate(-150px, 0px)', (skip ? 0 : delay));
    video.unexecuteAnimation('.corentinlatruite', 'rotateX(90deg)', (skip ? 0 : delay));

    document.querySelectorAll('.raphael, .raphael .bouche_deux, .raphaellepanda').forEach((element) => {
      Video.toggleVisibility(element, 'block', (skip ? 0 : delay));
    });
    video.executeAnimation('.g_raphael', 'pirouette_raphael_1 1.75s linear forwards', 7, delay / 1000);
    video.executeAnimation('.raphael', 'pirouette_raphael_2 1.75s linear forwards', 7 + delay / 1000);
    video.executeAnimation('.raphaellepanda', 'generique_texte 1.75s linear forwards', 7 + delay / 1000);
  };
  video.timeline[8] = (skip = false) => {
    /* 8 : Raphaël part, Maxime arrive. */
    const delay = 875;
    document.querySelectorAll('.raphael, .raphael .bouche_deux, .raphaellepanda').forEach((element) => {
      Video.toggleVisibility(element, 'none', (skip ? 0 : delay));
    });
    video.unexecuteAnimation('.g_raphael', 'translate(1570px, -600px)', (skip ? 0 : delay));
    video.unexecuteAnimation('.raphael', 'rotate(0deg) scale(7) translateY(0)', (skip ? 0 : delay));
    video.unexecuteAnimation('.raphaellepanda', 'rotateX(90deg)', (skip ? 0 : delay));

    document.querySelectorAll('.maxime, .maximeleserpentferarepasser').forEach((element) => {
      Video.toggleVisibility(element, 'block', (skip ? 0 : delay));
    });
    video.executeAnimation('.g_maxime', 'derape_maxime_1 1.75s linear forwards', 8, delay / 1000);
    video.executeAnimation('.maxime', 'derape_maxime_2 1.75s linear forwards', 8 + delay / 1000);
    video.executeAnimation('.maximeleserpentferarepasser', 'generique_texte 1.75s linear forwards', 8 + delay / 1000);
  };
  video.timeline[10] = (skip = false) => {
    /* 10 : Maxime part, Théo-Gandalf arrive. */
    const delay = 625;
    document.querySelectorAll('.maxime, .maximeleserpentferarepasser').forEach((element) => {
      Video.toggleVisibility(element, 'none', (skip ? 0 : delay));
    });
    video.unexecuteAnimation('.g_maxime', 'translate(2250px, 420px)', (skip ? 0 : delay));
    video.unexecuteAnimation('.maxime', 'rotateY(0deg) scale(9) translateY(0)', (skip ? 0 : delay));
    video.unexecuteAnimation('.maximeleserpentferarepasser', 'rotateX(90deg)', (skip ? 0 : delay));

    document.querySelectorAll('.theo, .theogandalflecitron').forEach((element) => {
      Video.toggleVisibility(element, 'block', (skip ? 0 : delay));
    });
    video.executeAnimation('.g_theo', 'rebondit_theo 1.75s linear forwards', 8, delay / 1000);
    video.executeAnimation('.theogandalflecitron', 'generique_texte 1.75s linear forwards', 10 + delay / 1000);
  };
  video.timeline[12] = (skip = false) => {
    /* 12 : Théo-Gandalf part, la casserole arrive. */
    const delay = 375;
    document.querySelectorAll('.theo, .theogandalflecitron').forEach((element) => {
      Video.toggleVisibility(element, 'none', (skip ? 0 : delay));
    });
    video.unexecuteAnimation('.g_theo', 'translate(720px, -1080px)', (skip ? 0 : delay));
    video.unexecuteAnimation('.theogandalflecitron', 'rotateX(90deg)', (skip ? 0 : delay));

    document.querySelectorAll('.casserole, .unecasseroledeauchaude').forEach((element) => {
      Video.toggleVisibility(element, 'block', (skip ? 0 : delay));
    });
    video.executeAnimation('.unecasseroledeauchaude', 'generique_texte 1.75s linear forwards', 12 + delay / 1000);
  };
  video.timeline[14] = (skip = false) => {
    /* 14 : La casserole part, Georges arrive. */
    const delay = 125;
    document.querySelectorAll('.casserole, .unecasseroledeauchaude').forEach((element) => {
      Video.toggleVisibility(element, 'none', (skip ? 0 : delay));
    });
    video.unexecuteAnimation('.unecasseroledeauchaude', 'rotateX(90deg)', (skip ? 0 : delay));

    document.querySelectorAll('.georges, .georgeslasaucisse').forEach((element) => {
      Video.toggleVisibility(element, 'block');
    });
    video.executeAnimation('.georges, .georgeslasaucisse', 'generique_texte_opacity 3.5s linear forwards', 14 + delay / 1000);
  };
  video.timeline[18] = () => {
    /* 18 : Georges est pleinement arrivé. */
    video.unexecuteAnimation('.georges', 'scale(9) translateY(0)');
    video.unexecuteAnimation('.georgeslasaucisse', 'rotateX(0deg)');
    document.querySelectorAll('.georges, .georgeslasaucisse').forEach((element) => {
      element.style.opacity = '100%';
    });
  };
  video.timeline[20] = () => {
    /* 20 : Georges part du générique et arrive chez lui. */
    document.querySelectorAll('.georgeslasaucisse').forEach((element) => {
      Video.toggleVisibility(element, 'none');
    });
    document.querySelectorAll('.g_paysage_mur_hall, .g_paysage_sol, .g_porte_interieur, .g_couloir_escaliers, .g_trappe, .g_echelle, .g_porte_exterieur').forEach((element) => {
      Video.toggleVisibility(element, 'block');
    });
    // video.executeAnimation('.g_georges', 'retrecit_georges_1 1s linear forwards', 20);
    // video.executeAnimation('.georges', 'retrecit_georges_2 1s linear forwards', 20);
    document.querySelector('.g_georges').style.transform = 'translate(150px, 792px)';
    document.querySelector('.georges').style.transform = 'scale(1.5) translateY(0)';
    video.unexecuteAnimation('.georges', 'scale(1.5) translateY(0)');
  };
  video.timeline[21] = () => {
    /* 21 : Georges a fini d'arriver chez lui. */
    // video.unexecuteAnimation('.g_georges', 'translate(150px, 792px)');
    // video.unexecuteAnimation('.georges', 'scale(1.5)');
  };
  video.timeline[25] = () => {
    /* 25 : Titre de la vidéo affiché. */
    document.querySelectorAll('.georgeslasaucisseorganiseunfestival').forEach((element) => {
      Video.toggleVisibility(element, 'block');
    });
    video.executeAnimation('.georgeslasaucisseorganiseunfestival', 'generique_texte 2s linear forwards', 25);
  };
  video.timeline[27] = () => {
    /* 27 : Titre de la vidéo masqué. */
    document.querySelectorAll('.georgeslasaucisseorganiseunfestival').forEach((element) => {
      Video.toggleVisibility(element, 'none');
    });
    video.unexecuteAnimation('.georgeslasaucisseorganiseunfestival', 'rotateX(90deg)');
  };
  video.timeline[32] = () => {
    /* 32 : Festival de musique sur la plage, des gens dansent n'importe comment. */
    document.querySelectorAll('.georges, .g_paysage_mur_hall, .g_paysage_sol, .g_porte_interieur, .g_couloir_escaliers, .g_trappe, .g_echelle, .g_porte_exterieur').forEach((element) => {
      Video.toggleVisibility(element, 'none');
    });
    document.querySelectorAll('.g_plage, .gugusse, .soleil, .chaine_hifi').forEach((element) => {
      Video.toggleVisibility(element, 'block');
    });
    document.querySelectorAll('.decor').forEach((element) => {
      element.querySelector('rect:first-child').setAttribute('fill', '#e0e0e0');
    });
    video.executeAnimation('.gugusse', `trottinage ${audio.beat}s ease-in infinite`, 32);
    video.executeAnimation('.gugusse .jambe_gauche', `trottinage_jambe_gauche ${audio.beat * 2}s linear infinite`, 32);
    video.executeAnimation('.gugusse .jambe_droite', `trottinage_jambe_droite ${audio.beat * 2}s linear infinite`, 32, audio.beat);
    video.executeAnimation('.chaine_hifi', `pulse_hifi ${audio.beat / 2}s ease-in alternate infinite`, 32);
  };
  video.timeline[38] = () => {
    /* 38 : Une plage avec personne dessus parce qu'il fait trop froid. */
    video.unexecuteAnimation('.gugusse .jambe_gauche', 'rotate(0)');
    video.unexecuteAnimation('.gugusse .jambe_droite', 'rotate(0)');
    video.unexecuteAnimation('.chaine_hifi', 'scale(3)');
    video.executeAnimation('.g_soleil', 'fuite_soleil 0.25s linear forwards', 38);
    video.executeAnimation('.gugusse', 'fuite_gugusse 0.25s linear forwards', 38);
    document.querySelectorAll('.decor').forEach((element) => {
      element.querySelector('rect:first-child').setAttribute('fill', '#808080');
    });
    document.querySelectorAll('.flocon').forEach((element) => {
      Video.toggleVisibility(element, 'block');
      element.style.animationPlayState = 'running';
    });
  };
  video.timeline[39] = () => {
    /* 39 : Tout le monde est parti, c'est horrible. */
    video.unexecuteAnimation('.g_soleil', 'translate(1920px, 50px)');
    video.unexecuteAnimation('.gugusse', 'rotateY(90deg) scale(3) translateY(0)');
  };
  video.timeline[45] = () => {
    /* 45 : Les gens sont revenus sur la plage et re-dansent n'importe comment. */
    video.executeAnimation('.g_soleil', 'fuite_soleil 0.25s linear reverse forwards', 45);
    video.executeAnimation('.gugusse', `trottinage ${audio.beat}s ease-in infinite`, 45, 0.25);
    video.executeAnimation('.gugusse .jambe_gauche', `trottinage_jambe_gauche ${audio.beat * 2}s linear infinite`, 45, 0.25);
    video.executeAnimation('.gugusse .jambe_droite', `trottinage_jambe_droite ${audio.beat * 2}s linear infinite`, 45, 0.25 + audio.beat);
    video.executeAnimation('.chaine_hifi', `pulse_hifi ${audio.beat / 2}s ease-in alternate infinite`, 45);
  };
  video.timeline[52] = () => {
    /* 52 : Plan de Georges chez lui. */
    document.querySelectorAll('.flocon').forEach((element) => {
      Video.toggleVisibility(element, 'none');
      element.style.animationPlayState = 'paused';
    });
    document.querySelectorAll('.decor').forEach((element) => {
      element.querySelector('rect:first-child').setAttribute('fill', '#ffffff');
    });
    video.unexecuteAnimation('.g_soleil', 'translate(1450px, 50px)');
    video.unexecuteAnimation('.gugusse', 'rotateY(0deg) scale(3) translateY(0)');
    video.unexecuteAnimation('.gugusse .jambe_gauche', 'rotate(0)');
    video.unexecuteAnimation('.gugusse .jambe_droite', 'rotate(0)');
    video.unexecuteAnimation('.chaine_hifi', 'scale(3)');
    document.querySelectorAll('.g_plage, .gugusse, .soleil, .chaine_hifi').forEach((element) => {
      Video.toggleVisibility(element, 'none');
    });
    document.querySelectorAll('.georges, .g_paysage_mur_hall, .g_paysage_sol, .g_porte_interieur, .g_couloir_escaliers, .g_trappe, .g_echelle, .g_porte_exterieur').forEach((element) => {
      Video.toggleVisibility(element, 'block');
    });
  };
  video.timeline[55] = () => {
    /* 55 : Présentation de l'affiche du festival. */
    Video.toggleVisibility(document.querySelector('.affiche_festival'), 'block');
    video.executeAnimation('.affiche_festival', 'generique_texte_moitie_scale 0.5s linear forwards', 55);
  };
  video.timeline[56] = () => {
    /* 56 : Affiche présentée. */
    video.unexecuteAnimation('.affiche_festival', 'rotateX(0) scale(1.5)');
  };
  video.timeline[58] = () => {
    /* 58 : Entrée en scène imminente de toute la troupe. */
    document.querySelectorAll('.florent, .maxime, .theo').forEach((element) => {
      element.style.transform = 'scale(3) translateY(0)';
    });
    document.querySelectorAll('.corentin').forEach((element) => {
      element.style.transform = 'rotate(0deg) scale(3) translateX(0)';
    });
    document.querySelectorAll('.raphael').forEach((element) => {
      element.style.transform = 'rotate(0deg) scale(3) translateY(0)';
    });
    document.querySelector('.g_florent').style.transform = 'translate(100px, 1080px)';
    document.querySelector('.g_corentin').style.transform = 'translate(500px, 1125px)';
    document.querySelector('.corentin').style.transform = 'rotate(90deg) scale(3)';
    document.querySelector('.g_raphael').style.transform = 'translate(900px, 1205px)';
    document.querySelector('.g_maxime').style.transform = 'translate(1300px, 1185px)';
    document.querySelector('.g_theo').style.transform = 'translate(1700px, 1080px)';
    document.querySelectorAll('.florent, .corentin, .raphael, .maxime, .theo, .bouche_ouverte, .bouche_deux').forEach((element) => {
      Video.toggleVisibility(element, 'block');
    });
    document.querySelectorAll('.corentin .bouche_ouverte').forEach((element) => {
      element.style.transform = 'rotateX(0deg)';
    });
  };
  video.timeline[59] = (skip = false) => {
    /* 59 : Tout le monde fait Ouaiiis à l'écran. */
    const delay = 500;
    video.executeAnimation('.g_florent', 'ouais_florent 1.5s linear forwards', 59, delay / 1000);
    video.executeAnimation('.g_corentin', 'ouais_corentin 1.5s linear forwards', 59, delay / 1000);
    video.executeAnimation('.g_raphael', 'ouais_raphael 1.5s linear forwards', 59, delay / 1000);
    video.executeAnimation('.g_maxime', 'ouais_maxime 1.5s linear forwards', 59, delay / 1000);
    video.executeAnimation('.g_theo', 'ouais_theo 1.5s linear forwards', 59, delay / 1000);
  };
  video.timeline[61] = () => {
    /* 61 : Plan de Georges chez lui. */
    video.executeAnimation('.affiche_festival', 'generique_texte_moitie_scale 0.5s linear reverse forwards', 61);
    video.unexecuteAnimation('.g_florent', 'translate(100px, 1080px)');
    video.unexecuteAnimation('.g_corentin', 'translate(30px, 850px)');
    video.unexecuteAnimation('.g_raphael', 'translate(900px, 1205px)');
    video.unexecuteAnimation('.g_maxime', 'translate(1300px, 1185px)');
    video.unexecuteAnimation('.g_theo', 'translate(1700px, 1080px)');
    document.querySelectorAll('.corentin .bouche_ouverte').forEach((element) => {
      element.style.transform = 'rotateX(90deg)';
    });
    document.querySelectorAll('.florent, .corentin, .raphael, .maxime, .theo, .bouche_ouverte, .bouche_deux').forEach((element) => {
      Video.toggleVisibility(element, 'none');
    });
  };
  video.timeline[62] = () => {
    /* 62 : L'affiche disparaît. */
    video.unexecuteAnimation('.affiche_festival', 'rotateX(90deg) scale(1.5)');
    Video.toggleVisibility(document.querySelector('.affiche_festival'), 'none');
  };
  video.timeline[67] = () => {
    /* 67 : Ouverture du cadre des objectifs. */
    document.querySelectorAll('.organisationdufestival').forEach((element) => {
      Video.toggleVisibility(element, 'block');
    });
    video.executeAnimation('.organisationdufestival', 'generique_texte_long 16s linear forwards', 67);
  };
  video.timeline[68] = () => {
    /* 68 : Premier point : déposer une demande de subventions. */
    video.executeAnimation('.deposerunedemandedesubventions', 'generique_texte_moitie 0.5s linear forwards', 68);
  };
  video.timeline[69] = () => {
    /* 69 : Texte affiché. */
    video.unexecuteAnimation('.deposerunedemandedesubventions', 'rotateX(0)');
  };
  video.timeline[72] = (skip = false) => {
    /* 72 : Deuxième point : achat et installation du matériel. */
    const delay = 500;
    video.executeAnimation('.acheteretinstallerlemateriel', 'generique_texte_moitie 0.5s linear forwards', 72, delay / 1000);
  };
  video.timeline[73] = () => {
    /* 73 : Texte affiché. */
    video.unexecuteAnimation('.acheteretinstallerlemateriel', 'rotateX(0)');
  };
  video.timeline[77] = () => {
    /* 77 : Troisième point : faire le festival. */
    video.executeAnimation('.danserethurlertresfort', 'generique_texte_moitie 0.5s linear forwards', 77);
  };
  video.timeline[78] = () => {
    /* 78 : Texte affiché. */
    video.unexecuteAnimation('.danserethurlertresfort', 'rotateX(0)');
  };
  video.timeline[83] = () => {
    /* 83 : Plan de Georges chez lui. */
    document.querySelectorAll('.organisationdufestival').forEach((element) => {
      Video.toggleVisibility(element, 'none');
    });
    video.unexecuteAnimation('.organisationdufestival', 'rotateX(90deg)');
  };
  video.timeline[89] = (skip = false) => {
    /* 89 : Transition. */
    let delay = 500;
    video.executeAnimation('#scene', 'transition 1s linear forwards', 89, delay / 1000);
    delay = 750;
    document.querySelectorAll('.georges, .g_paysage_mur_hall, .g_paysage_sol, .g_porte_interieur, .g_couloir_escaliers, .g_trappe, .g_echelle, .g_porte_exterieur').forEach((element) => {
      Video.toggleVisibility(element, 'none', (skip ? 0 : delay));
    });
    document.querySelectorAll('.georgeslasaucisse').forEach((element) => {
      Video.toggleVisibility(element, 'block', (skip ? 0 : delay));
    });
    Video.setTransform('.georgeslasaucisse', 'rotateY(180deg)', (skip ? 0 : delay));
    Video.setTransform('.g_georgeslasaucisse', 'scale(3) translate(80px, 70px)', (skip ? 0 : delay));
  };
  video.timeline[90] = (skip = false) => {
    /* 90 : Plan général sur le bureau des demandes de subventions. Georges a une pile de papiers avec lui. */
    let delay = 250;
    document.querySelectorAll('.georgeslasaucisse').forEach((element) => {
      Video.toggleVisibility(element, 'none', (skip ? 0 : delay));
    });
    Video.setTransform('.g_georges', 'translate(200px, 705px)', (skip ? 0 : delay));
    Video.setTransform('.georges', 'scale(3) translateY(0)', (skip ? 0 : delay));
    document.querySelectorAll('.g_sol2, .g_mur, .affiche_mangeurs, .ordinateur_bureau, .bibliotheque_tres_mini, .telephone, .ecran, .papier, .philippe, .georges').forEach((element) => {
      Video.toggleVisibility(element, 'block', (skip ? 0 : delay));
    });
    delay = 500;
    video.unexecuteAnimation('#scene', 'rotateY(0deg)', (skip ? 0 : delay));
  };
  video.timeline[98] = () => {
    /* 98 : Gros plan sur Georges. */
    Video.setTransform('#scene', 'scale(3) translate(600px, -300px)');
  };
  video.timeline[101] = () => {
    /* 101 : Gros plan sur Philippe. */
    Video.setTransform('#scene', 'scale(3) translate(-300px, -300px)');
  };
  video.timeline[104] = (skip = false) => {
    /* 104 : Plan général. */
    const delay = 500;
    Video.setTransform('#scene', 'scale(1)', (skip ? 0 : delay));
  };
  video.timeline[109] = (skip = false) => {
    /* 109 : Georges présente le dossier papier. */
    video.executeAnimation('.papier', 'papier_gigote 1s linear forwards', 109);
  };
  video.timeline[110] = (skip = false) => {
    /* 110 : C'est fini de gigoter ce dossier, oui ? */
    video.unexecuteAnimation('.papier', 'rotateZ(0) scale(2.5)');
  };
  video.timeline[111] = (skip = false) => {
    /* 111 : Gros plan sur Philippe. */
    const delay = 500;
    Video.setTransform('#scene', 'scale(3) translate(-300px, -300px)', (skip ? 0 : delay));
  };
  video.timeline[112] = (skip = false) => {
    /* 112 : Plan général. */
    const delay = 500;
    Video.setTransform('#scene', 'scale(1)', (skip ? 0 : delay));
  };
  video.timeline[117] = (skip = false) => {
    /* 117 : Gros plan sur Georges. */
    const delay = 250;
    Video.setTransform('#scene', 'scale(3) translate(600px, -300px)', (skip ? 0 : delay));
  };
  video.timeline[119] = () => {
    /* 119 : Plan général. */
    Video.setTransform('#scene', 'scale(1)');
  };
  video.timeline[123] = (skip = false) => {
    /* 123 : Gros plan sur Georges. */
    const delay = 500;
    Video.setTransform('#scene', 'scale(3) translate(600px, -300px)', (skip ? 0 : delay));
  };
  video.timeline[126] = (skip = false) => {
    /* 126 : Gros plan sur Philippe. */
    const delay = 500;
    Video.setTransform('#scene', 'scale(3) translate(-300px, -300px)', (skip ? 0 : delay));
  };
  video.timeline[128] = () => {
    /* 128 : Gros plan sur Georges. */
    Video.setTransform('#scene', 'scale(3) translate(600px, -300px)');
  };
  video.timeline[130] = (skip = false) => {
    /* 130 : Gros plan sur Philippe. */
    const delay = 500;
    Video.setTransform('#scene', 'scale(3) translate(-300px, -300px)', (skip ? 0 : delay));
  };
  video.timeline[132] = () => {
    /* 132 : Gros plan sur Georges. */
    Video.setTransform('#scene', 'scale(3) translate(600px, -300px)');
  };
  video.timeline[133] = (skip = false) => {
    /* 133 : Plan général. */
    const delay = 500;
    Video.setTransform('#scene', 'scale(1)', (skip ? 0 : delay));
  };
  video.timeline[138] = (skip = false) => {
    /* 138 : Très gros plan sur Georges. */
    const delay = 500;
    Video.setTransform('#scene', 'scale(5) translate(675px, -250px)', (skip ? 0 : delay));
  };
  video.timeline[140] = (skip = false) => {
    /* 140 : Plan général. */
    const delay = 250;
    Video.setTransform('#scene', 'scale(1)', (skip ? 0 : delay));
  };
  video.timeline[150] = (skip = false) => {
    /* 150 : Très gros plan sur Philippe. */
    const delay = 500;
    Video.setTransform('#scene', 'scale(5) translate(-450px, -250px)', (skip ? 0 : delay));
  };
  video.timeline[154] = () => {
    /* 154 : Plan général. */
    Video.setTransform('#scene', 'scale(1)');
  };
  video.timeline[161] = () => {
    /* 161 : Gros plan sur Georges. */
    Video.setTransform('#scene', 'scale(3) translate(600px, -300px)');
  };
  video.timeline[163] = () => {
    /* 163 : Plan général. */
    Video.setTransform('#scene', 'scale(1)');
  };
  video.timeline[168] = () => {
    /* 168 : Le vidéoprojecteur tousse. */
    video.executeAnimation('.ordinateur_bureau .videoprojecteur', 'videoprojecteur_tousse 2s linear forwards', 168);
  };
  video.timeline[170] = () => {
    /* 170 : Le site est en visuel. */
    video.unexecuteAnimation('.ordinateur_bureau .videoprojecteur', 'rotate(0) scale(1) translate(0)');
    document.querySelector('.ordinateur_bureau .videoprojecteur circle').setAttribute('fill', '#000');
    Video.toggleVisibility(document.querySelector('.ecran'), 'none');
    Video.toggleVisibility(document.querySelector('.ecran_youpiprojet'), 'block');
  };
  video.timeline[175] = (skip = false) => {
    /* 175 : Gros plan sur Georges. */
    const delay = 500;
    Video.setTransform('#scene', 'scale(3) translate(600px, -300px)', (skip ? 0 : delay));
  };
  video.timeline[176] = () => {
    /* 176 : Plan général. */
    Video.setTransform('#scene', 'scale(1)');
  };
  video.timeline[185] = () => {
    /* 185 : Gros plan sur Georges. */
    Video.setTransform('#scene', 'scale(3) translate(600px, -300px)');
  };
  video.timeline[186] = (skip = false) => {
    /* 186 : Gros plan sur Philippe. */
    const delay = 500;
    Video.setTransform('#scene', 'scale(3) translate(-300px, -300px)', (skip ? 0 : delay));
  };
  video.timeline[187] = (skip = false) => {
    /* 187 : Gros plan sur Georges. */
    const delay = 500;
    Video.setTransform('#scene', 'scale(3) translate(600px, -300px)', (skip ? 0 : delay));
  };
  video.timeline[189] = (skip = false) => {
    /* 189 : Gros plan sur Philippe. */
    const delay = 500;
    Video.setTransform('#scene', 'scale(3) translate(-300px, -300px)', (skip ? 0 : delay));
  };
  video.timeline[191] = () => {
    /* 191 : Gros plan sur Georges. */
    Video.setTransform('#scene', 'scale(3) translate(600px, -300px)');
  };
  video.timeline[193] = (skip = false) => {
    /* 193 : Plan général. Ouverture de la page de choix d'inscription. */
    const delay = 250;
    Video.toggleVisibility(document.querySelector('.ecran_youpiprojet'), 'none', (skip ? 0 : delay));
    Video.toggleVisibility(document.querySelector('.ecran_youpiprojet_inscription_choix'), 'block', (skip ? 0 : delay));
    Video.setTransform('#scene', 'scale(1)', (skip ? 0 : delay));
  };
  video.timeline[199] = () => {
    /* 199 : Gros plan sur Georges. */
    Video.setTransform('#scene', 'scale(3) translate(600px, -300px)');
  };
  video.timeline[200] = () => {
    /* 200 : Plan général. Ouverture du formulaire d'inscription. */
    Video.toggleVisibility(document.querySelector('.ecran_youpiprojet_inscription_choix'), 'none');
    Video.toggleVisibility(document.querySelector('.ecran_youpiprojet_inscription'), 'block');
    Video.setTransform('#scene', 'scale(1)');
  };
  video.timeline[203] = (skip = false) => {
    /* 203 : Gros plan sur Georges. */
    const delay = 250;
    Video.setTransform('#scene', 'scale(3) translate(600px, -300px)', (skip ? 0 : delay));
  };
  video.timeline[207] = () => {
    /* 207 : Gros plan sur Philippe. */
    document.querySelector('.ecran_youpiprojet_inscription .nom').value = 'La Saucisse';
    document.querySelector('.ecran_youpiprojet_inscription .prenom').value = 'Georges';
    Video.setTransform('#scene', 'scale(3) translate(-300px, -300px)');
  };
  video.timeline[209] = (skip = false) => {
    /* 209 : Gros plan sur Georges. Pub à l'écran. */
    const delay = 500;
    Video.toggleVisibility(document.querySelector('.georgeslasaucissepointfr'), 'block', (skip ? 0 : delay));
    Video.setTransform('#scene', 'scale(3) translate(600px, -300px)', (skip ? 0 : delay));
  };
  video.timeline[212] = () => {
    /* 212 : Gros plan sur Philippe. */
    Video.setTransform('#scene', 'scale(3) translate(-300px, -300px)');
    Video.toggleVisibility(document.querySelector('.georgeslasaucissepointfr'), 'none');
  };
  video.timeline[215] = (skip = false) => {
    /* 215 : Gros plan sur Georges. Pub à l'écran. */
    const delay = 250;
    Video.toggleVisibility(document.querySelector('.georgeslasaucissearobasemastodonpointdesmupointfr'), 'block', (skip ? 0 : delay));
    Video.setTransform('#scene', 'scale(3) translate(600px, -300px)', (skip ? 0 : delay));
  };
  video.timeline[219] = (skip = false) => {
    /* 219 : Gros plan sur Philippe. */
    const delay = 500;
    Video.setTransform('#scene', 'scale(3) translate(-300px, -300px)', (skip ? 0 : delay));
    Video.toggleVisibility(document.querySelector('.georgeslasaucissearobasemastodonpointdesmupointfr'), 'none', (skip ? 0 : delay));
  };
  video.timeline[223] = (skip = false) => {
    /* 223 : Gros plan sur Georges. */
    const delay = 250;
    document.querySelector('.ecran_youpiprojet_inscription .email').value = 'À-CHANGER-PLUS-TARD';
    Video.setTransform('#scene', 'scale(3) translate(600px, -300px)', (skip ? 0 : delay));
  };
  video.timeline[225] = (skip = false) => {
    /* 225 : Plan général. */
    const delay = 750;
    Video.setTransform('#scene', 'scale(1)', (skip ? 0 : delay));
  };
  video.timeline[236] = (skip = false) => {
    /* 236 : Gros plan sur Georges. */
    const delay = 500;
    Video.setTransform('#scene', 'scale(3) translate(600px, -300px)', (skip ? 0 : delay));
  };
  video.timeline[239] = () => {
    /* 239 : Gros plan sur Philippe. */
    Video.setTransform('#scene', 'scale(3) translate(-300px, -300px)');
  };
  video.timeline[244] = (skip = false) => {
    /* 244 : Gros plan sur Georges, puis fin de l'inscription. */
    Video.setTransform('#scene', 'scale(3) translate(600px, -300px)');
    const delay = 500;
    Video.toggleVisibility(document.querySelector('.ecran_youpiprojet_inscription'), 'none');
    Video.toggleVisibility(document.querySelector('.ecran_youpiprojet_valide'), 'block');
    Video.setTransform('#scene', 'scale(1)', (skip ? 0 : delay));
  };
  video.timeline[250] = (skip = false) => {
    /* 250 : Très gros plan sur Georges, puis gros plan sur Philippe. */
    Video.setTransform('#scene', 'scale(5) translate(675px, -250px)');
    const delay = 500;
    Video.setTransform('#scene', 'scale(3) translate(-300px, -300px)', (skip ? 0 : delay));
  };
  video.timeline[256] = (skip = false) => {
    /* 256 : Gros plan sur Georges. */
    const delay = 500;
    Video.setTransform('#scene', 'scale(3) translate(600px, -300px)', (skip ? 0 : delay));
  };
  video.timeline[257] = (skip = false) => {
    /* 257 : Gros plan sur Philippe. */
    const delay = 500;
    Video.setTransform('#scene', 'scale(3) translate(-300px, -300px)', (skip ? 0 : delay));
  };
  video.timeline[262] = (skip = false) => {
    /* 262 : Très gros plan sur Georges. */
    const delay = 750;
    Video.setTransform('#scene', 'scale(5) translate(675px, -250px)', (skip ? 0 : delay));
  };
  video.timeline[263] = (skip = false) => {
    /* 263 : Gros plan sur Philippe. */
    const delay = 500;
    Video.setTransform('#scene', 'scale(3) translate(-300px, -300px)', (skip ? 0 : delay));
  };
  video.timeline[269] = (skip = false) => {
    /* 269 : Plan général. */
    const delay = 500;
    Video.setTransform('#scene', 'scale(1)', (skip ? 0 : delay));
  };
  video.timeline[271] = (skip = false) => {
    /* 271 : Gros plan sur Philippe. */
    const delay = 500;
    Video.setTransform('#scene', 'scale(3) translate(-300px, -300px)', (skip ? 0 : delay));
  };
  video.timeline[272] = (skip = false) => {
    /* 272 : Transition. */
    const delay = 750;
    Video.setTransform('#scene', 'scale(1)', (skip ? 0 : delay));
    video.executeAnimation('#scene', 'transition 1s linear forwards', 272, delay / 1000);
  };
  video.timeline[273] = (skip = false) => {
    /* 273 : Gros plan sur Georges avec un papier. */
    document.querySelectorAll('.g_sol2, .g_mur, .affiche_mangeurs, .ordinateur_bureau, .bibliotheque_tres_mini, .telephone, .ecran_youpiprojet_valide, .papier, .philippe, .georges').forEach((element) => {
      Video.toggleVisibility(element, 'none');
    });
    document.querySelectorAll('.georgeslasaucisse').forEach((element) => {
      Video.toggleVisibility(element, 'block');
    });
    let delay = 500;
    document.querySelectorAll('.georgeslasaucisse').forEach((element) => {
      Video.toggleVisibility(element, 'none', (skip ? 0 : delay));
    });
    document.querySelectorAll('.g_sol2, .g_mur, .affiche_motdepasse, .ordinateur_bureau, .bibliotheque_tres_mini, .telephone, .ecran_youpiprojet_demandes, .papier, .philippe, .georges').forEach((element) => {
      Video.toggleVisibility(element, 'block', (skip ? 0 : delay));
    });
    delay = 750;
    video.unexecuteAnimation('#scene', 'rotateY(0deg)', (skip ? 0 : delay));
    Video.setTransform('#scene', 'scale(3) translate(600px, -300px)', (skip ? 0 : delay));
  };
  video.timeline[279] = (skip = false) => {
    /* 279 : Plan général. */
    const delay = 500;
    Video.setTransform('#scene', 'scale(1)', (skip ? 0 : delay));
  };
  video.timeline[284] = () => {
    /* 284 : Gros plan sur Philippe. */
    Video.setTransform('#scene', 'scale(3) translate(-300px, -300px)');
  };
  video.timeline[286] = (skip = false) => {
    /* 286 : Gros plan sur Georges. */
    const delay = 750;
    Video.setTransform('#scene', 'scale(3) translate(600px, -300px)', (skip ? 0 : delay));
  };
  video.timeline[289] = () => {
    /* 289 : Gros plan sur Philippe. */
    Video.setTransform('#scene', 'scale(3) translate(-300px, -300px)');
    document.querySelectorAll('.ecran_youpiprojet_demandes').forEach((element) => {
      Video.toggleVisibility(element, 'none');
    });
    document.querySelectorAll('.ecran_youpiprojet_code').forEach((element) => {
      Video.toggleVisibility(element, 'block');
    });
  };
  video.timeline[292] = (skip = false) => {
    /* 292 : Gros plan sur Georges, puis gros plan sur Philippe. */
    Video.setTransform('#scene', 'scale(3) translate(600px, -300px)');
    const delay = 750;
    document.querySelector('.ecran_youpiprojet_code .validation').value = 'C';
    Video.setTransform('#scene', 'scale(2) translate(0px, -175px)', (skip ? 0 : delay));
  };
  video.timeline[293] = (skip = false) => {
    /* 293 : Gros plan sur Georges. */
    const delay = 750;
    Video.setTransform('#scene', 'scale(3) translate(600px, -300px)', (skip ? 0 : delay));
  };
  video.timeline[294] = (skip = false) => {
    /* 294 : Gros plan sur Philippe. */
    const delay = 750;
    document.querySelector('.ecran_youpiprojet_code .validation').value += '3';
    Video.setTransform('#scene', 'scale(2) translate(0px, -175px)', (skip ? 0 : delay));
  };
  video.timeline[295] = (skip = false) => {
    /* 295 : Gros plan sur Georges. */
    const delay = 750;
    Video.setTransform('#scene', 'scale(3) translate(600px, -300px)', (skip ? 0 : delay));
  };
  video.timeline[296] = (skip = false) => {
    /* 296 : Gros plan sur Philippe. */
    const delay = 750;
    document.querySelector('.ecran_youpiprojet_code .validation').value += '5';
    Video.setTransform('#scene', 'scale(2) translate(0px, -175px)', (skip ? 0 : delay));
  };
  video.timeline[297] = (skip = false) => {
    /* 297 : Gros plan sur Georges. */
    const delay = 750;
    Video.setTransform('#scene', 'scale(3) translate(600px, -300px)', (skip ? 0 : delay));
  };
  video.timeline[298] = (skip = false) => {
    /* 298 : Gros plan sur Philippe. */
    const delay = 750;
    document.querySelector('.ecran_youpiprojet_code .validation').value += '7';
    Video.setTransform('#scene', 'scale(2) translate(0px, -175px)', (skip ? 0 : delay));
  };
  video.timeline[299] = (skip = false) => {
    /* 299 : Gros plan sur Georges. */
    const delay = 750;
    Video.setTransform('#scene', 'scale(3) translate(600px, -300px)', (skip ? 0 : delay));
  };
  video.timeline[300] = (skip = false) => {
    /* 300 : Gros plan sur Philippe. */
    const delay = 500;
    document.querySelector('.ecran_youpiprojet_code .validation').value += '1';
    Video.setTransform('#scene', 'scale(2) translate(0px, -175px)', (skip ? 0 : delay));
  };
  video.timeline[301] = (skip = false) => {
    /* 301 : Gros plan sur Georges. */
    const delay = 500;
    Video.setTransform('#scene', 'scale(3) translate(600px, -300px)', (skip ? 0 : delay));
  };
  video.timeline[302] = (skip = false) => {
    /* 302 : Gros plan sur Philippe. */
    const delay = 500;
    document.querySelector('.ecran_youpiprojet_code .validation').value += '3';
    Video.setTransform('#scene', 'scale(2) translate(0px, -175px)', (skip ? 0 : delay));
  };
  video.timeline[303] = (skip = false) => {
    /* 303 : Gros plan sur Georges. */
    const delay = 500;
    Video.setTransform('#scene', 'scale(3) translate(600px, -300px)', (skip ? 0 : delay));
  };
  video.timeline[304] = (skip = false) => {
    /* 304 : Gros plan sur Philippe. */
    const delay = 500;
    document.querySelector('.ecran_youpiprojet_code .validation').value += 'N';
    Video.setTransform('#scene', 'scale(2) translate(0px, -175px)', (skip ? 0 : delay));
  };
  video.timeline[305] = (skip = false) => {
    /* 305 : Gros plan sur Georges. */
    const delay = 250;
    Video.setTransform('#scene', 'scale(3) translate(600px, -300px)', (skip ? 0 : delay));
  };
  video.timeline[306] = (skip = false) => {
    /* 306 : Gros plan sur Philippe. */
    const delay = 250;
    document.querySelector('.ecran_youpiprojet_code .validation').value += '0';
    Video.setTransform('#scene', 'scale(2) translate(0px, -175px)', (skip ? 0 : delay));
  };
  video.timeline[307] = (skip = false) => {
    /* 307 : Gros plan sur Georges. */
    const delay = 250;
    Video.setTransform('#scene', 'scale(3) translate(600px, -300px)', (skip ? 0 : delay));
  };
  video.timeline[308] = (skip = false) => {
    /* 308 : Gros plan sur Philippe. */
    const delay = 250;
    document.querySelector('.ecran_youpiprojet_code .validation').value += 'M';
    Video.setTransform('#scene', 'scale(2) translate(0px, -175px)', (skip ? 0 : delay));
  };
  video.timeline[309] = () => {
    /* 309 : Gros plan sur Georges. */
    Video.setTransform('#scene', 'scale(3) translate(600px, -300px)');
  };
  video.timeline[310] = () => {
    /* 310 : Gros plan sur Philippe. */
    document.querySelector('.ecran_youpiprojet_code .validation').value += 'D';
    Video.setTransform('#scene', 'scale(2) translate(0px, -175px)');
  };
  video.timeline[311] = () => {
    /* 311 : Gros plan sur Georges. */
    Video.setTransform('#scene', 'scale(3) translate(600px, -300px)');
  };
  video.timeline[312] = () => {
    /* 312 : Gros plan sur Philippe. */
    document.querySelector('.ecran_youpiprojet_code .validation').value += 'E';
    Video.setTransform('#scene', 'scale(2) translate(0px, -175px)');
  };
  video.timeline[313] = (skip = false) => {
    /* 313 : Gros plan sur Georges, puis gros plan sur Philippe. */
    Video.setTransform('#scene', 'scale(3) translate(600px, -300px)');
    const delay = 750;
    document.querySelector('.ecran_youpiprojet_code .validation').value = document.querySelector('.ecran_youpiprojet_code .validation').value.slice(0, -1);
    document.querySelector('.ecran_youpiprojet_code .validation').value += 'U';
    Video.setTransform('#scene', 'scale(2) translate(0px, -175px)', (skip ? 0 : delay));
  };
  video.timeline[317] = (skip = false) => {
    /* 317 : Gros plan sur Georges. */
    const delay = 250;
    Video.setTransform('#scene', 'scale(3) translate(600px, -300px)', (skip ? 0 : delay));
  };
  video.timeline[318] = () => {
    /* 318 : Gros plan sur Philippe. */
    document.querySelector('.ecran_youpiprojet_code .validation').value += 'A';
    Video.setTransform('#scene', 'scale(2) translate(0px, -175px)');
  };
  video.timeline[319] = () => {
    /* 319 : Gros plan sur Georges. */
    Video.setTransform('#scene', 'scale(3) translate(600px, -300px)');
  };
  video.timeline[321] = (skip = false) => {
    /* 321 : Gros plan sur Philippe. */
    const delay = 500;
    document.querySelector('.ecran_youpiprojet_code .validation').value = document.querySelector('.ecran_youpiprojet_code .validation').value.slice(0, -1);
    Video.setTransform('#scene', 'scale(2) translate(0px, -175px)', (skip ? 0 : delay));
  };
  video.timeline[322] = (skip = false) => {
    /* 322 : Gros plan sur Georges. */
    const delay = 500;
    Video.setTransform('#scene', 'scale(3) translate(600px, -300px)', (skip ? 0 : delay));
  };
  video.timeline[323] = (skip = false) => {
    /* 323 : Gros plan sur Philippe. */
    const delay = 500;
    Video.setTransform('#scene', 'scale(2) translate(0px, -175px)', (skip ? 0 : delay));
  };
  video.timeline[325] = (skip = false) => {
    /* 325 : Gros plan sur Georges. */
    const delay = 250;
    Video.setTransform('#scene', 'scale(3) translate(600px, -300px)', (skip ? 0 : delay));
  };
  video.timeline[326] = () => {
    /* 326 : Gros plan sur Philippe. */
    document.querySelector('.ecran_youpiprojet_code .validation').value += 'P';
    Video.setTransform('#scene', 'scale(2) translate(0px, -175px)');
  };
  video.timeline[328] = () => {
    /* 328 : Gros plan sur Georges. */
    Video.setTransform('#scene', 'scale(3) translate(600px, -300px)');
  };
  video.timeline[329] = () => {
    /* 329 : Gros plan sur Philippe. */
    document.querySelector('.ecran_youpiprojet_code .validation').value += 'R';
    Video.setTransform('#scene', 'scale(2) translate(0px, -175px)');
  };
  video.timeline[330] = () => {
    /* 330 : Gros plan sur Georges. */
    Video.setTransform('#scene', 'scale(3) translate(600px, -300px)');
  };
  video.timeline[331] = () => {
    /* 331 : Gros plan sur Philippe. */
    document.querySelector('.ecran_youpiprojet_code .validation').value += '0';
    Video.setTransform('#scene', 'scale(2) translate(0px, -175px)');
  };
  video.timeline[332] = () => {
    /* 332 : Gros plan sur Georges. */
    Video.setTransform('#scene', 'scale(3) translate(600px, -300px)');
  };
  video.timeline[333] = () => {
    /* 333 : Gros plan sur Philippe. */
    document.querySelector('.ecran_youpiprojet_code .validation').value += 'J';
    Video.setTransform('#scene', 'scale(2) translate(0px, -175px)');
  };
  video.timeline[334] = (skip = false) => {
    /* 334 : Gros plan sur Georges, puis gros plan sur Philippe. */
    Video.setTransform('#scene', 'scale(3) translate(600px, -300px)');
    const delay = 750;
    document.querySelector('.ecran_youpiprojet_code .validation').value += '3';
    Video.setTransform('#scene', 'scale(2) translate(0px, -175px)', (skip ? 0 : delay));
  };
  video.timeline[335] = (skip = false) => {
    /* 335 : Gros plan sur Georges. */
    const delay = 750;
    Video.setTransform('#scene', 'scale(3) translate(600px, -300px)', (skip ? 0 : delay));
  };
  video.timeline[336] = (skip = false) => {
    /* 336 : Gros plan sur Philippe. */
    const delay = 750;
    document.querySelector('.ecran_youpiprojet_code .validation').value += '7';
    Video.setTransform('#scene', 'scale(2) translate(0px, -175px)', (skip ? 0 : delay));
  };
  video.timeline[337] = (skip = false) => {
    /* 337 : Gros plan sur Georges. */
    const delay = 750;
    Video.setTransform('#scene', 'scale(3) translate(600px, -300px)', (skip ? 0 : delay));
  };
  video.timeline[338] = (skip = false) => {
    /* 338 : Gros plan sur Philippe. */
    const delay = 750;
    document.querySelectorAll('.ecran_youpiprojet_code').forEach((element) => {
      Video.toggleVisibility(element, 'none');
    });
    document.querySelectorAll('.ecran_youpiprojet_valide').forEach((element) => {
      Video.toggleVisibility(element, 'block');
    });
    Video.setTransform('#scene', 'scale(2) translate(0px, -175px)', (skip ? 0 : delay));
  };
  video.timeline[341] = () => {
    /* 341 : Gros plan sur Georges. */
    Video.setTransform('#scene', 'scale(3) translate(600px, -300px)');
  };
  video.timeline[342] = (skip = false) => {
    /* 342 : Gros plan sur Philippe. */
    const delay = 250;
    Video.setTransform('#scene', 'scale(3) translate(-300px, -300px)', (skip ? 0 : delay));
  };
  video.timeline[344] = (skip = false) => {
    /* 344 : Très gros plan sur Georges. */
    const delay = 500;
    Video.setTransform('#scene', 'scale(5) translate(675px, -250px)', (skip ? 0 : delay));
  };
  video.timeline[345] = (skip = false) => {
    /* 345 : Plan général. */
    const delay = 250;
    Video.setTransform('#scene', 'scale(1)', (skip ? 0 : delay));
  };
  video.timeline[355] = (skip = false) => {
    /* 355 : Très gros plan sur Georges, puis plan général. */
    Video.setTransform('#scene', 'scale(5) translate(675px, -250px)');
    const delay = 500;
    Video.setTransform('#scene', 'scale(1)', (skip ? 0 : delay));
  };
  video.timeline[366] = (skip = false) => {
    /* 366 : Gros plan sur Georges. */
    const delay = 750;
    Video.setTransform('#scene', 'scale(3) translate(600px, -300px)', (skip ? 0 : delay));
  };
  video.timeline[368] = () => {
    /* 368 : Gros plan sur Philippe. */
    Video.setTransform('#scene', 'scale(3) translate(-300px, -300px)');
  };
  video.timeline[369] = (skip = false) => {
    /* 369 : Transition. */
    let delay = 500;
    Video.setTransform('#scene', 'scale(1)', (skip ? 0 : delay));
    video.executeAnimation('#scene', 'transition 1s linear forwards', 369, delay / 1000);
    delay = 750;
    document.querySelectorAll('.g_sol2, .g_mur, .affiche_motdepasse, .ordinateur_bureau, .bibliotheque_tres_mini, .telephone, .ecran_youpiprojet_valide, .papier, .philippe, .georges').forEach((element) => {
      Video.toggleVisibility(element, 'none', (skip ? 0 : delay));
    });
    document.querySelectorAll('.georgeslasaucisse').forEach((element) => {
      Video.toggleVisibility(element, 'block', (skip ? 0 : delay));
    });
  };
  video.timeline[370] = (skip = false) => {
    /* 370 : Plan général. */
    let delay = 250;
    document.querySelectorAll('.georgeslasaucisse').forEach((element) => {
      Video.toggleVisibility(element, 'none', (skip ? 0 : delay));
    });
    document.querySelectorAll('.g_sol2, .g_mur, .affiche_citron, .ordinateur_bureau, .bibliotheque_tres_mini, .telephone, .ecran_youpiprojet_demandes_2, .philippe, .georges').forEach((element) => {
      Video.toggleVisibility(element, 'block', (skip ? 0 : delay));
    });
    delay = 500;
    video.unexecuteAnimation('#scene', 'rotateY(0deg)', (skip ? 0 : delay));
  };
  video.timeline[376] = (skip = false) => {
    /* 376 : Gros plan sur Georges. */
    const delay = 500;
    Video.setTransform('#scene', 'scale(3) translate(600px, -300px)', (skip ? 0 : delay));
  };
  video.timeline[379] = () => {
    /* 379 : Gros plan sur Philippe. */
    Video.setTransform('#scene', 'scale(3) translate(-300px, -300px)');
  };
  video.timeline[380] = (skip = false) => {
    /* 380 : Très gros plan sur Georges. */
    const delay = 500;
    Video.setTransform('#scene', 'scale(5) translate(675px, -250px)', (skip ? 0 : delay));
  };
  video.timeline[381] = (skip = false) => {
    /* 381 : Plan général. */
    const delay = 250;
    document.querySelectorAll('.ecran_youpiprojet_demandes_2').forEach((element) => {
      Video.toggleVisibility(element, 'none');
    });
    document.querySelectorAll('.ecran_youpiprojet_organisation').forEach((element) => {
      Video.toggleVisibility(element, 'block');
    });
    Video.setTransform('#scene', 'scale(1)', (skip ? 0 : delay));
  };
  video.timeline[393] = (skip = false) => {
    /* 393 : Gros plan sur Georges. */
    const delay = 750;
    Video.setTransform('#scene', 'scale(3) translate(600px, -300px)', (skip ? 0 : delay));
  };
  video.timeline[397] = (skip = false) => {
    /* 397 : Gros plan sur Philippe. */
    const delay = 750;
    Video.setTransform('#scene', 'scale(3) translate(-300px, -300px)', (skip ? 0 : delay));
  };
  video.timeline[399] = (skip = false) => {
    /* 399 : Gros plan sur Georges. */
    const delay = 500;
    Video.setTransform('#scene', 'scale(3) translate(600px, -300px)', (skip ? 0 : delay));
  };
  video.timeline[402] = () => {
    /* 402 : Plan général. */
    Video.setTransform('#scene', 'scale(1)');
  };
  video.timeline[406] = (skip = false) => {
    /* 406 : Gros plan sur Georges. */
    const delay = 750;
    Video.setTransform('#scene', 'scale(3) translate(600px, -300px)', (skip ? 0 : delay));
  };
  video.timeline[408] = () => {
    /* 408 : Plan général. */
    Video.setTransform('#scene', 'scale(1)');
  };
  video.timeline[414] = (skip = false) => {
    /* 414 : Gros plan sur Philippe. */
    const delay = 250;
    Video.setTransform('#scene', 'scale(3) translate(-300px, -300px)', (skip ? 0 : delay));
  };
  video.timeline[416] = () => {
    /* 416 : Plan général. */
    Video.setTransform('#scene', 'scale(1)');
  };
  video.timeline[418] = (skip = false) => {
    /* 418 : Gros plan sur Philippe. */
    const delay = 250;
    Video.setTransform('#scene', 'scale(3) translate(-300px, -300px)', (skip ? 0 : delay));
  };
  video.timeline[419] = (skip = false) => {
    /* 419 : Transition. */
    let delay = 250;
    Video.setTransform('#scene', 'scale(1)', (skip ? 0 : delay));
    video.executeAnimation('#scene', 'transition 1s linear forwards', 419, delay / 1000);
    delay = 500;
    document.querySelectorAll('.g_sol2, .g_mur, .affiche_citron, .ordinateur_bureau, .bibliotheque_tres_mini, .telephone, .ecran_youpiprojet_organisation, .philippe, .georges').forEach((element) => {
      Video.toggleVisibility(element, 'none', (skip ? 0 : delay));
    });
    document.querySelectorAll('.georgeslasaucisse').forEach((element) => {
      Video.toggleVisibility(element, 'block', (skip ? 0 : delay));
    });
  };
  video.timeline[420] = (skip = false) => {
    /* 420 : Plan général, Corentin est arrivé. */
    document.querySelector('.g_corentin').style.transform = 'translate(30px, 850px)';
    document.querySelector('.corentin').style.transform = 'rotateY(180deg) scale(3) translateX(0)';
    document.querySelector('.corentin .bouche_ouverte').style.transform = 'rotateX(0deg)';
    document.querySelectorAll('.georgeslasaucisse').forEach((element) => {
      Video.toggleVisibility(element, 'none');
    });
    document.querySelectorAll('.g_sol2, .g_mur, .affiche_langues, .ordinateur_bureau, .bibliotheque_tres_mini, .telephone, .ecran_youpiprojet_inscription_choix, .philippe, .georges, .corentin').forEach((element) => {
      Video.toggleVisibility(element, 'block');
    });
    const delay = 250;
    video.unexecuteAnimation('#scene', 'rotateY(0deg)', (skip ? 0 : delay));
  };
  video.timeline[425] = (skip = false) => {
    /* 425 : Gros plan sur Philippe. */
    document.querySelectorAll('.corentin .bouche_ouverte').forEach((element) => {
      Video.toggleVisibility(element, 'block');
    });
    const delay = 750;
    Video.setTransform('#scene', 'scale(3) translate(-300px, -300px)', (skip ? 0 : delay));
    document.querySelectorAll('.corentin .bouche_ouverte').forEach((element) => {
      Video.toggleVisibility(element, 'none', (skip ? 0 : delay));
    });
  };
  video.timeline[427] = (skip = false) => {
    /* 427 : Gros plan sur Corentin. */
    const delay = 750;
    document.querySelectorAll('.corentin .bouche_ouverte').forEach((element) => {
      Video.toggleVisibility(element, 'block', (skip ? 0 : delay));
    });
    Video.setTransform('#scene', 'scale(3) translate(640px, -300px)', (skip ? 0 : delay));
  };
  video.timeline[428] = (skip = false) => {
    /* 428 : Gros plan sur Philippe. */
    const delay = 500;
    document.querySelectorAll('.corentin .bouche_ouverte').forEach((element) => {
      Video.toggleVisibility(element, 'none', (skip ? 0 : delay));
    });
    Video.setTransform('#scene', 'scale(3) translate(-300px, -300px)', (skip ? 0 : delay));
  };
  video.timeline[430] = (skip = false) => {
    /* 430 : Gros plan sur Georges. */
    const delay = 500;
    Video.setTransform('#scene', 'scale(3) translate(600px, -300px)', (skip ? 0 : delay));
  };
  video.timeline[432] = (skip = false) => {
    /* 432 : Gros plan sur Philippe. */
    const delay = 500;
    Video.setTransform('#scene', 'scale(3) translate(-300px, -300px)', (skip ? 0 : delay));
  };
  video.timeline[434] = (skip = false) => {
    /* 434 : Gros plan sur Corentin. */
    const delay = 500;
    document.querySelectorAll('.corentin .bouche_ouverte').forEach((element) => {
      Video.toggleVisibility(element, 'block', (skip ? 0 : delay));
    });
    Video.setTransform('#scene', 'scale(3) translate(640px, -300px)', (skip ? 0 : delay));
  };
  video.timeline[435] = (skip = false) => {
    /* 435 : Gros plan sur Philippe. */
    document.querySelectorAll('.corentin .bouche_ouverte').forEach((element) => {
      Video.toggleVisibility(element, 'none');
    });
    Video.setTransform('#scene', 'scale(3) translate(-300px, -300px)');
  };
  video.timeline[436] = (skip = false) => {
    /* 436 : Gros plan sur Georges. */
    const delay = 750;
    Video.setTransform('#scene', 'scale(3) translate(600px, -300px)', (skip ? 0 : delay));
  };
  video.timeline[439] = () => {
    /* 439 : Plan général, le projecteur affiche SaucisseConnect. */
    document.querySelectorAll('.ecran_youpiprojet_inscription_choix').forEach((element) => {
      Video.toggleVisibility(element, 'none');
    });
    document.querySelectorAll('.ecran_youpiprojet_saucisseconnect').forEach((element) => {
      Video.toggleVisibility(element, 'block');
    });
    video.unexecuteAnimation('#scene', 'rotateY(0deg)');
  };
  video.timeline[448] = (skip = false) => {
    /* 448 : Gros plan sur Georges. */
    const delay = 500;
    Video.setTransform('#scene', 'scale(3) translate(600px, -300px)', (skip ? 0 : delay));
  };
  video.timeline[452] = (skip = false) => {
    /* 452 : Gros plan sur Philippe. */
    const delay = 250;
    Video.setTransform('#scene', 'scale(3) translate(-300px, -300px)', (skip ? 0 : delay));
  };
  video.timeline[458] = (skip = false) => {
    /* 458 : Gros plan sur Corentin, puis gros plan sur Georges. */
    document.querySelectorAll('.corentin .bouche_ouverte').forEach((element) => {
      Video.toggleVisibility(element, 'block');
    });
    Video.setTransform('#scene', 'scale(3) translate(640px, -300px)');
    const delay = 750;
    document.querySelectorAll('.corentin .bouche_ouverte').forEach((element) => {
      Video.toggleVisibility(element, 'none', (skip ? 0 : delay));
    });
    Video.setTransform('#scene', 'scale(3) translate(600px, -300px)', (skip ? 0 : delay));
  };
  video.timeline[459] = (skip = false) => {
    /* 459 : Gros plan sur Philippe. */
    const delay = 750;
    Video.setTransform('#scene', 'scale(3) translate(-300px, -300px)', (skip ? 0 : delay));
  };
  video.timeline[464] = (skip = false) => {
    /* 464 : Très gros plan sur Georges. */
    const delay = 750;
    Video.setTransform('#scene', 'scale(5) translate(675px, -250px)', (skip ? 0 : delay));
  };
  video.timeline[465] = (skip = false) => {
    /* 465 : Gros plan sur Philippe. */
    const delay = 500;
    Video.setTransform('#scene', 'scale(3) translate(-300px, -300px)', (skip ? 0 : delay));
  };
  video.timeline[466] = (skip = false) => {
    /* 466 : Transition. */
    let delay = 500;
    Video.setTransform('#scene', 'scale(1)', (skip ? 0 : delay));
    video.executeAnimation('#scene', 'transition 1s linear forwards', 466, delay / 1000);
    delay = 750;
    document.querySelectorAll('.g_sol2, .g_mur, .affiche_langues, .ordinateur_bureau, .bibliotheque_tres_mini, .telephone, .ecran_youpiprojet_saucisseconnect, .philippe, .georges, .corentin').forEach((element) => {
      Video.toggleVisibility(element, 'none', (skip ? 0 : delay));
    });
    document.querySelectorAll('.georgeslasaucisse').forEach((element) => {
      Video.toggleVisibility(element, 'block', (skip ? 0 : delay));
    });
  };
  video.timeline[467] = (skip = false) => {
    /* 467 : Gros plan sur Georges. */
    document.querySelector('.ordinateur_bureau .videoprojecteur circle').setAttribute('fill', '#fff');
    let delay = 250;
    document.querySelectorAll('.georgeslasaucisse').forEach((element) => {
      Video.toggleVisibility(element, 'none', (skip ? 0 : delay));
    });
    document.querySelectorAll('.g_sol2, .g_mur, .affiche, .ordinateur_bureau, .bibliotheque_tres_mini, .telephone, .ecran, .georges, .corentin').forEach((element) => {
      Video.toggleVisibility(element, 'block', (skip ? 0 : delay));
    });
    delay = 500;
    video.unexecuteAnimation('#scene', 'rotateY(0deg)', (skip ? 0 : delay));
    Video.setTransform('#scene', 'scale(3) translate(600px, -300px)', (skip ? 0 : delay));
  };
  video.timeline[470] = () => {
    /* 470 : Gros plan sur Philppe qui a été remplacé par un répondeur. */
    Video.setTransform('#scene', 'scale(5) translate(-700px, -300px)');
  };
  video.timeline[474] = (skip = false) => {
    /* 474 : Gros plan sur Georges. */
    const delay = 500;
    Video.setTransform('#scene', 'scale(3) translate(600px, -300px)', (skip ? 0 : delay));
  };
  video.timeline[475] = (skip = false) => {
    /* 475 : Plan général. */
    const delay = 500;
    Video.setTransform('#scene', 'scale(1)', (skip ? 0 : delay));
  };
  video.timeline[483] = (skip = false) => {
    /* 483 : Gros plan sur Georges. */
    const delay = 500;
    Video.setTransform('#scene', 'scale(3) translate(600px, -300px)', (skip ? 0 : delay));
  };
  video.timeline[485] = (skip = false) => {
    /* 485 : Gros plan sur le répondeur. */
    const delay = 250;
    Video.setTransform('#scene', 'scale(5) translate(-700px, -300px)', (skip ? 0 : delay));
  };
  video.timeline[486] = (skip = false) => {
    /* 486 : Transition. */
    let delay = 500;
    Video.setTransform('#scene', 'scale(1)', (skip ? 0 : delay));
    video.executeAnimation('#scene', 'transition 1s linear forwards', 486, delay / 1000);
    delay = 750;
    document.querySelectorAll('.g_sol2, .g_mur, .affiche, .ordinateur_bureau, .bibliotheque_tres_mini, .telephone, .ecran, .georges, .corentin').forEach((element) => {
      Video.toggleVisibility(element, 'none', (skip ? 0 : delay));
    });
    document.querySelectorAll('.georgeslasaucisse').forEach((element) => {
      Video.toggleVisibility(element, 'block', (skip ? 0 : delay));
    });
  };
  video.timeline[487] = (skip = false) => {
    /* 487 : Plan général. */
    document.querySelector('.ordinateur_bureau .videoprojecteur circle').setAttribute('fill', '#000');
    let delay = 250;
    document.querySelectorAll('.georgeslasaucisse').forEach((element) => {
      Video.toggleVisibility(element, 'none', (skip ? 0 : delay));
    });
    document.querySelectorAll('.g_sol2, .g_mur, .affiche_administratif, .ordinateur_bureau, .bibliotheque_tres_mini, .telephone, .ecran_youpiprojet, .philippe, .georges, .corentin').forEach((element) => {
      Video.toggleVisibility(element, 'block', (skip ? 0 : delay));
    });
    delay = 500;
    video.unexecuteAnimation('#scene', 'rotateY(0deg)', (skip ? 0 : delay));
  };
  video.timeline[491] = (skip = false) => {
    /* 491 : Gros plan sur Georges. */
    const delay = 500;
    Video.setTransform('#scene', 'scale(3) translate(600px, -300px)', (skip ? 0 : delay));
  };
  video.timeline[493] = (skip = false) => {
    /* 493 : Gros plan sur Corentin. */
    const delay = 750;
    document.querySelectorAll('.corentin .bouche_ouverte').forEach((element) => {
      Video.toggleVisibility(element, 'block', (skip ? 0 : delay));
    });
    Video.setTransform('#scene', 'scale(3) translate(640px, -300px)', (skip ? 0 : delay));
  };
  video.timeline[494] = (skip = false) => {
    /* 494 : Plan général. */
    document.querySelectorAll('.florent, .maxime, .theo').forEach((element) => {
      element.style.transform = 'scale(3) translateY(0)';
    });
    document.querySelectorAll('.raphael').forEach((element) => {
      element.style.transform = 'rotate(0deg) scale(3) translateY(0)';
    });
    document.querySelector('.g_florent').style.transform = 'translate(100px, 1080px)';
    document.querySelector('.g_raphael').style.transform = 'translate(900px, 1205px)';
    document.querySelector('.g_maxime').style.transform = 'translate(1300px, 1185px)';
    document.querySelector('.g_theo').style.transform = 'translate(1700px, 1080px)';
    document.querySelectorAll('.florent, .raphael, .maxime, .theo, .bouche_deux').forEach((element) => {
      Video.toggleVisibility(element, 'block');
    });
    const delay = 500;
    document.querySelectorAll('.corentin .bouche_ouverte, .ecran_youpiprojet').forEach((element) => {
      Video.toggleVisibility(element, 'none', (skip ? 0 : delay));
    });
    document.querySelectorAll('.ecran_youpiprojet_valide').forEach((element) => {
      Video.toggleVisibility(element, 'block', (skip ? 0 : delay));
    });
    Video.setTransform('#scene', 'scale(1)', (skip ? 0 : delay));
  };
  video.timeline[498] = (skip = false) => {
    /* 498 : Tout le monde fait Ouaiiis à l'écran. */
    const delay = 500;
    document.querySelectorAll('.bouche_ouverte').forEach((element) => {
      Video.toggleVisibility(element, 'block', (skip ? 0 : delay));
    });
    video.executeAnimation('.g_florent', 'ouais_florent_2 1.5s linear forwards', 498, delay / 1000);
    video.executeAnimation('.g_raphael', 'ouais_raphael_2 1.5s linear forwards', 498, delay / 1000);
    video.executeAnimation('.g_maxime', 'ouais_maxime_2 1.5s linear forwards', 498, delay / 1000);
    video.executeAnimation('.g_theo', 'ouais_theo_2 1.5s linear forwards', 498, delay / 1000);
  };
  video.timeline[500] = () => {
    /* 500 : Plan général. */
    video.unexecuteAnimation('.g_florent', 'translate(100px, 1080px)');
    video.unexecuteAnimation('.g_raphael', 'translate(1420px, 750px)');
    video.unexecuteAnimation('.g_maxime', 'translate(1300px, 1185px)');
    video.unexecuteAnimation('.g_theo', 'translate(1700px, 1080px)');
    document.querySelector('.raphael').style.transform = 'rotate(0deg) scale(3) translateY(0)';
    document.querySelectorAll('.florent, .raphael, .maxime, .theo, .bouche_ouverte, .bouche_deux').forEach((element) => {
      Video.toggleVisibility(element, 'none');
    });
  };
  video.timeline[505] = (skip = false) => {
    /* 505 : Très gros plan sur Georges, puis plan général. */
    Video.setTransform('#scene', 'scale(5) translate(675px, -250px)');
    const delay = 500;
    Video.setTransform('#scene', 'scale(1)', (skip ? 0 : delay));
  };
  video.timeline[510] = (skip = false) => {
    /* 510 : Gros plan sur Georges. */
    const delay = 500;
    Video.setTransform('#scene', 'scale(3) translate(600px, -300px)', (skip ? 0 : delay));
  };
  video.timeline[512] = (skip = false) => {
    /* 512 : Gros plan sur Philippe. */
    const delay = 500;
    Video.setTransform('#scene', 'scale(3) translate(-300px, -300px)', (skip ? 0 : delay));
  };
  video.timeline[514] = (skip = false) => {
    /* 514 : Gros plan sur Georges. */
    const delay = 250;
    Video.setTransform('#scene', 'scale(3) translate(600px, -300px)', (skip ? 0 : delay));
  };
  video.timeline[517] = () => {
    /* 517 : Gros plan sur Philippe. */
    Video.setTransform('#scene', 'scale(3) translate(-300px, -300px)');
  };
  video.timeline[519] = (skip = false) => {
    /* 519 : Très gros plan sur Georges. */
    const delay = 500;
    Video.setTransform('#scene', 'scale(5) translate(675px, -250px)', (skip ? 0 : delay));
  };
  video.timeline[520] = () => {
    /* 520 : Très gros plan sur Philippe. */
    Video.setTransform('#scene', 'scale(5) translate(-450px, -250px)');
  };
  video.timeline[521] = (skip = false) => {
    /* 521 : Très gros plan sur Georges, puis plan général. */
    Video.setTransform('#scene', 'scale(5) translate(675px, -250px)');
    const delay = 750;
    Video.setTransform('#scene', 'scale(1)', (skip ? 0 : delay));
  };
  video.timeline[525] = (skip = false) => {
    /* 525 : Gros plan sur Georges. */
    const delay = 250;
    Video.setTransform('#scene', 'scale(3) translate(600px, -300px)', (skip ? 0 : delay));
  };
  video.timeline[528] = () => {
    /* 528 : Plan général. */
    Video.setTransform('#scene', 'scale(1)');
  };
  video.timeline[540] = (skip = false) => {
    /* 540 : Gros plan sur Georges. */
    const delay = 500;
    Video.setTransform('#scene', 'scale(3) translate(600px, -300px)', (skip ? 0 : delay));
  };
  video.timeline[541] = (skip = false) => {
    /* 541 : Gros plan sur Philippe. */
    const delay = 750;
    Video.setTransform('#scene', 'scale(3) translate(-300px, -300px)', (skip ? 0 : delay));
  };
  video.timeline[543] = (skip = false) => {
    /* 543 : Transition. */
    Video.setTransform('#scene', 'scale(1)');
    video.executeAnimation('#scene', 'transition 1s linear forwards', 543);
    let delay = 250;
    document.querySelectorAll('.g_sol2, .g_mur, .affiche_administratif, .ordinateur_bureau, .bibliotheque_tres_mini, .telephone, .ecran_youpiprojet_valide, .philippe, .georges, .corentin').forEach((element) => {
      Video.toggleVisibility(element, 'none', (skip ? 0 : delay));
    });
    document.querySelectorAll('.georgeslasaucisse').forEach((element) => {
      Video.toggleVisibility(element, 'block', (skip ? 0 : delay));
    });
    delay = 750;
    document.querySelectorAll('.georgeslasaucisse').forEach((element) => {
      Video.toggleVisibility(element, 'none', (skip ? 0 : delay));
    });
    document.querySelectorAll('.g_sol2, .g_mur, .affiche_vacances, .ordinateur_bureau, .bibliotheque_tres_mini, .telephone, .ecran_youpiprojet, .papier, .philippe, .georges').forEach((element) => {
      Video.toggleVisibility(element, 'block', (skip ? 0 : delay));
    });
  };
  video.timeline[544] = () => {
    /* 544 : Gros plan sur Georges, qui tient un papier. */
    video.unexecuteAnimation('#scene', 'rotateY(0deg)');
    Video.setTransform('#scene', 'scale(3) translate(600px, -300px)');
  };
  video.timeline[547] = (skip = false) => {
    /* 547 : Gros plan sur Philippe. */
    const delay = 750;
    Video.setTransform('#scene', 'scale(3) translate(-300px, -300px)', (skip ? 0 : delay));
  };
  video.timeline[552] = () => {
    /* 552 : Plan général. */
    Video.setTransform('#scene', 'scale(1)');
  };
  video.timeline[559] = () => {
    /* 559 : Gros plan sur Philippe. */
    Video.setTransform('#scene', 'scale(3) translate(-300px, -300px)');
  };
  video.timeline[561] = (skip = false) => {
    /* 561 : Plan général. */
    const delay = 500;
    Video.setTransform('#scene', 'scale(1)', (skip ? 0 : delay));
  };
  video.timeline[568] = () => {
    /* 568 : Gros plan sur Georges. */
    Video.setTransform('#scene', 'scale(3) translate(600px, -300px)');
  };
  video.timeline[569] = (skip = false) => {
    /* 569 : Gros plan sur Philippe. */
    const delay = 250;
    Video.setTransform('#scene', 'scale(3) translate(-300px, -300px)', (skip ? 0 : delay));
  };
  video.timeline[570] = (skip = false) => {
    /* 570 : Transition. */
    let delay = 500;
    Video.setTransform('#scene', 'scale(1)', (skip ? 0 : delay));
    video.executeAnimation('#scene', 'transition 1s linear forwards', 570, delay / 1000);
    delay = 750;
    document.querySelectorAll('.g_sol2, .g_mur, .affiche_vacances, .ordinateur_bureau, .bibliotheque_tres_mini, .telephone, .ecran_youpiprojet, .papier, .philippe, .georges').forEach((element) => {
      Video.toggleVisibility(element, 'none', (skip ? 0 : delay));
    });
    document.querySelectorAll('.georgeslasaucisse').forEach((element) => {
      Video.toggleVisibility(element, 'block', (skip ? 0 : delay));
    });
  };
  video.timeline[571] = (skip = false) => {
    /* 571 : Gros plan sur Georges. */
    let delay = 250;
    document.querySelectorAll('.georgeslasaucisse').forEach((element) => {
      Video.toggleVisibility(element, 'none', (skip ? 0 : delay));
    });
    document.querySelectorAll('.g_sol2, .g_mur, .affiche_raphael, .ordinateur_bureau, .bibliotheque_tres_mini, .telephone, .ecran_youpiprojet_raphael, .georges, .raphael').forEach((element) => {
      Video.toggleVisibility(element, 'block', (skip ? 0 : delay));
    });
    Video.setTransform('.g_raphael', 'translate(1420px, 750px)', (skip ? 0 : delay));
    Video.setTransform('.raphael', 'rotate(0deg) scale(3) translateY(0)', (skip ? 0 : delay));
    delay = 500;
    video.unexecuteAnimation('#scene', 'rotateY(0deg)', (skip ? 0 : delay));
    Video.setTransform('#scene', 'scale(3) translate(600px, -300px)', (skip ? 0 : delay));
  };
  video.timeline[572] = (skip = false) => {
    /* 572 : Gros plan sur Raphaël, qui a pris la place de Philippe. */
    const delay = 500;
    document.querySelectorAll('.bouche_deux').forEach((element) => {
      Video.toggleVisibility(element, 'block', (skip ? 0 : delay));
    });
    Video.setTransform('#scene', 'scale(3) translate(-330px, -265px)', (skip ? 0 : delay));
  };
  video.timeline[574] = () => {
    /* 574 : Plan général. */
    document.querySelectorAll('.bouche_deux').forEach((element) => {
      Video.toggleVisibility(element, 'none');
    });
    Video.setTransform('#scene', 'scale(1)');
  };
  video.timeline[576] = (skip = false) => {
    /* 576 : Raphaël parle. */
    const delay = 500;
    document.querySelectorAll('.bouche_deux').forEach((element) => {
      Video.toggleVisibility(element, 'block', (skip ? 0 : delay));
    });
  };
  video.timeline[580] = (skip = false) => {
    /* 580 : Gros plan sur Georges. */
    const delay = 500;
    document.querySelectorAll('.bouche_deux').forEach((element) => {
      Video.toggleVisibility(element, 'none', (skip ? 0 : delay));
    });
    Video.setTransform('#scene', 'scale(3) translate(600px, -300px)', (skip ? 0 : delay));
  };
  video.timeline[584] = (skip = false) => {
    /* 584 : Très gros plan sur Raphaël. */
    const delay = 500;
    document.querySelectorAll('.bouche_deux').forEach((element) => {
      Video.toggleVisibility(element, 'block', (skip ? 0 : delay));
    });
    Video.setTransform('#scene', 'scale(5) translate(-485px, -180px)', (skip ? 0 : delay));
  };
  video.timeline[586] = () => {
    /* 586 : Gros plan sur Georges. */
    document.querySelectorAll('.bouche_deux').forEach((element) => {
      Video.toggleVisibility(element, 'none');
    });
    Video.setTransform('#scene', 'scale(3) translate(600px, -300px)');
  };
  video.timeline[587] = (skip = false) => {
    /* 587 : Plan général. */
    const delay = 500;
    document.querySelectorAll('.bouche_deux').forEach((element) => {
      Video.toggleVisibility(element, 'block', (skip ? 0 : delay));
    });
    Video.setTransform('#scene', 'scale(1)', (skip ? 0 : delay));
  };
  video.timeline[593] = () => {
    /* 593 : Très gros plan sur Georges. */
    document.querySelectorAll('.bouche_deux').forEach((element) => {
      Video.toggleVisibility(element, 'none');
    });
    Video.setTransform('#scene', 'scale(5) translate(675px, -250px)');
  };
  video.timeline[594] = (skip = false) => {
    /* 594 : Gros plan sur Raphaël. */
    const delay = 750;
    document.querySelectorAll('.bouche_deux').forEach((element) => {
      Video.toggleVisibility(element, 'block', (skip ? 0 : delay));
    });
    Video.setTransform('#scene', 'scale(3) translate(-330px, -265px)', (skip ? 0 : delay));
  };
  video.timeline[601] = (skip = false) => {
    /* 601 : Gros plan sur Georges. */
    const delay = 500;
    document.querySelectorAll('.bouche_deux').forEach((element) => {
      Video.toggleVisibility(element, 'none', (skip ? 0 : delay));
    });
    Video.setTransform('#scene', 'scale(3) translate(600px, -300px)', (skip ? 0 : delay));
  };
  video.timeline[605] = () => {
    /* 605 : Très très gros plan sur Raphaël. */
    document.querySelectorAll('.bouche_deux').forEach((element) => {
      Video.toggleVisibility(element, 'block');
    });
    Video.setTransform('#scene', 'scale(7) translate(-500px, -150px)');
  };
  video.timeline[606] = (skip = false) => {
    /* 606 : Gros plan sur Georges. */
    const delay = 500;
    document.querySelectorAll('.bouche_deux').forEach((element) => {
      Video.toggleVisibility(element, 'none', (skip ? 0 : delay));
    });
    Video.setTransform('#scene', 'scale(3) translate(600px, -300px)', (skip ? 0 : delay));
  };
  video.timeline[607] = (skip = false) => {
    /* 607 : Transition. */
    let delay = 500;
    Video.setTransform('#scene', 'scale(1)', (skip ? 0 : delay));
    video.executeAnimation('#scene', 'transition 1s linear forwards', 607, delay / 1000);
    delay = 750;
    document.querySelectorAll('.g_sol2, .g_mur, .affiche_raphael, .ordinateur_bureau, .bibliotheque_tres_mini, .telephone, .ecran_youpiprojet_raphael, .georges, .raphael').forEach((element) => {
      Video.toggleVisibility(element, 'none', (skip ? 0 : delay));
    });
    document.querySelectorAll('.georgeslasaucisse').forEach((element) => {
      Video.toggleVisibility(element, 'block', (skip ? 0 : delay));
    });
  };
  video.timeline[608] = (skip = false) => {
    /* 608 : Gros plan sur Georges qui tient une feuille d'aluminium. */
    let delay = 250;
    document.querySelectorAll('.georgeslasaucisse').forEach((element) => {
      Video.toggleVisibility(element, 'none', (skip ? 0 : delay));
    });
    document.querySelectorAll('.g_sol2, .g_mur, .affiche_fausse, .ordinateur_bureau, .bibliotheque_tres_mini, .telephone, .ecran_youpiprojet, .papier_alu, .philippe, .georges').forEach((element) => {
      Video.toggleVisibility(element, 'block', (skip ? 0 : delay));
    });
    delay = 500;
    video.unexecuteAnimation('#scene', 'rotateY(0deg)', (skip ? 0 : delay));
    Video.setTransform('#scene', 'scale(3) translate(600px, -300px)', (skip ? 0 : delay));
  };
  video.timeline[614] = () => {
    /* 614 : Gros plan sur Philippe. */
    Video.setTransform('#scene', 'scale(3) translate(-300px, -300px)');
  };
  video.timeline[615] = (skip = false) => {
    /* 615 : Gros plan sur Georges. */
    const delay = 500;
    Video.setTransform('#scene', 'scale(3) translate(600px, -300px)', (skip ? 0 : delay));
  };
  video.timeline[618] = () => {
    /* 618 : Gros plan sur Philippe. */
    Video.setTransform('#scene', 'scale(3) translate(-300px, -300px)');
  };
  video.timeline[619] = (skip = false) => {
    /* 619 : Transition. */
    const delay = 750;
    Video.setTransform('#scene', 'scale(1)', (skip ? 0 : delay));
    video.executeAnimation('#scene', 'transition 1s linear forwards', 619, delay / 1000);
  };
  video.timeline[620] = (skip = false) => {
    /* 620 : Gros plan sur Georges. */
    document.querySelectorAll('.g_sol2, .g_mur, .affiche_fausse, .ordinateur_bureau, .bibliotheque_tres_mini, .telephone, .ecran_youpiprojet, .papier_alu, .philippe, .georges').forEach((element) => {
      Video.toggleVisibility(element, 'none');
    });
    document.querySelectorAll('.georgeslasaucisse').forEach((element) => {
      Video.toggleVisibility(element, 'block');
    });
    let delay = 500;
    document.querySelectorAll('.georgeslasaucisse').forEach((element) => {
      Video.toggleVisibility(element, 'none', (skip ? 0 : delay));
    });
    document.querySelectorAll('.g_sol2, .g_mur, .affiche_subventions, .ordinateur_bureau, .bibliotheque_tres_mini, .telephone, .ecran_youpiprojet_valide, .philippe, .georges').forEach((element) => {
      Video.toggleVisibility(element, 'block', (skip ? 0 : delay));
    });
    delay = 750;
    video.unexecuteAnimation('#scene', 'rotateY(0deg)', (skip ? 0 : delay));
    Video.setTransform('#scene', 'scale(3) translate(600px, -300px)', (skip ? 0 : delay));
  };
  video.timeline[624] = () => {
    /* 624 : Plan général. */
    Video.setTransform('#scene', 'scale(1)');
    document.querySelectorAll('.florent, .maxime, .theo').forEach((element) => {
      element.style.transform = 'scale(3) translateY(0)';
    });
    document.querySelectorAll('.corentin').forEach((element) => {
      element.style.transform = 'rotate(0deg) scale(3) translateX(0)';
    });
    document.querySelectorAll('.raphael').forEach((element) => {
      element.style.transform = 'rotate(0deg) scale(3) translateY(0)';
    });
    document.querySelector('.g_florent').style.transform = 'translate(100px, 1080px)';
    document.querySelector('.g_corentin').style.transform = 'translate(500px, 1125px)';
    document.querySelector('.corentin').style.transform = 'rotate(90deg) scale(3)';
    document.querySelector('.g_raphael').style.transform = 'translate(900px, 1205px)';
    document.querySelector('.g_maxime').style.transform = 'translate(1300px, 1185px)';
    document.querySelector('.g_theo').style.transform = 'translate(1700px, 1080px)';
    document.querySelectorAll('.florent, .corentin, .raphael, .maxime, .theo, .bouche_ouverte, .bouche_deux').forEach((element) => {
      Video.toggleVisibility(element, 'block');
    });
  };
  video.timeline[626] = (skip = false) => {
    /* 626 : Tout le monde fait Ouaiiis à l'écran. */
    const delay = 500;
    video.executeAnimation('.g_florent', 'ouais_florent_2 1.5s linear forwards', 626, delay / 1000);
    video.executeAnimation('.g_corentin', 'ouais_corentin_2 1.5s linear forwards', 626, delay / 1000);
    video.executeAnimation('.g_raphael', 'ouais_raphael_2 1.5s linear forwards', 626, delay / 1000);
    video.executeAnimation('.g_maxime', 'ouais_maxime_2 1.5s linear forwards', 626, delay / 1000);
    video.executeAnimation('.g_theo', 'ouais_theo_2 1.5s linear forwards', 626, delay / 1000);
  };
  video.timeline[628] = () => {
    /* 628 : Plan général. */
    video.unexecuteAnimation('.g_florent', 'translate(100px, 1140px)');
    video.unexecuteAnimation('.g_corentin', 'translate(500px, 1125px)');
    video.unexecuteAnimation('.g_raphael', 'translate(900px, 1265px)');
    video.unexecuteAnimation('.g_maxime', 'translate(1300px, 1245px)');
    video.unexecuteAnimation('.g_theo', 'translate(1700px, 1140px)');
    document.querySelectorAll('.florent, .corentin, .raphael, .maxime, .theo, .bouche_ouverte, .bouche_deux').forEach((element) => {
      Video.toggleVisibility(element, 'none');
    });
  };
  video.timeline[633] = (skip = false) => {
    /* 633 : Gros plan sur Philippe. */
    const delay = 500;
    Video.setTransform('#scene', 'scale(3) translate(-300px, -300px)', (skip ? 0 : delay));
  };
  video.timeline[636] = () => {
    /* 636 : Gros plan sur Georges. */
    Video.setTransform('#scene', 'scale(3) translate(600px, -300px)');
  };
  video.timeline[639] = (skip = false) => {
    /* 639 : Gros plan sur Philippe. */
    const delay = 500;
    Video.setTransform('#scene', 'scale(3) translate(-300px, -300px)', (skip ? 0 : delay));
  };
  video.timeline[645] = (skip = false) => {
    /* 645 : Plan général. */
    const delay = 750;
    Video.setTransform('#scene', 'scale(1)', (skip ? 0 : delay));
  };
  video.timeline[653] = (skip = false) => {
    /* 653 : Gros plan sur Georges. */
    Video.setTransform('#scene', 'scale(3) translate(600px, -300px)');
  };
  video.timeline[654] = (skip = false) => {
    /* 654 : Plan général, La liste des subventions s'affiche. */
    const delay = 500;
    Video.toggleVisibility(document.querySelector('.ecran_youpiprojet_valide'), 'none');
    Video.toggleVisibility(document.querySelector('.ecran_youpiprojet_subventions'), 'block');
    Video.setTransform('#scene', 'scale(1)', (skip ? 0 : delay));
  };
  video.timeline[665] = () => {
    /* 665 : Gros plan sur Georges. */
    Video.setTransform('#scene', 'scale(3) translate(600px, -300px)');
  };
  video.timeline[667] = () => {
    /* 667 : Plan général. */
    document.querySelectorAll('.ecran_youpiprojet_subventions #check_1, .ecran_youpiprojet_subventions #check_2, .ecran_youpiprojet_subventions #check_3, .ecran_youpiprojet_subventions #check_4').forEach((element) => {
      element.setAttribute('checked', true);
    });
    Video.setTransform('#scene', 'scale(1)');
  };
  video.timeline[670] = () => {
    /* 670 : Gros plan sur Georges. */
    Video.setTransform('#scene', 'scale(3) translate(600px, -300px)');
  };
  video.timeline[672] = (skip = false) => {
    /* 672 : Gros plan sur Philippe. */
    const delay = 500;
    Video.setTransform('#scene', 'scale(3) translate(-300px, -300px)', (skip ? 0 : delay));
  };
  video.timeline[684] = () => {
    /* 684 : Gros plan sur Georges. */
    Video.setTransform('#scene', 'scale(3) translate(600px, -300px)');
  };
  video.timeline[687] = () => {
    /* 687 : Gros plan sur Philippe. */
    Video.setTransform('#scene', 'scale(3) translate(-300px, -300px)');
  };
  video.timeline[701] = (skip = false) => {
    /* 701 : Gros plan sur Georges. */
    const delay = 500;
    Video.setTransform('#scene', 'scale(3) translate(600px, -300px)', (skip ? 0 : delay));
  };
  video.timeline[705] = () => {
    /* 705 : Gros plan sur Philippe. */
    Video.setTransform('#scene', 'scale(3) translate(-300px, -300px)');
  };
  video.timeline[706] = (skip = false) => {
    /* 706 : Transition. */
    let delay = 250;
    Video.setTransform('#scene', 'scale(1)', (skip ? 0 : delay));
    video.executeAnimation('#scene', 'transition 1s linear forwards', 706, delay / 1000);
    delay = 500;
    document.querySelectorAll('.g_sol2, .g_mur, .affiche_subventions, .ordinateur_bureau, .bibliotheque_tres_mini, .telephone, .ecran_youpiprojet_subventions, .philippe, .georges').forEach((element) => {
      Video.toggleVisibility(element, 'none', (skip ? 0 : delay));
    });
    document.querySelectorAll('.georgeslasaucisse').forEach((element) => {
      Video.toggleVisibility(element, 'block', (skip ? 0 : delay));
    });
  };
  video.timeline[707] = (skip = false) => {
    /* 707 : Plan général. */
    document.querySelectorAll('.georgeslasaucisse').forEach((element) => {
      Video.toggleVisibility(element, 'none');
    });
    document.querySelectorAll('.g_sol2, .g_mur, .affiche_ecolo, .ordinateur_bureau, .bibliotheque_tres_mini, .telephone, .ecran_youpiprojet, .papier_pile, .philippe, .georges, .corentin').forEach((element) => {
      Video.toggleVisibility(element, 'block');
    });
    const delay = 250;
    video.unexecuteAnimation('#scene', 'rotateY(0deg)', (skip ? 0 : delay));
  };
  video.timeline[715] = () => {
    /* 715 : Gros plan sur Georges. */
    Video.setTransform('#scene', 'scale(3) translate(600px, -300px)');
  };
  video.timeline[718] = () => {
    /* 718 : Gros plan sur Philippe. */
    Video.setTransform('#scene', 'scale(3) translate(-300px, -300px)');
  };
  video.timeline[722] = (skip = false) => {
    /* 722 : Gros plan sur Georges. */
    const delay = 750;
    Video.setTransform('#scene', 'scale(3) translate(600px, -300px)', (skip ? 0 : delay));
  };
  video.timeline[725] = () => {
    /* 725 : Gros plan sur Philippe. */
    Video.setTransform('#scene', 'scale(3) translate(-300px, -300px)');
  };
  video.timeline[736] = () => {
    /* 736 : Très gros plan sur Georges. */
    Video.setTransform('#scene', 'scale(5) translate(675px, -250px)');
  };
  video.timeline[737] = (skip = false) => {
    /* 737 : Gros plan sur Philippe. */
    const delay = 750;
    Video.setTransform('#scene', 'scale(3) translate(-300px, -300px)', (skip ? 0 : delay));
  };
  video.timeline[739] = (skip = false) => {
    /* 739 : Transition. */
    Video.setTransform('#scene', 'scale(1)');
    video.executeAnimation('#scene', 'transition 1s linear forwards', 739);
    let delay = 250;
    document.querySelectorAll('.g_sol2, .g_mur, .affiche_ecolo, .ordinateur_bureau, .bibliotheque_tres_mini, .telephone, .ecran_youpiprojet, .papier_pile, .philippe, .georges, .corentin').forEach((element) => {
      Video.toggleVisibility(element, 'none', (skip ? 0 : delay));
    });
    document.querySelectorAll('.georgeslasaucisse').forEach((element) => {
      Video.toggleVisibility(element, 'block', (skip ? 0 : delay));
    });
    delay = 750;
    document.querySelectorAll('.georgeslasaucisse').forEach((element) => {
      Video.toggleVisibility(element, 'none', (skip ? 0 : delay));
    });
    document.querySelectorAll('.g_sol2, .g_mur, .affiche_youpiprojet, .ordinateur_bureau, .bibliotheque_tres_mini, .telephone, .ecran_youpiprojet, .philippe, .georges').forEach((element) => {
      Video.toggleVisibility(element, 'block', (skip ? 0 : delay));
    });
  };
  video.timeline[740] = () => {
    /* 740 : Gros plan sur Georges. */
    video.unexecuteAnimation('#scene', 'rotateY(0deg)');
    Video.setTransform('#scene', 'scale(3) translate(600px, -300px)');
  };
  video.timeline[742] = (skip = false) => {
    /* 742 : Gros plan sur Philippe. */
    const delay = 500;
    Video.setTransform('#scene', 'scale(3) translate(-300px, -300px)', (skip ? 0 : delay));
  };
  video.timeline[744] = (skip = false) => {
    /* 744 : Plan général. */
    const delay = 500;
    Video.setTransform('#scene', 'scale(1)', (skip ? 0 : delay));
    document.querySelectorAll('.florent, .maxime, .theo').forEach((element) => {
      element.style.transform = 'scale(3) translateY(0)';
    });
    document.querySelectorAll('.corentin').forEach((element) => {
      element.style.transform = 'rotate(0deg) scale(3) translateX(0)';
    });
    document.querySelectorAll('.raphael').forEach((element) => {
      element.style.transform = 'rotate(0deg) scale(3) translateY(0)';
    });
    document.querySelector('.g_florent').style.transform = 'translate(100px, 1080px)';
    document.querySelector('.g_corentin').style.transform = 'translate(500px, 1125px)';
    document.querySelector('.corentin').style.transform = 'rotate(90deg) scale(3)';
    document.querySelector('.g_raphael').style.transform = 'translate(900px, 1205px)';
    document.querySelector('.g_maxime').style.transform = 'translate(1300px, 1185px)';
    document.querySelector('.g_theo').style.transform = 'translate(1700px, 1080px)';
    document.querySelectorAll('.florent, .corentin, .raphael, .maxime, .theo, .bouche_ouverte, .bouche_deux').forEach((element) => {
      Video.toggleVisibility(element, 'block');
    });
  };
  video.timeline[749] = () => {
    /* 749 : Tout le monde fait Ouaiiis à l'écran. */
    document.querySelectorAll('.ecran_youpiprojet').forEach((element) => {
      Video.toggleVisibility(element, 'none');
    });
    document.querySelectorAll('.ecran_youpiprojet_valide').forEach((element) => {
      Video.toggleVisibility(element, 'block');
    });
    video.executeAnimation('.g_florent', 'ouais_florent_2 1.5s linear forwards', 749);
    video.executeAnimation('.g_corentin', 'ouais_corentin_2 1.5s linear forwards', 749);
    video.executeAnimation('.g_raphael', 'ouais_raphael_2 1.5s linear forwards', 749);
    video.executeAnimation('.g_maxime', 'ouais_maxime_2 1.5s linear forwards', 749);
    video.executeAnimation('.g_theo', 'ouais_theo_2 1.5s linear forwards', 749);
  };
  video.timeline[750] = (skip = false) => {
    /* 750 : Plan général. */
    const delay = 500;
    video.unexecuteAnimation('.g_florent', 'translate(100px, 1140px)', (skip ? 0 : delay));
    video.unexecuteAnimation('.g_corentin', 'translate(500px, 1125px)', (skip ? 0 : delay));
    video.unexecuteAnimation('.g_raphael', 'translate(900px, 1265px)', (skip ? 0 : delay));
    video.unexecuteAnimation('.g_maxime', 'translate(1300px, 1245px)', (skip ? 0 : delay));
    video.unexecuteAnimation('.g_theo', 'translate(1700px, 1140px)', (skip ? 0 : delay));
    document.querySelectorAll('.florent, .corentin, .raphael, .maxime, .theo, .bouche_ouverte, .bouche_deux').forEach((element) => {
      Video.toggleVisibility(element, 'none', (skip ? 0 : delay));
    });
  };
  video.timeline[755] = (skip = false) => {
    /* 755 : Gros plan sur Georges. */
    const delay = 500;
    Video.setTransform('#scene', 'scale(3) translate(600px, -300px)', (skip ? 0 : delay));
  };
  video.timeline[757] = (skip = false) => {
    /* 757 : Gros plan sur Philippe. */
    const delay = 500;
    Video.setTransform('#scene', 'scale(3) translate(-300px, -300px)', (skip ? 0 : delay));
  };
  video.timeline[760] = (skip = false) => {
    /* 760 : Très gros plan sur Georges. */
    const delay = 250;
    Video.setTransform('#scene', 'scale(5) translate(675px, -250px)', (skip ? 0 : delay));
  };
  video.timeline[761] = () => {
    /* 761 : Plan général. */
    Video.setTransform('#scene', 'scale(1)');
  };
  video.timeline[770] = () => {
    /* 770 : Gros plan sur Georges. */
    Video.setTransform('#scene', 'scale(3) translate(600px, -300px)');
  };
  video.timeline[772] = () => {
    /* 772 : Gros plan sur Philippe. */
    Video.setTransform('#scene', 'scale(3) translate(-300px, -300px)');
  };
  video.timeline[773] = (skip = false) => {
    /* 773 : Transition. */
    let delay = 250;
    Video.setTransform('#scene', 'scale(1)', (skip ? 0 : delay));
    video.executeAnimation('#scene', 'transition 1s linear forwards', 773, delay / 1000);
    delay = 500;
    document.querySelectorAll('.g_sol2, .g_mur, .affiche_youpiprojet, .ordinateur_bureau, .bibliotheque_tres_mini, .telephone, .ecran_youpiprojet_valide, .philippe, .georges').forEach((element) => {
      Video.toggleVisibility(element, 'none', (skip ? 0 : delay));
    });
    document.querySelectorAll('.georgeslasaucisse').forEach((element) => {
      Video.toggleVisibility(element, 'block', (skip ? 0 : delay));
    });
  };
  video.timeline[774] = (skip = false) => {
    /* 774 : Plan général de la réunion générale. */
    document.querySelectorAll('.georgeslasaucisse').forEach((element) => {
      Video.toggleVisibility(element, 'none');
    });
    Video.setTransform('.g_philippe', 'translate(800px, 725px)');
    document.querySelectorAll('.g_sol3, .g_mur2, .bureau_subventions, .ecran_youpiprojet_reunion, .philippe, .georges, .gugusse').forEach((element) => {
      Video.toggleVisibility(element, 'block');
    });
    const delay = 250;
    video.unexecuteAnimation('#scene', 'rotateY(0deg)', (skip ? 0 : delay));
  };
  video.timeline[779] = () => {
    /* 779 : Gros plan sur Georges. */
    Video.setTransform('#scene', 'scale(3) translate(600px, -300px)');
  };
  video.timeline[783] = () => {
    /* 783 : Gros plan sur Philippe. */
    Video.setTransform('#scene', 'scale(3) translate(110px, -300px)');
  };
  video.timeline[788] = () => {
    /* 788 : Gros plan sur Robert. */
    Video.setTransform('#scene', 'scale(3) translate(-190px, -300px)');
  };
  video.timeline[792] = () => {
    /* 792 : Gros plan sur Robert. */
    Video.setTransform('#scene', 'scale(3) translate(-480px, -300px)');
  };
  video.timeline[796] = (skip = false) => {
    /* 796 : Gros plan sur Robert. */
    const delay = 500;
    Video.setTransform('#scene', 'scale(3) translate(-640px, -300px)', (skip ? 0 : delay));
  };
  video.timeline[801] = () => {
    /* 801 : Gros plan sur Robert. */
    Video.setTransform('#scene', 'scale(3) translate(-880px, -300px)');
  };
  video.timeline[804] = (skip = false) => {
    /* 804 : Plan général de la réunion générale. */
    const delay = 500;
    Video.setTransform('#scene', 'scale(1)', (skip ? 0 : delay));
  };
  video.timeline[806] = (skip = false) => {
    /* 806 : Festival de musique sur la plage, des gens dansent n'importe comment. */
    const delay = 500;
    document.querySelectorAll('.g_sol3, .g_mur2, .bureau_subventions, .ecran_youpiprojet_reunion, .philippe, .georges').forEach((element) => {
      Video.toggleVisibility(element, 'none', (skip ? 0 : delay));
    });
    document.querySelectorAll('.g_plage, .soleil, .chaine_hifi').forEach((element) => {
      Video.toggleVisibility(element, 'block', (skip ? 0 : delay));
    });
    document.querySelectorAll('.decor').forEach((element) => {
      element.querySelector('rect:first-child').setAttribute('fill', '#e0e0e0');
    });
    video.executeAnimation('.gugusse', `trottinage ${audio.beat}s ease-in infinite`, 806, delay / 1000);
    video.executeAnimation('.gugusse .jambe_gauche', `trottinage_jambe_gauche ${audio.beat * 2}s linear infinite`, 806, delay / 1000);
    video.executeAnimation('.gugusse .jambe_droite', `trottinage_jambe_droite ${audio.beat * 2}s linear infinite`, 806, delay / 1000 + audio.beat);
    video.executeAnimation('.chaine_hifi', `pulse_hifi ${audio.beat / 2}s ease-in alternate infinite`, 806);
  };
  video.timeline[812] = (skip = false) => {
    /* 812 : Une plage avec personne dessus parce qu'il fait trop froid. */
    const delay = 500;
    video.unexecuteAnimation('.gugusse .jambe_gauche', 'rotate(0)', (skip ? 0 : delay));
    video.unexecuteAnimation('.gugusse .jambe_droite', 'rotate(0)', (skip ? 0 : delay));
    video.unexecuteAnimation('.chaine_hifi', 'scale(3)', (skip ? 0 : delay));
    video.executeAnimation('.g_soleil', 'fuite_soleil 0.25s linear forwards', 812, delay / 1000);
    setTimeout(() => {
      video.executeAnimation('.gugusse', 'fuite_gugusse 0.25s linear forwards', 812);
      document.querySelectorAll('.decor').forEach((element) => {
        element.querySelector('rect:first-child').setAttribute('fill', '#808080');
      });
    }, (skip ? 0 : delay));
    document.querySelectorAll('.flocon').forEach((element) => {
      Video.toggleVisibility(element, 'block', (skip ? 0 : delay));
      element.style.animationPlayState = 'running';
    });
  };
  video.timeline[813] = () => {
    /* 813 : Tout le monde est parti, c'est horrible. */
    video.unexecuteAnimation('.g_soleil', 'translate(1920px, 50px)');
    video.unexecuteAnimation('.gugusse', 'rotateY(90deg) scale(3) translateY(0)');
  };
  video.timeline[819] = () => {
    /* 819 : Les gens sont revenus sur la plage et re-dansent n'importe comment. */
    video.executeAnimation('.g_soleil', 'fuite_soleil 0.25s linear reverse forwards', 819, 0.25);
    video.executeAnimation('.gugusse', `trottinage ${audio.beat}s ease-in infinite`, 819, 0.25);
    video.executeAnimation('.gugusse .jambe_gauche', `trottinage_jambe_gauche ${audio.beat * 2}s linear infinite`, 819, 0.25);
    video.executeAnimation('.gugusse .jambe_droite', `trottinage_jambe_droite ${audio.beat * 2}s linear infinite`, 819, 0.25 + audio.beat);
    video.executeAnimation('.chaine_hifi', `pulse_hifi ${audio.beat / 2}s ease-in alternate infinite`, 819, 0.25);
  };
  video.timeline[826] = (skip = false) => {
    /* 826 : Plan général. */
    const delay = 500;
    document.querySelectorAll('.flocon').forEach((element) => {
      Video.toggleVisibility(element, 'none', (skip ? 0 : delay));
      element.style.animationPlayState = 'paused';
    });
    setTimeout(() => {
      document.querySelectorAll('.decor').forEach((element) => {
        element.querySelector('rect:first-child').setAttribute('fill', '#ffffff');
      });
    }, (skip ? 0 : delay));
    video.unexecuteAnimation('.g_soleil', 'translate(1450px, 50px)', (skip ? 0 : delay));
    video.unexecuteAnimation('.gugusse', 'rotateY(0deg) scale(3) translateY(0)', (skip ? 0 : delay));
    video.unexecuteAnimation('.gugusse .jambe_gauche', 'rotate(0)', (skip ? 0 : delay));
    video.unexecuteAnimation('.gugusse .jambe_droite', 'rotate(0)', (skip ? 0 : delay));
    video.unexecuteAnimation('.chaine_hifi', 'scale(3)', (skip ? 0 : delay));
    document.querySelectorAll('.g_plage, .soleil, .chaine_hifi').forEach((element) => {
      Video.toggleVisibility(element, 'none', (skip ? 0 : delay));
    });
    document.querySelectorAll('.g_sol3, .g_mur2, .bureau_subventions, .ecran_youpiprojet_reunion, .philippe, .georges').forEach((element) => {
      Video.toggleVisibility(element, 'block', (skip ? 0 : delay));
    });
  };
  video.timeline[834] = (skip = false) => {
    /* 834 : Gros plan sur Georges. */
    const delay = 250;
    Video.setTransform('#scene', 'scale(3) translate(600px, -300px)', (skip ? 0 : delay));
  };
  video.timeline[839] = () => {
    /* 839 : Gros plan sur Philippe. */
    Video.setTransform('#scene', 'scale(3) translate(110px, -300px)');
  };
  video.timeline[841] = () => {
    /* 841 : Gros plan sur Robert. */
    const positionsRoberts = [190, 480, 640, 880];
    Video.setTransform('#scene', `scale(3) translate(-${positionsRoberts[Utils.getRandomInt(positionsRoberts.length)]}px, -300px)`);
  };
  video.timeline[845] = (skip = false) => {
    /* 845 : Gros plan sur Robert. */
    const delay = 500;
    const positionsRoberts = [190, 480, 640, 880];
    Video.setTransform('#scene', `scale(3) translate(-${positionsRoberts[Utils.getRandomInt(positionsRoberts.length)]}px, -300px)`, (skip ? 0 : delay));
  };
  video.timeline[847] = () => {
    /* 847 : Gros plan sur Robert. */
    const positionsRoberts = [190, 480, 640, 880];
    Video.setTransform('#scene', `scale(3) translate(-${positionsRoberts[Utils.getRandomInt(positionsRoberts.length)]}px, -300px)`);
  };
  video.timeline[848] = () => {
    /* 848 : Gros plan sur Robert. */
    const positionsRoberts = [190, 480, 640, 880];
    Video.setTransform('#scene', `scale(3) translate(-${positionsRoberts[Utils.getRandomInt(positionsRoberts.length)]}px, -300px)`);
  };
  video.timeline[849] = () => {
    /* 849 : Gros plan sur Robert. */
    const positionsRoberts = [190, 480, 640, 880];
    Video.setTransform('#scene', `scale(3) translate(-${positionsRoberts[Utils.getRandomInt(positionsRoberts.length)]}px, -300px)`);
  };
  video.timeline[854] = () => {
    /* 854 : Très gros plan sur Georges. */
    Video.setTransform('#scene', 'scale(5) translate(675px, -250px)');
  };
  video.timeline[857] = (skip = false) => {
    /* 857 : Gros plan sur Robert. */
    const delay = 500;
    document.querySelectorAll('.ecran_youpiprojet_reunion').forEach((element) => {
      Video.toggleVisibility(element, 'none');
    });
    document.querySelectorAll('.ecran_youpiprojet_projet').forEach((element) => {
      Video.toggleVisibility(element, 'block');
    });
    const positionsRoberts = [190, 480, 640, 880];
    Video.setTransform('#scene', `scale(3) translate(-${positionsRoberts[Utils.getRandomInt(positionsRoberts.length)]}px, -300px)`, (skip ? 0 : delay));
  };
  video.timeline[867] = () => {
    /* 867 : Plan général. */
    Video.setTransform('#scene', 'scale(1)');
  };
  video.timeline[875] = () => {
    /* 875 : Gros plan sur Robert. */
    const delay = 500;
    const positionsRoberts = [190, 480, 640, 880];
    Video.setTransform('#scene', `scale(3) translate(-${positionsRoberts[Utils.getRandomInt(positionsRoberts.length)]}px, -300px)`);
  };
  video.timeline[881] = () => {
    /* 881 : Gros plan sur Georges. */
    Video.setTransform('#scene', 'scale(3) translate(600px, -300px)');
  };
  video.timeline[885] = (skip = false) => {
    /* 885 : Gros plan sur Philippe. */
    const delay = 500;
    Video.setTransform('#scene', 'scale(3) translate(110px, -300px)', (skip ? 0 : delay));
  };
  video.timeline[886] = (skip = false) => {
    /* 886 : Transition. */
    const delay = 750;
    Video.setTransform('#scene', 'scale(1)', (skip ? 0 : delay));
    video.executeAnimation('#scene', 'transition 1s linear forwards', 886, delay / 1000);
  };
  video.timeline[887] = (skip = false) => {
    /* 887 : Retour chez Georges. */
    document.querySelectorAll('.g_sol3, .g_mur2, .bureau_subventions, .ecran_youpiprojet_projet, .philippe, .georges, .gugusse').forEach((element) => {
      Video.toggleVisibility(element, 'none');
    });
    document.querySelectorAll('.georgeslasaucisse').forEach((element) => {
      Video.toggleVisibility(element, 'block');
    });
    let delay = 500;
    document.querySelectorAll('.georgeslasaucisse').forEach((element) => {
      Video.toggleVisibility(element, 'none', (skip ? 0 : delay));
    });
    document.querySelectorAll('.georges, .g_paysage_mur_hall, .g_paysage_sol, .g_porte_interieur, .g_couloir_escaliers, .g_trappe, .g_echelle, .g_porte_exterieur').forEach((element) => {
      Video.toggleVisibility(element, 'block', (skip ? 0 : delay));
    });
    Video.setTransform('.g_georges', 'translate(150px, 792px)', (skip ? 0 : delay));
    Video.setTransform('.georges', 'scale(1.5) translateY(0)', (skip ? 0 : delay));
    delay = 750;
    video.unexecuteAnimation('#scene', 'rotateY(0deg)', (skip ? 0 : delay));
  };
  video.timeline[894] = (skip = false) => {
    /* 894 : Générique de fin, merci à toutes et tous. */
    const delay = 250;
    document.querySelectorAll('.georges, .g_paysage_mur_hall, .g_paysage_sol, .g_porte_interieur, .g_couloir_escaliers, .g_trappe, .g_echelle, .g_porte_exterieur').forEach((element) => {
      Video.toggleVisibility(element, 'none');
    });
    Video.setTransform('.g_georges', 'translate(100px, 705px)', (skip ? 0 : delay));
    Video.setTransform('.georges', 'scale(3) translateY(0)', (skip ? 0 : delay));
    document.querySelectorAll('.florent, .maxime, .theo').forEach((element) => {
      element.style.transform = 'scale(3) translateY(0)';
    });
    document.querySelectorAll('.corentin').forEach((element) => {
      element.style.transform = 'rotate(90deg) scale(3) translateX(0)';
    });
    document.querySelectorAll('.raphael').forEach((element) => {
      element.style.transform = 'rotate(0deg) scale(3) translateY(0)';
    });
    document.querySelector('.g_florent').style.transform = 'translate(400px, 705px)';
    document.querySelector('.g_corentin').style.transform = 'translate(700px, 915px)';
    document.querySelector('.g_raphael').style.transform = 'translate(1000px, 750px)';
    document.querySelector('.g_maxime').style.transform = 'translate(1350px, 780px)';
    document.querySelector('.g_theo').style.transform = 'translate(1650px, 900px)';
    document.querySelectorAll('.theo').forEach((element) => {
      element.style.transformOrigin = '31px 90px';
    });
    document.querySelectorAll('.g_sol, .georges, .florent, .corentin, .raphael, .maxime, .theo, .raphael .bouche_deux, .g_cetaitgeorgeslasaucisseorganiseunfestival').forEach((element) => {
      Video.toggleVisibility(element, 'block', (skip ? 0 : delay));
    });
    const beat = 0.4375;
    video.executeAnimation('.georges, .florent', `trottinage ${beat}s ease-in infinite`, 894);
    video.executeAnimation('.georges .jambe_gauche, .florent .jambe_gauche', `trottinage_jambe_gauche ${beat}s linear infinite`, 894);
    video.executeAnimation('.georges .jambe_droite, .florent .jambe_droite', `trottinage_jambe_droite ${beat}s linear infinite`, 894);
    video.executeAnimation('.raphael', `pivote_raphael ${beat * 8}s ease-in infinite`, 894);
    video.executeAnimation('.maxime', `derape_maxime ${beat * 2}s ease-in infinite`, 894);
    video.executeAnimation('.theo', `saute_theo ${beat * 8}s ease-in infinite`, 894);
  };
  video.timeline[896] = () => {
    /* 896 : Générique de fin. */
    document.querySelectorAll('.g_cetaitgeorgeslasaucisseorganiseunfestival').forEach((element) => {
      Video.toggleVisibility(element, 'none');
    });
    document.querySelectorAll('.g_unevideorealiseepardesmu').forEach((element) => {
      Video.toggleVisibility(element, 'block');
    });
  };
  video.timeline[897] = (skip = false) => {
    /* 897 : Générique de fin. */
    const delay = 750;
    document.querySelectorAll('.g_unevideorealiseepardesmu').forEach((element) => {
      Video.toggleVisibility(element, 'none', (skip ? 0 : delay));
    });
    document.querySelectorAll('.g_montageenjavascriptetensvgsoussvgeorges').forEach((element) => {
      Video.toggleVisibility(element, 'block', (skip ? 0 : delay));
    });
  };
  video.timeline[899] = (skip = false) => {
    /* 899 : Générique de fin. */
    const delay = 500;
    document.querySelectorAll('.g_montageenjavascriptetensvgsoussvgeorges').forEach((element) => {
      Video.toggleVisibility(element, 'none', (skip ? 0 : delay));
    });
    document.querySelectorAll('.g_retrouvezgeorgessur').forEach((element) => {
      Video.toggleVisibility(element, 'block', (skip ? 0 : delay));
    });
  };
  video.timeline[901] = (skip = false) => {
    /* 901 : Générique de fin. */
    const delay = 250;
    document.querySelectorAll('.g_retrouvezgeorgessur').forEach((element) => {
      Video.toggleVisibility(element, 'none', (skip ? 0 : delay));
    });
    document.querySelectorAll('.g_mercipourcevisionnagedesolepourvotretemps').forEach((element) => {
      Video.toggleVisibility(element, 'block', (skip ? 0 : delay));
    });
  };
  video.timeline[903] = () => {
    /* 903 : Générique de fin. */
    document.querySelectorAll('.g_mercipourcevisionnagedesolepourvotretemps').forEach((element) => {
      Video.toggleVisibility(element, 'none');
    });
    document.querySelectorAll('.g_voila').forEach((element) => {
      Video.toggleVisibility(element, 'block');
    });
  };
  video.timeline[904] = (skip = false) => {
    /* 904 : Fin du générique de fin. */
    const delay = 750;
    document.querySelectorAll('.g_voila').forEach((element) => {
      Video.toggleVisibility(element, 'none', (skip ? 0 : delay));
    });
    document.querySelectorAll('.g_georgeslasaucissepointfr').forEach((element) => {
      element.style.transform = 'scale(2) translate(50px, 100px)';
    });
    document.querySelectorAll('.georgeslasaucissepointfr').forEach((element) => {
      Video.toggleVisibility(element, 'block', (skip ? 0 : delay));
    });
    video.unexecuteAnimation('.georges, .florent', 'scale(3) translateY(0)', (skip ? 0 : delay));
    video.unexecuteAnimation('.georges .jambe_gauche, .florent .jambe_gauche', 'rotate(0)', (skip ? 0 : delay));
    video.unexecuteAnimation('.georges .jambe_droite, .florent .jambe_droite', `rotate(0)`, (skip ? 0 : delay));
    video.unexecuteAnimation('.raphael', 'scale(3) rotate(0)', (skip ? 0 : delay));
    video.unexecuteAnimation('.maxime', 'scale(3) rotateY(0deg)', (skip ? 0 : delay));
    video.unexecuteAnimation('.theo', 'scale(3) rotateX(0deg)', (skip ? 0 : delay));
  };
  video.timeline[912] = (skip = false) => {
    /* 912 : Fin de la vidéo, c'est pas trop tôt. */
    const delay = 750;
  };
});

export default video;
