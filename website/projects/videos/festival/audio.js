import Audio from '../../../src/components/Video/Audio.js';

/** Premier synthé. */
const pulseSynth = new Tone.Synth({
  oscillator: {
    partials: [
      1,
      0,
      2,
      0,
      3,
    ],
  },
  envelope: {
    attack: 0.001,
    decay: 1.2,
    sustain: 0,
    release: 1.2,
  },
}).toDestination();
pulseSynth.volume.value = -15;

Tone.Transport.bpm.value = 109;

/** Rythme de la musique accompagnant la vidéo en battements par minute. */
const beat = 60 / Tone.Transport.bpm.value;

/** Contenu audio de la vidéo. */
const audio = new Audio(beat, 0, 1, beat * 10, {
  pulse1: './projects/videos/festival/notes/pulse1.json',
  pulse2: './projects/videos/festival/notes/pulse2.json',
  pulse3: './projects/videos/festival/notes/pulse3.json',
  pulse4: './projects/videos/festival/notes/pulse4.json',
}, () => {
  const pulse1Part = (new Tone.Part(((time, note) => {
    pulseSynth.triggerAttackRelease(note.name, note.duration, time, note.velocity);
  }), audio.notes.pulse1));
  const pulse2Part = (new Tone.Part(((time, note) => {
    pulseSynth.triggerAttackRelease(note.name, note.duration, time, note.velocity);
  }), audio.notes.pulse2));
  const pulse3Part = (new Tone.Part(((time, note) => {
    pulseSynth.triggerAttackRelease(note.name, note.duration, time, note.velocity);
  }), audio.notes.pulse3));
  const pulse4Part = (new Tone.Part(((time, note) => {
    pulseSynth.triggerAttackRelease(note.name, note.duration, time, note.velocity);
  }), audio.notes.pulse4));

  pulse1Part.loop = true;
  pulse2Part.loop = true;
  pulse3Part.loop = true;
  pulse4Part.loop = true;

  audio.instruments = [
    {
      tool: pulse1Part,
      strophes: [
        { start: 32, stop: 38 },
        { start: 68, stop: 83 },
        { start: 806.5, stop: 812.5 },
      ],
    },
    {
      tool: pulse2Part,
      strophes: [
        { start: 45, stop: 52 },
        { start: 68, stop: 83 },
        { start: 819.5, stop: 826.5 },
      ],
    },
    {
      tool: pulse3Part,
      strophes: [
        { start: 38, stop: 45 },
        { start: 812.5, stop: 819.5 },
      ],
    },
    {
      tool: pulse4Part,
      strophes: [
        { start: 52, stop: 59 },
        { start: 826.5, stop: 833.5 },
      ],
    },
  ];
});


export default audio;
