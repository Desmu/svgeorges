import Audio from '../../../src/components/Video/Audio.js';

/** Premier synthé. */
const pulseSynth = new Tone.Synth({
  oscillator: {
    partials: [
      1,
      0,
      2,
      0,
      3,
    ],
  },
  envelope: {
    attack: 0.001,
    decay: 1.2,
    sustain: 0,
    release: 1.2,
  },
}).toDestination();
pulseSynth.volume.value = -10;

/** Deuxième synthé. */
const triangleSynth = new Tone.Synth({
  portamento: 0.2,
  oscillator: {
    type: 'sawtooth',
  },
  envelope: {
    attack: 0.03,
    decay: 0.1,
    sustain: 0.2,
    release: 0.02,
  },
}).toDestination();
triangleSynth.volume.value = -10;

/** Troisième synthé. */
const squareSynth = new Tone.Synth({
  oscillator: {
    type: 'fatsawtooth',
    count: 3,
    spread: 30,
  },
  envelope: {
    attack: 0.01,
    decay: 0.1,
    sustain: 0.5,
    release: 0.4,
    attackCurve: 'exponential',
  },
}).toDestination();
squareSynth.volume.value = -10;

/** Quatrième synthé. */
const noiseSynth = new Tone.NoiseSynth().toDestination();
noiseSynth.volume.value = -15;

Tone.Transport.bpm.value = 120;

/** Rythme de la musique accompagnant la vidéo en battements par minute. */
const beat = 60 / Tone.Transport.bpm.value;

/** Contenu audio de la vidéo. */
const audio = new Audio(beat, 22, 7, beat * 64, {
  pulse: './projects/videos/promenade/notes/gl1.json',
  square: './projects/videos/promenade/notes/gl2.json',
  triangle: './projects/videos/promenade/notes/ob2.json',
  noise: './projects/videos/promenade/notes/ob1.json',
}, () => {
  Object.entries(audio.notes).forEach(([instrument, notes]) => {
    notes = Audio.shuffleNotes(notes);
    notes.forEach((note, i) => {
      audio.notes[instrument][i].time /= 2;
    });
  });
  audio.instruments = [
    {
      tool: (new Tone.Part(((time, note) => {
        pulseSynth.triggerAttackRelease(note.name, note.duration, time, note.velocity);
      }), audio.notes.pulse)),
      strophes: [
        { start: 22, stop: 38 },
        { start: 38, stop: 54 },
        { start: 78, stop: 94 },
        { start: 126, stop: 142 },
        { start: 168, stop: 184 },
        { start: 243, stop: 259 },
        { start: 279, stop: 295 },
      ],
    },
    {
      tool: (new Tone.Part(((time, note) => {
        squareSynth.triggerAttackRelease(note.name, note.duration, time, note.velocity);
      }), audio.notes.square)),
      strophes: [
        { start: 22, stop: 38 },
        { start: 38, stop: 54 },
        { start: 78, stop: 94 },
        { start: 126, stop: 142 },
        { start: 168, stop: 184 },
        { start: 243, stop: 259 },
        { start: 279, stop: 295 },
      ],
    },
    {
      tool: (new Tone.Part(((time, note) => {
        triangleSynth.triggerAttackRelease(note.name, note.duration, time, note.velocity);
      }), audio.notes.triangle)),
      strophes: [
        { start: 22, stop: 38 },
        { start: 38, stop: 54 },
        { start: 78, stop: 94 },
        { start: 126, stop: 142 },
        { start: 126, stop: 142 },
        { start: 168, stop: 184 },
        { start: 243, stop: 259 },
        { start: 279, stop: 295 },
      ],
    },
    {
      tool: (new Tone.Part(((time, note) => {
        noiseSynth.triggerAttackRelease(note.duration, time, note.velocity);
      }), audio.notes.noise)),
      strophes: [
        { start: 22, stop: 38 },
        { start: 38, stop: 54 },
        { start: 78, stop: 94 },
        { start: 126, stop: 142 },
        { start: 168, stop: 184 },
        { start: 243, stop: 259 },
        { start: 279, stop: 295 },
      ],
    },
  ];
});

export default audio;
