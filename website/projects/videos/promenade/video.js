import Utils from '../../../src/js/Utils.js';
import Subtitles from '../../../src/components/Video/Subtitles.js';
import Video from '../../../src/components/Video/Video.js';
import audio from './audio.js';

/** Fichiers SVG à charger. */
const svgPaths = [
  './assets/components/Georges/Georges.svg',
  './assets/vectors/characters/florent.svg',
  './assets/vectors/characters/theo.svg',
  './assets/vectors/characters/corentin.svg',
  './assets/vectors/characters/raphael.svg',
  './assets/vectors/characters/maxime.svg',
  './assets/vectors/text/georgeslasaucissepointfr.svg',
  './assets/vectors/backgrounds/video/chemin_foret.svg',
];

/** Contenu de la vidéo. */
const video = new Video(svgPaths, (new Subtitles('./projects/videos/promenade/subtitles.vtt')), () => {
  /* Création des arbres. */
  for (let i = 0; i < 68; i += 1) {
    document.querySelector('.decor').appendChild(Utils.generateArbre(250, 40));
    document.querySelectorAll('.arbre')[i].setAttribute('transform', `scale(4) translate(${i * 50}, ${1080 / 4 - parseInt(document.querySelectorAll('.arbre')[i].querySelector('rect').getAttribute('height'), 10)})`);
  }

  /* Création du chemin de cailloux du sol. */
  Utils.randomCailloux(document.querySelector('#chemin_foret'));
  let cheminPath = '';
  let currY = 0;
  for (let i = 1; i <= 13440; i += 1) {
    let headsOrTails = Utils.getRandomInt(2);
    if (currY === 5) {
      headsOrTails = 0;
    } else if (currY === -5) {
      headsOrTails = 1;
    }
    switch (headsOrTails) {
      case 0:
        if ((i < 8500) || (i > 8750)) {
          currY -= 1;
        }
        break;
      case 1:
        if ((i < 8500) || (i > 8750)) {
          currY += 1;
        }
        break;
      default:
        break;
    }
    cheminPath += `${i},${currY} `;
  }
  document.querySelector('.chemin_foret').setAttribute('points', `0,0 ${cheminPath}13440,0 13440,100 0,100`);

  /* Création de neige. */
  video.style = [];
  const limit = 16;
  for (let j = 0; j < 9; j += 1) {
    for (let i = 0; i < limit; i += 1) {
      const floconElem = document.createElementNS('http://www.w3.org/2000/svg', 'g');
      floconElem.classList.add('flocon', `flocon_${i + (j * limit)}`);
      const floconBrancheElem = document.createElementNS('http://www.w3.org/2000/svg', 'path');
      floconBrancheElem.setAttribute('fill', 'transparent');
      floconBrancheElem.setAttribute('stroke', '#fff');
      let x = 0;
      let y = 0;
      let path = `M ${x} ${y} `;
      const numberSteps = Utils.getRandomInt(5);
      for (let k = 0; k < numberSteps; k += 1) {
        [x, y] = Utils.changeXY(x, y);
        path += `L ${x} ${y}`;
        if (k + 1 === numberSteps) {
          [x, y] = Utils.changeXY(x, y);
          path += ' ';
        }
      }
      floconBrancheElem.setAttribute('d', path);
      for (let deg = 60; deg < 360; deg += 60) {
        const floconBrancheElemClone = floconBrancheElem.cloneNode(true);
        floconBrancheElemClone.setAttribute('transform', `rotate(${deg})`);
        floconElem.appendChild(floconBrancheElemClone);
      }
      floconElem.appendChild(floconBrancheElem);
      document.querySelector('.decor').appendChild(floconElem);
      video.style.push(`
        @keyframes chute_de_neige_${i + (j * limit)} {
          from { transform: translate(${11520 + i * 120}px, 0px); }
          12.5% { transform: translate(${11520 + i * 120 + 50}px, 135px); }
          25% { transform: translate(${11520 + i * 120}px, 270px); }
          37.5% { transform: translate(${11520 + i * 120 - 50}px, 405px); }
          50% { transform: translate(${11520 + i * 120}px, 540px); }
          62.5% { transform: translate(${11520 + i * 120 + 50}px, 675px); }
          75% { transform: translate(${11520 + i * 120}px, 810px); }
          87.5% { transform: translate(${11520 + i * 120 - 50}px, 945px); }
          to { transform: translate(${11520 + i * 120}px, 1080px); }
        }
      `);
      video.style.push(`
        .flocon_${i + (j * limit)} {
          animation-delay: ${j}s;
          animation-duration: ${Utils.getRandomInt(15) + 5}s;
          animation-name: chute_de_neige_${i + (j * limit)};
          animation-iteration-count: infinite;
          animation-play-state: paused;
          animation-timing-function: linear;
          transform: translate(${11520 + i * 120}px, -100px);
        }
      `);
    }
  }

  /* Création des animations de Noël. */
  const elements = {
    g_georges: [12545, 805],
    g_raphael: [12320, 765],
    g_maxime: [12145, 790],
    g_theo: [11870, 825],
    g_florent: [11700, 805],
  };
  const transforms = ['translate', 'scale', 'skew'];
  Object.entries(elements).forEach(([element, transform]) => {
    const steps = [];
    for (let j = 3.125; j < 100; j += 1) {
      let step = '';
      for (let i = 0; i < transforms.length; i += 1) {
        const transformFunction = transforms[i];
        let effect = '';
        switch (transformFunction) {
          case 'rotate':
            effect = `${(Utils.getRandomInt(2) === 1) ? '-' : ''}${Utils.getRandomInt(180)}deg`;
            break;
          case 'translate':
            effect = `${11520 + Utils.getRandomInt(1920)}px, ${Utils.getRandomInt(1080)}px`;
            break;
          case 'scale':
            effect = `${(Utils.getRandomInt(2) === 1) ? '-' : ''}${Utils.getRandomInt(5)}, ${(Utils.getRandomInt(2) === 1) ? '-' : ''}${Utils.getRandomInt(5)}`;
            break;
          case 'skew':
            effect = `${(Utils.getRandomInt(2) === 1) ? '-' : ''}${Utils.getRandomInt(45)}deg, ${(Utils.getRandomInt(2) === 1) ? '-' : ''}${Utils.getRandomInt(45)}deg`;
            break;
          default:
            break;
        }
        step += `${transformFunction}(${effect})`;
        if (i + 1 < transforms.length) {
          step += ' ';
        }
      }
      if ((step.indexOf('translate(') === -1) && (step.indexOf('translateX(') === -1) && (step.indexOf('translateY(') === -1)) {
        step += ` translate(${transform[0]}px, ${transform[1]}px)`;
      }
      steps.push(`${j}% { transform: ${step}; }`);
    }
    video.style.push(`
      @keyframes noel_${element} {
        from { transform: translate(${transform[0]}px, ${transform[1]}px); }
        ${steps.join(' ')}
        to { transform: translate(${transform[0]}px, ${transform[1]}px); }
      }
    `);
  });
}, () => {
  video.timeline = [];
  video.timeline[0] = () => {
    document.querySelectorAll('#scene > *').forEach((element) => {
      Video.toggleVisibility(element, 'block');
    });
    document.querySelectorAll('#scene, .g_florent, .g_theo, .g_corentin, .g_raphael, .g_maxime, .georgeslasaucissepointfr, .georges, .florent, .theo, .corentin, .raphael, .maxime, .georges .jambe_gauche, .florent .jambe_gauche, .georges .jambe_droite, .florent .jambe_droite, .corentin .bouche_ouverte').forEach((element) => {
      element.style.animation = 'none';
    });
    /* 0 : Initialisation de tout. */
    video.animations = [];
    document.querySelectorAll('#scene').forEach((element) => {
      element.style.transform = 'scale(4) translate(0px, 0px)';
      element.style.transformOrigin = '300px 875px';
    });
    document.querySelectorAll('.decor').forEach((element) => {
      element.querySelector('rect:first-child').setAttribute('fill', '#c0c0c0');
      Video.toggleVisibility(element, 'block');
    });

    document.querySelector('.decor > rect').style.transform = 'translate(0px, 0px)';
    document.querySelector('.g_chemin_foret').style.transform = 'translate(0px, 985px)';
    document.querySelector('.g_georges').style.transform = 'translate(300px, 805px)';
    document.querySelector('.g_raphael').style.transform = 'translate(4840px, 765px)';
    document.querySelector('.g_maxime').style.transform = 'translate(6900px, 790px)';
    document.querySelector('.g_theo').style.transform = 'translate(8565px, 900px)';
    document.querySelector('.g_florent').style.transform = 'translate(9000px, -1920px)';
    document.querySelector('.g_corentin').style.transform = 'translate(11450px, 805px)';
    document.querySelectorAll('.georgeslasaucissepointfr').forEach((element) => {
      element.style.transform = 'translate(0px, 0px)';
    });

    document.querySelectorAll('.georges').forEach((element) => {
      Video.toggleVisibility(element, 'block');
      element.style.transformOrigin = '27px 47px';
    });
    document.querySelectorAll('.raphael').forEach((element) => {
      element.style.transformOrigin = '42px 60px';
    });
    document.querySelectorAll('.maxime').forEach((element) => {
      element.style.transformOrigin = '40px 51px';
    });
    document.querySelectorAll('.theo').forEach((element) => {
      element.style.transformOrigin = '31px 45px';
    });
    document.querySelectorAll('.florent').forEach((element) => {
      element.style.transformOrigin = '27px 47px';
    });
    document.querySelectorAll('.corentin').forEach((element) => {
      element.style.transformOrigin = '18px 16px';
    });
    document.querySelectorAll('.florent, .theo, .corentin, .raphael, .maxime, .bouche_deux, .georgeslasaucissepointfr').forEach((element) => {
      Video.toggleVisibility(element, 'none');
    });
    document.querySelectorAll('.georges, .florent, .theo, .corentin, .raphael, .maxime').forEach((element) => {
      element.style.transform = 'scale(3)';
    });
    document.querySelectorAll('.georges .jambe_gauche, .florent .jambe_gauche').forEach((element) => {
      element.style.transformOrigin = '15px 60px';
    });
    document.querySelectorAll('.georges .jambe_droite, .florent .jambe_droite').forEach((element) => {
      element.style.transformOrigin = '30px 60px';
    });
    document.querySelectorAll('.corentin .bouche_ouverte').forEach((element) => {
      element.style.transform = 'rotateX(90deg)';
      element.style.transformOrigin = '15px 30px';
    });
    document.querySelectorAll('.maxime .bouche').forEach((element) => {
      Video.toggleVisibility(element, 'block');
    });
    document.querySelectorAll('.flocon').forEach((element, i) => {
      element.style.animationPlayState = 'paused';
      element.style.transform = `translate(${11520 + (i % 16) * 120}px, -100px)`;
    });
    document.querySelector('.decor > rect').style.backgroundColor = '#c0c0c0';
    document.querySelectorAll('.arbre').forEach((arbre) => {
      Video.toggleVisibility(arbre, 'block');
    });
    for (let i = 0; i < window.videoCache.promenade.specialAnimationsIntervals.length; i += 1) {
      clearInterval(window.videoCache.promenade.specialAnimationsIntervals[i]);
    }
    for (let i = 0; i < window.videoCache.promenade.specialAnimationsTimeouts.length; i += 1) {
      clearTimeout(window.videoCache.promenade.specialAnimationsTimeouts[i]);
    }

    Video.toggleVisibility(document.querySelector('#loading'), 'none');
  };
  video.timeline[4] = () => {
    /* 4 : Dezoom de Georges vers la scène. */
    video.executeAnimation('#scene', 'dezoom 1s forwards', 4);
  };
  video.timeline[6] = () => {
    /* 6 : Fin du dezoom. */
    video.unexecuteAnimation('#scene', 'scale(1)');
  };
  video.timeline[16] = () => {
    /* 16 : Rezoom. */
    document.querySelectorAll('#scene').forEach((element) => {
      element.style.transform = 'scale(4)';
    });
  };
  video.timeline[20] = () => {
    /* 20 : Redezoom. */
    document.querySelectorAll('#scene').forEach((element) => {
      element.style.transform = 'scale(1)';
    });
  };
  video.timeline[22] = () => {
    /* 22 : Début de la chanson. */
    video.executeAnimation('#scene', 'deplacement_decor_1 32s linear', 22);
    video.executeAnimation('.decor > rect', 'non_deplacement_decor_1 32s linear', 22);
    video.executeAnimation('.g_georges', 'marche_georges_1 32s linear', 22);
    video.executeAnimation('.georges .jambe_droite', `marche_jambe_droite ${audio.beat * 2}s linear infinite`, 22);
    video.executeAnimation('.georges .jambe_gauche', `marche_jambe_gauche ${audio.beat * 2}s linear infinite`, 22);
  };
  video.timeline[38] = () => {
    /* 38 : Première strophe. Placement de Raphaël. */
    document.querySelectorAll('.raphael').forEach((element) => {
      Video.toggleVisibility(element, 'block');
    });
    document.querySelectorAll('.raphael .bouche_deux').forEach((element) => {
      Video.toggleVisibility(element, 'block');
    });
    video.executeAnimation('.raphael', `animation_joie ${audio.beat * 2}s linear infinite`, 38);
  };
  video.timeline[54] = () => {
    /* 54 : Fin de la première strophe. Raphël parle. */
    video.unexecuteAnimation('.g_georges', 'translate(4120px, 805px)');
    video.unexecuteAnimation('.georges .jambe_droite', 'translate(0px, 0px)');
    video.unexecuteAnimation('.georges .jambe_gauche', 'translate(0px, 0px)');
    video.unexecuteAnimation('.raphael', 'scale(3) rotate(0)');
    video.unexecuteAnimation('.decor > rect', 'translate(3840px, 0)');
    video.unexecuteAnimation('#scene', 'translate(-3840px, 0)');
  };
  video.timeline[58] = () => {
    /* 58 : Raphaël se tait. */
    document.querySelectorAll('.raphael .bouche_deux').forEach((element) => {
      Video.toggleVisibility(element, 'none');
    });
  };
  video.timeline[65] = () => {
    /* 65 : Georges est guilleret. */
    video.executeAnimation('.georges', 'animation_joie 0.5s linear infinite', 65, 0.5);
    video.executeAnimation('.georges .jambe_gauche', 'joie_jambe_gauche 0.5s linear', 65, 0.5);
    video.executeAnimation('.georges .jambe_droite', 'joie_jambe_droite 0.5s linear', 65, 0.5);
  };
  video.timeline[66] = () => {
    /* 66 : Georges n'est plus guilleret. Raphaël parle. */
    video.unexecuteAnimation('.georges', 'scale(3) rotate(0)');
    video.unexecuteAnimation('.georges .jambe_gauche', 'rotate(0)');
    video.unexecuteAnimation('.georges .jambe_droite', 'rotate(0)');
    document.querySelectorAll('.raphael .bouche_deux').forEach((element) => {
      Video.toggleVisibility(element, 'block');
    });
  };
  video.timeline[70] = () => {
    /* 70 : Raphaël se tait. */
    document.querySelectorAll('.raphael .bouche_deux').forEach((element) => {
      Video.toggleVisibility(element, 'none');
    });
  };
  video.timeline[74] = () => {
    /* 74 : Zoom brutal sur Raphaël qui est bien trop ravi de ce qu'il lui arrive. */
    document.querySelectorAll('.raphael .bouche_deux').forEach((element) => {
      Video.toggleVisibility(element, 'block');
    });
    document.querySelectorAll('#scene').forEach((element) => {
      element.style.transform = 'scale(10) translateX(-4515px) translateY(150px)';
    });
  };
  video.timeline[75] = () => {
    /* 75 : Dezoom brutal parce que ça va bien cinq minutes. */
    document.querySelectorAll('#scene').forEach((element) => {
      element.style.transform = 'scale(1) translate(-3840px)';
    });
  };
  video.timeline[78] = () => {
    /* 78 : Deuxième strophe. Placement de Maxime. */
    video.executeAnimation('#scene', 'deplacement_decor_2 16s linear', 78);
    video.executeAnimation('.decor > rect', 'non_deplacement_decor_2 16s linear', 78);
    video.executeAnimation('.g_georges', 'marche_georges_2 16s linear', 78);
    video.executeAnimation('.georges .jambe_droite', `marche_jambe_droite ${audio.beat * 2}s linear infinite`, 78);
    video.executeAnimation('.georges .jambe_gauche', `marche_jambe_gauche ${audio.beat * 2}s linear infinite`, 78);
    video.executeAnimation('.g_raphael', 'marche_raphael_1 16s linear', 78);
    video.executeAnimation('.raphael', `animation_joie ${audio.beat * 2}s linear infinite`, 78);
    document.querySelectorAll('.maxime').forEach((element) => {
      Video.toggleVisibility(element, 'block');
    });
  };
  video.timeline[94] = () => {
    /* 94 : Fin de la deuxième strophe. Maxime parle. */
    document.querySelectorAll('.raphael .bouche_deux').forEach((element) => {
      Video.toggleVisibility(element, 'none');
    });
    video.unexecuteAnimation('.g_georges', 'translate(6110px, 805px)');
    video.unexecuteAnimation('.georges .jambe_droite', 'translate(0px, 0px)');
    video.unexecuteAnimation('.georges .jambe_gauche', 'translate(0px, 0px)');
    video.unexecuteAnimation('.g_raphael', 'translate(5875px, 765px)');
    video.unexecuteAnimation('.raphael', 'scale(3) rotate(0)');
    video.unexecuteAnimation('.decor > rect', 'translate(5760px, 0)');
    video.unexecuteAnimation('#scene', 'translate(-5760px, 0)');
    document.querySelectorAll('.maxime .bouche').forEach((element) => {
      Video.toggleVisibility(element, 'none');
    });
    document.querySelectorAll('.maxime .bouche_deux').forEach((element) => {
      Video.toggleVisibility(element, 'block');
    });
  };
  video.timeline[97] = () => {
    /* 97 : Maxime parle plus. */
    document.querySelectorAll('.maxime .bouche_deux').forEach((element) => {
      Video.toggleVisibility(element, 'none');
    });
    document.querySelectorAll('.maxime .bouche').forEach((element) => {
      Video.toggleVisibility(element, 'block');
    });
  };
  video.timeline[101] = () => {
    /* 101 : Maxime parle. */
    document.querySelectorAll('.maxime .bouche').forEach((element) => {
      Video.toggleVisibility(element, 'none');
    });
    document.querySelectorAll('.maxime .bouche_deux').forEach((element) => {
      Video.toggleVisibility(element, 'block');
    });
  };
  video.timeline[106] = () => {
    /* 106 : Maxime parle plus. Raphaël parle, par contre. */
    document.querySelectorAll('.maxime .bouche_deux').forEach((element) => {
      Video.toggleVisibility(element, 'none');
    });
    document.querySelectorAll('.maxime .bouche').forEach((element) => {
      Video.toggleVisibility(element, 'block');
    });
    document.querySelectorAll('.raphael .bouche_deux').forEach((element) => {
      Video.toggleVisibility(element, 'block');
    });
  };
  video.timeline[109] = () => {
    /* 109 : Raphaël parle plus. */
    document.querySelectorAll('.raphael .bouche_deux').forEach((element) => {
      Video.toggleVisibility(element, 'none');
    });
  };
  video.timeline[112] = () => {
    /* 112 : Maxime parle. */
    document.querySelectorAll('.maxime .bouche').forEach((element) => {
      Video.toggleVisibility(element, 'none');
    });
    document.querySelectorAll('.maxime .bouche_deux').forEach((element) => {
      Video.toggleVisibility(element, 'block');
    });
  };
  video.timeline[114] = () => {
    /* 114 : Maxime parle plus. Zoom sur Georges. */
    document.querySelectorAll('.maxime .bouche_deux').forEach((element) => {
      Video.toggleVisibility(element, 'none');
    });
    document.querySelectorAll('.maxime .bouche').forEach((element) => {
      Video.toggleVisibility(element, 'block');
    });
    document.querySelectorAll('#scene').forEach((element) => {
      element.style.transform = 'scale(4) translateX(-5760px)';
    });
  };
  video.timeline[117] = () => {
    /* 117 : Maxime parle. Zoom sur lui. */
    document.querySelectorAll('.maxime .bouche').forEach((element) => {
      Video.toggleVisibility(element, 'none');
    });
    document.querySelectorAll('.maxime .bouche_deux').forEach((element) => {
      Video.toggleVisibility(element, 'block');
    });
    document.querySelectorAll('#scene').forEach((element) => {
      element.style.transform = 'scale(4) translateX(-6500px)';
    });
  };
  video.timeline[120] = () => {
    /* 120 : Maxime parle plus. Dezoom. */
    document.querySelectorAll('.maxime .bouche_deux').forEach((element) => {
      Video.toggleVisibility(element, 'none');
    });
    document.querySelectorAll('.maxime .bouche').forEach((element) => {
      Video.toggleVisibility(element, 'block');
    });
    document.querySelectorAll('#scene').forEach((element) => {
      element.style.transform = 'scale(1) translate(-5760px)';
    });
  };
  video.timeline[123] = () => {
    /* 123 : Maxime parle. */
    document.querySelectorAll('.maxime .bouche').forEach((element) => {
      Video.toggleVisibility(element, 'none');
    });
    document.querySelectorAll('.maxime .bouche_deux').forEach((element) => {
      Video.toggleVisibility(element, 'block');
    });
  };
  video.timeline[126] = () => {
    /* 126 : Troisième strophe. Placement de Théo. */
    document.querySelectorAll('.raphael .bouche_deux').forEach((element) => {
      Video.toggleVisibility(element, 'block');
    });
    video.executeAnimation('#scene', 'deplacement_decor_3 16s linear', 126);
    video.executeAnimation('.decor > rect', 'non_deplacement_decor_3 16s linear', 126);
    video.executeAnimation('.g_georges', 'marche_georges_3 16s linear', 126);
    video.executeAnimation('.georges .jambe_droite', `marche_jambe_droite ${audio.beat * 2}s linear infinite`, 126);
    video.executeAnimation('.georges .jambe_gauche', `marche_jambe_gauche ${audio.beat * 2}s linear infinite`, 126);
    video.executeAnimation('.g_raphael', 'marche_raphael_2 16s linear', 126);
    video.executeAnimation('.raphael', `animation_joie ${audio.beat * 2}s linear infinite`, 126);
    video.executeAnimation('.g_maxime', 'marche_maxime_1 16s linear', 126);
    document.querySelectorAll('.theo').forEach((element) => {
      Video.toggleVisibility(element, 'block');
    });
  };
  video.timeline[142] = () => {
    /* 142 : Fin de la troisième strophe. */
    document.querySelectorAll('.raphael .bouche_deux').forEach((element) => {
      Video.toggleVisibility(element, 'none');
    });
    document.querySelectorAll('.maxime .bouche_deux').forEach((element) => {
      Video.toggleVisibility(element, 'none');
    });
    document.querySelectorAll('.maxime .bouche').forEach((element) => {
      Video.toggleVisibility(element, 'block');
    });
    video.unexecuteAnimation('.g_georges', 'translate(8275px, 805px)');
    video.unexecuteAnimation('.georges .jambe_droite', 'translate(0px, 0px)');
    video.unexecuteAnimation('.georges .jambe_gauche', 'translate(0px, 0px)');
    video.unexecuteAnimation('.g_raphael', 'translate(8025px, 765px)');
    video.unexecuteAnimation('.raphael', 'scale(3) rotate(0)');
    video.unexecuteAnimation('.g_maxime', 'translate(7850px, 790px) scale(-1, 1)');
    video.unexecuteAnimation('.decor > rect', 'translate(7680px, 0)');
    video.unexecuteAnimation('#scene', 'translate(-7680px, 0)');
  };
  video.timeline[149] = () => {
    /* 149 : Théo est agacé. */
    video.executeAnimation('.theo', 'sautillement 1s linear', 149);
  };
  video.timeline[150] = () => {
    /* 150 : Théo se désagace. */
    video.unexecuteAnimation('.theo', 'scale(3) translate(0px, 0px)');
  };
  video.timeline[152] = () => {
    /* 152 : Maxime parle. */
    document.querySelectorAll('.maxime .bouche').forEach((element) => {
      Video.toggleVisibility(element, 'none');
    });
    document.querySelectorAll('.maxime .bouche_deux').forEach((element) => {
      Video.toggleVisibility(element, 'block');
    });
  };
  video.timeline[154] = () => {
    /* 154 : Maxime parle plus. */
    document.querySelectorAll('.maxime .bouche_deux').forEach((element) => {
      Video.toggleVisibility(element, 'none');
    });
    document.querySelectorAll('.maxime .bouche').forEach((element) => {
      Video.toggleVisibility(element, 'block');
    });
  };
  video.timeline[156] = () => {
    /* 156 : Raphaël parle. */
    document.querySelectorAll('.raphael .bouche_deux').forEach((element) => {
      Video.toggleVisibility(element, 'block');
    });
  };
  video.timeline[162] = () => {
    /* 162 : Raphaël parle plus. */
    document.querySelectorAll('.raphael .bouche_deux').forEach((element) => {
      Video.toggleVisibility(element, 'none');
    });
  };
  video.timeline[165] = () => {
    /* 165 : Georges est guilleret. */
    video.executeAnimation('.georges', 'animation_joie 0.5s linear infinite', 166, 0.5);
    video.executeAnimation('.georges .jambe_gauche', 'joie_jambe_gauche 0.5s linear', 166, 0.5);
    video.executeAnimation('.georges .jambe_droite', 'joie_jambe_droite 0.5s linear', 166, 0.5);
  };
  video.timeline[166] = () => {
    /* 166 : Georges n'est plus guilleret. */
    video.unexecuteAnimation('.georges', 'scale(3) rotate(0)');
    video.unexecuteAnimation('.georges .jambe_gauche', 'rotate(0)');
    video.unexecuteAnimation('.georges .jambe_droite', 'rotate(0)');
  };
  video.timeline[168] = () => {
    /* 168 : Quatrième strophe. Placement de Florent. */
    document.querySelectorAll('.raphael .bouche_deux').forEach((element) => {
      Video.toggleVisibility(element, 'block');
    });
    document.querySelectorAll('.maxime .bouche').forEach((element) => {
      Video.toggleVisibility(element, 'none');
    });
    document.querySelectorAll('.maxime .bouche_deux').forEach((element) => {
      Video.toggleVisibility(element, 'block');
    });
    video.executeAnimation('.georges', `animation_joie ${audio.beat * 2}s linear infinite`, 168);
    video.executeAnimation('.georges .jambe_gauche', `joie_jambe_gauche ${audio.beat * 2}s linear infinite`, 168);
    video.executeAnimation('.georges .jambe_droite', `joie_jambe_droite ${audio.beat * 2}s linear infinite`, 168);
    video.executeAnimation('.raphael', `animation_joie ${audio.beat * 2}s linear infinite`, 168);
    video.executeAnimation('.maxime', `animation_joie ${audio.beat * 2}s linear infinite`, 168);
    video.executeAnimation('.theo', `animation_joie ${audio.beat * 2}s linear infinite`, 168);
    video.executeAnimation('.g_florent', 'marche_florent_1 16s linear', 168);
    document.querySelectorAll('.florent').forEach((element) => {
      Video.toggleVisibility(element, 'block');
    });
  };
  video.timeline[184] = () => {
    /* 184 : Fin de la quatrième strophe. */
    document.querySelectorAll('.raphael .bouche_deux').forEach((element) => {
      Video.toggleVisibility(element, 'none');
    });
    document.querySelectorAll('.maxime .bouche_deux').forEach((element) => {
      Video.toggleVisibility(element, 'none');
    });
    document.querySelectorAll('.maxime .bouche').forEach((element) => {
      Video.toggleVisibility(element, 'block');
    });
    video.unexecuteAnimation('.georges', 'scale(3) rotate(0)');
    video.unexecuteAnimation('.georges .jambe_gauche', 'rotate(0)');
    video.unexecuteAnimation('.georges .jambe_droite', 'rotate(0)');
    video.unexecuteAnimation('.raphael', 'scale(3) rotate(0)');
    video.unexecuteAnimation('.maxime', 'scale(3) rotate(0)');
    video.unexecuteAnimation('.theo', 'scale(3) rotate(0)');
    video.unexecuteAnimation('.g_florent', 'translate(9000px, 805px) scale(-1, 1)');
  };
  video.timeline[194] = () => {
    /* 194 : Théo est agacé. */
    video.executeAnimation('.theo', 'sautillement 1s linear', 194);
  };
  video.timeline[195] = () => {
    /* 195 : Théo se désagace. */
    video.unexecuteAnimation('.theo', 'scale(3) translate(0px, 0px)');
  };
  video.timeline[198] = () => {
    /* 198 : Maxime parle. */
    document.querySelectorAll('.maxime .bouche').forEach((element) => {
      Video.toggleVisibility(element, 'none');
    });
    document.querySelectorAll('.maxime .bouche_deux').forEach((element) => {
      Video.toggleVisibility(element, 'block');
    });
  };
  video.timeline[203] = () => {
    /* 203 : Maxime parle plus. */
    document.querySelectorAll('.maxime .bouche_deux').forEach((element) => {
      Video.toggleVisibility(element, 'none');
    });
    document.querySelectorAll('.maxime .bouche').forEach((element) => {
      Video.toggleVisibility(element, 'block');
    });
  };
  video.timeline[204] = () => {
    /* 204 : Florent est agacé. */
    video.executeAnimation('.florent', 'sautillement 1s linear', 204);
    document.querySelector('.florent .jambe_gauche').style.transform = 'rotate(45deg)';
    document.querySelector('.florent .jambe_droite').style.transform = 'rotate(-45deg)';
  };
  video.timeline[205] = () => {
    /* 205 : Florent se désagace. */
    video.unexecuteAnimation('.florent', 'scale(3) translate(0px, 0px)');
    document.querySelector('.florent .jambe_gauche').style.transform = 'rotate(0deg)';
    document.querySelector('.florent .jambe_droite').style.transform = 'rotate(0deg)';
  };
  video.timeline[206] = () => {
    /* 206 : Raphaël parle. Zoom sur lui. */
    document.querySelectorAll('.raphael .bouche_deux').forEach((element) => {
      Video.toggleVisibility(element, 'block');
    });
    document.querySelectorAll('#scene').forEach((element) => {
      element.style.transform = 'scale(4) translateX(-7600px) translateY(100px)';
    });
  };
  video.timeline[213] = () => {
    /* 213 : Raphaël parle plus, mais Théo si. Zoom sur lui. */
    document.querySelectorAll('.raphael .bouche_deux').forEach((element) => {
      Video.toggleVisibility(element, 'none');
    });
    document.querySelectorAll('#scene').forEach((element) => {
      element.style.transform = 'scale(4) translateX(-8150px) translateY(-100px)';
    });
  };
  video.timeline[219] = () => {
    /* 219 : Zoom sur Florent. */
    document.querySelectorAll('#scene').forEach((element) => {
      element.style.transform = 'scale(4) translateX(-8565px) translateY(-66px)';
    });
  };
  video.timeline[223] = () => {
    /* 223 : Magie de Florent. */
    document.querySelectorAll('#scene > *:not(.g_florent)').forEach((element) => {
      Video.toggleVisibility(element, 'none');
    });
    video.executeAnimation('.florent', 'magie_florent 3s ease-in forwards', 223);
  };
  video.timeline[225] = () => {
    /* 225 : Théo trouve comment sortir du trou tout seul pendant le sort. */
    Video.toggleVisibility(document.querySelector('.trou_chemin_foret'), 'none');
    document.querySelector('.g_theo').style.transform = 'translate(8565px, 825px)';
  };
  video.timeline[226] = () => {
    /* 226 : Magie de Florent finie. */
    document.querySelectorAll('#scene > *:not(.g_florent)').forEach((element) => {
      Video.toggleVisibility(element, 'block');
    });
    video.unexecuteAnimation('.florent', 'scale(3)');
    document.querySelectorAll('#scene').forEach((element) => {
      element.style.transform = 'scale(1) translate(-7680px)';
    });
  };
  video.timeline[227] = () => {
    /* 227 : Zoom brutal sur Raphaël qui est vraiment trop ravi par ce spectacle. */
    document.querySelectorAll('.raphael .bouche_deux').forEach((element) => {
      Video.toggleVisibility(element, 'block');
    });
    document.querySelectorAll('#scene').forEach((element) => {
      element.style.transform = 'scale(15) translateX(-7720px) translateY(150px)';
    });
  };
  video.timeline[233] = () => {
    /* 233 : Dezoom brutal parce que tout le monde en a marre. */
    document.querySelectorAll('.raphael .bouche_deux').forEach((element) => {
      Video.toggleVisibility(element, 'none');
    });
    document.querySelectorAll('#scene').forEach((element) => {
      element.style.transform = 'scale(1) translate(-7680px)';
    });
  };
  video.timeline[238] = () => {
    /* 238 : Georges est guilleret. */
    video.executeAnimation('.georges', 'animation_joie 0.5s linear', 238);
    video.executeAnimation('.georges .jambe_gauche', 'joie_jambe_gauche 0.5s linear', 238);
    video.executeAnimation('.georges .jambe_droite', 'joie_jambe_droite 0.5s linear', 238);
  };
  video.timeline[239] = () => {
    /* 239 : Georges n'est plus guilleret. Maxime parle. */
    video.unexecuteAnimation('.georges', 'scale(3) rotate(0)');
    video.unexecuteAnimation('.georges .jambe_gauche', 'rotate(0)');
    video.unexecuteAnimation('.georges .jambe_droite', 'rotate(0)');
    document.querySelectorAll('.maxime .bouche').forEach((element) => {
      Video.toggleVisibility(element, 'none');
    });
    document.querySelectorAll('.maxime .bouche_deux').forEach((element) => {
      Video.toggleVisibility(element, 'block');
    });
  };
  video.timeline[240] = () => {
    /* 240 : Maxime parle plus. */
    document.querySelectorAll('.maxime .bouche_deux').forEach((element) => {
      Video.toggleVisibility(element, 'none');
    });
    document.querySelectorAll('.maxime .bouche').forEach((element) => {
      Video.toggleVisibility(element, 'block');
    });
  };
  video.timeline[243] = () => {
    /* 243 : Cinquième strophe. Placement de Corentin. */
    document.querySelectorAll('.raphael .bouche_deux').forEach((element) => {
      Video.toggleVisibility(element, 'block');
    });
    document.querySelectorAll('.maxime .bouche').forEach((element) => {
      Video.toggleVisibility(element, 'none');
    });
    document.querySelectorAll('.maxime .bouche_deux').forEach((element) => {
      Video.toggleVisibility(element, 'block');
    });
    video.executeAnimation('#scene', 'deplacement_decor_4 16s linear', 243);
    video.executeAnimation('.decor > rect', 'non_deplacement_decor_4 16s linear', 243);
    video.executeAnimation('.g_georges', 'marche_georges_4 16s linear', 243);
    video.executeAnimation('.georges .jambe_droite', `marche_jambe_droite ${audio.beat * 2}s linear infinite`, 243);
    video.executeAnimation('.georges .jambe_gauche', `marche_jambe_gauche ${audio.beat * 2}s linear infinite`, 243);
    video.executeAnimation('.g_raphael', 'marche_raphael_3 16s linear', 243);
    video.executeAnimation('.raphael', `animation_joie ${audio.beat * 2}s linear infinite`, 243);
    video.executeAnimation('.g_maxime', 'marche_maxime_2 16s linear', 243);
    video.executeAnimation('.g_theo', 'marche_theo_1 16s linear', 243);
    video.executeAnimation('.theo', `sautillement_theo ${audio.beat * 4}s linear infinite`, 243);
    video.executeAnimation('.g_florent', 'marche_florent_2 16s linear', 243);
    video.executeAnimation('.florent .jambe_droite', `marche_jambe_droite ${audio.beat * 2}s linear infinite`, 243);
    video.executeAnimation('.florent .jambe_gauche', `marche_jambe_gauche ${audio.beat * 2}s linear infinite`, 243);
    document.querySelectorAll('.corentin').forEach((element) => {
      Video.toggleVisibility(element, 'block');
    });
  };
  video.timeline[259] = () => {
    /* 259 : Fin de la cinquième strophe. Tout le monde dit bonjour à Corentin. */
    video.unexecuteAnimation('.g_georges', 'translate(10525px, 805px)');
    video.unexecuteAnimation('.georges .jambe_droite', 'translate(0px, 0px)');
    video.unexecuteAnimation('.georges .jambe_gauche', 'translate(0px, 0px)');
    video.unexecuteAnimation('.g_raphael', 'translate(10300px, 765px)');
    video.unexecuteAnimation('.raphael', 'scale(3) rotate(0)');
    video.unexecuteAnimation('.g_maxime', 'translate(10125px, 790px) scale(-1, 1)');
    video.unexecuteAnimation('.g_theo', 'translate(9850px, 825px)');
    video.unexecuteAnimation('.theo', 'scale(3) translate(0px, 0px)');
    video.unexecuteAnimation('.g_florent', 'translate(9675px, 805px)');
    video.unexecuteAnimation('.florent .jambe_droite', 'translate(0px, 0px)');
    video.unexecuteAnimation('.florent .jambe_gauche', 'translate(0px, 0px)');
    video.unexecuteAnimation('.decor > rect', 'translate(9600px, 0)');
    video.unexecuteAnimation('#scene', 'translate(-9600px, 0)');
  };
  video.timeline[261] = () => {
    /* 261 : Tout le monde a dit bonjour, Corentin est horrifié. */
    document.querySelectorAll('.raphael .bouche_deux').forEach((element) => {
      Video.toggleVisibility(element, 'none');
    });
    document.querySelectorAll('.maxime .bouche_deux').forEach((element) => {
      Video.toggleVisibility(element, 'none');
    });
    document.querySelectorAll('.maxime .bouche').forEach((element) => {
      Video.toggleVisibility(element, 'block');
    });
    document.querySelectorAll('.corentin .bouche_ouverte').forEach((element) => {
      element.style.transform = 'rotateX(0deg)';
    });
  };
  video.timeline[262] = () => {
    /* 262 : Corentin a cessé d'être horrifié. Maxime s'interroge néanmoins. */
    document.querySelectorAll('.corentin .bouche_ouverte').forEach((element) => {
      element.style.transform = 'rotateX(90deg)';
    });
    document.querySelectorAll('.maxime .bouche').forEach((element) => {
      Video.toggleVisibility(element, 'none');
    });
    document.querySelectorAll('.maxime .bouche_deux').forEach((element) => {
      Video.toggleVisibility(element, 'block');
    });
  };
  video.timeline[264] = () => {
    /* 264 : Maxime s'est interrogé. */
    document.querySelectorAll('.maxime .bouche_deux').forEach((element) => {
      Video.toggleVisibility(element, 'none');
    });
    document.querySelectorAll('.maxime .bouche').forEach((element) => {
      Video.toggleVisibility(element, 'block');
    });
  };
  video.timeline[268] = () => {
    /* 268 : Corentin répond. */
    document.querySelectorAll('.corentin .bouche_ouverte').forEach((element) => {
      element.style.transform = 'rotateX(0deg)';
    });
  };
  video.timeline[269] = () => {
    /* 269 : Corentin a répondu. */
    document.querySelectorAll('.corentin .bouche_ouverte').forEach((element) => {
      element.style.transform = 'rotateX(90deg)';
    });
  };
  video.timeline[274] = () => {
    /* 274 : Corentin répond encore. */
    document.querySelectorAll('.corentin .bouche_ouverte').forEach((element) => {
      element.style.transform = 'rotateX(0deg)';
    });
  };
  video.timeline[275] = () => {
    /* 275 : Corentin a encore répondu et se déplace désormais. Raphaël est exagérément content. */
    document.querySelectorAll('.corentin .bouche_ouverte').forEach((element) => {
      element.style.transform = 'rotateX(90deg)';
    });
    document.querySelectorAll('.raphael .bouche_deux').forEach((element) => {
      Video.toggleVisibility(element, 'block');
    });
    video.executeAnimation('.g_corentin', 'corentin_cache_cache 4s linear', 275);
  };
  video.timeline[279] = () => {
    /* 279 : Sixième strophe, c'est la fête, il y a de la neige. */
    video.unexecuteAnimation('.g_corentin', 'translate(11450px, 925px) rotate(90deg)');
    document.querySelectorAll('.maxime .bouche').forEach((element) => {
      Video.toggleVisibility(element, 'none');
    });
    document.querySelectorAll('.maxime .bouche_deux').forEach((element) => {
      Video.toggleVisibility(element, 'block');
    });
    document.querySelectorAll('.corentin .bouche_ouverte').forEach((element) => {
      element.style.transform = 'rotateX(0deg)';
    });
    video.executeAnimation('#scene', 'deplacement_decor_5 16s linear', 279);
    video.executeAnimation('.decor > rect', 'non_deplacement_decor_5 16s linear', 279);
    video.executeAnimation('.g_georges', 'marche_georges_5 16s linear', 279);
    video.executeAnimation('.georges .jambe_droite', `marche_jambe_droite ${audio.beat * 2}s linear infinite`, 279);
    video.executeAnimation('.georges .jambe_gauche', `marche_jambe_gauche ${audio.beat * 2}s linear infinite`, 279);
    video.executeAnimation('.g_raphael', 'marche_raphael_4 16s linear', 279);
    video.executeAnimation('.raphael', `animation_joie ${audio.beat * 2}s linear infinite`, 279);
    video.executeAnimation('.g_maxime', 'marche_maxime_3 16s linear', 279);
    video.executeAnimation('.g_theo', 'marche_theo_2 16s linear', 279);
    video.executeAnimation('.theo', `sautillement_theo ${audio.beat * 4}s linear infinite`, 279);
    video.executeAnimation('.g_florent', 'marche_florent_3 16s linear', 279);
    video.executeAnimation('.florent .jambe_droite', `marche_jambe_droite ${audio.beat * 2}s linear infinite`, 279);
    video.executeAnimation('.florent .jambe_gauche', `marche_jambe_gauche ${audio.beat * 2}s linear infinite`, 279);
    video.executeAnimation('.g_corentin', 'marche_corentin_1 16s linear', 279);
    document.querySelectorAll('.flocon').forEach((element) => {
      element.style.animationPlayState = 'running';
    });
  };
  video.timeline[295] = () => {
    /* 295 : Fin de la sixième strophe. */
    document.querySelectorAll('.raphael .bouche_deux').forEach((element) => {
      Video.toggleVisibility(element, 'none');
    });
    document.querySelectorAll('.maxime .bouche_deux').forEach((element) => {
      Video.toggleVisibility(element, 'none');
    });
    document.querySelectorAll('.maxime .bouche').forEach((element) => {
      Video.toggleVisibility(element, 'block');
    });
    document.querySelectorAll('.corentin .bouche_ouverte').forEach((element) => {
      element.style.transform = 'rotateX(90deg)';
    });
    video.unexecuteAnimation('.g_georges', 'translate(12545px, 805px)');
    video.unexecuteAnimation('.georges .jambe_droite', 'translate(0px, 0px)');
    video.unexecuteAnimation('.georges .jambe_gauche', 'translate(0px, 0px)');
    video.unexecuteAnimation('.g_raphael', 'translate(12320px, 765px)');
    video.unexecuteAnimation('.raphael', 'scale(3) rotate(0)');
    video.unexecuteAnimation('.g_maxime', 'translate(12145px, 790px) scale(-1, 1)');
    video.unexecuteAnimation('.g_theo', 'translate(11870px, 825px)');
    video.unexecuteAnimation('.theo', 'scale(3) translate(0px, 0px)');
    video.unexecuteAnimation('.g_florent', 'translate(11700px, 805px)');
    video.unexecuteAnimation('.florent .jambe_droite', 'translate(0px, 0px)');
    video.unexecuteAnimation('.florent .jambe_gauche', 'translate(0px, 0px)');
    video.unexecuteAnimation('.g_corentin', 'translate(11590px, 805px) scale(-1, 1)');
    video.unexecuteAnimation('.decor > rect', 'translate(11520px, 0)');
    video.unexecuteAnimation('#scene', 'translate(-11520px, 0)');
  };
  video.timeline[301] = () => {
    /* 301 : C'EST NOËL HAA. */
    document.querySelectorAll('.raphael .bouche_deux').forEach((element) => {
      Video.toggleVisibility(element, 'block');
    });
    document.querySelectorAll('.maxime .bouche').forEach((element) => {
      Video.toggleVisibility(element, 'none');
    });
    document.querySelectorAll('.maxime .bouche_deux').forEach((element) => {
      Video.toggleVisibility(element, 'block');
    });
    document.querySelectorAll('.corentin .bouche_ouverte').forEach((element) => {
      element.style.transform = 'rotateX(0deg)';
    });
    video.executeAnimation('.georges', `animation_joie ${audio.beat * 2}s linear infinite`, 301);
    video.executeAnimation('.georges .jambe_gauche', `joie_jambe_gauche ${audio.beat * 2}s linear infinite`, 301);
    video.executeAnimation('.georges .jambe_droite', `joie_jambe_droite ${audio.beat * 2}s linear infinite`, 301);
    video.executeAnimation('.raphael', `animation_joie ${audio.beat * 2}s linear infinite`, 301);
    video.executeAnimation('.maxime', `animation_joie ${audio.beat * 2}s linear infinite`, 301);
    video.executeAnimation('.theo', `animation_joie ${audio.beat * 2}s linear infinite`, 301);
    video.executeAnimation('.florent', `animation_joie ${audio.beat * 2}s linear infinite`, 301);
    video.executeAnimation('.decor > rect', 'disparition_foret 16s linear', 301);
    video.executeAnimation('.decor, .chemin_foret', `sautillement_scene ${audio.beat * 2}s linear infinite`, 301);
    document.querySelectorAll('.arbre').forEach((arbre, i) => {
      window.videoCache.promenade.specialAnimationsTimeouts[i] = setTimeout(() => {
        Video.toggleVisibility(arbre, 'none');
      }, Utils.getRandomInt(16) * 1000);
    });
    video.executeAnimation('.g_georges', 'noel_g_georges 16s linear infinite', 301);
    video.executeAnimation('.g_raphael', 'noel_g_raphael 16s linear infinite', 301);
    video.executeAnimation('.g_maxime', 'noel_g_maxime 16s linear infinite', 301);
    video.executeAnimation('.g_theo', 'noel_g_theo 16s linear infinite', 301);
    video.executeAnimation('.g_florent', 'noel_g_florent 16s linear infinite', 301);
    video.executeAnimation('.corentin', 'sautillement 16s linear', 301);
  };
  video.timeline[317] = () => {
    /* 317 : C'est plus Noël. */
    for (let i = 0; i < window.videoCache.promenade.specialAnimationsIntervals.length; i += 1) {
      clearInterval(window.videoCache.promenade.specialAnimationsIntervals[i]);
    }
    for (let i = 0; i < window.videoCache.promenade.specialAnimationsTimeouts.length; i += 1) {
      clearTimeout(window.videoCache.promenade.specialAnimationsTimeouts[i]);
    }
    document.querySelectorAll('.arbre').forEach((arbre) => {
      Video.toggleVisibility(arbre, 'none');
    });
    document.querySelectorAll('.raphael .bouche_deux').forEach((element) => {
      Video.toggleVisibility(element, 'none');
    });
    document.querySelectorAll('.maxime .bouche_deux').forEach((element) => {
      Video.toggleVisibility(element, 'none');
    });
    document.querySelectorAll('.maxime .bouche').forEach((element) => {
      Video.toggleVisibility(element, 'block');
    });
    document.querySelectorAll('.corentin .bouche_ouverte').forEach((element) => {
      element.style.transform = 'rotateX(90deg)';
    });
    video.unexecuteAnimation('.georges', 'scale(3) rotate(0)');
    video.unexecuteAnimation('.georges .jambe_gauche', 'rotate(0)');
    video.unexecuteAnimation('.georges .jambe_droite', 'rotate(0)');
    video.unexecuteAnimation('.raphael', 'scale(3) rotate(0)');
    video.unexecuteAnimation('.maxime', 'scale(3) rotate(0)');
    video.unexecuteAnimation('.theo', 'scale(3) rotate(0)');
    video.unexecuteAnimation('.florent', 'scale(3) rotate(0)');
    video.unexecuteAnimation('.florent .jambe_gauche', 'rotate(0)');
    video.unexecuteAnimation('.florent .jambe_droite', 'rotate(0)');
    video.unexecuteAnimation('.decor, .chemin_foret', '');
    document.querySelector('.decor > rect').setAttribute('fill', '#fff');
    document.querySelectorAll('.georgeslasaucissepointfr').forEach((element) => {
      Video.toggleVisibility(element, 'block');
    });
    video.unexecuteAnimation('.g_georges', 'translate(12545px, 805px)');
    video.unexecuteAnimation('.g_raphael', 'translate(12320px, 765px)');
    video.unexecuteAnimation('.g_maxime', 'translate(12145px, 790px) scale(-1, 1)');
    video.unexecuteAnimation('.g_theo', 'translate(11870px, 825px)');
    video.unexecuteAnimation('.g_florent', 'translate(11700px, 805px)');
    video.unexecuteAnimation('.corentin', 'scale(3) translate(0px, 0px)');
    video.executeAnimation('.georgeslasaucissepointfr', 'georgeslasaucissepointfr 8s linear forwards', 318);
  };
  video.timeline[326] = () => {
    /* 326 : Fin de la vidéo. */
    video.unexecuteAnimation('.georgeslasaucissepointfr', 'translate(10000px, 100px)');
    document.querySelectorAll('.georgeslasaucissepointfr').forEach((element) => {
      Video.toggleVisibility(element, 'none');
    });
  };
});

export default video;
