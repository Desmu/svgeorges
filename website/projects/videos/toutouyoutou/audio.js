import Audio from '../../../src/components/Video/Audio.js';

/** Premier kick. */
const kick = new Tone.MembraneSynth({
  envelope: {
    sustain: 0,
    attack: 0.02,
    decay: 0.8,
  },
}).toDestination();
kick.volume.value = -10;

/** Deuxième kick. */
const kick2 = new Tone.MembraneSynth({
  envelope: {
    sustain: 0,
    attack: 0.02,
    decay: 0.8,
  },
}).toDestination();
kick2.volume.value = -10;

/** Piano. */
const piano = new Tone.PolySynth(Tone.Synth, {
  oscillator: {
    partials: [0, 2, 3, 4],
  },
}).toDestination();
piano.volume.value = -10;
piano.loop = true;

Tone.Transport.bpm.value = 140;

/** Rythme de la musique accompagnant la vidéo en battements par minute. */
const beat = 60 / Tone.Transport.bpm.value;

/** Contenu audio de la vidéo. */
const audio = new Audio(beat, 23, 7, beat * 64);
audio.instruments = [
  {
    tool: (new Tone.Loop(((time) => {
      kick.triggerAttackRelease('C2', '8n', time);
    }), '2n')),
    start: audio.strophesStart,
    stop: audio.strophesStart + audio.strophesDuration * 7,
  },
  {
    tool: (new Tone.Loop(((time) => {
      kick2.triggerAttackRelease('C4', '8n', time);
    }), '4n')),
    start: audio.strophesStart,
    stop: audio.strophesStart + audio.strophesDuration * 7,
  },
  {
    tool: (new Tone.Sequence(((time, note) => {
      if (note) {
        piano.triggerAttackRelease(note, '8n', time);
      }
    }), [
      'G4', 'G4', 'F4', 'G4', '', '', '', '',
      'G4', 'G4', 'F4', 'G4', '', '', '', 'E4',
      'F4', '', 'F4', '', 'F4', '', 'G4', '',
      'G4', 'G4', 'F4', 'G4', '', '', '', '',

      'G4', 'G4', 'F4', 'G4', '', '', '', '',
      'G4', 'G4', 'F4', 'G4', '', '', '', 'E4',
      'F4', '', 'F4', '', 'F4', '', 'G4', '',
      'A4', 'A4', 'G4', 'A4', '', '', '', 'A4',

      'B4', '', 'B4', '', 'B4', '', 'C5', '',
      'B4', 'B4', 'A4', 'B4', '', '', '', 'F4',
      'G4', '', 'G4', '', 'G4', '', 'A4', '',
      'F4', 'F4', ['E4', 'D4'], 'F4', '', '', '', 'A4',

      'B4', '', 'B4', '', 'B4', '', 'C5', '',
      'B4', 'B4', 'A4', 'B4', '', '', '', 'F4',
      'G4', '', 'G4', '', 'G4', '', 'A4', '',
      'E4', 'E4', ['D4', 'C4'], 'D4', '', '', '', '',

    ], '8n')),
    start: audio.strophesStart,
    stop: audio.strophesStart + audio.strophesDuration * 6 + audio.strophesDuration / 2,
  },
  {
    tool: (new Tone.Sequence(((time, note) => {
      if (note) {
        piano.triggerAttackRelease(note, '8n', time);
      }
    }), ['G4', '', 'F4', '', 'G4', '', 'F4', ''], '8n')),
    start: audio.strophesStart + audio.strophesDuration * 6 + audio.strophesDuration / 2,
    stop: audio.strophesStart + audio.strophesDuration * 7,
  },
];

export default audio;
