import Utils from '../../../src/js/Utils.js';
import Subtitles from '../../../src/components/Video/Subtitles.js';
import Video from '../../../src/components/Video/Video.js';
import audio from './audio.js';

/** Fichiers SVG à charger. */
const svgPaths = [
  './assets/components/Georges/Georges.svg',
  './assets/vectors/characters/gugusse.svg',
  './assets/vectors/characters/gugusse.svg',
  './assets/vectors/characters/gugusse.svg',
  './assets/vectors/characters/florent.svg',
  './assets/vectors/characters/theo.svg',
  './assets/vectors/characters/corentin.svg',
  './assets/vectors/characters/raphael.svg',
  './assets/vectors/characters/maxime.svg',
  './assets/vectors/misc/camion_pompiers.svg',
  './assets/vectors/text/georgeslasaucissepointfr.svg',
  './assets/vectors/backgrounds/video/sol.svg',
];

/** Contenu de la vidéo. */
const video = new Video(svgPaths, (new Subtitles('./projects/videos/toutouyoutou/subtitles.vtt')), () => {
  /* Création du pattern de cercles présent dans le décor.
     Utiliser plutôt une balise <pattern> ferait perdre la compatibilité Firefox. */
  for (let i = 0; i < 90; i += 1) {
    const x = (i % 10) * 192;
    const y = parseInt(i / 10, 10) * 108;
    const circleGroupElem = document.createElementNS('http://www.w3.org/2000/svg', 'g');
    circleGroupElem.innerHTML = "<circle fill='#000' cx='20' cy='15' r='1'/>";
    circleGroupElem.setAttribute('transform', `translate(${x}, ${y})`);
    circleGroupElem.firstChild.style.animation = 'none';
    circleGroupElem.firstChild.style.transform = 'translateX(0) translateY(0) scale(1)';
    document.querySelectorAll('.decor')[0].appendChild(circleGroupElem);
  }
  video.raphael = document.querySelector('.raphael').innerHTML;
}, () => {
  video.timeline = [];
  video.timeline[0] = () => {
    document.querySelectorAll('#scene, .decor circle, .g_florent, .g_theo, .g_corentin, .g_raphael, .g_maxime, .g_camion_pompiers, .georgeslasaucissepointfr, .georges, .florent, .theo, .corentin, .raphael, .maxime, .camion_pompiers, .georges .jambe_gauche, .florent .jambe_gauche, .georges .jambe_droite, .florent .jambe_droite, .corentin .bouche_ouverte, .gugusse, .gugusse .bras_gauche, .gugusse .bras_droit, .gugusse .jambe_gauche, .gugusse .jambe_droite').forEach((element) => {
      element.style.animation = 'none';
    });
    /* 0 : Initialisation de tout. */
    video.animations = [];
    document.querySelectorAll('#scene').forEach((element) => {
      element.style.transform = 'scale(4)';
      element.style.transformOrigin = '680px 880px';
    });
    document.querySelectorAll('.decor').forEach((element) => {
      Video.toggleVisibility(element, 'block');
    });
    document.querySelectorAll('.decor circle').forEach((element) => {
      element.style.transform = 'translateX(0) translateY(0) scale(1)';
    });

    document.querySelector('.g_sol').style.transform = 'translate(0px, 980px)';
    document.querySelector('.g_georges').style.transform = 'translate(680px, 705px)';
    document.querySelector('.g_florent').style.transform = 'translate(-200px, 705px)';
    document.querySelector('.g_theo').style.transform = 'translate(-240px, 720px)';
    document.querySelector('.g_corentin').style.transform = 'translate(1920px, 800px)';
    document.querySelector('.g_raphael').style.transform = 'translate(-280px, 770px)';
    document.querySelector('.g_maxime').style.transform = 'translate(1920px, 680px)';
    document.querySelector('.g_camion_pompiers').style.transform = 'translate(3000px, 280px) rotateY(180deg)';
    document.querySelectorAll('.georgeslasaucissepointfr').forEach((element) => {
      element.style.transform = 'translate(0px, 0px)';
    });

    document.querySelectorAll('.georges').forEach((element) => {
      Video.toggleVisibility(element, 'block');
    });
    document.querySelectorAll('.florent, .theo, .corentin, .raphael, .maxime, .bouche_deux, .camion_pompiers, .georgeslasaucissepointfr').forEach((element) => {
      Video.toggleVisibility(element, 'none');
    });
    document.querySelectorAll('.georges, .florent, .theo, .corentin, .raphael, .maxime, .camion_pompiers').forEach((element) => {
      element.style.transform = 'scale(3) translateY(0)';
    });
    document.querySelectorAll('.georges .jambe_gauche, .florent .jambe_gauche').forEach((element) => {
      element.style.transformOrigin = '15px 60px';
    });
    document.querySelectorAll('.georges .jambe_droite, .florent .jambe_droite').forEach((element) => {
      element.style.transformOrigin = '30px 60px';
    });
    document.querySelectorAll('.corentin .bouche_ouverte').forEach((element) => {
      element.style.transform = 'rotateX(90deg)';
      element.style.transformOrigin = '15px 30px';
    });
    for (let i = 0; i < window.videoCache.toutouyoutou.specialAnimationsIntervals.length; i += 1) {
      clearInterval(window.videoCache.toutouyoutou.specialAnimationsIntervals[i]);
    }
    for (let i = 0; i < window.videoCache.toutouyoutou.specialAnimationsTimeouts.length; i += 1) {
      clearTimeout(window.videoCache.toutouyoutou.specialAnimationsTimeouts[i]);
    }
    document.querySelectorAll('.raphael').forEach((element) => {
      element.innerHTML = video.raphael;
      element.style.transformOrigin = '45px 65px';
    });
    document.querySelectorAll('.raphael .bras_gauche, .raphael .bras_droit, .raphael .jambe_gauche, .raphael .jambe_droite').forEach((element) => {
      element.style.transformOrigin = '50px 50px';
    });
    document.querySelectorAll('.maxime .bouche').forEach((element) => {
      Video.toggleVisibility(element, 'block');
    });

    document.querySelectorAll('.gugusse').forEach((element) => {
      Video.toggleVisibility(element, 'block');
      element.style.transform = 'scale(3) translateY(0)';
    });
    document.querySelectorAll('.g_gugusse').forEach((element, index) => {
      element.setAttribute('transform', `translate(${200 * index + 1080}, 725)`);
    });
    document.querySelectorAll('.gugusse .bras_gauche, .gugusse .bras_droit').forEach((element) => {
      element.style.transform = 'rotate(0)';
      element.style.transformOrigin = '15px 30px';
    });
    document.querySelectorAll('.gugusse .jambe_gauche, .gugusse .jambe_droite').forEach((element) => {
      element.style.transform = 'rotate(0)';
      element.style.transformOrigin = '15px 60px';
    });

    Video.toggleVisibility(document.querySelector('#loading'), 'none');
  };
  video.timeline[4] = () => {
    /* 4 : Dezoom de Georges vers la scène. */
    video.executeAnimation('#scene', 'dezoom 1s forwards', 4);
  };
  video.timeline[5] = () => {
    /* 5 : Fin du dezoom. */
    video.unexecuteAnimation('#scene', 'scale(1)');
  };
  video.timeline[16] = () => {
    /* 16 : Les gugusses gardent les bras le long du corps. */
    video.executeAnimation('.gugusse .bras_gauche', 'rotatebrasgauche 0.5s forwards', 16);
    video.executeAnimation('.gugusse .bras_droit', 'rotatebrasdroit 0.5s forwards', 16);
  };
  video.timeline[17] = () => {
    /* 17 : Fin de l'animation des bras. */
    video.unexecuteAnimation('.gugusse .bras_gauche', 'rotate(-135deg)');
    video.unexecuteAnimation('.gugusse .bras_droit', 'rotate(135deg)');
  };
  video.timeline[23] = () => {
    /* 23 : Lancement de la musique, du trottinage et du décor. */
    video.executeAnimation('.georges, .gugusse', `trottinage ${audio.beat}s ease-in infinite`, 23);
    video.executeAnimation('.georges .jambe_gauche, .gugusse .jambe_gauche', `trottinage_jambe_gauche ${audio.beat * 2}s linear infinite`, 23);
    video.executeAnimation('.georges .jambe_droite, .gugusse .jambe_droite', `trottinage_jambe_droite ${audio.beat * 2}s linear infinite`, 23, audio.beat);
    video.executeAnimation('.decor circle', `circles ${audio.beat * 8}s linear infinite`, 23);
  };
  video.timeline[43] = () => {
    /* 43 : Arrivée de Florent. */
    document.querySelectorAll('.florent').forEach((element) => {
      Video.toggleVisibility(element, 'block');
    });
    video.executeAnimation('.g_florent', `marche_florent ${audio.beat * 16}s ease-in forwards`, 43);
    video.executeAnimation('.florent .jambe_droite', `marche_jambe_droite ${audio.beat * 2}s linear infinite`, 43);
    video.executeAnimation('.florent .jambe_gauche', `marche_jambe_gauche ${audio.beat * 2}s linear infinite`, 43);
  };
  video.timeline[50] = (skip = false) => {
    /* 50 : Lancement de la deuxième strophe, les exercices flexi-jambiers. */
    const delay = audio.strophesIndexes[1] - 50;
    let delayskip = delay;
    if (skip) {
      delayskip = 0;
    }
    video.unexecuteAnimation('.georges, .gugusse', 'scale(3) translate(0px, 0px)');
    video.unexecuteAnimation('.georges .jambe_gauche, .gugusse .jambe_gauche', 'rotate(0)');
    video.unexecuteAnimation('.georges .jambe_droite, .gugusse .jambe_droite', 'rotate(0)');
    video.unexecuteAnimation('.g_florent', 'translate(280px, 705px)');
    video.unexecuteAnimation('.florent .jambe_droite', 'translate(0px, 0px)');
    video.unexecuteAnimation('.florent .jambe_gauche', 'translate(0px, 0px)');
    video.executeAnimation('.georges .jambe_gauche, .florent .jambe_gauche, .gugusse .jambe_gauche', `flexi_jambe_gauche ${audio.beat * 64}s linear forwards`, (audio.strophesIndexes[1] + delayskip));
    video.executeAnimation('.georges .jambe_droite, .florent .jambe_droite, .gugusse .jambe_droite', `flexi_jambe_droite ${audio.beat * 64}s linear forwards`, (audio.strophesIndexes[1] + delayskip));
  };
  video.timeline[77] = (skip = false) => {
    /* 77 : Lancement de la troisième strophe, Florent s'en va. */
    const delay = audio.strophesIndexes[2] - 77;
    let delayskip = delay;
    if (skip) {
      delayskip = 0;
    }
    video.unexecuteAnimation('.georges .jambe_droite, .florent .jambe_droite, .gugusse .jambe_droite', 'rotate(-270deg)');
    video.unexecuteAnimation('.georges .jambe_gauche, .florent .jambe_gauche, .gugusse .jambe_gauche', 'rotate(270deg)');
    video.executeAnimation('.georges, .gugusse', `trottinage ${audio.beat}s ease-in infinite`, audio.strophesIndexes[2]);
    video.executeAnimation('.georges .jambe_gauche, .gugusse .jambe_gauche', `trottinage_jambe_gauche ${audio.beat * 2}s linear infinite`, (audio.strophesIndexes[2] + delayskip));
    video.executeAnimation('.georges .jambe_droite, .gugusse .jambe_droite', `trottinage_jambe_droite ${audio.beat * 2}s linear infinite`, (audio.strophesIndexes[2] + delayskip), audio.beat);
    video.executeAnimation('.g_florent', `marche_florent ${audio.beat * 16}s ease-in reverse forwards`, audio.strophesIndexes[2]);
    video.executeAnimation('.florent .jambe_droite', `marche_jambe_droite_cassee ${audio.beat * 2}s linear infinite`, (audio.strophesIndexes[2] + delayskip));
    video.executeAnimation('.florent .jambe_gauche', `marche_jambe_gauche_cassee ${audio.beat * 2}s linear infinite`, (audio.strophesIndexes[2] + delayskip));
  };
  video.timeline[83] = () => {
    /* 83 : Florent est sorti de l'écran. */
    video.unexecuteAnimation('.g_florent', 'translate(-200px, 705px)');
    video.unexecuteAnimation('.florent .jambe_droite', 'translate(0px, 0px) rotate(-270deg)');
    video.unexecuteAnimation('.florent .jambe_gauche', 'translate(0px, 0px) rotate(270deg)');
    document.querySelectorAll('.florent').forEach((element) => {
      Video.toggleVisibility(element, 'none');
    });
  };
  video.timeline[94] = () => {
    /* 94 : Théo arrive à l'écran. */
    document.querySelectorAll('.theo').forEach((element) => {
      Video.toggleVisibility(element, 'block');
    });
    video.executeAnimation('.g_theo', `marche_theo ${audio.beat * 16}s ease-in forwards`, 94);
  };
  video.timeline[105] = (skip = false) => {
    /* 105 : Lancement de la quatrième strophe, les exercices flexi-torsiers. */
    const delay = audio.strophesIndexes[3] - 105;
    let delayskip = delay;
    if (skip) {
      delayskip = 0;
    }
    video.unexecuteAnimation('.g_theo', 'rotateX(0deg) translate(240px, 720px)');
    video.unexecuteAnimation('.georges, .gugusse', 'scale(3) translate(0px, 0px)');
    video.unexecuteAnimation('.georges .jambe_gauche, .gugusse .jambe_gauche', 'rotate(0)');
    video.unexecuteAnimation('.georges .jambe_droite, .gugusse .jambe_droite', 'rotate(0)');
    video.executeAnimation('.georges, .gugusse, .theo', `flexi_torse ${audio.beat * 64}s linear forwards`, (audio.strophesIndexes[3] + delayskip));
  };
  video.timeline[132] = (skip = false) => {
    /* 132 : Lancement de la cinquième strophe, Théo s'en va. */
    const delay = audio.strophesIndexes[4] - 132;
    let delayskip = delay;
    if (skip) {
      delayskip = 0;
    }
    video.unexecuteAnimation('.georges, .gugusse, .theo', 'scale(3)');
    video.executeAnimation('.georges, .gugusse', `trottinage ${audio.beat}s ease-in infinite`, audio.strophesIndexes[4]);
    video.executeAnimation('.georges .jambe_gauche, .gugusse .jambe_gauche', `trottinage_jambe_gauche ${audio.beat * 2}s linear infinite`, (audio.strophesIndexes[4] + delayskip));
    video.executeAnimation('.georges .jambe_droite, .gugusse .jambe_droite', `trottinage_jambe_droite ${audio.beat * 2}s linear infinite`, (audio.strophesIndexes[4] + delayskip), audio.beat);
    video.executeAnimation('.g_theo', `marche_theo ${audio.beat * 16}s ease-in reverse forwards`, (audio.strophesIndexes[4] + delayskip));
  };
  video.timeline[138] = () => {
    /* 138 : Corentin arrive à l'écran. */
    document.querySelectorAll('.corentin').forEach((element) => {
      Video.toggleVisibility(element, 'block');
    });
    video.executeAnimation('.g_corentin', `translate_corentin ${audio.beat}s ease-in forwards`, 138);
  };
  video.timeline[139] = () => {
    /* 139 : Théo sort de l'écran, Corentin chante. */
    video.unexecuteAnimation('.g_theo', 'rotateX(0deg) translate(-240px, 720px)');
    document.querySelectorAll('.theo').forEach((element) => {
      Video.toggleVisibility(element, 'none');
    });
    video.unexecuteAnimation('.g_corentin', 'translate(1815px, 800px)');
    video.executeAnimation('.corentin .bouche_ouverte', `corentin_parle ${audio.beat * 32}s ease-in forwards`, 139, audio.beat);
  };
  video.timeline[153] = () => {
    /* 153 : Corentin a fini de chanter, c'est triste. */
    video.unexecuteAnimation('.corentin .bouche_ouverte', 'rotateX(90deg)');
    video.executeAnimation('.g_corentin', `translate_corentin ${audio.beat}s ease-in reverse forwards`, 153);
  };
  video.timeline[154] = () => {
    /* 154 : Corentin sort de l'écran. */
    video.unexecuteAnimation('.g_corentin', 'translate(1920px, 800px)');
    document.querySelectorAll('.corentin').forEach((element) => {
      Video.toggleVisibility(element, 'none');
    });
  };
  video.timeline[155] = () => {
    /* 155 : Raphaël arrive à l'écran. */
    document.querySelectorAll('.raphael').forEach((element) => {
      Video.toggleVisibility(element, 'block');
    });
    video.executeAnimation('.g_raphael', `marche_raphael ${audio.beat * 8}s ease-in forwards`, 155);
    video.executeAnimation('.raphael', `pivote_raphael ${audio.beat * 8}s ease-in forwards`, 155);
  };
  video.timeline[160] = (skip = false) => {
    /* 160 : Lancement de la sixième strophe, le mélange des exercices. */
    const delay = audio.strophesIndexes[5] - 160;
    let delayskip = delay;
    if (skip) {
      delayskip = 0;
    }
    video.unexecuteAnimation('.g_raphael', 'translate(360px, 770px)');
    video.unexecuteAnimation('.raphael', 'scale(3) rotate(0)');
    video.unexecuteAnimation('.georges, .gugusse', 'scale(3) translate(0px, 0px)');
    video.unexecuteAnimation('.georges .jambe_gauche, .gugusse .jambe_gauche', 'rotate(0)');
    video.unexecuteAnimation('.georges .jambe_droite, .gugusse .jambe_droite', 'rotate(0)');
    document.querySelectorAll('.raphael .bouche_deux').forEach((element) => {
      Video.toggleVisibility(element, 'block');
    });
    video.executeAnimation('.georges, .gugusse, .raphael', `melange ${audio.beat * 32}s linear forwards`, (audio.strophesIndexes[5] + delayskip));
  };
  video.timeline[174] = () => {
    /* 174 : Moitié de la sixième strophe, Raphaël perd le contrôle. */
    video.unexecuteAnimation('.georges, .gugusse, .raphael', 'scale(3)');
    video.executeAnimation('.georges, .gugusse', `trottinage ${audio.beat}s ease-in infinite`, audio.strophesIndexes[4]);
    video.executeAnimation('.georges .jambe_gauche, .gugusse .jambe_gauche', `trottinage_jambe_gauche ${audio.beat * 2}s linear infinite`, 174);
    video.executeAnimation('.georges .jambe_droite, .gugusse .jambe_droite', `trottinage_jambe_droite ${audio.beat * 2}s linear infinite`, 174, audio.beat);
    video.executeAnimation('.raphael .bras_gauche, .raphael .bras_droit, .raphael .jambe_gauche, .raphael .jambe_droite', `pivotage_membrier ${audio.beat * 2}s linear infinite`, 174);
    let time = 0;
    document.querySelectorAll('.raphael [d], .raphael [cx]').forEach((elem, i) => {
      window.videoCache.toutouyoutou.specialAnimationsTimeouts[i] = setTimeout(() => {
        window.videoCache.toutouyoutou.specialAnimationsIntervals[i] = setInterval(() => {
          const element = document.querySelectorAll('.raphael [d], .raphael [cx]')[i];
          if (element.getAttribute('d') !== undefined) {
            element.setAttribute('d', `M ${Utils.getRandomInt(100)} ${Utils.getRandomInt(100)} ${Utils.randomPath()}`);
          } else if (element.getAttribute('cx') !== undefined) {
            element.setAttribute('cx', Utils.getRandomInt(100));
            element.setAttribute('cy', Utils.getRandomInt(100));
          }
        }, (audio.beat * 1000));
      }, time);
      time += (audio.beat * 1000);
    });
  };
  video.timeline[187] = (skip = false) => {
    /* 187 : Lancement de la dernière strophe, Raphaël est cassé, Maxime arrive à l'écran. */
    const delay = audio.strophesIndexes[6] - 187;
    let delayskip = delay;
    if (skip) {
      delayskip = 0;
    }
    video.unexecuteAnimation('.georges, .gugusse', 'scale(3) translate(0px, 0px)');
    video.unexecuteAnimation('.georges .jambe_gauche, .gugusse .jambe_gauche', 'rotate(0)');
    video.unexecuteAnimation('.georges .jambe_droite, .gugusse .jambe_droite', 'rotate(0)');
    document.querySelectorAll('.maxime').forEach((element) => {
      Video.toggleVisibility(element, 'block');
    });
    video.executeAnimation('.g_maxime', `translate_maxime ${audio.beat * 8}s linear forwards`, (audio.strophesIndexes[6] + delayskip));
  };
  video.timeline[190] = () => {
    /* 190 : Maxime parle. */
    document.querySelectorAll('.maxime .bouche').forEach((element) => {
      Video.toggleVisibility(element, 'none');
    });
    document.querySelectorAll('.maxime .bouche_deux').forEach((element) => {
      Video.toggleVisibility(element, 'block');
    });
  };
  video.timeline[191] = () => {
    /* 191 : Maxime a arrêté de translater. */
    video.unexecuteAnimation('.g_maxime', 'translate(1615px, 680px)');
  };
  video.timeline[193] = () => {
    /* 193 : Maxime parle plus. */
    document.querySelectorAll('.maxime .bouche_deux').forEach((element) => {
      Video.toggleVisibility(element, 'none');
    });
    document.querySelectorAll('.maxime .bouche').forEach((element) => {
      Video.toggleVisibility(element, 'block');
    });
  };
  video.timeline[194] = () => {
    /* 194 : Maxime parle. */
    document.querySelectorAll('.maxime .bouche').forEach((element) => {
      Video.toggleVisibility(element, 'none');
    });
    document.querySelectorAll('.maxime .bouche_deux').forEach((element) => {
      Video.toggleVisibility(element, 'block');
    });
  };
  video.timeline[197] = () => {
    /* 197 : Maxime parle plus. */
    document.querySelectorAll('.maxime .bouche_deux').forEach((element) => {
      Video.toggleVisibility(element, 'none');
    });
    document.querySelectorAll('.maxime .bouche').forEach((element) => {
      Video.toggleVisibility(element, 'block');
    });
  };
  video.timeline[198] = () => {
    /* 198 : Maxime parle. */
    document.querySelectorAll('.maxime .bouche').forEach((element) => {
      Video.toggleVisibility(element, 'none');
    });
    document.querySelectorAll('.maxime .bouche_deux').forEach((element) => {
      Video.toggleVisibility(element, 'block');
    });
  };
  video.timeline[200] = () => {
    /* 200 : Maxime parle plus. */
    document.querySelectorAll('.maxime .bouche_deux').forEach((element) => {
      Video.toggleVisibility(element, 'none');
    });
    document.querySelectorAll('.maxime .bouche').forEach((element) => {
      Video.toggleVisibility(element, 'block');
    });
  };
  video.timeline[201] = () => {
    /* 201 : Les pompiers arrivent. */
    document.querySelectorAll('.camion_pompiers').forEach((element) => {
      Video.toggleVisibility(element, 'block');
    });
    video.executeAnimation('.g_camion_pompiers', `translate_camion ${audio.beat * 32}s linear forwards`, 201);
    video.executeAnimation('.camion_pompiers .pinpon', `animation_spring_left_right ${audio.beat}s linear alternate infinite`, 201);
    video.executeAnimation('.gugusse_pompier', `animation_spring_right_left ${audio.beat}s linear alternate infinite`, 201);
  };
  video.timeline[212] = () => {
    /* 212 : Tout le monde part. */
    video.executeAnimation('.g_maxime', `translate_maxime ${audio.beat * 4}s linear reverse forwards`, 212);
    video.executeAnimation('.g_raphael', `marche_raphael ${audio.beat * 4}s linear reverse forwards`, 212);
  };
  video.timeline[215] = () => {
    /* 215 : Fin de la musique. */
    video.unexecuteAnimation('.g_maxime', 'translate(1920px, 680px)');
    video.unexecuteAnimation('.camion_pompiers .pinpon', 'translate(0px, 0px)');
    video.unexecuteAnimation('.gugusse_pompier', 'translate(0px, 0px)');
    video.unexecuteAnimation('.g_camion_pompiers', 'translate(0px, 280px) rotateY(180deg)');
    video.unexecuteAnimation('.decor circle', 'translate(0px, 0px) scale(1)');
    document.querySelectorAll('.raphael, .maxime, .camion_pompiers').forEach((element) => {
      Video.toggleVisibility(element, 'none');
    });
    for (let i = 0; i < window.videoCache.toutouyoutou.specialAnimationsIntervals.length; i += 1) {
      clearInterval(window.videoCache.toutouyoutou.specialAnimationsIntervals[i]);
    }
    for (let i = 0; i < window.videoCache.toutouyoutou.specialAnimationsTimeouts.length; i += 1) {
      clearTimeout(window.videoCache.toutouyoutou.specialAnimationsTimeouts[i]);
    }
    document.querySelectorAll('.georgeslasaucissepointfr').forEach((element) => {
      Video.toggleVisibility(element, 'block');
    });
    video.executeAnimation('.georgeslasaucissepointfr', `georgeslasaucissepointfr ${audio.beat * 16}s linear forwards`, 215);
  };
  video.timeline[224] = () => {
    /* 224 : Fin de la vidéo. */
    video.unexecuteAnimation('.georgeslasaucissepointfr', 'translate(-1920px, 100px)');
    document.querySelectorAll('.georgeslasaucissepointfr').forEach((element) => {
      Video.toggleVisibility(element, 'none');
    });
  };
});

export default video;
