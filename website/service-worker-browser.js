// Installation de la page hors-ligne.
self.addEventListener('install', (e) => {
  const offlineRequest = new Request('./src/sw/offline.html');
  e.waitUntil(fetch(offlineRequest).then((response) => caches.open('offline').then((cache) => cache.put(offlineRequest, response))));
});

// Affichage de la page hors-ligne.
self.addEventListener('fetch', (e) => {
  e.request.respondWith(fetch(e.request).catch(() => caches.open('offline').then((cache) => cache.match('./src/sw/offline.html'))));
});
