# ROADMAP

- Utiliser des Web Components pour Comic.js et Video.js .
- Implanter une police d'écriture à chasse fixe maison (voir comment fonctionnent les polices SVG au préalable).

- Jouer un son différent sur chaque page (option désactivée par défaut sur browser, activée par défaut sur PWA).
- PWA : Ajouter un système de Notifications Web Push.
- PWA : Ajouter un système d'API hors ligne en IndexedDB.
