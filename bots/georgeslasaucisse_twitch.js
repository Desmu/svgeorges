import tmi from 'tmi.js';

import Utils from './src/Utils.js';
import Blague from './src/Blague.js';

/** Configuration du bot. */
const configuration = Utils.fileToJSON('../configuration.json');
/** Client tmi.js . */
const client = new tmi.client({
  connection: {
    reconnect: true,
  },
  identity: {
    username: configuration.twitch.username,
    password: configuration.twitch.password,
  },
  channels: configuration.twitch.channels,
});

client.connect().catch(console.error);

client.on('chat', (channel, user, message, isSelf) => {
  if (isSelf) {
    return;
  }
  switch (message) {
    case configuration.twitch.commands.blague:
      Blague.blague(configuration.discord, (blaguePart) => {
        client.say(channel, blaguePart.replace(/\n/g, ' '));
      });
      break;
    default:
      break;
  }
});

process.on('error', (err) => { console.log(err); });
