import Discord from 'discord.js';

import Utils from './src/Utils.js';
import Blague from './src/Blague.js';
import Video from './src/Video.js';

/** Configuration du bot. */
const configuration = Utils.fileToJSON('../configuration.json');
/** Client Discord.js . */
const client = new Discord.Client({ intents: [Discord.Intents.FLAGS.GUILDS, Discord.Intents.FLAGS.GUILD_MESSAGES] });

client.login(configuration.discord.token);

client.on('messageCreate', (msg) => {
  switch (msg.content) {
    case configuration.discord.commands.blague:
      Blague.blague(configuration.discord, (blaguePart) => {
        msg.channel.send({ content: blaguePart }).catch(console.error);
      });
      break;
    case configuration.discord.commands.blague_serie:
      Blague.blagueSerie(configuration.discord, (blaguePart) => {
        msg.channel.send({ content: blaguePart }).catch(console.error);
      });
      break;
    case configuration.discord.commands.video:
      Video.video(configuration.discord, (video) => {
        msg.channel.send({ content: `${video.nom_video} ${video.url_video}` }).catch(console.error);
      });
      break;
    default:
      break;
  }
});

process.on('error', (err) => { console.log(err); });
