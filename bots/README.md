# SVGeorges - Bots

Bots pour messageries instantanées communiquant avec l'API Georges la Saucisse.

## Description

Les points d'entrée suivants sont supportés :
- `blague` : Poste une blague au hasard. Si cette blague comporte une réponse, elle est postée plusieurs secondes plus tard.
- `blague/serie` : Poste une série de blagues au hasard, espacées de plusieurs secondes.
- `video` : Poste une vidéo au hasard.

|           | blague | blague/serie | video |
| --------- | ------ | ------------ | ----- |
| Matrix    | o      | o            | x     |
| Discord   | o      | o            | o     |
| Mastodon  | o      | x            | o     |
| Twitch    | o      | x            | x     |

## Installation

- Installer [Node.js](https://nodejs.org) en version 13 ou supérieure.
- [Télécharger les bots](https://gitlab.com/Desmu/svgeorges/-/archive/master/svgeorges-master.zip?path=bots).
- Renommer le fichier `configuration.example.json` en `configuration.json` et en modifier les paramètres (voir paragraphe `Configuration`) via un éditeur de texte autre que le Bloc-Notes (Atom, Notepad++, Visual Studio, Sublime Text, ...).

### Bot Mastodon

- Dans une fenêtre de navigation privée, créer un nouveau compte sur l'instance Mastodon de votre choix, lui donner le nom et l'avatar de votre choix.
- Dans les paramètres utilisateur, puis dans le menu `Profil > Apparence`, cliquer sur `Ceci est un robot` puis cliquer sur `Enregistrer les modifications`.
- Dans les paramètres utilisateur, puis dans le menu `Développement`, donner un nom au bot puis cliquer sur `Envoyer`.
- Dans les paramètres utilisateur, puis dans le menu `Développement`, cliquer sur le nom de votre bot, puis copier-coller `Votre jeton d'accès` dans le champ `mastodon.token` du fichier `configuration.json`.

### Bot Matrix

- Dans le dossier du projet, installer les dépendances externes avec l'une des options suivantes :
  * Exécuter le fichier `matrix_install.js` avec le programme `Node.js` (par défaut situé à l'emplacement `C:\Program Files\nodejs\node.exe` sur Windows, s'il n'est pas affiché dans la liste des programmes disponibles).
  * Exécuter la commande `npm install matrix-bot-sdk` dans le dossier du programme depuis le terminal.
- Depuis [Element](https://app.element.io/) dans une fenêtre de navigation privée, créer un nouveau compte sur l'instance Matrix de votre choix, lui donner le nom et l'avatar de votre choix.
- Dans les paramètres utilisateur, puis dans le menu `Aide et À propos`, cliquer sur `Access Token` puis copier-coller le jeton affiché dans le champ `matrix.token` du fichier `configuration.json`.
- Fermer la fenêtre de navigation privée sans vous déconnecter (la déconnexion réinitialise le jeton copié à l'étape précédente).

### Bot Discord

- Dans le dossier du projet, installer les dépendances externes avec l'une des options suivantes :
  * Exécuter le fichier `discord_install.js` avec le programme `Node.js` (par défaut situé à l'emplacement `C:\Program Files\nodejs\node.exe` sur Windows, s'il n'est pas affiché dans la liste des programmes disponibles).
  * Exécuter la commande `npm install discord.js` dans le dossier du programme depuis le terminal.
- [Créer une nouvelle application pour Discord](https://discord.com/developers/applications), lui donner le nom et l'avatar de votre choix.
- Cliquer sur l'application nouvellement créée, puis la définir comme Bot dans le menu `Bot`, puis copier-coller le jeton affiché dans le champ `discord.token` du fichier `configuration.json`.
- Dans le menu `OAuth2`, ajouter le scope `bot` et la permission `Send Messages`, puis accéder à l'URL générée pour ajouter le bot à un serveur.

### Bot Twitch

- Dans le dossier du projet, installer les dépendances externes avec l'une des options suivantes :
  * Exécuter le fichier `twitch_install.js` avec le programme `Node.js` (par défaut situé à l'emplacement `C:\Program Files\nodejs\node.exe` sur Windows, s'il n'est pas affiché dans la liste des programmes disponibles).
  * Exécuter la commande `npm install tmi.js` dans le dossier du programme depuis le terminal.
- Créer un nouveau compte Twitch, lui donner le nom et l'avatar de votre choix.
- Générer un mot de passe oAuth pour ce compte depuis la page [Twitch Chat OAuth Password Generator](https://twitchapps.com/tmi/), puis copier-coller le jeton affiché dans le champ `twitch.password` du fichier `configuration.json`.
- Copier-coller le nom du compte dans le champ `twitch.username`.

## Lancement

- Lancer le bot souhaité avec l'une des options suivantes, en remplaçant `CLIENT` par le nom du service souhaité (`mastodon`, `matrix`, `discord` ou `twitch`) :
  * Exécuter le fichier `georgeslasaucisse_CLIENT.js` avec le programme `Node.js`.
  * Exécuter la commande `node georgeslasaucisse_CLIENT.js` dans le dossier du programme depuis le terminal.

## Configuration

Les paramètres des bots sont classés par service (`mastodon`, `matrix`, `discord` ou `twitch`).

### Configuration Mastodon

- `url` : lien du serveur Mastodon sur lequel est hébergé le compte (`mastodon.social`).
- `token` : Jeton du compte à récupérer depuis Mastodon (voir `Installation > Bot Mastodon`).
- `time_before_answer` : Nombre de secondes avant que le bot ne poste la suite d'une blague comportant une réponse (`0`).

### Configuration Matrix

- `url` : lien du serveur Matrix sur lequel est hébergé le compte (`https://matrix.org/`).
- `token` : Jeton du compte à récupérer depuis Element (voir `Installation > Bot Matrix`).
- `time_before_answer` : Nombre de secondes avant que le bot ne poste la suite d'une blague comportant une réponse (`5`).
- `time_before_next_blague` : Nombre de secondes avant que le bot ne poste la blague suivante d'une série de blagues (`10`).
- `commands` : Commandes utilisées sur Discord pour interagir avec le bot (laisser une commande vide pour désactiver la fonctionnalité).

### Configuration Discord

- `token` : Jeton du bot à récupérer sur le portail développeurs Discord (voir `Installation > Bot Discord`).
- `time_before_answer` : Nombre de secondes avant que le bot ne poste la suite d'une blague comportant une réponse (`5`).
- `time_before_next_blague` : Nombre de secondes avant que le bot ne poste la blague suivante d'une série de blagues (`10`).
- `commands` : Commandes utilisées sur Discord pour interagir avec le bot (laisser une commande vide pour désactiver la fonctionnalité).

### Configuration Twitch

- `username` : Nom du compte du bot.
- `password` : Jeton du bot à récupérer sur la page de génération du mot de passe OAuth.
- `channels` : Liste des chaînes Twitch sur lesquelles connecter le bot.
- `time_before_answer` : Nombre de secondes avant que le bot ne poste la suite d'une blague comportant une réponse (`5`).
- `commands` : Commandes utilisées sur Twitch pour interagir avec le bot (laisser une commande vide pour désactiver la fonctionnalité).

## Librairies utilisées

- Matrix : [matrix-bot-sdk](https://www.npmjs.com/package/matrix-bot-sdk)
- Discord : [discord.js](https://discord.js.org/)
- Twitch : [tmi.js](https://tmijs.com/)
