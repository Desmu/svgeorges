import https from 'https';

/**
 * Classe de fonctions génériques pour l'API.
 */
export default class Api {
  /**
   * Modèle de requête envoyée à l'API.
   *
   * @param {string} path Chemin de l'API appelé.
   * @param {Function} callback Fonction de retour envoyée à la réponse.
   */
  static request(path, callback) {
    const options = {
      host: 'www.georgeslasaucisse.fr',
      path,
      headers: { 'User-Agent': 'request' },
    };
    https.get(options, (res) => {
      let json = '';
      res.on('data', (chunk) => {
        json += chunk;
      });
      res.on('end', () => {
        if (res.statusCode === 200) {
          callback(JSON.parse(json));
        } else {
          console.log(`Status : ${res.statusCode}`);
        }
      });
    }).on('error', (err) => {
      console.log(`HTTPS Error : ${err}`);
    });
  }
}
