import fs from 'fs';
import path from 'path';
import url from 'url';

/**
 * Classe de fonctions diverses.
 */
export default class Utils {
  /**
   * Récupération d'un chemin absolu de fichier d'après un chemin relatif.
   *
   * @param {string} filepath Chemin du fichier.
   * @returns {object} Le JSON récupéré.
   */
  static pathSolver(filepath) {
    return path.resolve(path.dirname(url.fileURLToPath(import.meta.url)), filepath);
  }

  /**
   * Récupération d'un chemin absolu de fichier d'après un chemin relatif.
   *
   * @param {string} filepath Chemin du fichier.
   * @returns {object} Le JSON récupéré.
   */
  static pathToFile(filepath) {
    return fs.readFileSync(Utils.pathSolver(filepath), 'utf8');
  }

  /**
   * Récupération d'un JSON d'après un chemin de fichier.
   *
   * @param {string} filepath Chemin du fichier.
   * @returns {object} Le JSON récupéré.
   */
  static fileToJSON(filepath) {
    return JSON.parse(Utils.pathToFile(filepath));
  }
}
