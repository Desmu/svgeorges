import Api from './Api.js';

/**
 * Classe dédiée aux appels API des fonctions de blagues.
 */
export default class Blague {
  /**
   * Récupération d'une blague au hasard.
   *
   * @param {object} configuration Configuration du bot.
   * @param {Function} callback Fonction appelée à la réponse de l'API.
   */
  static blague(configuration, callback) {
    Api.request('/api/blague', (blague) => {
      callback(blague.question_blague);
      if (blague.reponse_blague) {
        setTimeout(() => {
          callback(blague.reponse_blague);
        }, configuration.time_before_answer * 1000);
      }
    });
  }

  /**
   * Récupération d'une blague au hasard et de sa réponse en même temps.
   *
   * @param {object} configuration Configuration du bot.
   * @param {Function} callback Fonction appelée à la réponse de l'API.
   */
  static blagueAll(configuration, callback) {
    Api.request('/api/blague', (blague) => {
      callback(blague);
    });
  }

  /**
   * Récupération d'une série de blagues au hasard.
   *
   * @param {object} configuration Configuration du bot.
   * @param {Function} callback Fonction appelée à la réponse de l'API.
   */
  static blagueSerie(configuration, callback) {
    Api.request('/api/blague/serie', (blagues) => {
      blagues.forEach((blague, index) => {
        setTimeout(() => {
          callback(blague.question_blague);
          if (blague.reponse_blague) {
            setTimeout(() => {
              callback(blague.reponse_blague);
            }, configuration.time_before_answer * 1000);
          }
        }, configuration.time_before_next_blague * 1000 * index);
      });
    });
  }
}
