import Api from './Api.js';

/**
 * Classe dédiée aux appels API des fonctions de vidéos.
 */
export default class Video {
  /**
   * Récupération d'une vidéo au hasard.
   *
   * @param {object} configuration Configuration du bot.
   * @param {Function} callback Fonction appelée à la réponse de l'API.
   */
  static video(configuration, callback) {
    Api.request('/api/video', (video) => {
      callback(video);
    });
  }
}
