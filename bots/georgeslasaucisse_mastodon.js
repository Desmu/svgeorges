import https from 'https';
import querystring from 'querystring';

import cron from 'cron';

import Utils from './src/Utils.js';
import Blague from './src/Blague.js';
import Video from './src/Video.js';

/** Configuration du bot. */
const configuration = Utils.fileToJSON('../configuration.json');

const mastodonPost = https.request({
  host: configuration.mastodon.url,
  path: '/api/v1/statuses',
  method: 'POST',
  headers: {
    Authorization: `Bearer ${configuration.mastodon.token}`,
  },
}, (res) => {
  let json = '';
  res.on('data', (chunk) => {
    json += chunk;
  });
  res.on('end', () => {
    if (res.statusCode !== 200) {
      console.log(json);
    }
  });
}).on('error', (err) => {
  console.log(`HTTPS Error : ${err}`);
});

const jobBlague = new cron.CronJob('00 00 2,14 * * *', (() => {
  Blague.blagueAll(configuration.mastodon, (blaguePart) => {
    const blaguePost = https.request({
      host: configuration.mastodon.url,
      path: '/api/v1/statuses',
      method: 'POST',
      headers: {
        Authorization: `Bearer ${configuration.mastodon.token}`,
      },
    }, (res) => {
      let json = '';
      res.on('data', (chunk) => {
        json += chunk;
      });
      res.on('end', () => {
        if (res.statusCode !== 200) {
          console.log(json);
        }
      });
    }).on('error', (err) => {
      console.log(`HTTPS Error : ${err}`);
    });
    blaguePost.write(querystring.stringify({
      status: `${blaguePart.question_blague}\n${blaguePart.reponse_blague}`,
    }));
    blaguePost.end();
  });
}), null, true, 'Europe/Paris');
jobBlague.start();

const jobVideo = new cron.CronJob('00 00 20 * * 0', (() => {
  Video.video(configuration.mastodon, (video) => {
    const videoPost = https.request({
      host: configuration.mastodon.url,
      path: '/api/v1/statuses',
      method: 'POST',
      headers: {
        Authorization: `Bearer ${configuration.mastodon.token}`,
      },
    }, (res) => {
      let json = '';
      res.on('data', (chunk) => {
        json += chunk;
      });
      res.on('end', () => {
        if (res.statusCode !== 200) {
          console.log(json);
        }
      });
    }).on('error', (err) => {
      console.log(`HTTPS Error : ${err}`);
    });
    videoPost.write(querystring.stringify({
      status: `${video.nom_video} ${video.url_video} #PeerTube`,
    }));
    videoPost.end();
  });
}), null, true, 'Europe/Paris');
jobVideo.start();

process.on('error', (err) => { console.log(err); });
