import MatrixBotSdk from 'matrix-bot-sdk';

import Utils from './src/Utils.js';
import Blague from './src/Blague.js';

/** Configuration du bot. */
const configuration = Utils.fileToJSON('../configuration.json');
/* Synchronise le bot à chaque redémarrage. */
const storage = new MatrixBotSdk.SimpleFsStorageProvider('georgeslasaucisse-bot.json');
/* Client Matrix. */
const client = new MatrixBotSdk.MatrixClient(configuration.matrix.url, configuration.matrix.token, storage);
MatrixBotSdk.AutojoinRoomsMixin.setupOnClient(client);

client.on('room.message', (roomId, event) => {
  if (!event.content) return;
  if (event.content.msgtype !== 'm.text') return;
  if (event.content.body) {
    if (event.content.body.startsWith(configuration.matrix.commands.blague)) {
      Blague.blague(configuration.matrix, (blaguePart) => {
        client.sendNotice(roomId, blaguePart);
      });
    } else if (event.content.body.startsWith(configuration.matrix.commands.blague_serie)) {
      Blague.blagueSerie(configuration.matrix, (blaguePart) => {
        client.sendNotice(roomId, blaguePart);
      });
    }
  }
});

client.start();

process.on('error', (err) => { console.log(err); });
