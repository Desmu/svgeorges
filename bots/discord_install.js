import child_process from 'child_process';
import Utils from './src/Utils.js';

process.chdir(Utils.pathSolver('.'));

child_process.exec('npm install discord.js', (error, stdout, stderr) => {
  if (error) {
    console.log(`error: ${error.message}`);
    return;
  }
  if (stderr) {
    console.log(`stderr: ${stderr}`);
    return;
  }
  console.log(`stdout: ${stdout}`);
});
