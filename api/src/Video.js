import Utils from '../Utils.js';

/**
 * Fonctions liées aux vidéos classiques.
 */
export default class Video {
  /**
   * Récupération des infos d'une vidéo au hasard.
   *
   * @returns {object} Les informations de la vidéo récupérée.
   */
  static getRandomVideo() {
    let video = {};
    Video.videos.sort(() => 0.5 - Math.random()).some((vid) => {
      if (vid.obligatoire_serie_video && (vid.obligatoire_serie_video === true)) {
        return false;
      }
      video = vid;
      return true;
    });
    return video;
  }

  /**
   * Récupération des infos d'une playlist de vidéos.
   *
   * @param {object} args L'ID de la playlist.
   * @returns {object} Les informations de la playlist récupérée.
   */
  static getPlaylist(args) {
    let playlist = Video.playlists.find((p) => p.id_playlist === args.id_playlist);
    if (!playlist) {
      playlist = {
        error: 'Playlist inexistante.',
        playlists: Video.playlists,
      };
    }
    return playlist;
  }

  /**
   * Récupération des infos d'une vidéo d'après sa playlist et son épisode.
   *
   * @param {object} args L'ID de la playlist et le numéro de la vidéo.
   * @returns {object} Les informations de la vidéo récupérée.
   */
  static getVideoByPlaylist(args) {
    let video = Video.videos.find((v) => ((v.id_playlist === args.id_playlist) && (v.numero_video === parseFloat(args.numero_video, 10))));
    if (!video) {
      video = { error: 'Vidéo inexistante.' };
    }
    return video;
  }

  /**
   * Récupération des infos de toutes les playlists.
   *
   * @returns {object} Les informations des playlists.
   */
  static getPlaylists() {
    return Video.playlists;
  }

  /**
   * Point d'entrée.
   *
   * @param {string} url L'URL entrée par l'utilisateur.
   * @param {string} method La méthode HTTP utilisée.
   * @param {object} args Les arguments de l'URL.
   * @returns {object} L'objet renvoyé.
   */
  static parseURL(url, method, args) {
    let data = {};
    if (method === 'GET') {
      switch (url) {
        case '/video':
          data = Video.getRandomVideo();
          break;
        case '/video/{id_playlist}':
          data = Video.getPlaylist(args);
          break;
        case '/video/{id_playlist}/{numero_video}':
          data = Video.getVideoByPlaylist(args);
          break;
        case '/video/playlists':
          data = Video.getPlaylists();
          break;
        default:
          break;
      }
    }
    return data;
  }
}

Video.playlists = Utils.fileToJSON('./data/playlists.json').playlists;
Video.videos = Utils.fileToJSON('./data/videos.json').videos;
