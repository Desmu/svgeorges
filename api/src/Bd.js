import Utils from '../Utils.js';

/**
 * Fonctions liées à la bande dessinée papier.
 */
export default class Bd {
  /**
   * Récupération des infos d'une BD au hasard.
   *
   * @returns {object} Les informations de la BD récupérée.
   */
  static getRandomBd() {
    let bd = {};
    Bd.bd.sort(() => 0.5 - Math.random()).some((b) => {
      if (b.obligatoire_serie_bd && (b.obligatoire_serie_bd === true)) {
        return false;
      }
      bd = b;
      return true;
    });
    return bd;
  }

  /**
   * Récupération des infos d'une saison de BD.
   *
   * @param {object} args L'ID de la saison.
   * @returns {object} Les informations de la saison récupérée.
   */
  static getSaison(args) {
    let saison = Bd.saisons.find((s) => s.id_saison === args.id_saison);
    if (!saison) {
      saison = {
        error: 'Saison inexistante.',
        saisons: Bd.saisons,
      };
    }
    return saison;
  }

  /**
   * Récupération des infos d'une BD d'après sa saison et son épisode.
   *
   * @param {object} args L'ID de la saison et le numéro de la BD.
   * @returns {object} Les informations de la BD récupérée.
   */
  static getBdBySaison(args) {
    let bd = Bd.bd.find((b) => ((b.id_saison === args.id_saison) && (b.numero_bd === parseFloat(args.numero_bd, 10))));
    if (!bd) {
      bd = { error: 'BD inexistante.' };
    }
    return bd;
  }

  /**
   * Récupération des infos de toutes les saisons.
   *
   * @returns {object} Les informations des saisons.
   */
  static getSaisons() {
    return Bd.saisons;
  }

  /**
   * Point d'entrée.
   *
   * @param {string} url L'URL entrée par l'utilisateur.
   * @param {string} method La méthode HTTP utilisée.
   * @param {object} args Les arguments de l'URL.
   * @returns {object} L'objet renvoyé.
   */
  static parseURL(url, method, args) {
    let data = {};
    if (method === 'GET') {
      switch (url) {
        case '/bd':
          data = Bd.getRandomBd();
          break;
        case '/bd/{id_saison}':
          data = Bd.getSaison(args);
          break;
        case '/bd/{id_saison}/{numero_bd}':
          data = Bd.getBdBySaison(args);
          break;
        case '/bd/saisons':
          data = Bd.getSaisons();
          break;
        default:
          break;
      }
    }
    return data;
  }
}

Bd.saisons = Utils.fileToJSON('./data/saisons.json').saisons;
Bd.bd = Utils.fileToJSON('./data/bd.json').bd;
