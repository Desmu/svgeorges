import fs from 'fs';
import path from 'path';
import url from 'url';
import Utils from '../Utils.js';

/**
 * Fonctions liées aux contes.
 */
export default class Conte {
  /**
   * Récupération des infos d'un conte.
   *
   * @param {object} args L'ID du conte.
   * @returns {object} Les informations du conte récupéré.
   */
  static getConte(args) {
    let conte = Conte.contes.find((s) => s.id_conte === args.id_conte);
    if (!conte) {
      conte = {
        error: 'Conte inexistant.',
        contes: Conte.contes,
      };
    }
    return conte;
  }

  /**
   * Récupération des infos d'un chapitre d'après son conte.
   *
   * @param {object} args L'ID du conte et le numéro du chapitre.
   * @returns {object} Les informations du chapitre récupéré.
   */
  static getChapitreByConte(args) {
    let chapitre = Conte.chapitres.find((c) => ((c.id_conte === args.id_conte) && (c.numero_chapitre === parseFloat(args.numero_chapitre, 10))));
    if (!chapitre) {
      chapitre = { error: 'Chapitre inexistant.' };
    }
    return chapitre;
  }

  /**
   * Génération d'un epub d'un conte.
   *
   * @param {object} args L'ID du conte et le mode de génération.
   * @returns {object} L'epub du conte.
   */
  static getConteEpub(args) {
    let conte = Conte.contes.find((s) => s.id_conte === args.id_conte);
    const modes = ['demo', 'base', 'images', 'sons', 'total'];
    if (!conte) {
      conte = {
        error: 'Conte inexistant.',
        contes: Conte.contes,
      };
      return conte;
    }
    if (modes.indexOf(args.mode) === -1) {
      conte = {
        error: 'Mode de génération de conte inexistant.',
        modes,
      };
      return conte;
    }
    return Utils.generateEpub(conte, args.mode);
  }

  /**
   * Récupération des infos de tous les contes.
   *
   * @returns {object} Les informations des contes.
   */
  static getContes() {
    return Conte.contes;
  }

  /**
   * Point d'entrée.
   *
   * @param {string} userUrl L'URL entrée par l'utilisateur.
   * @param {string} method La méthode HTTP utilisée.
   * @param {object} args Les arguments de l'URL.
   * @returns {object} L'objet renvoyé.
   */
  static parseURL(userUrl, method, args) {
    let data = {};
    if (method === 'GET') {
      switch (userUrl) {
        case '/conte/{id_conte}':
          data = Conte.getConte(args);
          break;
        case '/conte/{id_conte}/{numero_chapitre}':
          data = Conte.getChapitreByConte(args);
          break;
        case '/conte/{id_conte}/epub/{mode}':
          data = Conte.getConteEpub(args);
          break;
        case '/conte':
          data = Conte.getContes();
          break;
        default:
          break;
      }
    }
    return data;
  }
}

Conte.contes = Utils.fileToJSON('./data/contes.json').contes;
Conte.chapitres = Utils.fileToJSON('./data/chapitres.json').chapitres;
Conte.chapitres.forEach((chapitre, i) => {
  Conte.chapitres[i].script_chapitre = fs.readFileSync(path.resolve(path.dirname(url.fileURLToPath(import.meta.url)), chapitre.script_chapitre), 'utf8');
});
