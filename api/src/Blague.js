import Utils from '../Utils.js';

/**
 * Fonctions liées aux blagues.
 */
export default class Blague {
  /**
   * Récupération des infos d'une blague au hasard.
   *
   * @returns {object} Les informations de la blague récupérée.
   */
  static getRandomBlague() {
    let blague = {};
    Blague.blagues.sort(() => 0.5 - Math.random()).some((bl) => {
      if (bl.obligatoire_serie_blague && (bl.obligatoire_serie_blague === true)) {
        return false;
      }
      blague = bl;
      return true;
    });
    return blague;
  }

  /**
   * Récupération des infos de toutes les blagues d'une série au hasard.
   *
   * @returns {Array} Les informations des blagues récupérées.
   */
  static getRandomSerieBlague() {
    let seriesBlagues = [];
    Blague.blagues.filter((bl) => bl.serie_blague !== '').forEach((bl) => {
      if (seriesBlagues.indexOf(bl.serie_blague) === -1) {
        seriesBlagues.push(bl.serie_blague);
      }
    });
    seriesBlagues = seriesBlagues.sort(() => 0.5 - Math.random());
    const serieBlagues = Blague.blagues.filter((bl) => bl.serie_blague === seriesBlagues[0]);
    return serieBlagues;
  }

  /**
   * Point d'entrée.
   *
   * @param {string} url L'URL entrée par l'utilisateur.
   * @param {string} method La méthode HTTP utilisée.
   * @param {object} args Les arguments de l'URL.
   * @returns {object} L'objet renvoyé.
   */
  static parseURL(url, method, args) {
    let data = {};
    if (method === 'GET') {
      switch (url) {
        case '/blague':
          data = Blague.getRandomBlague();
          break;
        case '/blague/serie':
          data = Blague.getRandomSerieBlague();
          break;
        default:
          break;
      }
    }
    return data;
  }
}

Blague.blagues = Utils.fileToJSON('./data/blagues.json').blagues;
