import Utils from '../Utils.js';

/**
 * Fonctions liées aux réseaux sociaux et bots.
 */
export default class Reseaux {
  /**
   * Récupération des infos de tous les réseaux.
   *
   * @returns {object} Les informations des réseaux.
   */
  static getReseaux() {
    return Reseaux.reseaux;
  }

  /**
   * Point d'entrée.
   *
   * @param {string} url L'URL entrée par l'utilisateur.
   * @param {string} method La méthode HTTP utilisée.
   * @param {object} args Les arguments de l'URL.
   * @returns {object} L'objet renvoyé.
   */
  static parseURL(url, method, args) {
    let data = {};
    if (method === 'GET') {
      switch (url) {
        case '/reseaux':
          data = Reseaux.getReseaux();
          break;
        default:
          break;
      }
    }
    return data;
  }
}

Reseaux.reseaux = Utils.fileToJSON('./data/reseaux.json').reseaux;
