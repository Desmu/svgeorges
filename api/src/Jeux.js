import Utils from '../Utils.js';

/**
 * Fonctions liées aux jeux.
 */
export default class Jeux {
  /**
   * Récupération des infos de tous les jeux.
   *
   * @returns {object} Les informations des jeux récupérés.
   */
  static getJeux() {
    return Jeux.jeux;
  }

  /**
   * Point d'entrée.
   *
   * @param {string} url L'URL entrée par l'utilisateur.
   * @param {string} method La méthode HTTP utilisée.
   * @param {object} args Les arguments de l'URL.
   * @returns {object} L'objet renvoyé.
   */
  static parseURL(url, method, args) {
    let data = {};
    if (method === 'GET') {
      switch (url) {
        case '/jeux':
          data = Jeux.getJeux();
          break;
        default:
          break;
      }
    }
    return data;
  }
}

Jeux.jeux = Utils.fileToJSON('./data/jeux.json').jeux;
