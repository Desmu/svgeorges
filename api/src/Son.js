import Utils from '../Utils.js';

/**
 * Fonctions liées aux sons classiques.
 */
export default class Son {
  /**
   * Récupération des infos d'un son au hasard.
   *
   * @returns {object} Les informations du son récupéré.
   */
  static getRandomSon() {
    let son = {};
    Son.sons.sort(() => 0.5 - Math.random()).some((s) => {
      if (s.obligatoire_serie_son && (s.obligatoire_serie_son === true)) {
        return false;
      }
      son = s;
      return true;
    });
    return son;
  }

  /**
   * Récupération des infos d'un album de sons.
   *
   * @param {object} args L'ID de l'album.
   * @returns {object} Les informations de l'album récupéré.
   */
  static getAlbum(args) {
    let album = Son.albums.find((a) => a.id_album === args.id_album);
    if (!album) {
      album = {
        error: 'Album inexistant.',
        albums: Son.albums,
      };
    }
    return album;
  }

  /**
   * Récupération des infos d'un son d'après son album et son épisode.
   *
   * @param {object} args L'ID de l'album et le numéro du son.
   * @returns {object} Les informations du son récupéré.
   */
  static getSonByAlbum(args) {
    let son = Son.sons.find((s) => ((s.id_album === args.id_album) && (s.numero_son === parseFloat(args.numero_son, 10))));
    if (!son) {
      son = { error: 'Son inexistant.' };
    }
    return son;
  }

  /**
   * Récupération des infos de tous les albums.
   *
   * @returns {object} Les informations des albums.
   */
  static getAlbums() {
    return Son.albums;
  }

  /**
   * Point d'entrée.
   *
   * @param {string} url L'URL entrée par l'utilisateur.
   * @param {string} method La méthode HTTP utilisée.
   * @param {object} args Les arguments de l'URL.
   * @returns {object} L'objet renvoyé.
   */
  static parseURL(url, method, args) {
    let data = {};
    if (method === 'GET') {
      switch (url) {
        case '/son':
          data = Son.getRandomSon();
          break;
        case '/son/{id_album}':
          data = Son.getAlbum(args);
          break;
        case '/son/{id_album}/{numero_son}':
          data = Son.getSonByAlbum(args);
          break;
        case '/son/albums':
          data = Son.getAlbums();
          break;
        default:
          break;
      }
    }
    return data;
  }
}

Son.albums = Utils.fileToJSON('./data/albums.json').albums;
Son.sons = Utils.fileToJSON('./data/sons.json').sons;
