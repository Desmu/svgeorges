import Utils from '../Utils.js';

/**
 * Fonctions liées aux Saviez-Georges.
 */
export default class Funfact {
  /**
   * Récupération des infos d'un Saviez-Georges au hasard.
   *
   * @returns {object} Les informations du Saviez-Georges récupéré.
   */
  static getRandomFunfact() {
    let funfact = {};
    Funfact.funfacts.sort(() => 0.5 - Math.random()).some((ff) => {
      funfact = ff;
      return true;
    });
    return funfact;
  }

  /**
   * Point d'entrée.
   *
   * @param {string} url L'URL entrée par l'utilisateur.
   * @param {string} method La méthode HTTP utilisée.
   * @param {object} args Les arguments de l'URL.
   * @returns {object} L'objet renvoyé.
   */
  static parseURL(url, method, args) {
    let data = {};
    if (method === 'GET') {
      switch (url) {
        case '/funfact':
          data = Funfact.getRandomFunfact();
          break;
        default:
          break;
      }
    }
    return data;
  }
}

Funfact.funfacts = Utils.fileToJSON('./data/funfacts.json').funfacts;
