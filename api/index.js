import fs from 'fs';
import https from 'https';

import Bd from './src/Bd.js';
import Blague from './src/Blague.js';
import Conte from './src/Conte.js';
import Funfact from './src/Funfact.js';
import Jeux from './src/Jeux.js';
import Reseaux from './src/Reseaux.js';
import Son from './src/Son.js';
import Video from './src/Video.js';
import Utils from './Utils.js';

/** Fichier de configuration. */
const configuration = Utils.fileToJSON('./configuration.json');
/** Définition d'API. */
const api = Utils.fileToJSON('./openapi.json');

https.createServer({
  cert: fs.readFileSync(`${configuration.certPem}`),
  key: fs.readFileSync(`${configuration.keyPem}`),
  connectionsCheckingInterval: 120000,
}, (req, res) => {
  let code = 200;
  let data = api;
  let reqUrl = req.url
    .replace(/&/g, '&amp;')
    .replace(/"/g, '&quot;')
    .replace(/'/g, '&#039;')
    .replace(/</g, '&lt;')
    .replace(/>/g, '&gt;');
  if (reqUrl[reqUrl.length - 1] === '/') {
    reqUrl = reqUrl.substring(0, reqUrl.length - 1);
  }

  Object.entries(api.paths).some(([pathUrl, methods]) => {
    const reqUrlSplitted = reqUrl.split('/');
    const pathUrlSplitted = pathUrl.split('/');
    if ((pathUrl.indexOf(reqUrlSplitted[1]) === 1) && (reqUrlSplitted.length === pathUrlSplitted.length) && methods[req.method.toLowerCase()]) {
      let correctPath = true;
      const args = {};
      pathUrlSplitted.forEach((splitPart, index) => {
        if ((reqUrlSplitted[index] !== splitPart) && ((splitPart.indexOf('{') === -1) || (splitPart.indexOf('}') === -1))) {
          code = 400;
          data = { error: 'Requête invalide.' };
          correctPath = false;
        } else if (reqUrlSplitted[index] !== splitPart) {
          args[splitPart.substring(1, splitPart.length - 1)] = reqUrlSplitted[index];
        }
      });
      if (correctPath) {
        switch (pathUrlSplitted[1]) {
          case 'bd':
            data = Bd.parseURL(pathUrl, req.method, args);
            break;
          case 'video':
            data = Video.parseURL(pathUrl, req.method, args);
            break;
          case 'son':
            data = Son.parseURL(pathUrl, req.method, args);
            break;
          case 'jeux':
            data = Jeux.parseURL(pathUrl, req.method, args);
            break;
          case 'conte':
            data = Conte.parseURL(pathUrl, req.method, args);
            break;
          case 'reseaux':
            data = Reseaux.parseURL(pathUrl, req.method, args);
            break;
          case 'blague':
            data = Blague.parseURL(pathUrl, req.method, args);
            break;
          case 'funfact':
            data = Funfact.parseURL(pathUrl, req.method, args);
            break;
          default:
            break;
        }
        if (data.error) {
          code = 400;
        }
        return true;
      }
    }
    return false;
  });

  if (data.buffer) {
    res.writeHead(code, {
      'Content-Disposition': `attachment; filename=${data.filename}`,
      'Content-Type': data.mimetype,
    });
    res.write(data.buffer, 'binary');
    res.end(null, 'binary');
  } else {
    res.writeHead(code, { 'Content-Type': 'application/json' });
    res.write(JSON.stringify(data));
    res.end();
  }
}).listen(configuration.port);
