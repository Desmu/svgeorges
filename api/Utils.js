import fs from 'fs';
import path from 'path';
import url from 'url';
import AdmZip from 'adm-zip';

/**
 * Classe de fonctions diverses.
 */
export default class Utils {
  /**
   * Conversion d'un chemin relatif de fichier vers un chemin absolu.
   *
   * @param {string} filepath Chemin du fichier.
   * @returns {string} Le chemin absolu du fichier.
   */
  static resolveFilePath(filepath) {
    return path.resolve(path.dirname(url.fileURLToPath(import.meta.url)), filepath);
  }

  /**
   * Récupération d'un JSON d'après un chemin de fichier.
   *
   * @param {string} filepath Chemin du fichier.
   * @returns {object} Le JSON récupéré.
   */
  static fileToJSON(filepath) {
    return JSON.parse(fs.readFileSync(Utils.resolveFilePath(filepath)), 'utf8');
  }

  /**
   * Génération d'un epub d'un conte.
   *
   * @param {object} conte Objet décrivant le conte.
   * @param {string} mode Le mode de génération de l'epub.
   * @returns {object} L'epub du conte.
   */
  static generateEpub(conte, mode) {
    const fullIds = {
      maxime: '01_maxime',
      theo: '02_theo',
      raphael: '03_raphael',
      corentin: '04_corentin',
      florent: '05_florent',
    };
    const zip = new AdmZip({
      noSort: true,
    });
    zip.addLocalFile(Utils.resolveFilePath(`./ebooks/${fullIds[conte.id_conte]}/base/mimetype`));
    zip.getEntry('mimetype').header.method = 0;
    zip.addLocalFolder(Utils.resolveFilePath(`./ebooks/${fullIds[conte.id_conte]}/base/META-INF`), './META-INF');
    switch (mode) {
      case 'demo':
        zip.addLocalFolder(Utils.resolveFilePath(`./ebooks/${fullIds[conte.id_conte]}/base/OEBPS/images`), './OEBPS/images');
        zip.addLocalFile(Utils.resolveFilePath(`./ebooks/${fullIds[conte.id_conte]}/base/OEBPS/text/introduction.xhtml`), './OEBPS/text');
        zip.addLocalFile(Utils.resolveFilePath(`./ebooks/${fullIds[conte.id_conte]}/base/OEBPS/text/chapitre01.xhtml`), './OEBPS/text');
        zip.addLocalFile(Utils.resolveFilePath(`./ebooks/${fullIds[conte.id_conte]}/base/OEBPS/text/chapitre02.xhtml`), './OEBPS/text');
        zip.addLocalFile(Utils.resolveFilePath(`./ebooks/${fullIds[conte.id_conte]}/base/OEBPS/text/chapitre03.xhtml`), './OEBPS/text');
        zip.addLocalFile(Utils.resolveFilePath(`./ebooks/${fullIds[conte.id_conte]}/base/OEBPS/text/conclusion.xhtml`), './OEBPS/text');
        zip.addLocalFile(Utils.resolveFilePath(`./ebooks/${fullIds[conte.id_conte]}/images/OEBPS/images/chapitre01.png`), './OEBPS/images');
        zip.addLocalFile(Utils.resolveFilePath(`./ebooks/${fullIds[conte.id_conte]}/images/OEBPS/images/chapitre02.png`), './OEBPS/images');
        zip.addLocalFile(Utils.resolveFilePath(`./ebooks/${fullIds[conte.id_conte]}/images/OEBPS/images/chapitre03.png`), './OEBPS/images');
        zip.addLocalFile(Utils.resolveFilePath(`./ebooks/${fullIds[conte.id_conte]}/sons/OEBPS/audio/navigation.mp3`), './OEBPS/audio');
        zip.addLocalFile(Utils.resolveFilePath(`./ebooks/${fullIds[conte.id_conte]}/sons/OEBPS/audio/introduction.mp3`), './OEBPS/audio');
        zip.addLocalFile(Utils.resolveFilePath(`./ebooks/${fullIds[conte.id_conte]}/sons/OEBPS/audio/chapitre01.mp3`), './OEBPS/audio');
        zip.addLocalFile(Utils.resolveFilePath(`./ebooks/${fullIds[conte.id_conte]}/sons/OEBPS/audio/chapitre02.mp3`), './OEBPS/audio');
        zip.addLocalFile(Utils.resolveFilePath(`./ebooks/${fullIds[conte.id_conte]}/sons/OEBPS/audio/chapitre03.mp3`), './OEBPS/audio');
        zip.addLocalFile(Utils.resolveFilePath(`./ebooks/${fullIds[conte.id_conte]}/sons/OEBPS/audio/conclusion.mp3`), './OEBPS/audio');
        zip.addLocalFile(Utils.resolveFilePath(`./ebooks/${fullIds[conte.id_conte]}/sons/OEBPS/sync/navigation.smil`), './OEBPS/sync');
        zip.addLocalFile(Utils.resolveFilePath(`./ebooks/${fullIds[conte.id_conte]}/sons/OEBPS/sync/introduction.smil`), './OEBPS/sync');
        zip.addLocalFile(Utils.resolveFilePath(`./ebooks/${fullIds[conte.id_conte]}/sons/OEBPS/sync/chapitre01.smil`), './OEBPS/sync');
        zip.addLocalFile(Utils.resolveFilePath(`./ebooks/${fullIds[conte.id_conte]}/sons/OEBPS/sync/chapitre02.smil`), './OEBPS/sync');
        zip.addLocalFile(Utils.resolveFilePath(`./ebooks/${fullIds[conte.id_conte]}/sons/OEBPS/sync/chapitre03.smil`), './OEBPS/sync');
        zip.addLocalFile(Utils.resolveFilePath(`./ebooks/${fullIds[conte.id_conte]}/sons/OEBPS/sync/conclusion.smil`), './OEBPS/sync');
        zip.addLocalFolder(Utils.resolveFilePath(`./ebooks/${fullIds[conte.id_conte]}/demo`));
        break;
      case 'base':
        zip.addLocalFolder(Utils.resolveFilePath(`./ebooks/${fullIds[conte.id_conte]}/base/OEBPS`), './OEBPS');
        break;
      case 'images':
        zip.addLocalFolder(Utils.resolveFilePath(`./ebooks/${fullIds[conte.id_conte]}/base/OEBPS`), './OEBPS');
        zip.addLocalFolder(Utils.resolveFilePath(`./ebooks/${fullIds[conte.id_conte]}/images`));
        break;
      case 'sons':
        zip.addLocalFolder(Utils.resolveFilePath(`./ebooks/${fullIds[conte.id_conte]}/base/OEBPS`), './OEBPS');
        zip.addLocalFolder(Utils.resolveFilePath(`./ebooks/${fullIds[conte.id_conte]}/sons`));
        break;
      case 'total':
        zip.addLocalFolder(Utils.resolveFilePath(`./ebooks/${fullIds[conte.id_conte]}/base/OEBPS`), './OEBPS');
        zip.addLocalFolder(Utils.resolveFilePath(`./ebooks/${fullIds[conte.id_conte]}/images`));
        zip.addLocalFolder(Utils.resolveFilePath(`./ebooks/${fullIds[conte.id_conte]}/sons`));
        zip.addLocalFolder(Utils.resolveFilePath(`./ebooks/${fullIds[conte.id_conte]}/total`));
        break;
      default:
        break;
    }
    return {
      buffer: zip.toBuffer(),
      filename: `${conte.nom_conte}.epub`,
      mimetype: 'application/epub+zip',
    };
  }
}
