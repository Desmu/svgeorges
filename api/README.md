# SVGeorges - API

API transmettant toutes les données relatives à Georges la Saucisse (liens vers les réseaux sociaux, BD, vidéos, sons, jeux, contes, funfacts, blagues, ...) et générant les ebooks.

## Installation

Renommer le fichier `configuration.example.json` en `configuration.json` et en modifier les paramètres :
- `certPem` : chemin du fichier `fullchain.pem` utilisé pour la connexion HTTPS (`/etc/letsencrypt/live/www.domain.tld/fullchain.pem`),
- `keyPem` : chemin du fichier `privkey.pem` utilisé pour la connexion HTTPS (`/etc/letsencrypt/live/www.domain.tld/privkey.pem`),
- `port` : port utilisé pour communiquer avec l'API (`12913`).

## Lancement

- Exécuter `node index.js`.

## Liens

- Fichier OpenAPI : https://gitlab.com/Desmu/svgeorges/-/blob/master/api/openapi.json
- Point d'entrée : https://www.georgeslasaucisse.fr/api

## Librairies utilisées

- [ADM-ZIP](https://github.com/cthackers/adm-zip) : Librairie de création de ZIP pour exporter les ebooks des contes.
