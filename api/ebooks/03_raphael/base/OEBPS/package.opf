<?xml version='1.0' encoding='utf-8'?>
<package xmlns="http://www.idpf.org/2007/opf" unique-identifier="id" version="3.0">
  <metadata xmlns:dc="http://purl.org/dc/elements/1.1/" xmlns:opf="http://www.idpf.org/2007/opf">
    <dc:identifier id="id">georgeslasaucisse_grandconte03_raphael</dc:identifier>
    <dc:title id="title">Raphaël le Panda conduit un Océan</dc:title>
    <dc:creator id="creator">Desmu</dc:creator>
    <dc:rights>CC BY-NC-SA 4.0</dc:rights>
    <dc:language>fr</dc:language>
    <dc:date>2019-11-24T00:00:00Z</dc:date>
    <meta property="dcterms:modified">2019-11-24T00:00:00Z</meta>
    <meta property="belongs-to-collection" id="collection">Les Grands Contes de Georges la Saucisse</meta>
    <meta property="title-type" refines="#title">main</meta>
    <meta property="file-as" refines="#title">Raphaël le Panda conduit un Océan</meta>
    <meta property="role" refines="#creator" scheme="marc:relators">aut</meta>
    <meta property="file-as" refines="#creator">Desmu</meta>
    <meta property="collection-type" refines="#collection">series</meta>
    <meta property="group-position" refines="#collection">3</meta>
    <meta property="schema:accessMode">textual</meta>
    <meta property="schema:accessibilityFeature">alternativeText</meta>
    <meta property="schema:accessibilityFeature">displayTransformability</meta>
    <meta property="schema:accessibilityFeature">readingOrder</meta>
    <meta property="schema:accessibilityFeature">tableOfContents</meta>
    <meta property="schema:accessibilityFeature">unlocked</meta>
    <meta property="schema:accessibilityHazard">noFlashingHazard</meta>
    <meta property="schema:accessibilityHazard">noMotionSimulationHazard</meta>
    <meta property="schema:accessibilityHazard">noSoundHazard</meta>
    <meta property="schema:accessibilitySummary">Ce livre a été vérifié et corrigé par les validateurs "EPUBCheck" et "Ace by DAISY".</meta>
    <meta property="schema:accessModeSufficient">textual</meta>
  </metadata>
  <manifest>
    <item href="toc.ncx" id="toc" media-type="application/x-dtbncx+xml"/>
    <item href="images/cover.png" id="cover" media-type="image/png" properties="cover-image" fallback="navigationtext"/>
    <item href="text/navigation.xhtml" id="navigationtext" media-type="application/xhtml+xml" properties="nav"/>
    <item href="text/introduction.xhtml" id="introductiontext" media-type="application/xhtml+xml"/>
    <item href="text/chapitre01.xhtml" id="chapitre01text" media-type="application/xhtml+xml"/>
    <item href="text/chapitre02.xhtml" id="chapitre02text" media-type="application/xhtml+xml"/>
    <item href="text/chapitre03.xhtml" id="chapitre03text" media-type="application/xhtml+xml"/>
    <item href="text/chapitre04.xhtml" id="chapitre04text" media-type="application/xhtml+xml"/>
    <item href="text/chapitre05.xhtml" id="chapitre05text" media-type="application/xhtml+xml"/>
    <item href="text/chapitre06.xhtml" id="chapitre06text" media-type="application/xhtml+xml"/>
    <item href="text/chapitre07.xhtml" id="chapitre07text" media-type="application/xhtml+xml"/>
    <item href="text/chapitre08.xhtml" id="chapitre08text" media-type="application/xhtml+xml"/>
    <item href="text/chapitre09.xhtml" id="chapitre09text" media-type="application/xhtml+xml"/>
    <item href="text/chapitre10.xhtml" id="chapitre10text" media-type="application/xhtml+xml"/>
    <item href="text/chapitre11.xhtml" id="chapitre11text" media-type="application/xhtml+xml"/>
    <item href="text/chapitre12.xhtml" id="chapitre12text" media-type="application/xhtml+xml"/>
    <item href="text/chapitre13.xhtml" id="chapitre13text" media-type="application/xhtml+xml"/>
    <item href="text/chapitre14.xhtml" id="chapitre14text" media-type="application/xhtml+xml"/>
    <item href="text/chapitre15.xhtml" id="chapitre15text" media-type="application/xhtml+xml"/>
    <item href="text/chapitre16.xhtml" id="chapitre16text" media-type="application/xhtml+xml"/>
    <item href="text/chapitre17.xhtml" id="chapitre17text" media-type="application/xhtml+xml"/>
    <item href="text/chapitre18.xhtml" id="chapitre18text" media-type="application/xhtml+xml"/>
    <item href="text/chapitre19.xhtml" id="chapitre19text" media-type="application/xhtml+xml"/>
    <item href="text/chapitre20.xhtml" id="chapitre20text" media-type="application/xhtml+xml"/>
    <item href="text/chapitre21.xhtml" id="chapitre21text" media-type="application/xhtml+xml"/>
    <item href="text/chapitre22.xhtml" id="chapitre22text" media-type="application/xhtml+xml"/>
    <item href="text/chapitre23.xhtml" id="chapitre23text" media-type="application/xhtml+xml"/>
    <item href="text/chapitre24.xhtml" id="chapitre24text" media-type="application/xhtml+xml"/>
    <item href="text/chapitre25.xhtml" id="chapitre25text" media-type="application/xhtml+xml"/>
    <item href="text/chapitre26.xhtml" id="chapitre26text" media-type="application/xhtml+xml"/>
    <item href="text/chapitre27.xhtml" id="chapitre27text" media-type="application/xhtml+xml"/>
    <item href="text/chapitre28.xhtml" id="chapitre28text" media-type="application/xhtml+xml"/>
    <item href="text/conclusion.xhtml" id="conclusiontext" media-type="application/xhtml+xml"/>
    <item href="images/blurb.png" id="blurb" media-type="image/png" fallback="navigationtext"/>
  </manifest>
  <spine toc="toc">
    <itemref idref="cover"/>
    <itemref idref="navigationtext"/>
    <itemref idref="introductiontext"/>
    <itemref idref="chapitre01text"/>
    <itemref idref="chapitre02text"/>
    <itemref idref="chapitre03text"/>
    <itemref idref="chapitre04text"/>
    <itemref idref="chapitre05text"/>
    <itemref idref="chapitre06text"/>
    <itemref idref="chapitre07text"/>
    <itemref idref="chapitre08text"/>
    <itemref idref="chapitre09text"/>
    <itemref idref="chapitre10text"/>
    <itemref idref="chapitre11text"/>
    <itemref idref="chapitre12text"/>
    <itemref idref="chapitre13text"/>
    <itemref idref="chapitre14text"/>
    <itemref idref="chapitre15text"/>
    <itemref idref="chapitre16text"/>
    <itemref idref="chapitre17text"/>
    <itemref idref="chapitre18text"/>
    <itemref idref="chapitre19text"/>
    <itemref idref="chapitre20text"/>
    <itemref idref="chapitre21text"/>
    <itemref idref="chapitre22text"/>
    <itemref idref="chapitre23text"/>
    <itemref idref="chapitre24text"/>
    <itemref idref="chapitre25text"/>
    <itemref idref="chapitre26text"/>
    <itemref idref="chapitre27text"/>
    <itemref idref="chapitre28text"/>
    <itemref idref="conclusiontext"/>
    <itemref idref="blurb"/>
  </spine>
</package>
