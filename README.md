# SVGeorges

Compilation de projets relatifs à [Georges la Saucisse](https://www.georgeslasaucisse.fr/) :
- [Site web](https://gitlab.com/Desmu/svgeorges/-/tree/master/website) : Moteur de création d'aventures au format SVG (bandes dessinées, vidéos, pages web).
- [API](https://gitlab.com/Desmu/svgeorges/-/tree/master/api) : API comprenant les listes de toutes les aventures et le générateur d'ebooks.
- [Bots](https://gitlab.com/Desmu/svgeorges/-/tree/master/bots) : Bots rattachés à l'API.

## Installation

- Installer [Node.js](https://nodejs.org/) avec son gestionnaire de paquets NPM.

## Développement

- Exécuter la commande `npm run start` pour récupérer les dépendances (linters, librairies).
- Exécuter la commande `npm run eslint` pour générer l'analyse du Javascript.
- Exécuter la commande `npm run stylelint` pour générer l'analyse du CSS.

### Production

- Exécuter la commande `npm run build` pour générer des versions complètes et optimisées des projets pour serveur de production dans le dossier `build`.
- Ou `npm run build-minify` pour générer les versions sans librairies.
- Ou `npm run build-modules` pour générer les librairies après avoir exécuté la commande précédente.

## Architecture

- `api` : sous-projet comprenant l'API (plus de détails sur [le README dédié](https://gitlab.com/Desmu/svgeorges/-/blob/master/api/README.md)).
- `audit` : fichiers reprenant les analyses de code (voir paragraphe `Développement`).
- `bots` : sous-projet comprenant des bots reliés à l'API (plus de détails sur [le README dédié](https://gitlab.com/Desmu/svgeorges/-/blob/master/bots/README.md)).
- `build` : fichiers générés pour être mis en ligne (voir paragraphe `Production`).
- `downloads` : fichiers statiques hors projet.
- `scripts` : scripts exécutés en ligne de commande (voir paragraphes `Développement` et `Production`).
- `website` : sous-projet comprenant le site web (plus de détails sur [le README dédié](https://gitlab.com/Desmu/svgeorges/-/blob/master/website/README.md)).
